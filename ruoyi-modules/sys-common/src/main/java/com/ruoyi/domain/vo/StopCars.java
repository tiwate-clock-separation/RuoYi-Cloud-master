package com.ruoyi.domain.vo;

import com.ruoyi.common.core.web.domain.BaseEntity;

public class StopCars extends BaseEntity {
    private String licenseNumber;
    private double money;
    private long chid;

    public String getLicenseNumber() {
        return licenseNumber;
    }

    public void setLicenseNumber(String licenseNumber) {
        this.licenseNumber = licenseNumber;
    }

    public double getMoney() {
        return money;
    }

    public void setMoney(double money) {
        this.money = money;
    }

    public long getChid() {
        return chid;
    }

    public void setChid(long chid) {
        this.chid = chid;
    }

    @Override
    public String toString() {
        return "StopCars{" +
                "licenseNumber='" + licenseNumber + '\'' +
                ", money=" + money +
                ", chid=" + chid +
                '}';
    }
}
