package com.ruoyi.controller;

import java.util.List;
import java.io.IOException;
import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.log.annotation.Log;
import com.ruoyi.common.log.enums.BusinessType;
import com.ruoyi.common.security.annotation.RequiresPermissions;
import com.ruoyi.domain.CarsHomeEqBoards;
import com.ruoyi.service.ICarsHomeEqBoardsService;
import com.ruoyi.common.core.web.controller.BaseController;
import com.ruoyi.common.core.web.domain.AjaxResult;
import com.ruoyi.common.core.utils.poi.ExcelUtil;
import com.ruoyi.common.core.web.page.TableDataInfo;

/**
 * 车场设备板卡Controller
 * 
 * @author chn
 * @date 2023-12-07
 */
@RestController
@RequestMapping("/boards")
public class CarsHomeEqBoardsController extends BaseController
{
    @Autowired
    private ICarsHomeEqBoardsService carsHomeEqBoardsService;

    /**
     * 查询车场设备板卡列表
     */
    @RequiresPermissions("sys-cars-home:boards:list")
    @GetMapping("/list")
    public TableDataInfo list(CarsHomeEqBoards carsHomeEqBoards)
    {
        startPage();
        List<CarsHomeEqBoards> list = carsHomeEqBoardsService.selectCarsHomeEqBoardsList(carsHomeEqBoards);
        return getDataTable(list);
    }

//    /**
//     * 导出车场设备板卡列表
//     */
//    @RequiresPermissions("sys-cars-home:boards:export")
//    @Log(title = "车场设备板卡", businessType = BusinessType.EXPORT)
//    @PostMapping("/export")
//    public void export(HttpServletResponse response, CarsHomeEqBoards carsHomeEqBoards)
//    {
//        List<CarsHomeEqBoards> list = carsHomeEqBoardsService.selectCarsHomeEqBoardsList(carsHomeEqBoards);
//        ExcelUtil<CarsHomeEqBoards> util = new ExcelUtil<CarsHomeEqBoards>(CarsHomeEqBoards.class);
//        util.exportExcel(response, list, "车场设备板卡数据");
//    }
//
//    /**
//     * 获取车场设备板卡详细信息
//     */
//    @RequiresPermissions("sys-cars-home:boards:query")
//    @GetMapping(value = "/{boardsid}")
//    public AjaxResult getInfo(@PathVariable("boardsid") Long boardsid)
//    {
//        return success(carsHomeEqBoardsService.selectCarsHomeEqBoardsByBoardsid(boardsid));
//    }
//
//    /**
//     * 新增车场设备板卡
//     */
//    @RequiresPermissions("sys-cars-home:boards:add")
//    @Log(title = "车场设备板卡", businessType = BusinessType.INSERT)
//    @PostMapping
//    public AjaxResult add(@RequestBody CarsHomeEqBoards carsHomeEqBoards)
//    {
//        return toAjax(carsHomeEqBoardsService.insertCarsHomeEqBoards(carsHomeEqBoards));
//    }
//
//    /**
//     * 修改车场设备板卡
//     */
//    @RequiresPermissions("sys-cars-home:boards:edit")
//    @Log(title = "车场设备板卡", businessType = BusinessType.UPDATE)
//    @PutMapping
//    public AjaxResult edit(@RequestBody CarsHomeEqBoards carsHomeEqBoards)
//    {
//        return toAjax(carsHomeEqBoardsService.updateCarsHomeEqBoards(carsHomeEqBoards));
//    }
//
//    /**
//     * 删除车场设备板卡
//     */
//    @RequiresPermissions("sys-cars-home:boards:remove")
//    @Log(title = "车场设备板卡", businessType = BusinessType.DELETE)
//	@DeleteMapping("/{boardsids}")
//    public AjaxResult remove(@PathVariable Long[] boardsids)
//    {
//        return toAjax(carsHomeEqBoardsService.deleteCarsHomeEqBoardsByBoardsids(boardsids));
//    }
}
