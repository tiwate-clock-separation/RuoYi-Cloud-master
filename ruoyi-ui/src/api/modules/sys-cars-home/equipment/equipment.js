import request from '@/utils/request'

// 查询车场设备列表
export function listEquipment(query) {
  return request({
    url: '/sys-cars-home/equipment/list',
    method: 'get',
    params: query
  })
}

// 查询车场设备详细
export function getEquipment(eqid) {
  return request({
    url: '/sys-cars-home/equipment/' + eqid,
    method: 'get'
  })
}

// 新增车场设备
export function addEquipment(data) {
  return request({
    url: '/sys-cars-home/equipment',
    method: 'post',
    data: data
  })
}

// 修改车场设备
export function updateEquipment(data) {
  return request({
    url: '/sys-cars-home/equipment',
    method: 'put',
    data: data
  })
}

// 删除车场设备
export function delEquipment(eqid) {
  return request({
    url: '/sys-cars-home/equipment/' + eqid,
    method: 'delete'
  })
}
