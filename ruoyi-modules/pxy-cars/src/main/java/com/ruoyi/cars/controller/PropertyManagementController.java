package com.ruoyi.cars.controller;

import java.util.List;
import java.io.IOException;
import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.log.annotation.Log;
import com.ruoyi.common.log.enums.BusinessType;
import com.ruoyi.common.security.annotation.RequiresPermissions;
import com.ruoyi.domain.PropertyManagement;
import com.ruoyi.cars.service.IPropertyManagementService;
import com.ruoyi.common.core.web.controller.BaseController;
import com.ruoyi.common.core.web.domain.AjaxResult;
import com.ruoyi.common.core.utils.poi.ExcelUtil;
import com.ruoyi.common.core.web.page.TableDataInfo;

/**
 * 物业管理Controller
 *
 * @author ruoyi
 * @date 2023-11-30
 */
@RestController
@RequestMapping("/management")
public class PropertyManagementController extends BaseController
{
    @Autowired
    private IPropertyManagementService propertyManagementService;

    /**
     * 查询物业管理列表
     */
    @RequiresPermissions("pxy-cars:management:list")
    @GetMapping("/list")
    public TableDataInfo list(PropertyManagement propertyManagement)
    {
        startPage();
        List<PropertyManagement> list = propertyManagementService.selectPropertyManagementList(propertyManagement);
        return getDataTable(list);
    }

    /**
     * 导出物业管理列表
     */
    @RequiresPermissions("pxy-cars:management:export")
    @Log(title = "物业管理", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, PropertyManagement propertyManagement)
    {
        List<PropertyManagement> list = propertyManagementService.selectPropertyManagementList(propertyManagement);
        ExcelUtil<PropertyManagement> util = new ExcelUtil<PropertyManagement>(PropertyManagement.class);
        util.exportExcel(response, list, "物业管理数据");
    }

    /**
     * 获取物业管理详细信息
     */
    @RequiresPermissions("pxy-cars:management:query")
    @GetMapping(value = "/{pmid}")
    public AjaxResult getInfo(@PathVariable("pmid") Long pmid)
    {
        return success(propertyManagementService.selectPropertyManagementByPmid(pmid));
    }

    /**
     * 新增物业管理
     */
    @RequiresPermissions("pxy-cars:management:add")
    @Log(title = "物业管理", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody PropertyManagement propertyManagement)
    {
        return toAjax(propertyManagementService.insertPropertyManagement(propertyManagement));
    }

    /**
     * 修改物业管理
     */
    @RequiresPermissions("pxy-cars:management:edit")
    @Log(title = "物业管理", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody PropertyManagement propertyManagement)
    {
        return toAjax(propertyManagementService.updatePropertyManagement(propertyManagement));
    }

    /**
     * 删除物业管理
     */
    @RequiresPermissions("pxy-cars:management:remove")
    @Log(title = "物业管理", businessType = BusinessType.DELETE)
	@DeleteMapping("/{pmids}")
    public AjaxResult remove(@PathVariable Long[] pmids)
    {
        return toAjax(propertyManagementService.deletePropertyManagementByPmids(pmids));
    }
}
