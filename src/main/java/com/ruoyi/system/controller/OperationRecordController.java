package com.ruoyi.system.controller;

import java.util.List;
import java.io.IOException;
import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.log.annotation.Log;
import com.ruoyi.common.log.enums.BusinessType;
import com.ruoyi.common.security.annotation.RequiresPermissions;
import com.ruoyi.system.domain.OperationRecord;
import com.ruoyi.system.service.IOperationRecordService;
import com.ruoyi.common.core.web.controller.BaseController;
import com.ruoyi.common.core.web.domain.AjaxResult;
import com.ruoyi.common.core.utils.poi.ExcelUtil;
import com.ruoyi.common.core.web.page.TableDataInfo;

/**
 * 设备操作记录Controller
 * 
 * @author ruoyi
 * @date 2023-11-29
 */
@RestController
@RequestMapping("/record")
public class OperationRecordController extends BaseController
{
    @Autowired
    private IOperationRecordService operationRecordService;

    /**
     * 查询设备操作记录列表
     */
    @RequiresPermissions("system:record:list")
    @GetMapping("/list")
    public TableDataInfo list(OperationRecord operationRecord)
    {
        startPage();
        List<OperationRecord> list = operationRecordService.selectOperationRecordList(operationRecord);
        return getDataTable(list);
    }

    /**
     * 导出设备操作记录列表
     */
    @RequiresPermissions("system:record:export")
    @Log(title = "设备操作记录", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, OperationRecord operationRecord)
    {
        List<OperationRecord> list = operationRecordService.selectOperationRecordList(operationRecord);
        ExcelUtil<OperationRecord> util = new ExcelUtil<OperationRecord>(OperationRecord.class);
        util.exportExcel(response, list, "设备操作记录数据");
    }

    /**
     * 获取设备操作记录详细信息
     */
    @RequiresPermissions("system:record:query")
    @GetMapping(value = "/{orid}")
    public AjaxResult getInfo(@PathVariable("orid") Long orid)
    {
        return success(operationRecordService.selectOperationRecordByOrid(orid));
    }

    /**
     * 新增设备操作记录
     */
    @RequiresPermissions("system:record:add")
    @Log(title = "设备操作记录", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody OperationRecord operationRecord)
    {
        return toAjax(operationRecordService.insertOperationRecord(operationRecord));
    }

    /**
     * 修改设备操作记录
     */
    @RequiresPermissions("system:record:edit")
    @Log(title = "设备操作记录", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody OperationRecord operationRecord)
    {
        return toAjax(operationRecordService.updateOperationRecord(operationRecord));
    }

    /**
     * 删除设备操作记录
     */
    @RequiresPermissions("system:record:remove")
    @Log(title = "设备操作记录", businessType = BusinessType.DELETE)
	@DeleteMapping("/{orids}")
    public AjaxResult remove(@PathVariable Long[] orids)
    {
        return toAjax(operationRecordService.deleteOperationRecordByOrids(orids));
    }
}
