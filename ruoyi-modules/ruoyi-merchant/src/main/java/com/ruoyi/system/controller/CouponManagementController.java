package com.ruoyi.system.controller;

import java.util.List;
import java.io.IOException;
import javax.servlet.http.HttpServletResponse;

import com.ruoyi.system.service.ICouponManagementService;
import com.ruoyi.system.service.QrCodeService;
import com.ruoyi.system.util.BaseResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.log.annotation.Log;
import com.ruoyi.common.log.enums.BusinessType;
import com.ruoyi.common.security.annotation.RequiresPermissions;
import com.ruoyi.domain.CouponManagement;
import com.ruoyi.common.core.web.controller.BaseController;
import com.ruoyi.common.core.web.domain.AjaxResult;
import com.ruoyi.common.core.utils.poi.ExcelUtil;
import com.ruoyi.common.core.web.page.TableDataInfo;

/**
 * 优惠卷管理Controller
 * 
 * @author lbw
 * @date 2023-12-07
 */
@RestController
@RequestMapping("/coupon")
public class CouponManagementController extends BaseController
{
    @Autowired
    private ICouponManagementService couponManagementService;


    @Autowired
    private QrCodeService codeService;


    /**
     * 查询优惠卷管理列表
     */
    @RequiresPermissions("sys-coupon:management:list")
    @GetMapping("/list")
    public TableDataInfo list(CouponManagement couponManagement)
    {
        startPage();
        List<CouponManagement> list = couponManagementService.selectCouponManagementList(couponManagement);
        return getDataTable(list);
    }

    /**
     * 导出优惠卷管理列表
     */
    @RequiresPermissions("sys-coupon:management:export")
    @Log(title = "优惠卷管理", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, CouponManagement couponManagement)
    {
        List<CouponManagement> list = couponManagementService.selectCouponManagementList(couponManagement);
        ExcelUtil<CouponManagement> util = new ExcelUtil<CouponManagement>(CouponManagement.class);
        util.exportExcel(response, list, "优惠卷管理数据");
    }

    /**
     * 获取优惠卷管理详细信息
     */
    @RequiresPermissions("sys-coupon:management:query")
    @GetMapping(value = "/{cmid}")
    public AjaxResult getInfo(@PathVariable("cmid") Long cmid)
    {
        return success(couponManagementService.selectCouponManagementByCmid(cmid));
    }

    /**
     * 新增优惠卷管理
     */
    @RequiresPermissions("sys-coupon:management:add")
    @Log(title = "优惠卷管理", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody CouponManagement couponManagement)
    {
        return toAjax(couponManagementService.insertCouponManagement(couponManagement));
    }

    /**
     * 修改优惠卷管理
     */
    @RequiresPermissions("sys-coupon:management:edit")
    @Log(title = "优惠卷管理", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody CouponManagement couponManagement)
    {
        return toAjax(couponManagementService.updateCouponManagement(couponManagement));
    }

    /**
     * 删除优惠卷管理
     */
    @RequiresPermissions("sys-coupon:management:remove")
    @Log(title = "优惠卷管理", businessType = BusinessType.DELETE)
	@DeleteMapping("/{cmids}")
    public AjaxResult remove(@PathVariable Long[] cmids)
    {
        return toAjax(couponManagementService.deleteCouponManagementByCmids(cmids));
    }



    //生成二维码并将其返回给前端调用者_hutool
    @PostMapping("/generate/v3")
    public BaseResponse generateV3(String content,HttpServletResponse servletResponse){
        BaseResponse response=new BaseResponse("200","成功",null);
        try {
            //将生成的二维码文件存放于文件目录中
            //final String fileName=LOCALDATEFORMAT.get().format(new Date());
            //codeService.createCodeToFile(content,RootPath+File.separator+fileName+".png");

            //将生成的二维码文件直接返回给前端响应流
            codeService.createCodeToStream(content,servletResponse);
        }catch (Exception e){
            response=new BaseResponse("500","失败",e.getMessage());
        }
        return response;
    }
}
