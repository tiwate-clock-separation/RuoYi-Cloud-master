package com.ruoyi.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.mapper.CarsHomeTollDayMapper;
import com.ruoyi.domain.CarsHomeTollDay;
import com.ruoyi.service.ICarsHomeTollDayService;

/**
 * 按天收费Service业务层处理
 * 
 * @author chn
 * @date 2023-12-14
 */
@Service
public class CarsHomeTollDayServiceImpl implements ICarsHomeTollDayService 
{
    @Autowired
    private CarsHomeTollDayMapper carsHomeTollDayMapper;

    /**
     * 查询按天收费
     * 
     * @param dayid 按天收费主键
     * @return 按天收费
     */
    @Override
    public CarsHomeTollDay selectCarsHomeTollDayByDayid(Long dayid)
    {
        return carsHomeTollDayMapper.selectCarsHomeTollDayByDayid(dayid);
    }

    /**
     * 查询按天收费列表
     * 
     * @param carsHomeTollDay 按天收费
     * @return 按天收费
     */
    @Override
    public List<CarsHomeTollDay> selectCarsHomeTollDayList(CarsHomeTollDay carsHomeTollDay)
    {
        return carsHomeTollDayMapper.selectCarsHomeTollDayList(carsHomeTollDay);
    }

    /**
     * 新增按天收费
     * 
     * @param carsHomeTollDay 按天收费
     * @return 结果
     */
    @Override
    public int insertCarsHomeTollDay(CarsHomeTollDay carsHomeTollDay)
    {
        return carsHomeTollDayMapper.insertCarsHomeTollDay(carsHomeTollDay);
    }

    /**
     * 修改按天收费
     * 
     * @param carsHomeTollDay 按天收费
     * @return 结果
     */
    @Override
    public int updateCarsHomeTollDay(CarsHomeTollDay carsHomeTollDay)
    {
        return carsHomeTollDayMapper.updateCarsHomeTollDay(carsHomeTollDay);
    }

    /**
     * 批量删除按天收费
     * 
     * @param dayids 需要删除的按天收费主键
     * @return 结果
     */
    @Override
    public int deleteCarsHomeTollDayByDayids(Long[] dayids)
    {
        return carsHomeTollDayMapper.deleteCarsHomeTollDayByDayids(dayids);
    }

    /**
     * 删除按天收费信息
     * 
     * @param dayid 按天收费主键
     * @return 结果
     */
    @Override
    public int deleteCarsHomeTollDayByDayid(Long dayid)
    {
        return carsHomeTollDayMapper.deleteCarsHomeTollDayByDayid(dayid);
    }
}
