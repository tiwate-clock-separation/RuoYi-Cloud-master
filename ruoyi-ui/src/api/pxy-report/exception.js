import request from '@/utils/request'

// 查询异常报表列表
export function listException(query) {
  return request({
    url: '/pxy-report/exception/list',
    method: 'get',
    params: query
  })
}

// 查询异常报表详细
export function getException(exceid) {
  return request({
    url: '/pxy-report/exception/' + exceid,
    method: 'get'
  })
}

// 新增异常报表
export function addException(data) {
  return request({
    url: '/pxy-report/exception',
    method: 'post',
    data: data
  })
}

// 修改异常报表
export function updateException(data) {
  return request({
    url: '/pxy-report/exception',
    method: 'put',
    data: data
  })
}

// 删除异常报表
export function delException(exceid) {
  return request({
    url: '/pxy-report/exception/' + exceid,
    method: 'delete'
  })
}
