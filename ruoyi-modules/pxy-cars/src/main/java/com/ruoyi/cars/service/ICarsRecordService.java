package com.ruoyi.cars.service;

import java.util.List;
import com.ruoyi.domain.CarsRecord;

/**
 * 进出记录Service接口
 *
 * @author ruoyi
 * @date 2023-11-30
 */
public interface ICarsRecordService
{
    /**
     * 查询进出记录
     *
     * @param crid 进出记录主键
     * @return 进出记录
     */
    public CarsRecord selectCarsRecordByCrid(Long crid);

    /**
     * 查询进出记录列表
     *
     * @param carsRecord 进出记录
     * @return 进出记录集合
     */
    public List<CarsRecord> selectCarsRecordList(CarsRecord carsRecord);

    /**
     * 新增进出记录
     *
     * @param carsRecord 进出记录
     * @return 结果
     */
    public int insertCarsRecord(CarsRecord carsRecord);

    /**
     * 修改进出记录
     *
     * @param carsRecord 进出记录
     * @return 结果
     */
    public int updateCarsRecord(CarsRecord carsRecord);

    /**
     * 批量删除进出记录
     *
     * @param crids 需要删除的进出记录主键集合
     * @return 结果
     */
    public int deleteCarsRecordByCrids(Long[] crids);

    /**
     * 删除进出记录信息
     *
     * @param crid 进出记录主键
     * @return 结果
     */
    public int deleteCarsRecordByCrid(Long crid);
}
