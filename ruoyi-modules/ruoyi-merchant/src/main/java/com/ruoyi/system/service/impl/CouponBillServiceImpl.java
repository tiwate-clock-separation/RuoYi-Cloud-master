package com.ruoyi.system.service.impl;

import java.util.List;
import com.ruoyi.common.core.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.CouponBillMapper;
import com.ruoyi.domain.CouponBill;
import com.ruoyi.system.service.ICouponBillService;

/**
 * 优惠卷账单Service业务层处理
 *
 * @author ruoyi
 * @date 2023-12-26
 */
@Service
public class CouponBillServiceImpl implements ICouponBillService
{
    @Autowired
    private CouponBillMapper couponBillMapper;

    /**
     * 查询优惠卷账单
     *
     * @param id 优惠卷账单主键
     * @return 优惠卷账单
     */
    @Override
    public CouponBill selectCouponBillById(Long id)
    {
        return couponBillMapper.selectCouponBillById(id);
    }

    /**
     * 查询优惠卷账单列表
     *
     * @param couponBill 优惠卷账单
     * @return 优惠卷账单
     */
    @Override
    public List<CouponBill> selectCouponBillList(CouponBill couponBill)
    {
        return couponBillMapper.selectCouponBillList(couponBill);
    }

    /**
     * 新增优惠卷账单
     *
     * @param couponBill 优惠卷账单
     * @return 结果
     */
    @Override
    public int insertCouponBill(CouponBill couponBill)
    {
        couponBill.setCreateTime(DateUtils.getNowDate());
        return couponBillMapper.insertCouponBill(couponBill);
    }

    /**
     * 修改优惠卷账单
     *
     * @param couponBill 优惠卷账单
     * @return 结果
     */
    @Override
    public int updateCouponBill(CouponBill couponBill)
    {
        couponBill.setUpdateTime(DateUtils.getNowDate());
        return couponBillMapper.updateCouponBill(couponBill);
    }

    /**
     * 批量删除优惠卷账单
     *
     * @param ids 需要删除的优惠卷账单主键
     * @return 结果
     */
    @Override
    public int deleteCouponBillByIds(Long[] ids)
    {
        return couponBillMapper.deleteCouponBillByIds(ids);
    }

    /**
     * 删除优惠卷账单信息
     *
     * @param id 优惠卷账单主键
     * @return 结果
     */
    @Override
    public int deleteCouponBillById(Long id)
    {
        return couponBillMapper.deleteCouponBillById(id);
    }
}
