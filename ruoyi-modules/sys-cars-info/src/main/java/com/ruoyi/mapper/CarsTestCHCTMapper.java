package com.ruoyi.mapper;

import com.ruoyi.domain.CarsChCt;
import org.apache.ibatis.annotations.*;

import java.util.List;

@Mapper
public interface CarsTestCHCTMapper {
//    增
    @Insert("insert into cars_ch_ct values(${carsChCt.chid},${carsChCt.ctid})")
    public int insertPojo(CarsChCt carsChCt);
//    删
    @Delete("delete from cars_ch_ct where chid = ${carsChCt.chid} and ctid = ${carsChCt.ctid}")
    public int delByPojo(CarsChCt carsChCt);
//    改
//    @Update("update ")
//    查
    @Select("select chid,ctid from cars_ch_ct")
    public List<CarsChCt> getAll();
    @Select("select chid,ctid from cars_ch_ct where chid = #{chid}")
    public List<CarsChCt> getAllByChid(Long chid);
}
