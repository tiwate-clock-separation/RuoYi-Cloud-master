package com.ruoyi.staff.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.log.annotation.Log;
import com.ruoyi.common.log.enums.BusinessType;
import com.ruoyi.common.security.annotation.RequiresPermissions;
import com.ruoyi.domain.EmpH5;
import com.ruoyi.staff.service.IEmpH5Service;
import com.ruoyi.common.core.web.controller.BaseController;
import com.ruoyi.common.core.web.domain.AjaxResult;
import com.ruoyi.common.core.utils.poi.ExcelUtil;
import com.ruoyi.common.core.web.page.TableDataInfo;

/**
 * H5人员Controller
 * 
 * @author ruoyi
 * @date 2023-12-15
 */
@RestController
@RequestMapping("/h5")
public class EmpH5Controller extends BaseController
{
    @Autowired
    private IEmpH5Service empH5Service;

    /**
     * 查询H5人员列表
     */
    @RequiresPermissions("pxy-staff:h5:list")
    @GetMapping("/list")
    public TableDataInfo list(EmpH5 empH5)
    {
        startPage();
        List<EmpH5> list = empH5Service.selectEmpH5List(empH5);
        return getDataTable(list);
    }

    /**
     * 导出H5人员列表
     */
    @RequiresPermissions("pxy-staff:h5:export")
    @Log(title = "H5人员", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, EmpH5 empH5)
    {
        List<EmpH5> list = empH5Service.selectEmpH5List(empH5);
        ExcelUtil<EmpH5> util = new ExcelUtil<EmpH5>(EmpH5.class);
        util.exportExcel(response, list, "H5人员数据");
    }

    /**
     * 获取H5人员详细信息
     */
    @RequiresPermissions("pxy-staff:h5:query")
    @GetMapping(value = "/{ehid}")
    public AjaxResult getInfo(@PathVariable("ehid") Long ehid)
    {
        return success(empH5Service.selectEmpH5ByEhid(ehid));
    }

    /**
     * 新增H5人员
     */
    @RequiresPermissions("pxy-staff:h5:add")
    @Log(title = "H5人员", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody EmpH5 empH5)
    {
        return toAjax(empH5Service.insertEmpH5(empH5));
    }

    /**
     * 修改H5人员
     */
    @RequiresPermissions("pxy-staff:h5:edit")
    @Log(title = "H5人员", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody EmpH5 empH5)
    {
        return toAjax(empH5Service.updateEmpH5(empH5));
    }

    /**
     * 删除H5人员
     */
    @RequiresPermissions("pxy-staff:h5:remove")
    @Log(title = "H5人员", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ehids}")
    public AjaxResult remove(@PathVariable Long[] ehids)
    {
        return toAjax(empH5Service.deleteEmpH5ByEhids(ehids));
    }
}
