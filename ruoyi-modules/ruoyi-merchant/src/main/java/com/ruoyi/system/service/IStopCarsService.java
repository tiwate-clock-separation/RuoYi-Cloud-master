package com.ruoyi.system.service;

import org.springframework.stereotype.Service;

public interface IStopCarsService {

    public int stopMoney(String licenseNumber,double money,long chid);
}
