package com.ruoyi.controller;

import java.util.List;
import java.io.IOException;
import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.log.annotation.Log;
import com.ruoyi.common.log.enums.BusinessType;
import com.ruoyi.common.security.annotation.RequiresPermissions;
import com.ruoyi.domain.CarsHomeAlipay;
import com.ruoyi.service.ICarsHomeAlipayService;
import com.ruoyi.common.core.web.controller.BaseController;
import com.ruoyi.common.core.web.domain.AjaxResult;
import com.ruoyi.common.core.utils.poi.ExcelUtil;
import com.ruoyi.common.core.web.page.TableDataInfo;

/**
 * 支付宝支付配置Controller
 * 
 * @author chn
 * @date 2023-12-27
 */
@RestController
@RequestMapping("/alipay")
public class CarsHomeAlipayController extends BaseController
{
    @Autowired
    private ICarsHomeAlipayService carsHomeAlipayService;

    /**
     * 查询支付宝支付配置列表
     */
    @RequiresPermissions("sys-cars-home:alipay:list")
    @GetMapping("/list")
    public TableDataInfo list(CarsHomeAlipay carsHomeAlipay)
    {
        startPage();
        List<CarsHomeAlipay> list = carsHomeAlipayService.selectCarsHomeAlipayList(carsHomeAlipay);
        return getDataTable(list);
    }

    /**
     * 导出支付宝支付配置列表
     */
    @RequiresPermissions("sys-cars-home:alipay:export")
    @Log(title = "支付宝支付配置", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, CarsHomeAlipay carsHomeAlipay)
    {
        List<CarsHomeAlipay> list = carsHomeAlipayService.selectCarsHomeAlipayList(carsHomeAlipay);
        ExcelUtil<CarsHomeAlipay> util = new ExcelUtil<CarsHomeAlipay>(CarsHomeAlipay.class);
        util.exportExcel(response, list, "支付宝支付配置数据");
    }

    /**
     * 获取支付宝支付配置详细信息
     */
    @RequiresPermissions("sys-cars-home:alipay:query")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(carsHomeAlipayService.selectCarsHomeAlipayById(id));
    }

    /**
     * 新增支付宝支付配置
     */
    @RequiresPermissions("sys-cars-home:alipay:add")
    @Log(title = "支付宝支付配置", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody CarsHomeAlipay carsHomeAlipay)
    {
        return toAjax(carsHomeAlipayService.insertCarsHomeAlipay(carsHomeAlipay));
    }

    /**
     * 修改支付宝支付配置
     */
    @RequiresPermissions("sys-cars-home:alipay:edit")
    @Log(title = "支付宝支付配置", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody CarsHomeAlipay carsHomeAlipay)
    {
        return toAjax(carsHomeAlipayService.updateCarsHomeAlipay(carsHomeAlipay));
    }

    /**
     * 删除支付宝支付配置
     */
    @RequiresPermissions("sys-cars-home:alipay:remove")
    @Log(title = "支付宝支付配置", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(carsHomeAlipayService.deleteCarsHomeAlipayByIds(ids));
    }
}
