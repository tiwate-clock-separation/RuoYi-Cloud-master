import request from '@/utils/request'

// 查询业主车辆审核列表
export function listCars(query) {
  return request({
    url: '/ruoyi-audit/cars/list',
    method: 'get',
    params: query
  })
}

// 查询业主车辆审核详细
export function getCars(caid) {
  return request({
    url: '/ruoyi-audit/cars/' + caid,
    method: 'get'
  })
}

// 新增业主车辆审核
export function addCars(data) {
  return request({
    url: '/ruoyi-audit/cars',
    method: 'post',
    data: data
  })
}

// 修改业主车辆审核
export function updateCars(data) {
  return request({
    url: '/ruoyi-audit/cars',
    method: 'put',
    data: data
  })
}

// 删除业主车辆审核
export function delCars(caid) {
  return request({
    url: '/ruoyi-audit/cars/' + caid,
    method: 'delete'
  })
}
