package com.ruoyi.mapper;

import java.util.List;
import com.ruoyi.domain.CarsHomeEqIdentify;

/**
 * 车牌识别Mapper接口
 * 
 * @author chn
 * @date 2023-12-07
 */
public interface CarsHomeEqIdentifyMapper 
{
    /**
     * 查询车牌识别
     * 
     * @param entifyid 车牌识别主键
     * @return 车牌识别
     */
    public CarsHomeEqIdentify selectCarsHomeEqIdentifyByEntifyid(Long entifyid);

    /**
     * 查询车牌识别列表
     * 
     * @param carsHomeEqIdentify 车牌识别
     * @return 车牌识别集合
     */
    public List<CarsHomeEqIdentify> selectCarsHomeEqIdentifyList(CarsHomeEqIdentify carsHomeEqIdentify);

    /**
     * 新增车牌识别
     * 
     * @param carsHomeEqIdentify 车牌识别
     * @return 结果
     */
    public int insertCarsHomeEqIdentify(CarsHomeEqIdentify carsHomeEqIdentify);

    /**
     * 修改车牌识别
     * 
     * @param carsHomeEqIdentify 车牌识别
     * @return 结果
     */
    public int updateCarsHomeEqIdentify(CarsHomeEqIdentify carsHomeEqIdentify);

    /**
     * 删除车牌识别
     * 
     * @param entifyid 车牌识别主键
     * @return 结果
     */
    public int deleteCarsHomeEqIdentifyByEntifyid(Long entifyid);

    /**
     * 批量删除车牌识别
     * 
     * @param entifyids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteCarsHomeEqIdentifyByEntifyids(Long[] entifyids);
}
