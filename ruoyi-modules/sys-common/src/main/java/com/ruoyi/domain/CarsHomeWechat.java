package com.ruoyi.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.core.annotation.Excel;
import com.ruoyi.common.core.web.domain.BaseEntity;

/**
 * 微信支付对象 cars_home_weChat
 * 
 * @author chn
 * @date 2023-12-27
 */
public class CarsHomeWechat extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 自增ID */
    private Long id;

    /** 唯一编码,前台使用 */
    @Excel(name = "唯一编码,前台使用")
    private String code;

    /** 状态 */
    @Excel(name = "状态")
    private Long status;

    /** 是否删除 */
    @Excel(name = "是否删除")
    private Long deleted;

    /** 商户名 */
    @Excel(name = "商户名")
    private String merchantName;

    /** 商户号 */
    @Excel(name = "商户号")
    private Long merchantId;

    /** 微信支付签名 */
    @Excel(name = "微信支付签名")
    private String wechatSignature;

    /** 商家占比 */
    @Excel(name = "商家占比")
    private Long merchantRatio;

    /** 姓名 */
    @Excel(name = "姓名")
    private String name;

    /** 手机号 */
    @Excel(name = "手机号")
    private String phone;

    /** openId */
    @Excel(name = "openId")
    private String openId;

    /** 个人占比 */
    @Excel(name = "个人占比")
    private Long individualRatio;

    /** 车场占比 */
    @Excel(name = "车场占比")
    private Long carsHomeRatio;

    /** 商户类型(0商家1个人) */
    @Excel(name = "商户类型(0商家1个人)")
    private Long merchantType;

    /** 商户模式(0单商户1服务商) */
    @Excel(name = "商户模式(0单商户1服务商)")
    private Long merchantMode;

    /** 所属车场 */
    @Excel(name = "所属车场")
    private Long chid;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setCode(String code) 
    {
        this.code = code;
    }

    public String getCode() 
    {
        return code;
    }
    public void setStatus(Long status) 
    {
        this.status = status;
    }

    public Long getStatus() 
    {
        return status;
    }
    public void setDeleted(Long deleted) 
    {
        this.deleted = deleted;
    }

    public Long getDeleted() 
    {
        return deleted;
    }
    public void setMerchantName(String merchantName) 
    {
        this.merchantName = merchantName;
    }

    public String getMerchantName() 
    {
        return merchantName;
    }
    public void setMerchantId(Long merchantId) 
    {
        this.merchantId = merchantId;
    }

    public Long getMerchantId() 
    {
        return merchantId;
    }
    public void setWechatSignature(String wechatSignature) 
    {
        this.wechatSignature = wechatSignature;
    }

    public String getWechatSignature() 
    {
        return wechatSignature;
    }
    public void setMerchantRatio(Long merchantRatio) 
    {
        this.merchantRatio = merchantRatio;
    }

    public Long getMerchantRatio() 
    {
        return merchantRatio;
    }
    public void setName(String name) 
    {
        this.name = name;
    }

    public String getName() 
    {
        return name;
    }
    public void setPhone(String phone) 
    {
        this.phone = phone;
    }

    public String getPhone() 
    {
        return phone;
    }
    public void setOpenId(String openId) 
    {
        this.openId = openId;
    }

    public String getOpenId() 
    {
        return openId;
    }
    public void setIndividualRatio(Long individualRatio) 
    {
        this.individualRatio = individualRatio;
    }

    public Long getIndividualRatio() 
    {
        return individualRatio;
    }
    public void setCarsHomeRatio(Long carsHomeRatio) 
    {
        this.carsHomeRatio = carsHomeRatio;
    }

    public Long getCarsHomeRatio() 
    {
        return carsHomeRatio;
    }
    public void setMerchantType(Long merchantType) 
    {
        this.merchantType = merchantType;
    }

    public Long getMerchantType() 
    {
        return merchantType;
    }
    public void setMerchantMode(Long merchantMode) 
    {
        this.merchantMode = merchantMode;
    }

    public Long getMerchantMode() 
    {
        return merchantMode;
    }
    public void setChid(Long chid) 
    {
        this.chid = chid;
    }

    public Long getChid() 
    {
        return chid;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("code", getCode())
            .append("status", getStatus())
            .append("deleted", getDeleted())
            .append("createTime", getCreateTime())
            .append("updateTime", getUpdateTime())
            .append("merchantName", getMerchantName())
            .append("merchantId", getMerchantId())
            .append("wechatSignature", getWechatSignature())
            .append("merchantRatio", getMerchantRatio())
            .append("name", getName())
            .append("phone", getPhone())
            .append("openId", getOpenId())
            .append("individualRatio", getIndividualRatio())
            .append("carsHomeRatio", getCarsHomeRatio())
            .append("merchantType", getMerchantType())
            .append("merchantMode", getMerchantMode())
            .append("chid", getChid())
            .toString();
    }
}
