package com.ruoyi.controller;

import java.util.List;
import java.io.IOException;
import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.log.annotation.Log;
import com.ruoyi.common.log.enums.BusinessType;
import com.ruoyi.common.security.annotation.RequiresPermissions;
import com.ruoyi.domain.CarsHomeWechat;
import com.ruoyi.service.ICarsHomeWechatService;
import com.ruoyi.common.core.web.controller.BaseController;
import com.ruoyi.common.core.web.domain.AjaxResult;
import com.ruoyi.common.core.utils.poi.ExcelUtil;
import com.ruoyi.common.core.web.page.TableDataInfo;

/**
 * 微信支付Controller
 * 
 * @author chn
 * @date 2023-12-27
 */
@RestController
@RequestMapping("/weChat")
public class CarsHomeWechatController extends BaseController
{
    @Autowired
    private ICarsHomeWechatService carsHomeWechatService;

    /**
     * 查询微信支付列表
     */
    @RequiresPermissions("sys-cars-home:weChat:list")
    @GetMapping("/list")
    public TableDataInfo list(CarsHomeWechat carsHomeWechat)
    {
        startPage();
        List<CarsHomeWechat> list = carsHomeWechatService.selectCarsHomeWechatList(carsHomeWechat);
        return getDataTable(list);
    }

    /**
     * 导出微信支付列表
     */
    @RequiresPermissions("sys-cars-home:weChat:export")
    @Log(title = "微信支付", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, CarsHomeWechat carsHomeWechat)
    {
        List<CarsHomeWechat> list = carsHomeWechatService.selectCarsHomeWechatList(carsHomeWechat);
        ExcelUtil<CarsHomeWechat> util = new ExcelUtil<CarsHomeWechat>(CarsHomeWechat.class);
        util.exportExcel(response, list, "微信支付数据");
    }

    /**
     * 获取微信支付详细信息
     */
    @RequiresPermissions("sys-cars-home:weChat:query")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(carsHomeWechatService.selectCarsHomeWechatById(id));
    }

    /**
     * 新增微信支付
     */
    @RequiresPermissions("sys-cars-home:weChat:add")
    @Log(title = "微信支付", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody CarsHomeWechat carsHomeWechat)
    {
        return toAjax(carsHomeWechatService.insertCarsHomeWechat(carsHomeWechat));
    }

    /**
     * 修改微信支付
     */
    @RequiresPermissions("sys-cars-home:weChat:edit")
    @Log(title = "微信支付", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody CarsHomeWechat carsHomeWechat)
    {
        return toAjax(carsHomeWechatService.updateCarsHomeWechat(carsHomeWechat));
    }

    /**
     * 删除微信支付
     */
    @RequiresPermissions("sys-cars-home:weChat:remove")
    @Log(title = "微信支付", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(carsHomeWechatService.deleteCarsHomeWechatByIds(ids));
    }
}
