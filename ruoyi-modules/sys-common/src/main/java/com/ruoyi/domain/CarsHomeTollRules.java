package com.ruoyi.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.core.annotation.Excel;
import com.ruoyi.common.core.web.domain.BaseEntity;

/**
 * 车场收费规则类型对象 cars_home_toll_rules
 * 
 * @author ruoyi
 * @date 2023-12-04
 */
public class CarsHomeTollRules extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 自增ID */
    private Long chtrid;

    /** 唯一编码,前台使用 */
    @Excel(name = "唯一编码,前台使用")
    private String code;

    /** 状态 */
    @Excel(name = "状态")
    private Long status;

    /** 是否删除 */
    @Excel(name = "是否删除")
    private Long deleted;

    /** 车场收费规则名称 */
    @Excel(name = "车场收费规则名称")
    private String tollRulesName;

    public void setChtrid(Long chtrid) 
    {
        this.chtrid = chtrid;
    }

    public Long getChtrid() 
    {
        return chtrid;
    }
    public void setCode(String code) 
    {
        this.code = code;
    }

    public String getCode() 
    {
        return code;
    }
    public void setStatus(Long status) 
    {
        this.status = status;
    }

    public Long getStatus() 
    {
        return status;
    }
    public void setDeleted(Long deleted) 
    {
        this.deleted = deleted;
    }

    public Long getDeleted() 
    {
        return deleted;
    }
    public void setTollRulesName(String tollRulesName) 
    {
        this.tollRulesName = tollRulesName;
    }

    public String getTollRulesName() 
    {
        return tollRulesName;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("chtrid", getChtrid())
            .append("code", getCode())
            .append("status", getStatus())
            .append("deleted", getDeleted())
            .append("createTime", getCreateTime())
            .append("updateTime", getUpdateTime())
            .append("tollRulesName", getTollRulesName())
            .toString();
    }
}
