package com.ruoyi.domain;

import com.ruoyi.common.core.annotation.Excel;
import com.ruoyi.common.core.web.domain.BaseEntity;

/**
 * 商家管理对象 merchant_management
 *
 * @author ruoyi
 * @date 2023-12-04
 */
public class MerchantManagement extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 自增ID */
    private Long pmid;

    /** 唯一编码,前台使用 */
    @Excel(name = "唯一编码,前台使用")
    private String code;

    /** 状态 */
    @Excel(name = "状态")
    private Long status;

    /** 是否删除 */
    @Excel(name = "是否删除")
    private Long deleted;

    /** 商家名称 */
    @Excel(name = "商家名称")
    private String merchantName;

    /** 商家用户 */
    @Excel(name = "商家用户")
    private User user;

    /** 商家余额 */
    @Excel(name = "商家余额")
    private Long balance;

    /** 可透支额度 */
    @Excel(name = "可透支额度")
    private Long credit;

    @Excel(name = "充值折扣")
    private Long discount;

    /** 商家所处车厂 */
    @Excel(name = "商家所处车厂")
    private Long mcid;

    private CarsHome carsHome;

    @Excel(name = "备注")
    private String remark;

    public void setPmid(Long pmid)
    {
        this.pmid = pmid;
    }

    public Long getPmid()
    {
        return pmid;
    }
    public void setCode(String code)
    {
        this.code = code;
    }

    public String getCode()
    {
        return code;
    }
    public void setStatus(Long status)
    {
        this.status = status;
    }

    public Long getStatus()
    {
        return status;
    }
    public void setDeleted(Long deleted)
    {
        this.deleted = deleted;
    }

    public Long getDeleted()
    {
        return deleted;
    }
    public void setMerchantName(String merchantName)
    {
        this.merchantName = merchantName;
    }

    public String getMerchantName()
    {
        return merchantName;
    }

    public void setMcid(Long mcid)
    {
        this.mcid = mcid;
    }

    public Long getMcid()
    {
        return mcid;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public CarsHome getCarsHome() {
        return carsHome;
    }

    public void setCarsHome(CarsHome carsHome) {
        this.carsHome = carsHome;
    }

    public Long getBalance() {
        return balance;
    }

    public void setBalance(Long balance) {
        this.balance = balance;
    }

    public Long getCredit() {
        return credit;
    }

    public void setCredit(Long credit) {
        this.credit = credit;
    }

    public Long getDiscount() {
        return discount;
    }

    public void setDiscount(Long discount) {
        this.discount = discount;
    }

    @Override
    public String getRemark() {
        return remark;
    }

    @Override
    public void setRemark(String remark) {
        this.remark = remark;
    }

    @Override
    public String toString() {
        return "MerchantManagement{" +
                "pmid=" + pmid +
                ", code='" + code + '\'' +
                ", status=" + status +
                ", deleted=" + deleted +
                ", merchantName='" + merchantName + '\'' +
                ", user=" + user +
                ", balance=" + balance +
                ", credit=" + credit +
                ", discount=" + discount +
                ", mcid=" + mcid +
                ", carsHome=" + carsHome +
                ", remark='" + remark + '\'' +
                '}';
    }
}
