

        package com.ruoyi.domain;

        import org.apache.commons.lang3.builder.ToStringBuilder;
        import org.apache.commons.lang3.builder.ToStringStyle;
        import com.ruoyi.common.core.annotation.Excel;
        import com.ruoyi.common.core.web.domain.BaseEntity;

/**
 * 车牌识别对象 cars_home_eq_identify
 *
 * @author chn
 * @date 2023-12-08
 */
public class CarsHomeEqIdentify extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 车场设备id */
    private Long entifyid;

    /** 状态(上线下线） */
    @Excel(name = "状态(上线下线）")
    private Long status;

    /** 车道id */
    @Excel(name = "车道id")
    private Long chlid;

    /** 设备名称 */
    @Excel(name = "设备名称")
    private String entifyname;

    /** 设备IP */
    @Excel(name = "设备IP")
    private String entifyip;

    /** 设备编号 */
    @Excel(name = "设备编号")
    private String eqnumber;

    /** 关联码 */
    @Excel(name = "关联码")
    private String correlation;

    /** 主副相机(0主相机1副相机) */
    @Excel(name = "主副相机(0主相机1副相机)")
    private Long mors;

    /** 异常开闸(0不开闸1开闸) */
    @Excel(name = "异常开闸(0不开闸1开闸)")
    private Long oorg;

    /** 设备日志(0不开启1开启) */
    @Excel(name = "设备日志(0不开启1开启)")
    private Long log;

    /** 所在场地(0外场1内场) */
    @Excel(name = "所在场地(0外场1内场)")
    private Long venue;

    /** 设备IP */
    @Excel(name = "设备IP")
    private String ipaddr;

    /** 相机品牌 */
    @Excel(name = "相机品牌")
    private Long cameraid;

    /** 板卡名称 */
    @Excel(name = "板卡名称")
    private Long boardsid;

    private CarsHomeEqBoards carsHomeEqBoards;
    private CarsHomeEqCameratype carsHomeEqCameratype;

    public CarsHomeEqBoards getCarsHomeEqBoards() {
        return carsHomeEqBoards;
    }

    public void setCarsHomeEqBoards(CarsHomeEqBoards carsHomeEqBoards) {
        this.carsHomeEqBoards = carsHomeEqBoards;
    }

    public CarsHomeEqCameratype getCarsHomeEqCameratype() {
        return carsHomeEqCameratype;
    }

    public void setCarsHomeEqCameratype(CarsHomeEqCameratype carsHomeEqCameratype) {
        this.carsHomeEqCameratype = carsHomeEqCameratype;
    }

    /** 相机协议(0HTTP1MQTT） */
    @Excel(name = "相机协议(0HTTP1MQTT）")
    private Long agreement;

    /** 视频接入(0视频1图片) */
    @Excel(name = "视频接入(0视频1图片)")
    private Long video;

    /** 屏幕类型(0四方屏1二行竖屏) */
    @Excel(name = "屏幕类型(0四方屏1二行竖屏)")
    private Long screentype;

    /** 播报音量 */
    @Excel(name = "播报音量")
    private Long volume;

    /** 唯一编码,前台使用 */
    @Excel(name = "唯一编码,前台使用")
    private String code;

    /** 是否删除 */
    @Excel(name = "是否删除")
    private Long deleted;

    /** APPIP */
    @Excel(name = "APPIP")
    private String appip;

    /** 广告配置(0没有1有） */
    @Excel(name = "广告配置(0没有1有）")
    private String adedit;

    /** 广告内容 */
    @Excel(name = "广告内容")
    private String ad;

    public void setEntifyid(Long entifyid)
    {
        this.entifyid = entifyid;
    }

    public Long getEntifyid()
    {
        return entifyid;
    }
    public void setStatus(Long status)
    {
        this.status = status;
    }

    public Long getStatus()
    {
        return status;
    }
    public void setChlid(Long chlid)
    {
        this.chlid = chlid;
    }

    public Long getChlid()
    {
        return chlid;
    }
    public void setEntifyname(String entifyname)
    {
        this.entifyname = entifyname;
    }

    public String getEntifyname()
    {
        return entifyname;
    }
    public void setEntifyip(String entifyip)
    {
        this.entifyip = entifyip;
    }

    public String getEntifyip()
    {
        return entifyip;
    }
    public void setEqnumber(String eqnumber)
    {
        this.eqnumber = eqnumber;
    }

    public String getEqnumber()
    {
        return eqnumber;
    }
    public void setCorrelation(String correlation)
    {
        this.correlation = correlation;
    }

    public String getCorrelation()
    {
        return correlation;
    }
    public void setMors(Long mors)
    {
        this.mors = mors;
    }

    public Long getMors()
    {
        return mors;
    }
    public void setOorg(Long oorg)
    {
        this.oorg = oorg;
    }

    public Long getOorg()
    {
        return oorg;
    }
    public void setLog(Long log)
    {
        this.log = log;
    }

    public Long getLog()
    {
        return log;
    }
    public void setVenue(Long venue)
    {
        this.venue = venue;
    }

    public Long getVenue()
    {
        return venue;
    }
    public void setIpaddr(String ipaddr)
    {
        this.ipaddr = ipaddr;
    }

    public String getIpaddr()
    {
        return ipaddr;
    }
    public void setCameraid(Long cameraid)
    {
        this.cameraid = cameraid;
    }

    public Long getCameraid()
    {
        return cameraid;
    }
    public void setBoardsid(Long boardsid)
    {
        this.boardsid = boardsid;
    }

    public Long getBoardsid()
    {
        return boardsid;
    }
    public void setAgreement(Long agreement)
    {
        this.agreement = agreement;
    }

    public Long getAgreement()
    {
        return agreement;
    }
    public void setVideo(Long video)
    {
        this.video = video;
    }

    public Long getVideo()
    {
        return video;
    }
    public void setScreentype(Long screentype)
    {
        this.screentype = screentype;
    }

    public Long getScreentype()
    {
        return screentype;
    }
    public void setVolume(Long volume)
    {
        this.volume = volume;
    }

    public Long getVolume()
    {
        return volume;
    }
    public void setCode(String code)
    {
        this.code = code;
    }

    public String getCode()
    {
        return code;
    }
    public void setDeleted(Long deleted)
    {
        this.deleted = deleted;
    }

    public Long getDeleted()
    {
        return deleted;
    }
    public void setAppip(String appip)
    {
        this.appip = appip;
    }

    public String getAppip()
    {
        return appip;
    }
    public void setAdedit(String adedit)
    {
        this.adedit = adedit;
    }

    public String getAdedit()
    {
        return adedit;
    }
    public void setAd(String ad)
    {
        this.ad = ad;
    }

    public String getAd()
    {
        return ad;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
                .append("entifyid", getEntifyid())
                .append("status", getStatus())
                .append("chlid", getChlid())
                .append("entifyname", getEntifyname())
                .append("entifyip", getEntifyip())
                .append("eqnumber", getEqnumber())
                .append("correlation", getCorrelation())
                .append("mors", getMors())
                .append("oorg", getOorg())
                .append("log", getLog())
                .append("venue", getVenue())
                .append("ipaddr", getIpaddr())
                .append("cameraid", getCameraid())
                .append("boardsid", getBoardsid())
                .append("agreement", getAgreement())
                .append("video", getVideo())
                .append("screentype", getScreentype())
                .append("volume", getVolume())
                .append("remark", getRemark())
                .append("code", getCode())
                .append("deleted", getDeleted())
                .append("createTime", getCreateTime())
                .append("updateTime", getUpdateTime())
                .append("appip", getAppip())
                .append("adedit", getAdedit())
                .append("ad", getAd())
                .toString();
    }
}
