import request from '@/utils/request'

// 查询车辆信息列表
export function listCars_info(query) {
  return request({
    url: '/sys-cars-info/cars_info/list',
    method: 'get',
    params: query
  })
}

// 查询车辆信息详细
export function getCars_info(cmid) {
  return request({
    url: '/sys-cars-info/cars_info/' + cmid,
    method: 'get'
  })
}

// 新增车辆信息
export function addCars_info(data) {
  return request({
    url: '/sys-cars-info/cars_info',
    method: 'post',
    data: data
  })
}

// 修改车辆信息
export function updateCars_info(data) {
  return request({
    url: '/sys-cars-info/cars_info',
    method: 'put',
    data: data
  })
}

// 删除车辆信息
export function delCars_info(cmid) {
  return request({
    url: '/sys-cars-info/cars_info/' + cmid,
    method: 'delete'
  })
}
