package com.ruoyi.system.api;

import com.ruoyi.auth.form.RegisterBody;
import com.ruoyi.common.core.domain.R;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

@Component
@FeignClient(value = "ruoyi-auth")
public interface AuthUserApi {

    @PostMapping("register")
    public R<?> register(@RequestBody RegisterBody registerBody);

}
