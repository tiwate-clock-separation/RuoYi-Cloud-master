package com.ruoyi.controller;

import java.util.ArrayList;
import java.util.List;
import java.io.IOException;
import javax.servlet.http.HttpServletResponse;

import com.ruoyi.domain.CarsHome;
import com.ruoyi.domain.PropertyManagement;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.log.annotation.Log;
import com.ruoyi.common.log.enums.BusinessType;
import com.ruoyi.common.security.annotation.RequiresPermissions;
import com.ruoyi.domain.CarsHomeLane;
import com.ruoyi.service.ICarsHomeLaneService;
import com.ruoyi.common.core.web.controller.BaseController;
import com.ruoyi.common.core.web.domain.AjaxResult;
import com.ruoyi.common.core.utils.poi.ExcelUtil;
import com.ruoyi.common.core.web.page.TableDataInfo;

/**
 * 车道配置Controller
 * 
 * @author chn
 * @date 2023-12-05
 */
@RestController
@RequestMapping("/lane")
public class CarsHomeLaneController extends BaseController
{
    @Autowired
    private ICarsHomeLaneService carsHomeLaneService;

    /**
     * 查询车道配置列表
     */
    @RequiresPermissions("sys-cars-home:lane:list")
    @GetMapping("/list")
    public TableDataInfo list(CarsHomeLane carsHomeLane, CarsHome carsHome, PropertyManagement propertyManagement)
    {


        startPage();
        carsHomeLane.setCarsHome(carsHome);
        carsHomeLane.setPropertyManagement(propertyManagement);

        List<CarsHomeLane> list = carsHomeLaneService.selectCarsHomeLaneList(carsHomeLane);
        return getDataTable(list);
    }

    /**
     * 导出车道配置列表
     */
    @RequiresPermissions("sys-cars-home:lane:export")
    @Log(title = "车道配置", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, CarsHomeLane carsHomeLane)
    {
        List<CarsHomeLane> list = carsHomeLaneService.selectCarsHomeLaneList(carsHomeLane);
        ExcelUtil<CarsHomeLane> util = new ExcelUtil<CarsHomeLane>(CarsHomeLane.class);
        util.exportExcel(response, list, "车道配置数据");
    }

    /**
     * 获取车道配置详细信息
     */
    @RequiresPermissions("sys-cars-home:lane:query")
    @GetMapping(value = "/{chlid}")
    public AjaxResult getInfo(@PathVariable("chlid") Long chlid)
    {
        return success(carsHomeLaneService.selectCarsHomeLaneByChlid(chlid));
    }

    /**
     * 新增车道配置
     */
    @RequiresPermissions("sys-cars-home:lane:add")
    @Log(title = "车道配置", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody CarsHomeLane carsHomeLane)
    {
        return toAjax(carsHomeLaneService.insertCarsHomeLane(carsHomeLane));
    }

    /**
     * 修改车道配置
     */
    @RequiresPermissions("sys-cars-home:lane:edit")
    @Log(title = "车道配置", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody CarsHomeLane carsHomeLane)
    {
        return toAjax(carsHomeLaneService.updateCarsHomeLane(carsHomeLane));
    }

    /**
     * 删除车道配置
     */
    @RequiresPermissions("sys-cars-home:lane:remove")
    @Log(title = "车道配置", businessType = BusinessType.DELETE)
	@DeleteMapping("/{chlids}")
    public AjaxResult remove(@PathVariable Long[] chlids)
    {
        return toAjax(carsHomeLaneService.deleteCarsHomeLaneByChlids(chlids));
    }
}
