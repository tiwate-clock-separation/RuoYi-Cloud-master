import request from '@/utils/request'

// 查询物业管理列表
export function listProperty(query) {
  return request({
    url: '/sys-property/property/list',
    method: 'get',
    params: query
  })
}

// 查询物业管理详细
export function getProperty(pmid) {
  return request({
    url: '/sys-property/property/' + pmid,
    method: 'get'
  })
}

// 新增物业管理
export function addProperty(data) {
  return request({
    url: '/sys-property/property',
    method: 'post',
    data: data
  })
}

// 修改物业管理
export function updateProperty(data) {
  return request({
    url: '/sys-property/property',
    method: 'put',
    data: data
  })
}

// 删除物业管理
export function delProperty(pmid) {
  return request({
    url: '/sys-property/property/' + pmid,
    method: 'delete'
  })
}

// 停车计费支付
export function stopCars(data) {
  return request({
    url: '/sys-merchant/stop/cars',
    method: 'post',
    data: data
  })
}
