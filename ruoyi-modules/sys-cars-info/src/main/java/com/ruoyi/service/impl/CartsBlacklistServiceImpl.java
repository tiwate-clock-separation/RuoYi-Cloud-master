package com.ruoyi.service.impl;

import java.util.List;
import com.ruoyi.common.core.utils.DateUtils;
import com.ruoyi.domain.CartsBlacklist;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.mapper.CartsBlacklistMapper;
import com.ruoyi.service.ICartsBlacklistService;

/**
 * 车辆黑名单Service业务层处理
 *
 * @author ruoyi
 * @date 2023-12-13
 */
@Service
public class CartsBlacklistServiceImpl implements ICartsBlacklistService
{
    @Autowired
    private CartsBlacklistMapper cartsBlacklistMapper;

    /**
     * 查询车辆黑名单
     *
     * @param cbid 车辆黑名单主键
     * @return 车辆黑名单
     */
    @Override
    public CartsBlacklist selectCartsBlacklistByCbid(Long cbid)
    {
        return cartsBlacklistMapper.selectCartsBlacklistByCbid(cbid);
    }

    /**
     * 查询车辆黑名单列表
     *
     * @param cartsBlacklist 车辆黑名单
     * @return 车辆黑名单
     */
    @Override
    public List<CartsBlacklist> selectCartsBlacklistList(CartsBlacklist cartsBlacklist)
    {
        return cartsBlacklistMapper.selectCartsBlacklistList(cartsBlacklist);
    }

    /**
     * 新增车辆黑名单
     *
     * @param cartsBlacklist 车辆黑名单
     * @return 结果
     */
    @Override
    public int insertCartsBlacklist(CartsBlacklist cartsBlacklist)
    {
        cartsBlacklist.setCreateTime(DateUtils.getNowDate());
        return cartsBlacklistMapper.insertCartsBlacklist(cartsBlacklist);
    }

    /**
     * 修改车辆黑名单
     *
     * @param cartsBlacklist 车辆黑名单
     * @return 结果
     */
    @Override
    public int updateCartsBlacklist(CartsBlacklist cartsBlacklist)
    {
        cartsBlacklist.setUpdateTime(DateUtils.getNowDate());
        return cartsBlacklistMapper.updateCartsBlacklist(cartsBlacklist);
    }

    /**
     * 批量删除车辆黑名单
     *
     * @param cbids 需要删除的车辆黑名单主键
     * @return 结果
     */
    @Override
    public int deleteCartsBlacklistByCbids(Long[] cbids)
    {
        return cartsBlacklistMapper.deleteCartsBlacklistByCbids(cbids);
    }

    /**
     * 删除车辆黑名单信息
     *
     * @param cbid 车辆黑名单主键
     * @return 结果
     */
    @Override
    public int deleteCartsBlacklistByCbid(Long cbid)
    {
        return cartsBlacklistMapper.deleteCartsBlacklistByCbid(cbid);
    }
}
