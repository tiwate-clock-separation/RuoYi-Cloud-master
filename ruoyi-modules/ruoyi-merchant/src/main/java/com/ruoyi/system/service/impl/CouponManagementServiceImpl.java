package com.ruoyi.system.service.impl;

import java.util.List;
import com.ruoyi.common.core.utils.DateUtils;
import com.ruoyi.system.mapper.CouponManagementMapper;
import com.ruoyi.system.service.ICouponManagementService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.domain.CouponManagement;

/**
 * 优惠卷管理Service业务层处理
 * 
 * @author lbw
 * @date 2023-12-07
 */
@Service
public class CouponManagementServiceImpl implements ICouponManagementService
{
    @Autowired
    private CouponManagementMapper couponManagementMapper;

    /**
     * 查询优惠卷管理
     * 
     * @param cmid 优惠卷管理主键
     * @return 优惠卷管理
     */
    @Override
    public CouponManagement selectCouponManagementByCmid(Long cmid)
    {
        return couponManagementMapper.selectCouponManagementByCmid(cmid);
    }

    /**
     * 查询优惠卷管理列表
     * 
     * @param couponManagement 优惠卷管理
     * @return 优惠卷管理
     */
    @Override
    public List<CouponManagement> selectCouponManagementList(CouponManagement couponManagement)
    {
        return couponManagementMapper.selectCouponManagementList(couponManagement);
    }

    /**
     * 新增优惠卷管理
     * 
     * @param couponManagement 优惠卷管理
     * @return 结果
     */
    @Override
    public int insertCouponManagement(CouponManagement couponManagement)
    {
        couponManagement.setCreateTime(DateUtils.getNowDate());
        return couponManagementMapper.insertCouponManagement(couponManagement);
    }

    /**
     * 修改优惠卷管理
     * 
     * @param couponManagement 优惠卷管理
     * @return 结果
     */
    @Override
    public int updateCouponManagement(CouponManagement couponManagement)
    {
        couponManagement.setUpdateTime(DateUtils.getNowDate());
        return couponManagementMapper.updateCouponManagement(couponManagement);
    }

    /**
     * 批量删除优惠卷管理
     * 
     * @param cmids 需要删除的优惠卷管理主键
     * @return 结果
     */
    @Override
    public int deleteCouponManagementByCmids(Long[] cmids)
    {
        return couponManagementMapper.deleteCouponManagementByCmids(cmids);
    }

    /**
     * 删除优惠卷管理信息
     * 
     * @param cmid 优惠卷管理主键
     * @return 结果
     */
    @Override
    public int deleteCouponManagementByCmid(Long cmid)
    {
        return couponManagementMapper.deleteCouponManagementByCmid(cmid);
    }
}
