import request from '@/utils/request'

// 查询物业流水列表
export function listPropertyWater(query) {
  return request({
    url: '/sys-property/propertyWater/list',
    method: 'get',
    params: query
  })
}

// 查询物业流水详细
export function getPropertyWater(pwid) {
  return request({
    url: '/sys-property/propertyWater/' + pwid,
    method: 'get'
  })
}

// 新增物业流水
export function addPropertyWater(data) {
  return request({
    url: '/sys-property/propertyWater',
    method: 'post',
    data: data
  })
}

// 修改物业流水
export function updatePropertyWater(data) {
  return request({
    url: '/sys-property/propertyWater',
    method: 'put',
    data: data
  })
}

// 删除物业流水
export function delPropertyWater(pwid) {
  return request({
    url: '/sys-property/propertyWater/' + pwid,
    method: 'delete'
  })
}

// 获取物业集合
export function getPropertyApi(query) {
  return request({
    url: '/sys-merchant/management/propertylist',
    method: 'get',
    params: query
  })
}

// 获取费用类型集合
export function getttApiList(query) {
  return request({
    url: '/sys-property/propertyApi/ttlist',
    method: 'get',
    params: query
  })
}

// 获取支付类型集合
export function getptApiList(query) {
  return request({
    url: '/sys-property/propertyApi/ptlist',
    method: 'get',
    params: query
  })
}

