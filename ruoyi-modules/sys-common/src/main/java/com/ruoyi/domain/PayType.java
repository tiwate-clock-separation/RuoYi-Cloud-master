package com.ruoyi.domain;

import com.ruoyi.common.core.annotation.Excel;

/**
 * 支付类型  微信
 */
public class PayType {
    @Excel(name = "支付编号")
    private Long ptid;
    @Excel(name = "支付类型")
    private String typeName;

    public Long getPtid() {
        return ptid;
    }

    public void setPtid(Long ptid) {
        this.ptid = ptid;
    }

    public String getTypeName() {
        return typeName;
    }

    public void setTypeName(String typeName) {
        this.typeName = typeName;
    }

    @Override
    public String toString() {
        return "PayType{" +
                "Ptid=" + ptid +
                ", TypeName='" + typeName + '\'' +
                '}';
    }
}
