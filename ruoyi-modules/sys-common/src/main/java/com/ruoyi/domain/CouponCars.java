package com.ruoyi.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.core.annotation.Excel;
import com.ruoyi.common.core.web.domain.BaseEntity;

/**
 * 优惠卷车辆连对象 coupon_cars
 *
 * @author ruoyi
 * @date 2023-12-15
 */
public class CouponCars extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 序号 */
    private Long ccid;

    /** 唯一编码,前台使用 */
    private String code;

    /** 使用状态 */
    @Excel(name = "使用状态")
    private Long status;

    /** 是否删除 */
    private Long deleted;

    /** 优惠券id */
    @Excel(name = "优惠券id")
    private Long coid;

    /** 车牌号 */
    @Excel(name = "车牌号")
    private String licenseNumber;

    @Excel(name = "优惠券名称")
    private String couponName;

    @Excel(name = "优惠券金额")
    private String couponPrice;

    @Excel(name = "商家名称")
    private String merchantName;

    @Excel(name = "车场名称")
    private String chname;

    private Integer mmid;
    private Integer chid;

    public void setCcid(Long ccid)
    {
        this.ccid = ccid;
    }

    public Long getCcid()
    {
        return ccid;
    }
    public void setCode(String code)
    {
        this.code = code;
    }

    public String getCode()
    {
        return code;
    }
    public void setStatus(Long status)
    {
        this.status = status;
    }

    public Long getStatus()
    {
        return status;
    }
    public void setDeleted(Long deleted)
    {
        this.deleted = deleted;
    }

    public Long getDeleted()
    {
        return deleted;
    }
    public void setCoid(Long coid)
    {
        this.coid = coid;
    }

    public Long getCoid()
    {
        return coid;
    }
    public void setLicenseNumber(String licenseNumber)
    {
        this.licenseNumber = licenseNumber;
    }

    public String getLicenseNumber()
    {
        return licenseNumber;
    }


    public String getCouponName() {
        return couponName;
    }

    public void setCouponName(String couponName) {
        this.couponName = couponName;
    }

    public String getCouponPrice() {
        return couponPrice;
    }

    public void setCouponPrice(String couponPrice) {
        this.couponPrice = couponPrice;
    }

    public String getMerchantName() {
        return merchantName;
    }

    public void setMerchantName(String merchantName) {
        this.merchantName = merchantName;
    }

    public String getChname() {
        return chname;
    }

    public void setChname(String chname) {
        this.chname = chname;
    }


    public Integer getMmid() {
        return mmid;
    }

    public void setMmid(Integer mmid) {
        this.mmid = mmid;
    }

    public Integer getChid() {
        return chid;
    }

    public void setChid(Integer chid) {
        this.chid = chid;
    }

    @Override
    public String toString() {
        return "CouponCars{" +
                "ccid=" + ccid +
                ", code='" + code + '\'' +
                ", status=" + status +
                ", deleted=" + deleted +
                ", coid=" + coid +
                ", licenseNumber='" + licenseNumber + '\'' +
                ", couponName='" + couponName + '\'' +
                ", couponPrice='" + couponPrice + '\'' +
                ", merchantName='" + merchantName + '\'' +
                ", chname='" + chname + '\'' +
                '}';
    }
}
