package com.ruoyi.audit.service.impl;

import java.util.List;
import com.ruoyi.common.core.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.audit.mapper.CarsAduitMapper;
import com.ruoyi.domain.CarsAduit;
import com.ruoyi.audit.service.ICarsAduitService;

/**
 * 业主车辆审核Service业务层处理
 *
 * @author ruoyi
 * @date 2023-12-03
 */
@Service
public class CarsAduitServiceImpl implements ICarsAduitService
{
    @Autowired
    private CarsAduitMapper carsAduitMapper;

    /**
     * 查询业主车辆审核
     *
     * @param caid 业主车辆审核主键
     * @return 业主车辆审核
     */
    @Override
    public CarsAduit selectCarsAduitByCaid(Long caid)
    {
        return carsAduitMapper.selectCarsAduitByCaid(caid);
    }

    /**
     * 查询业主车辆审核列表
     *
     * @param carsAduit 业主车辆审核
     * @return 业主车辆审核
     */
    @Override
    public List<CarsAduit> selectCarsAduitList(CarsAduit carsAduit)
    {
        return carsAduitMapper.selectCarsAduitList(carsAduit);
    }

    /**
     * 新增业主车辆审核
     *
     * @param carsAduit 业主车辆审核
     * @return 结果
     */
    @Override
    public int insertCarsAduit(CarsAduit carsAduit)
    {
        carsAduit.setCreateTime(DateUtils.getNowDate());
        return carsAduitMapper.insertCarsAduit(carsAduit);
    }

    /**
     * 修改业主车辆审核
     *
     * @param carsAduit 业主车辆审核
     * @return 结果
     */
    @Override
    public int updateCarsAduit(CarsAduit carsAduit)
    {
        carsAduit.setUpdateTime(DateUtils.getNowDate());
        return carsAduitMapper.updateCarsAduit(carsAduit);
    }

    /**
     * 批量删除业主车辆审核
     *
     * @param caids 需要删除的业主车辆审核主键
     * @return 结果
     */
    @Override
    public int deleteCarsAduitByCaids(Long[] caids)
    {
        return carsAduitMapper.deleteCarsAduitByCaids(caids);
    }

    /**
     * 删除业主车辆审核信息
     *
     * @param caid 业主车辆审核主键
     * @return 结果
     */
    @Override
    public int deleteCarsAduitByCaid(Long caid)
    {
        return carsAduitMapper.deleteCarsAduitByCaid(caid);
    }
}
