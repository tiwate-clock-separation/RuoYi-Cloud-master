package com.ruoyi.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.core.annotation.Excel;
import com.ruoyi.common.core.web.domain.BaseEntity;

/**
 * 设备记录对象 operation_record
 *
 * @author ruoyi
 * @date 2023-12-04
 */
public class OperationRecord extends BaseEntity {
    private static final long serialVersionUID = 1L;

    /**
     * 自增ID
     */
    private Long orid;

    /**
     * 唯一编码,前台使用
     */
    @Excel(name = "唯一编码,前台使用")
    private String code;

    /**
     * 状态
     */
    @Excel(name = "状态")
    private Long status;

    /**
     * 是否删除
     */
    @Excel(name = "是否删除")
    private Long deleted;

    /**
     * 车辆类型
     */
    @Excel(name = "车辆类型")
    private Long ctid;

    /**
     * 备注
     */
    @Excel(name = "备注")
    private String operationRemark;

    /**
     * 小区id
     */
    @Excel(name = "小区id")
    private Long pmid;

    /**
     * 停车场id
     */
    @Excel(name = "停车场id")
    private Long hpid;
    @Excel(name = "车道id")
    private Long chlid;
    @Excel(name = "设备ip")
    private String sheip;
    @Excel(name = "类型")
    private String opertype;
    @Excel(name = "设备状态")
    private String operstatus;

    private CarsHome carsHome;

    private PropertyManagement propertyManagement;

    private CarsHomeLane carsHomeLane;

    public CarsHomeLane getCarsHomeLane() {
        return carsHomeLane;
    }

    public void setCarsHomeLane(CarsHomeLane carsHomeLane) {
        this.carsHomeLane = carsHomeLane;
    }

    public Long getChlid() {
        return chlid;
    }

    public void setChlid(Long chlid) {
        this.chlid = chlid;
    }

    public String getSheip() {
        return sheip;
    }

    public void setSheip(String sheip) {
        this.sheip = sheip;
    }

    public String getOpertype() {
        return opertype;
    }

    public void setOpertype(String opertype) {
        this.opertype = opertype;
    }

    public String getOperstatus() {
        return operstatus;
    }

    public void setOperstatus(String operstatus) {
        this.operstatus = operstatus;
    }

    public CarsHome getCarsHome() {
        return carsHome;
    }

    public void setCarsHome(CarsHome carsHome) {
        this.carsHome = carsHome;
    }

    public PropertyManagement getPropertyManagement() {
        return propertyManagement;
    }

    public void setPropertyManagement(PropertyManagement propertyManagement) {
        this.propertyManagement = propertyManagement;
    }

    public void setOrid(Long orid) {
        this.orid = orid;
    }

    public Long getOrid() {
        return orid;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getCode() {
        return code;
    }

    public void setStatus(Long status) {
        this.status = status;
    }

    public Long getStatus() {
        return status;
    }

    public void setDeleted(Long deleted) {
        this.deleted = deleted;
    }

    public Long getDeleted() {
        return deleted;
    }

    public void setCtid(Long ctid) {
        this.ctid = ctid;
    }

    public Long getCtid() {
        return ctid;
    }

    public void setOperationRemark(String operationRemark) {
        this.operationRemark = operationRemark;
    }

    public String getOperationRemark() {
        return operationRemark;
    }

    public Long getPmid() {
        return pmid;
    }

    public void setPmid(Long pmid) {
        this.pmid = pmid;
    }

    public void setHpid(Long hpid) {
        this.hpid = hpid;
    }

    public Long getHpid() {
        return hpid;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this, ToStringStyle.MULTI_LINE_STYLE)
                .append("orid", getOrid())
                .append("code", getCode())
                .append("status", getStatus())
                .append("deleted", getDeleted())
                .append("createTime", getCreateTime())
                .append("updateTime", getUpdateTime())
                .append("ctid", getCtid())
                .append("operationRemark", getOperationRemark())
                .append("ohid", getPmid())
                .append("hpid", getHpid())
                .toString();
    }
}
