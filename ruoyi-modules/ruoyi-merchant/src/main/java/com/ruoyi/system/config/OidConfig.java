package com.ruoyi.system.config;

import com.ruoyi.domain.util.SnowFlake;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class OidConfig {

    @Bean
    public SnowFlake snowFlake(){
        return new SnowFlake(0, 0);
    }
}
