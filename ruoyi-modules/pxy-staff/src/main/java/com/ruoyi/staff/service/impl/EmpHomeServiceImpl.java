package com.ruoyi.staff.service.impl;

import java.util.List;
import com.ruoyi.common.core.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.staff.mapper.EmpHomeMapper;
import com.ruoyi.domain.EmpHome;
import com.ruoyi.staff.service.IEmpHomeService;

/**
 * 物业人员Service业务层处理
 * 
 * @author ruoyi
 * @date 2023-12-23
 */
@Service
public class EmpHomeServiceImpl implements IEmpHomeService 
{
    @Autowired
    private EmpHomeMapper empHomeMapper;

    /**
     * 查询物业人员
     * 
     * @param ehid 物业人员主键
     * @return 物业人员
     */
    @Override
    public EmpHome selectEmpHomeByEhid(Long ehid)
    {
        return empHomeMapper.selectEmpHomeByEhid(ehid);
    }

    /**
     * 查询物业人员列表
     * 
     * @param empHome 物业人员
     * @return 物业人员
     */
    @Override
    public List<EmpHome> selectEmpHomeList(EmpHome empHome)
    {
        return empHomeMapper.selectEmpHomeList(empHome);
    }

    /**
     * 新增物业人员
     * 
     * @param empHome 物业人员
     * @return 结果
     */
    @Override
    public int insertEmpHome(EmpHome empHome)
    {
        empHome.setCreateTime(DateUtils.getNowDate());
        return empHomeMapper.insertEmpHome(empHome);
    }

    /**
     * 修改物业人员
     * 
     * @param empHome 物业人员
     * @return 结果
     */
    @Override
    public int updateEmpHome(EmpHome empHome)
    {
        empHome.setUpdateTime(DateUtils.getNowDate());
        return empHomeMapper.updateEmpHome(empHome);
    }

    /**
     * 批量删除物业人员
     * 
     * @param ehids 需要删除的物业人员主键
     * @return 结果
     */
    @Override
    public int deleteEmpHomeByEhids(Long[] ehids)
    {
        return empHomeMapper.deleteEmpHomeByEhids(ehids);
    }

    /**
     * 删除物业人员信息
     * 
     * @param ehid 物业人员主键
     * @return 结果
     */
    @Override
    public int deleteEmpHomeByEhid(Long ehid)
    {
        return empHomeMapper.deleteEmpHomeByEhid(ehid);
    }
}
