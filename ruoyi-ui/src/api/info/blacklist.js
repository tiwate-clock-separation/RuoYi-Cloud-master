import request from '@/utils/request'

// 查询车辆黑名单列表
export function listBlacklist(query) {
  return request({
    url: '/sys-cars-info/blacklist/list',
    method: 'get',
    params: query
  })
}

// 查询车辆黑名单详细
export function getBlacklist(cbid) {
  return request({
    url: '/sys-cars-info/blacklist/' + cbid,
    method: 'get'
  })
}

// 新增车辆黑名单
export function addBlacklist(data) {
  return request({
    url: '/sys-cars-info/blacklist',
    method: 'post',
    data: data
  })
}

// 修改车辆黑名单
export function updateBlacklist(data) {
  return request({
    url: '/sys-cars-info/blacklist',
    method: 'put',
    data: data
  })
}

// 删除车辆黑名单
export function delBlacklist(cbid) {
  return request({
    url: '/sys-cars-info/blacklist/' + cbid,
    method: 'delete'
  })
}
