package com.ruoyi.system.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.core.annotation.Excel;
import com.ruoyi.common.core.web.domain.BaseEntity;

/**
 * 设备操作记录对象 operation_record
 * 
 * @author ruoyi
 * @date 2023-11-29
 */
public class OperationRecord extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 自增ID */
    private Long orid;

    /** 唯一编码,前台使用 */
    @Excel(name = "唯一编码,前台使用")
    private String code;

    /** 状态 */
    @Excel(name = "状态")
    private Long status;

    /** 是否删除 */
    @Excel(name = "是否删除")
    private Long deleted;

    /** 车辆类型 */
    @Excel(name = "车辆类型")
    private Long ctid;

    /** 备注 */
    @Excel(name = "备注")
    private String operationRemark;

    /** 小区id */
    @Excel(name = "小区id")
    private Long pmid;

    /** 停车场id */
    @Excel(name = "停车场id")
    private Long hpid;

    public void setOrid(Long orid) 
    {
        this.orid = orid;
    }

    public Long getOrid() 
    {
        return orid;
    }
    public void setCode(String code) 
    {
        this.code = code;
    }

    public String getCode() 
    {
        return code;
    }
    public void setStatus(Long status) 
    {
        this.status = status;
    }

    public Long getStatus() 
    {
        return status;
    }
    public void setDeleted(Long deleted) 
    {
        this.deleted = deleted;
    }

    public Long getDeleted() 
    {
        return deleted;
    }
    public void setCtid(Long ctid) 
    {
        this.ctid = ctid;
    }

    public Long getCtid() 
    {
        return ctid;
    }
    public void setOperationRemark(String operationRemark) 
    {
        this.operationRemark = operationRemark;
    }

    public String getOperationRemark() 
    {
        return operationRemark;
    }

    public Long getPmid() {
        return pmid;
    }

    public void setPmid(Long pmid) {
        this.pmid = pmid;
    }

    public void setHpid(Long hpid)
    {
        this.hpid = hpid;
    }

    public Long getHpid() 
    {
        return hpid;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("orid", getOrid())
            .append("code", getCode())
            .append("status", getStatus())
            .append("deleted", getDeleted())
            .append("createTime", getCreateTime())
            .append("updateTime", getUpdateTime())
            .append("ctid", getCtid())
            .append("operationRemark", getOperationRemark())
            .append("pmid", getPmid())
            .append("hpid", getHpid())
            .toString();
    }
}
