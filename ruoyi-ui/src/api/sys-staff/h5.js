import request from '@/utils/request'

// 查询H5人员列表
export function listH5(query) {
  return request({
    url: '/sys-staff/h5/list',
    method: 'get',
    params: query
  })
}

// 查询H5人员详细
export function getH5(ehid) {
  return request({
    url: '/sys-staff/h5/' + ehid,
    method: 'get'
  })
}

// 新增H5人员
export function addH5(data) {
  return request({
    url: '/sys-staff/h5',
    method: 'post',
    data: data
  })
}

// 修改H5人员
export function updateH5(data) {
  return request({
    url: '/sys-staff/h5',
    method: 'put',
    data: data
  })
}

// 删除H5人员
export function delH5(ehid) {
  return request({
    url: '/sys-staff/h5/' + ehid,
    method: 'delete'
  })
}
