package com.ruoyi.system.mapper.result;

import com.ruoyi.domain.CarsMsg;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Select;

public interface CarsMsgResult {
    @Select(
            "select cmid,license_number,cars_tel from cars_msg where cmid = #{cmid}"
    )
    @Results({
            @Result(property = "licenseNumber",column = "license_number"),
            @Result(property = "carsTel",column = "cars_tel")
    }
    )
    public CarsMsg selectCarsMsgByCmid(Integer cmid);
}
