import request from '@/utils/request'

// 查询商家管理列表
export function listManagement(query) {
  return request({
    url: '/sys-merchant/management/list',
    method: 'get',
    params: query
  })
}

// 查询商家管理详细
export function getManagement(pmid) {
  return request({
    url: '/sys-merchant/management/' + pmid,
    method: 'get'
  })
}

// 新增商家管理
export function addManagement(data) {
  return request({
    url: '/sys-merchant/management',
    method: 'post',
    data: data
  })
}

// 修改商家管理
export function updateManagement(data) {
  return request({
    url: '/sys-merchant/management',
    method: 'put',
    data: data
  })
}

// 商家充值
export function topup(pmid,topupNum) {
  return request({
    url: '/sys-merchant/management/topup/'+pmid+"/"+topupNum,
    method: 'put',
  })
}

// 删除商家管理
export function delManagement(pmid) {
  return request({
    url: '/sys-merchant/management/' + pmid,
    method: 'delete'
  })
}

// 获取车场集合
export function getCarsHomeApi(query) {
  return request({
    url: '/sys-cars-home/home/list',
    method: 'get',
    params: query
  })
}

// 获取物业集合
export function getPropertyApi(query) {
  return request({
    url: '/sys-merchant/management/propertylist',
    method: 'get',
    params: query
  })
}
