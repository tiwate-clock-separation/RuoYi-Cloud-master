package com.ruoyi.system.service.impl;

import java.util.List;
import com.ruoyi.common.core.utils.DateUtils;
import com.ruoyi.domain.CarsMeal;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.CarsMealMapper;
import com.ruoyi.system.service.ICarsMealService;

/**
 * 车辆套餐Service业务层处理
 * 
 * @author ruoyi
 * @date 2023-12-13
 */
@Service
public class CarsMealServiceImpl implements ICarsMealService 
{
    @Autowired
    private CarsMealMapper carsMealMapper;

    /**
     * 查询车辆套餐
     * 
     * @param cmid 车辆套餐主键
     * @return 车辆套餐
     */
    @Override
    public CarsMeal selectCarsMealByCmid(Long cmid)
    {
        return carsMealMapper.selectCarsMealByCmid(cmid);
    }

    /**
     * 查询车辆套餐列表
     * 
     * @param carsMeal 车辆套餐
     * @return 车辆套餐
     */
    @Override
    public List<CarsMeal> selectCarsMealList(CarsMeal carsMeal)
    {
        return carsMealMapper.selectCarsMealList(carsMeal);
    }

    /**
     * 新增车辆套餐
     * 
     * @param carsMeal 车辆套餐
     * @return 结果
     */
    @Override
    public int insertCarsMeal(CarsMeal carsMeal)
    {
        carsMeal.setCreateTime(DateUtils.getNowDate());
        return carsMealMapper.insertCarsMeal(carsMeal);
    }

    /**
     * 修改车辆套餐
     * 
     * @param carsMeal 车辆套餐
     * @return 结果
     */
    @Override
    public int updateCarsMeal(CarsMeal carsMeal)
    {
        carsMeal.setUpdateTime(DateUtils.getNowDate());
        return carsMealMapper.updateCarsMeal(carsMeal);
    }

    /**
     * 批量删除车辆套餐
     * 
     * @param cmids 需要删除的车辆套餐主键
     * @return 结果
     */
    @Override
    public int deleteCarsMealByCmids(Long[] cmids)
    {
        return carsMealMapper.deleteCarsMealByCmids(cmids);
    }

    /**
     * 删除车辆套餐信息
     * 
     * @param cmid 车辆套餐主键
     * @return 结果
     */
    @Override
    public int deleteCarsMealByCmid(Long cmid)
    {
        return carsMealMapper.deleteCarsMealByCmid(cmid);
    }
}
