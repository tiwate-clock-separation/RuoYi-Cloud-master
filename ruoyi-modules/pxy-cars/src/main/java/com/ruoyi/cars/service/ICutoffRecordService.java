package com.ruoyi.cars.service;

import com.ruoyi.domain.CutoffRecord;

import java.util.List;

/**
 * 开闸记录Service接口
 *
 * @author ruoyi
 * @date 2023-11-29
 */
public interface ICutoffRecordService
{
    /**
     * 查询开闸记录
     *
     * @param corid 开闸记录主键
     * @return 开闸记录
     */
    public CutoffRecord selectCutoffRecordByCorid(Long corid);

    /**
     * 查询开闸记录列表
     *
     * @param cutoffRecord 开闸记录
     * @return 开闸记录集合
     */
    public List<CutoffRecord> selectCutoffRecordList(CutoffRecord cutoffRecord);

    /**
     * 新增开闸记录
     *
     * @param cutoffRecord 开闸记录
     * @return 结果
     */
    public int insertCutoffRecord(CutoffRecord cutoffRecord);

    /**
     * 修改开闸记录
     *
     * @param cutoffRecord 开闸记录
     * @return 结果
     */
    public int updateCutoffRecord(CutoffRecord cutoffRecord);

    /**
     * 批量删除开闸记录
     *
     * @param corids 需要删除的开闸记录主键集合
     * @return 结果
     */
    public int deleteCutoffRecordByCorids(Long[] corids);

    /**
     * 删除开闸记录信息
     *
     * @param corid 开闸记录主键
     * @return 结果
     */
    public int deleteCutoffRecordByCorid(Long corid);
}
