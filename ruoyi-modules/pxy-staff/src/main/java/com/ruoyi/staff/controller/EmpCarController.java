package com.ruoyi.staff.controller;

import java.util.List;
import java.io.IOException;
import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.log.annotation.Log;
import com.ruoyi.common.log.enums.BusinessType;
import com.ruoyi.common.security.annotation.RequiresPermissions;
import com.ruoyi.domain.EmpCar;
import com.ruoyi.staff.service.IEmpCarService;
import com.ruoyi.common.core.web.controller.BaseController;
import com.ruoyi.common.core.web.domain.AjaxResult;
import com.ruoyi.common.core.utils.poi.ExcelUtil;
import com.ruoyi.common.core.web.page.TableDataInfo;

/**
 * 车场人员Controller
 * 
 * @author pxy
 * @date 2023-12-23
 */
@RestController
@RequestMapping("/car")
public class EmpCarController extends BaseController
{
    @Autowired
    private IEmpCarService empCarService;

    /**
     * 查询车场人员列表
     */
    @RequiresPermissions("pxy-staff:car:list")
    @GetMapping("/list")
    public TableDataInfo list(EmpCar empCar)
    {
        startPage();
        List<EmpCar> list = empCarService.selectEmpCarList(empCar);
        return getDataTable(list);
    }

    /**
     * 导出车场人员列表
     */
    @RequiresPermissions("pxy-staff:car:export")
    @Log(title = "车场人员", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, EmpCar empCar)
    {
        List<EmpCar> list = empCarService.selectEmpCarList(empCar);
        ExcelUtil<EmpCar> util = new ExcelUtil<EmpCar>(EmpCar.class);
        util.exportExcel(response, list, "车场人员数据");
    }

    /**
     * 获取车场人员详细信息
     */
    @RequiresPermissions("pxy-staff:car:query")
    @GetMapping(value = "/{carid}")
    public AjaxResult getInfo(@PathVariable("carid") Long carid)
    {
        return success(empCarService.selectEmpCarByCarid(carid));
    }

    /**
     * 新增车场人员
     */
    @RequiresPermissions("pxy-staff:car:add")
    @Log(title = "车场人员", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody EmpCar empCar)
    {
        return toAjax(empCarService.insertEmpCar(empCar));
    }

    /**
     * 修改车场人员
     */
    @RequiresPermissions("pxy-staff:car:edit")
    @Log(title = "车场人员", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody EmpCar empCar)
    {
        return toAjax(empCarService.updateEmpCar(empCar));
    }

    /**
     * 删除车场人员
     */
    @RequiresPermissions("pxy-staff:car:remove")
    @Log(title = "车场人员", businessType = BusinessType.DELETE)
	@DeleteMapping("/{carids}")
    public AjaxResult remove(@PathVariable Long[] carids)
    {
        return toAjax(empCarService.deleteEmpCarByCarids(carids));
    }
}
