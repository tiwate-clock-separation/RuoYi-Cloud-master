package com.ruoyi.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.core.annotation.Excel;
import com.ruoyi.common.core.web.domain.BaseEntity;

/**
 * 优惠卷管理对象 coupon_management
 * 
 * @author lbw
 * @date 2023-12-07
 */
public class CouponManagement extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 自增ID */
    private Long cmid;

    /** 唯一编码,前台使用 */
    private String code;

    /** 状态 */
    private Long status;

    /** 是否删除 */
    private Long deleted;

    /** 优惠卷名称 */
    @Excel(name = "优惠卷名称")
    private String couponName;

    /** 优惠卷金额 */
    @Excel(name = "优惠卷金额")
    private Long couponPrice;

    /** 优惠卷类型 */
    @Excel(name = "优惠卷类型")
    private String couponType;

    /** 优惠卷发行数量 */
    @Excel(name = "优惠卷发行数量")
    private Long couponAduitNum;

    /** 优惠券库存数量 */
    @Excel(name = "优惠券库存数量")
    private Long couponInventoryNum;

    /** 有效时长 */
    @Excel(name = "有效时长")
    private Long effectiveDuration;

    /** 获取限制 */
    @Excel(name = "获取限制")
    private Long accessRestriction;

    /** 车场id */
    @Excel(name = "车场id")
    private Long chid;

    private CarsHome carsHome;

    /** 商家id */
    @Excel(name = "商家id")
    private Long mmid;

    private MerchantManagement merchantManagement;

    public void setCmid(Long cmid) 
    {
        this.cmid = cmid;
    }

    public Long getCmid() 
    {
        return cmid;
    }
    public void setCode(String code) 
    {
        this.code = code;
    }

    public String getCode() 
    {
        return code;
    }
    public void setStatus(Long status) 
    {
        this.status = status;
    }

    public Long getStatus() 
    {
        return status;
    }
    public void setDeleted(Long deleted) 
    {
        this.deleted = deleted;
    }

    public Long getDeleted() 
    {
        return deleted;
    }
    public void setCouponName(String couponName) 
    {
        this.couponName = couponName;
    }

    public String getCouponName() 
    {
        return couponName;
    }
    public void setCouponPrice(Long couponPrice) 
    {
        this.couponPrice = couponPrice;
    }

    public Long getCouponPrice() 
    {
        return couponPrice;
    }
    public void setCouponType(String couponType) 
    {
        this.couponType = couponType;
    }

    public String getCouponType() 
    {
        return couponType;
    }
    public void setCouponAduitNum(Long couponAduitNum) 
    {
        this.couponAduitNum = couponAduitNum;
    }

    public Long getCouponAduitNum() 
    {
        return couponAduitNum;
    }
    public void setCouponInventoryNum(Long couponInventoryNum) 
    {
        this.couponInventoryNum = couponInventoryNum;
    }

    public Long getCouponInventoryNum() 
    {
        return couponInventoryNum;
    }
    public void setEffectiveDuration(Long effectiveDuration) 
    {
        this.effectiveDuration = effectiveDuration;
    }

    public Long getEffectiveDuration() 
    {
        return effectiveDuration;
    }
    public void setAccessRestriction(Long accessRestriction) 
    {
        this.accessRestriction = accessRestriction;
    }

    public Long getAccessRestriction() 
    {
        return accessRestriction;
    }
    public void setChid(Long chid) 
    {
        this.chid = chid;
    }

    public Long getChid() 
    {
        return chid;
    }
    public void setMmid(Long mmid) 
    {
        this.mmid = mmid;
    }

    public Long getMmid() 
    {
        return mmid;
    }

    public CarsHome getCarsHome() {
        return carsHome;
    }

    public void setCarsHome(CarsHome carsHome) {
        this.carsHome = carsHome;
    }

    public MerchantManagement getMerchantManagement() {
        return merchantManagement;
    }

    public void setMerchantManagement(MerchantManagement merchantManagement) {
        this.merchantManagement = merchantManagement;
    }

    @Override
    public String toString() {
        return "CouponManagement{" +
                "cmid=" + cmid +
                ", code='" + code + '\'' +
                ", status=" + status +
                ", deleted=" + deleted +
                ", couponName='" + couponName + '\'' +
                ", couponPrice=" + couponPrice +
                ", couponType='" + couponType + '\'' +
                ", couponAduitNum=" + couponAduitNum +
                ", couponInventoryNum=" + couponInventoryNum +
                ", effectiveDuration=" + effectiveDuration +
                ", accessRestriction=" + accessRestriction +
                ", chid=" + chid +
                ", carsHome=" + carsHome +
                ", mmid=" + mmid +
                ", merchantManagement=" + merchantManagement +
                '}';
    }
}
