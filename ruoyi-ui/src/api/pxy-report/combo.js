import request from '@/utils/request'

// 查询卡套餐报列表
export function listCombo(query) {
  return request({
    url: '/pxy-report/combo/list',
    method: 'get',
    params: query
  })
}

// 查询卡套餐报详细
export function getCombo(comid) {
  return request({
    url: '/pxy-report/combo/' + comid,
    method: 'get'
  })
}

// 新增卡套餐报
export function addCombo(data) {
  return request({
    url: '/pxy-report/combo',
    method: 'post',
    data: data
  })
}

// 修改卡套餐报
export function updateCombo(data) {
  return request({
    url: '/pxy-report/combo',
    method: 'put',
    data: data
  })
}

// 删除卡套餐报
export function delCombo(comid) {
  return request({
    url: '/pxy-report/combo/' + comid,
    method: 'delete'
  })
}
