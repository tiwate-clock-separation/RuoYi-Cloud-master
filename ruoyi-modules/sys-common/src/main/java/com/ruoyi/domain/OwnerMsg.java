package com.ruoyi.domain;

import java.util.List;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.core.annotation.Excel;
import com.ruoyi.common.core.web.domain.BaseEntity;

/**
 * 业主信息管理对象 owner_msg
 *
 * @author ruoyi
 * @date 2023-12-07
 */
public class OwnerMsg extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 自增ID */
    private Long omid;

    /** 唯一编码,前台使用 */
    @Excel(name = "唯一编码,前台使用")
    private String code;

    /** 状态 */
    @Excel(name = "状态")
    private Long status;

    /** 是否删除 */
    @Excel(name = "是否删除")
    private Long deleted;

    /** 业主姓名 */
    @Excel(name = "业主姓名")
    private String ownerName;

    /** 业主电话 */
    @Excel(name = "业主电话")
    private String ownerTel;

    /** 门牌号 */
    @Excel(name = "门牌号")
    private String ownerAddress;

    /** 所属小区id */
    @Excel(name = "所属小区id")
    private Long pmid;

    /** 停车场id */
    @Excel(name = "停车场id")
    private Long chid;

    /** 类型id */
    @Excel(name = "住户类型id")
    private Long omType;
    private PropertyManagement propertyManagement;
    private CarsHome carsHome;

    public Long getOmType() {
        return omType;
    }

    public void setOmType(Long omType) {
        this.omType = omType;
    }

    /**
     * 物业管理信息
     */


    @Override
    public String toString() {
        return "OwnerMsg{" +
                "omid=" + omid +
                ", code='" + code + '\'' +
                ", status=" + status +
                ", deleted=" + deleted +
                ", ownerName='" + ownerName + '\'' +
                ", ownerTel='" + ownerTel + '\'' +
                ", ownerAddress='" + ownerAddress + '\'' +
                ", pmid=" + pmid +
                ", chid=" + chid +
                ", omType=" + omType +
                ", propertyManagement=" + propertyManagement +
                ", carsHome=" + carsHome +
                "} " + super.toString();
    }

    public Long getOmid() {
        return omid;
    }

    public void setOmid(Long omid) {
        this.omid = omid;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Long getStatus() {
        return status;
    }

    public void setStatus(Long status) {
        this.status = status;
    }

    public Long getDeleted() {
        return deleted;
    }

    public void setDeleted(Long deleted) {
        this.deleted = deleted;
    }

    public String getOwnerName() {
        return ownerName;
    }

    public void setOwnerName(String ownerName) {
        this.ownerName = ownerName;
    }

    public String getOwnerTel() {
        return ownerTel;
    }

    public void setOwnerTel(String ownerTel) {
        this.ownerTel = ownerTel;
    }

    public String getOwnerAddress() {
        return ownerAddress;
    }

    public void setOwnerAddress(String ownerAddress) {
        this.ownerAddress = ownerAddress;
    }

    public Long getPmid() {
        return pmid;
    }

    public void setPmid(Long pmid) {
        this.pmid = pmid;
    }

    public Long getChid() {
        return chid;
    }

    public void setChid(Long chid) {
        this.chid = chid;
    }

    public PropertyManagement getPropertyManagement() {
        return propertyManagement;
    }

    public void setPropertyManagement(PropertyManagement propertyManagement) {
        this.propertyManagement = propertyManagement;
    }

    public CarsHome getCarsHome() {
        return carsHome;
    }

    public void setCarsHome(CarsHome carsHome) {
        this.carsHome = carsHome;
    }


    public OwnerMsg() {
    }

    public OwnerMsg(Long omid, String code, Long status, Long deleted, String ownerName, String ownerTel, String ownerAddress, Long pmid, Long chid, Long omType, PropertyManagement propertyManagement, CarsHome carsHome) {
        this.omid = omid;
        this.code = code;
        this.status = status;
        this.deleted = deleted;
        this.ownerName = ownerName;
        this.ownerTel = ownerTel;
        this.ownerAddress = ownerAddress;
        this.pmid = pmid;
        this.chid = chid;
        this.omType = omType;
        this.propertyManagement = propertyManagement;
        this.carsHome = carsHome;
    }
}
