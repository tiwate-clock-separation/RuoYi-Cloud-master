package com.ruoyi.service;

import java.util.List;
import com.ruoyi.domain.CarsChCt;

/**
 * 【请填写功能名称】Service接口
 *
 * @author ruoyi
 * @date 2023-12-12
 */
public interface ICarsChCtService
{
    /**
     * 查询【请填写功能名称】
     *
     * @param chid 【请填写功能名称】主键
     * @return 【请填写功能名称】
     */
    public CarsChCt selectCarsChCtByChid(Long chid);

    /**
     * 查询【请填写功能名称】列表
     *
     * @param carsChCt 【请填写功能名称】
     * @return 【请填写功能名称】集合
     */
    public List<CarsChCt> selectCarsChCtList(CarsChCt carsChCt);

    /**
     * 新增【请填写功能名称】
     *
     * @param carsChCt 【请填写功能名称】
     * @return 结果
     */
    public int insertCarsChCt(CarsChCt carsChCt);

    /**
     * 修改【请填写功能名称】
     *
     * @param carsChCt 【请填写功能名称】
     * @return 结果
     */
    public int updateCarsChCt(CarsChCt carsChCt);

    /**
     * 批量删除【请填写功能名称】
     *
     * @param chids 需要删除的【请填写功能名称】主键集合
     * @return 结果
     */
    public int deleteCarsChCtByChids(Long[] chids);

    /**
     * 删除【请填写功能名称】信息
     *
     * @param chid 【请填写功能名称】主键
     * @return 结果
     */
    public int deleteCarsChCtByChid(Long chid);
}
