package com.ruoyi.service;

import java.util.List;
import com.ruoyi.domain.CarsType;

/**
 * 车辆类型Service接口
 *
 * @author ruoyi
 * @date 2023-12-12
 */
public interface ICarsTypeService
{
    /**
     * 查询车辆类型
     *
     * @param ctid 车辆类型主键
     * @return 车辆类型
     */
    public CarsType selectCarsTypeByCtid(Long ctid);

    /**
     * 查询车辆类型列表
     *
     * @param carsType 车辆类型
     * @return 车辆类型集合
     */
    public List<CarsType> selectCarsTypeList(CarsType carsType);

    /**
     * 新增车辆类型
     *
     * @param carsType 车辆类型
     * @return 结果
     */
    public int insertCarsType(CarsType carsType);

    /**
     * 修改车辆类型
     *
     * @param carsType 车辆类型
     * @return 结果
     */
    public int updateCarsType(CarsType carsType);

    /**
     * 批量删除车辆类型
     *
     * @param ctids 需要删除的车辆类型主键集合
     * @return 结果
     */
    public int deleteCarsTypeByCtids(Long[] ctids);

    /**
     * 删除车辆类型信息
     *
     * @param ctid 车辆类型主键
     * @return 结果
     */
    public int deleteCarsTypeByCtid(Long ctid);
}
