package com.ruoyi.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.core.annotation.Excel;
import com.ruoyi.common.core.web.domain.BaseEntity;

/**
 * 业主车辆审核对象 cars_aduit
 *
 * @author ruoyi
 * @date 2023-12-03
 */
public class CarsAduit extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 自增ID */
    private Long caid;

    /** 唯一编码,前台使用 */
    @Excel(name = "唯一编码,前台使用")
    private String code;

    /** 状态 */
    @Excel(name = "状态")
    private Long status;

    /** 是否删除 */
    @Excel(name = "是否删除")
    private Long deleted;

    /** 小区id */
    @Excel(name = "小区id")
    private Long ohid;

    /** 车场id */
    @Excel(name = "车场id")
    private Long hpid;

    /** 业主id */
    @Excel(name = "业主id")
    private Long omid;

    /** 车牌号 */
    @Excel(name = "车牌号")
    private String carsNum;

    public void setCaid(Long caid)
    {
        this.caid = caid;
    }

    public Long getCaid()
    {
        return caid;
    }
    public void setCode(String code)
    {
        this.code = code;
    }

    public String getCode()
    {
        return code;
    }
    public void setStatus(Long status)
    {
        this.status = status;
    }

    public Long getStatus()
    {
        return status;
    }
    public void setDeleted(Long deleted)
    {
        this.deleted = deleted;
    }

    public Long getDeleted()
    {
        return deleted;
    }
    public void setOhid(Long ohid)
    {
        this.ohid = ohid;
    }

    public Long getOhid()
    {
        return ohid;
    }
    public void setHpid(Long hpid)
    {
        this.hpid = hpid;
    }

    public Long getHpid()
    {
        return hpid;
    }
    public void setOmid(Long omid)
    {
        this.omid = omid;
    }

    public Long getOmid()
    {
        return omid;
    }
    public void setCarsNum(String carsNum)
    {
        this.carsNum = carsNum;
    }

    public String getCarsNum()
    {
        return carsNum;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("caid", getCaid())
            .append("code", getCode())
            .append("status", getStatus())
            .append("deleted", getDeleted())
            .append("createTime", getCreateTime())
            .append("updateTime", getUpdateTime())
            .append("ohid", getOhid())
            .append("hpid", getHpid())
            .append("omid", getOmid())
            .append("carsNum", getCarsNum())
            .append("remark", getRemark())
            .toString();
    }
}
