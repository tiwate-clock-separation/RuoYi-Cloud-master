package com.ruoyi.domain;


import com.ruoyi.common.core.annotation.Excel;

/**
 * 业主车类型车场中间表 cars_ch_ct
 *
 * @author ruoyi
 * @date 2023-12-03
 */
public class CarsChCt {
    /** 状态 */
    @Excel(name = "车场id")
    private Long chid;
    /** 状态 */
    @Excel(name = "车辆类型id")
    private Long ctid;

    private CarsHome carsHome;
    private CarsType carsType;

    public CarsChCt() {
    }

    public CarsChCt(Long chid, Long ctid, CarsHome carsHome, CarsType carsType) {
        this.chid = chid;
        this.ctid = ctid;
        this.carsHome = carsHome;
        this.carsType = carsType;
    }

    public Long getChid() {
        return chid;
    }

    public void setChid(Long chid) {
        this.chid = chid;
    }

    public Long getCtid() {
        return ctid;
    }

    public void setCtid(Long ctid) {
        this.ctid = ctid;
    }

    public CarsHome getCarsHome() {
        return carsHome;
    }

    public void setCarsHome(CarsHome carsHome) {
        this.carsHome = carsHome;
    }

    public CarsType getCarsType() {
        return carsType;
    }

    public void setCarsType(CarsType carsType) {
        this.carsType = carsType;
    }
}
