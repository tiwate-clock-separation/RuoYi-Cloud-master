package com.ruoyi.service.impl;

import java.util.List;
import com.ruoyi.common.core.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.mapper.CarsHomeWechatMapper;
import com.ruoyi.domain.CarsHomeWechat;
import com.ruoyi.service.ICarsHomeWechatService;

/**
 * 微信支付Service业务层处理
 * 
 * @author chn
 * @date 2023-12-27
 */
@Service
public class CarsHomeWechatServiceImpl implements ICarsHomeWechatService 
{
    @Autowired
    private CarsHomeWechatMapper carsHomeWechatMapper;

    /**
     * 查询微信支付
     * 
     * @param id 微信支付主键
     * @return 微信支付
     */
    @Override
    public CarsHomeWechat selectCarsHomeWechatById(Long id)
    {
        return carsHomeWechatMapper.selectCarsHomeWechatById(id);
    }

    /**
     * 查询微信支付列表
     * 
     * @param carsHomeWechat 微信支付
     * @return 微信支付
     */
    @Override
    public List<CarsHomeWechat> selectCarsHomeWechatList(CarsHomeWechat carsHomeWechat)
    {
        return carsHomeWechatMapper.selectCarsHomeWechatList(carsHomeWechat);
    }

    /**
     * 新增微信支付
     * 
     * @param carsHomeWechat 微信支付
     * @return 结果
     */
    @Override
    public int insertCarsHomeWechat(CarsHomeWechat carsHomeWechat)
    {
        carsHomeWechat.setCreateTime(DateUtils.getNowDate());
        return carsHomeWechatMapper.insertCarsHomeWechat(carsHomeWechat);
    }

    /**
     * 修改微信支付
     * 
     * @param carsHomeWechat 微信支付
     * @return 结果
     */
    @Override
    public int updateCarsHomeWechat(CarsHomeWechat carsHomeWechat)
    {
        carsHomeWechat.setUpdateTime(DateUtils.getNowDate());
        return carsHomeWechatMapper.updateCarsHomeWechat(carsHomeWechat);
    }

    /**
     * 批量删除微信支付
     * 
     * @param ids 需要删除的微信支付主键
     * @return 结果
     */
    @Override
    public int deleteCarsHomeWechatByIds(Long[] ids)
    {
        return carsHomeWechatMapper.deleteCarsHomeWechatByIds(ids);
    }

    /**
     * 删除微信支付信息
     * 
     * @param id 微信支付主键
     * @return 结果
     */
    @Override
    public int deleteCarsHomeWechatById(Long id)
    {
        return carsHomeWechatMapper.deleteCarsHomeWechatById(id);
    }
}
