package com.ruoyi.service;

import java.util.List;
import com.ruoyi.domain.CarsHomeEqCameratype;

/**
 * 车厂设备相机类型Service接口
 * 
 * @author chn
 * @date 2023-12-07
 */
public interface ICarsHomeEqCameratypeService 
{
    /**
     * 查询车厂设备相机类型
     * 
     * @param cameraid 车厂设备相机类型主键
     * @return 车厂设备相机类型
     */
    public CarsHomeEqCameratype selectCarsHomeEqCameratypeByCameraid(Long cameraid);

    /**
     * 查询车厂设备相机类型列表
     * 
     * @param carsHomeEqCameratype 车厂设备相机类型
     * @return 车厂设备相机类型集合
     */
    public List<CarsHomeEqCameratype> selectCarsHomeEqCameratypeList(CarsHomeEqCameratype carsHomeEqCameratype);

    /**
     * 新增车厂设备相机类型
     * 
     * @param carsHomeEqCameratype 车厂设备相机类型
     * @return 结果
     */
    public int insertCarsHomeEqCameratype(CarsHomeEqCameratype carsHomeEqCameratype);

    /**
     * 修改车厂设备相机类型
     * 
     * @param carsHomeEqCameratype 车厂设备相机类型
     * @return 结果
     */
    public int updateCarsHomeEqCameratype(CarsHomeEqCameratype carsHomeEqCameratype);

    /**
     * 批量删除车厂设备相机类型
     * 
     * @param cameraids 需要删除的车厂设备相机类型主键集合
     * @return 结果
     */
    public int deleteCarsHomeEqCameratypeByCameraids(Long[] cameraids);

    /**
     * 删除车厂设备相机类型信息
     * 
     * @param cameraid 车厂设备相机类型主键
     * @return 结果
     */
    public int deleteCarsHomeEqCameratypeByCameraid(Long cameraid);
}
