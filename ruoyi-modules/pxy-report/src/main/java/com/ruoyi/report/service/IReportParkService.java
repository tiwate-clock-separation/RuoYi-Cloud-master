package com.ruoyi.report.service;

import java.util.List;
import com.ruoyi.domain.ReportPark;

/**
 * 停车报Service接口
 * 
 * @author ruoyi
 * @date 2023-12-07
 */
public interface IReportParkService 
{
    /**
     * 查询停车报
     * 
     * @param parkid 停车报主键
     * @return 停车报
     */
    public ReportPark selectReportParkByParkid(Long parkid);

    /**
     * 查询停车报列表
     * 
     * @param reportPark 停车报
     * @return 停车报集合
     */
    public List<ReportPark> selectReportParkList(ReportPark reportPark);

    /**
     * 新增停车报
     * 
     * @param reportPark 停车报
     * @return 结果
     */
    public int insertReportPark(ReportPark reportPark);

    /**
     * 修改停车报
     * 
     * @param reportPark 停车报
     * @return 结果
     */
    public int updateReportPark(ReportPark reportPark);

    /**
     * 批量删除停车报
     * 
     * @param parkids 需要删除的停车报主键集合
     * @return 结果
     */
    public int deleteReportParkByParkids(Long[] parkids);

    /**
     * 删除停车报信息
     * 
     * @param parkid 停车报主键
     * @return 结果
     */
    public int deleteReportParkByParkid(Long parkid);
}
