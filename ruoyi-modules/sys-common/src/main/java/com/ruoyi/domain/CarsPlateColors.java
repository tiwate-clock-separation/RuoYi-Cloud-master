package com.ruoyi.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.core.annotation.Excel;
import com.ruoyi.common.core.web.domain.BaseEntity;

/**
 * 车辆牌照颜色对象 cars_plate_colors
 *
 * @author chn
 * @date 2023-12-04
 */
public class CarsPlateColors extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 自增ID */
    private Long lpcid;

    /** 唯一编码,前台使用 */
    @Excel(name = "唯一编码,前台使用")
    private String code;

    /** 状态 */
    @Excel(name = "状态")
    private Long status;

    /** 是否删除 */
    @Excel(name = "是否删除")
    private Long deleted;

    /** 车辆牌照颜色名称 */
    @Excel(name = "车辆牌照颜色名称")
    private String carsPlateName;

    @Override
    public String toString() {
        return "CarsPlateColors{" +
                "lpcid=" + lpcid +
                ", code='" + code + '\'' +
                ", status=" + status +
                ", deleted=" + deleted +
                ", carsPlateName='" + carsPlateName + '\'' +
                "} " + super.toString();
    }

    public CarsPlateColors(Long lpcid, String code, Long status, Long deleted, String carsPlateName) {
        this.lpcid = lpcid;
        this.code = code;
        this.status = status;
        this.deleted = deleted;
        this.carsPlateName = carsPlateName;
    }

    public CarsPlateColors() {
    }

    public Long getLpcid() {
        return lpcid;
    }

    public void setLpcid(Long lpcid) {
        this.lpcid = lpcid;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Long getStatus() {
        return status;
    }

    public void setStatus(Long status) {
        this.status = status;
    }

    public Long getDeleted() {
        return deleted;
    }

    public void setDeleted(Long deleted) {
        this.deleted = deleted;
    }

    public String getCarsPlateName() {
        return carsPlateName;
    }

    public void setCarsPlateName(String carsPlateName) {
        this.carsPlateName = carsPlateName;
    }
}
