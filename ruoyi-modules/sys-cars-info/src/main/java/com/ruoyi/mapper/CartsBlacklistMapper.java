package com.ruoyi.mapper;

import com.ruoyi.domain.CartsBlacklist;

import java.util.List;

/**
 * 车辆黑名单Mapper接口
 * 
 * @author ruoyi
 * @date 2023-12-13
 */
public interface CartsBlacklistMapper 
{
    /**
     * 查询车辆黑名单
     * 
     * @param cbid 车辆黑名单主键
     * @return 车辆黑名单
     */
    public CartsBlacklist selectCartsBlacklistByCbid(Long cbid);

    /**
     * 查询车辆黑名单列表
     * 
     * @param cartsBlacklist 车辆黑名单
     * @return 车辆黑名单集合
     */
    public List<CartsBlacklist> selectCartsBlacklistList(CartsBlacklist cartsBlacklist);

    /**
     * 新增车辆黑名单
     * 
     * @param cartsBlacklist 车辆黑名单
     * @return 结果
     */
    public int insertCartsBlacklist(CartsBlacklist cartsBlacklist);

    /**
     * 修改车辆黑名单
     * 
     * @param cartsBlacklist 车辆黑名单
     * @return 结果
     */
    public int updateCartsBlacklist(CartsBlacklist cartsBlacklist);

    /**
     * 删除车辆黑名单
     * 
     * @param cbid 车辆黑名单主键
     * @return 结果
     */
    public int deleteCartsBlacklistByCbid(Long cbid);

    /**
     * 批量删除车辆黑名单
     * 
     * @param cbids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteCartsBlacklistByCbids(Long[] cbids);
}
