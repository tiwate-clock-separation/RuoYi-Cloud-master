import request from '@/utils/request'

// 查询车场计费规则列表
export function listRules(query) {
  return request({
    url: '/sys-cars-home/rules/list',
    method: 'get',
    params: query
  })
}

// 查询车场计费规则详细
export function getRules(chrid) {
  return request({
    url: '/sys-cars-home/rules/' + chrid,
    method: 'get'
  })
}

// 新增车场计费规则
export function addRules(data) {
  return request({
    url: '/sys-cars-home/rules',
    method: 'post',
    data: data
  })
}

// 修改车场计费规则
export function updateRules(data) {
  return request({
    url: '/sys-cars-home/rules',
    method: 'put',
    data: data
  })
}

// 删除车场计费规则
export function delRules(chrid) {
  return request({
    url: '/sys-cars-home/rules/' + chrid,
    method: 'delete'
  })
}
