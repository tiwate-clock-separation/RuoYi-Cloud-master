package com.ruoyi.controller;

import java.util.List;
import java.io.IOException;
import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.log.annotation.Log;
import com.ruoyi.common.log.enums.BusinessType;
import com.ruoyi.common.security.annotation.RequiresPermissions;
import com.ruoyi.domain.CarsChCt;
import com.ruoyi.service.ICarsChCtService;
import com.ruoyi.common.core.web.controller.BaseController;
import com.ruoyi.common.core.web.domain.AjaxResult;
import com.ruoyi.common.core.utils.poi.ExcelUtil;
import com.ruoyi.common.core.web.page.TableDataInfo;

/**
 * 【请填写功能名称】Controller
 *
 * @author ruoyi
 * @date 2023-12-12
 */
@RestController
@RequestMapping("/ct")
public class CarsChCtController extends BaseController
{
    @Autowired
    private ICarsChCtService carsChCtService;

    /**
     * 查询【请填写功能名称】列表
     */
    @RequiresPermissions("system:ct:list")
    @GetMapping("/list")
    public TableDataInfo list(CarsChCt carsChCt)
    {
        startPage();
        List<CarsChCt> list = carsChCtService.selectCarsChCtList(carsChCt);
        return getDataTable(list);
    }

    /**
     * 导出【请填写功能名称】列表
     */
    @RequiresPermissions("system:ct:export")
    @Log(title = "【请填写功能名称】", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, CarsChCt carsChCt)
    {
        List<CarsChCt> list = carsChCtService.selectCarsChCtList(carsChCt);
        ExcelUtil<CarsChCt> util = new ExcelUtil<CarsChCt>(CarsChCt.class);
        util.exportExcel(response, list, "【请填写功能名称】数据");
    }

    /**
     * 获取【请填写功能名称】详细信息
     */
    @RequiresPermissions("system:ct:query")
    @GetMapping(value = "/{chid}")
    public AjaxResult getInfo(@PathVariable("chid") Long chid)
    {
        return success(carsChCtService.selectCarsChCtByChid(chid));
    }

    /**
     * 新增【请填写功能名称】
     */
    @RequiresPermissions("system:ct:add")
    @Log(title = "【请填写功能名称】", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody CarsChCt carsChCt)
    {
        return toAjax(carsChCtService.insertCarsChCt(carsChCt));
    }

    /**
     * 修改【请填写功能名称】
     */
    @RequiresPermissions("system:ct:edit")
    @Log(title = "【请填写功能名称】", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody CarsChCt carsChCt)
    {
        return toAjax(carsChCtService.updateCarsChCt(carsChCt));
    }

    /**
     * 删除【请填写功能名称】
     */
    @RequiresPermissions("system:ct:remove")
    @Log(title = "【请填写功能名称】", businessType = BusinessType.DELETE)
	@DeleteMapping("/{chids}")
    public AjaxResult remove(@PathVariable Long[] chids)
    {
        return toAjax(carsChCtService.deleteCarsChCtByChids(chids));
    }
}
