package com.ruoyi.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.core.annotation.Excel;
import com.ruoyi.common.core.web.domain.BaseEntity;

/**
 * 车场计费规则对象 cars_home_rules
 * 
 * @author ruoyi
 * @date 2023-12-02
 */
public class CarsHomeRules extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 自增规则id */
    private Long chrid;

    /** 唯一编码,前台使用 */
    @Excel(name = "唯一编码,前台使用")
    private String code;

    /** 状态 */
    @Excel(name = "状态")
    private Long status;

    /** 是否删除 */
    @Excel(name = "是否删除")
    private Long deleted;

    /** 车场id */
    @Excel(name = "车场id")
    private Long chid;




    /** 规则名称 */
    @Excel(name = "规则名称")
    private String ruleName;

    /** 收费类型 */
    @Excel(name = "收费类型")
    private Long chtrid;

    private CarsHomeTollTime carsHomeTollTime;
    private CarsHomeTollDay carsHomeTollDay;
    private CarsHomeTollMinute carsHomeTollMinute;

    public CarsHomeTollTime getCarsHomeTollTime() {
        return carsHomeTollTime;
    }

    public void setCarsHomeTollTime(CarsHomeTollTime carsHomeTollTime) {
        this.carsHomeTollTime = carsHomeTollTime;
    }

    public CarsHomeTollDay getCarsHomeTollDay() {
        return carsHomeTollDay;
    }

    public void setCarsHomeTollDay(CarsHomeTollDay carsHomeTollDay) {
        this.carsHomeTollDay = carsHomeTollDay;
    }

    public CarsHomeTollMinute getCarsHomeTollMinute() {
        return carsHomeTollMinute;
    }

    public void setCarsHomeTollMinute(CarsHomeTollMinute carsHomeTollMinute) {
        this.carsHomeTollMinute = carsHomeTollMinute;
    }

    /** 节假日是否免费0收费1免费 */
    @Excel(name = "节假日是否免费0收费1免费")
    private Long holidaysFree;

    /** 车牌颜色id */
    @Excel(name = "车牌颜色id")
    private Long lpcid;

    /** 车辆类型id */
    @Excel(name = "车辆类型id")
    private Long carsTypeid;

    /** 详细规则 */
    @Excel(name = "详细规则")
    private String rulesTxt;


    private CarsHome carsHome;
    private CarsHomeTollRules carsHomeTollRules;

    private CarsPlateColors carsPlateColors;

    private  CarsType carsType;

    public CarsType getCarsType() {
        return carsType;
    }

    public void setCarsType(CarsType carsType) {
        this.carsType = carsType;
    }

    public CarsHome getCarsHome() {
        return carsHome;
    }

    public void setCarsHome(CarsHome carsHome) {
        this.carsHome = carsHome;
    }

    public CarsHomeTollRules getCarsHomeTollRules() {
        return carsHomeTollRules;
    }

    public void setCarsHomeTollRules(CarsHomeTollRules carsHomeTollRules) {
        this.carsHomeTollRules = carsHomeTollRules;
    }

    public CarsPlateColors getCarsPlateColors() {
        return carsPlateColors;
    }

    public void setCarsPlateColors(CarsPlateColors carsPlateColors) {
        this.carsPlateColors = carsPlateColors;
    }

    public void setChrid(Long chrid)
    {
        this.chrid = chrid;
    }

    public Long getChrid() 
    {
        return chrid;
    }
    public void setCode(String code) 
    {
        this.code = code;
    }

    public String getCode() 
    {
        return code;
    }
    public void setStatus(Long status) 
    {
        this.status = status;
    }

    public Long getStatus() 
    {
        return status;
    }
    public void setDeleted(Long deleted) 
    {
        this.deleted = deleted;
    }

    public Long getDeleted() 
    {
        return deleted;
    }
    public void setChid(Long chid) 
    {
        this.chid = chid;
    }

    public Long getChid() 
    {
        return chid;
    }
    public void setRuleName(String ruleName) 
    {
        this.ruleName = ruleName;
    }

    public String getRuleName() 
    {
        return ruleName;
    }
    public void setChtrid(Long chtrid) 
    {
        this.chtrid = chtrid;
    }

    public Long getChtrid() 
    {
        return chtrid;
    }
    public void setHolidaysFree(Long holidaysFree) 
    {
        this.holidaysFree = holidaysFree;
    }

    public Long getHolidaysFree() 
    {
        return holidaysFree;
    }
    public void setLpcid(Long lpcid) 
    {
        this.lpcid = lpcid;
    }

    public Long getLpcid() 
    {
        return lpcid;
    }
    public void setCarsTypeid(Long carsTypeid) 
    {
        this.carsTypeid = carsTypeid;
    }

    public Long getCarsTypeid() 
    {
        return carsTypeid;
    }
    public void setRulesTxt(String rulesTxt) 
    {
        this.rulesTxt = rulesTxt;
    }

    public String getRulesTxt() 
    {
        return rulesTxt;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("chrid", getChrid())
            .append("code", getCode())
            .append("status", getStatus())
            .append("deleted", getDeleted())
            .append("createTime", getCreateTime())
            .append("updateTime", getUpdateTime())
            .append("chid", getChid())
            .append("ruleName", getRuleName())
            .append("chtrid", getChtrid())
            .append("holidaysFree", getHolidaysFree())
            .append("lpcid", getLpcid())
            .append("carsTypeid", getCarsTypeid())
            .append("rulesTxt", getRulesTxt())
            .toString();
    }
}
