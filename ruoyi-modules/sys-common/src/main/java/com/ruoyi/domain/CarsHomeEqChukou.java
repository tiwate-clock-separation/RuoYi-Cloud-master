package com.ruoyi.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.core.annotation.Excel;
import com.ruoyi.common.core.web.domain.BaseEntity;

/**
 * 出场设备对象 cars_home_eq_chukou
 * 
 * @author ruoyi
 * @date 2023-12-12
 */
public class CarsHomeEqChukou extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 自增ID */
    private Long ckid;

    /** 唯一编码,前台使用 */
    @Excel(name = "唯一编码,前台使用")
    private String code;

    /** 状态 */
    @Excel(name = "状态")
    private Long status;

    /** 是否删除 */
    @Excel(name = "是否删除")
    private Long deleted;

    /** 设备名称 */
    @Excel(name = "设备名称")
    private String ckname;

    /** 设备ip */
    @Excel(name = "设备ip")
    private String ckip;

    /** 车道IP */
    @Excel(name = "车道IP")
    private Long chlid;

    private CarsHomeLane carsHomeLane;

    public CarsHomeLane getCarsHomeLane() {
        return carsHomeLane;
    }

    public void setCarsHomeLane(CarsHomeLane carsHomeLane) {
        this.carsHomeLane = carsHomeLane;
    }

    public void setCkid(Long ckid)
    {
        this.ckid = ckid;
    }

    public Long getCkid() 
    {
        return ckid;
    }
    public void setCode(String code) 
    {
        this.code = code;
    }

    public String getCode() 
    {
        return code;
    }
    public void setStatus(Long status) 
    {
        this.status = status;
    }

    public Long getStatus() 
    {
        return status;
    }
    public void setDeleted(Long deleted) 
    {
        this.deleted = deleted;
    }

    public Long getDeleted() 
    {
        return deleted;
    }
    public void setCkname(String ckname) 
    {
        this.ckname = ckname;
    }

    public String getCkname() 
    {
        return ckname;
    }
    public void setCkip(String ckip) 
    {
        this.ckip = ckip;
    }

    public String getCkip() 
    {
        return ckip;
    }
    public void setChlid(Long chlid) 
    {
        this.chlid = chlid;
    }

    public Long getChlid() 
    {
        return chlid;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("ckid", getCkid())
            .append("code", getCode())
            .append("status", getStatus())
            .append("deleted", getDeleted())
            .append("createTime", getCreateTime())
            .append("updateTime", getUpdateTime())
            .append("ckname", getCkname())
            .append("ckip", getCkip())
            .append("chlid", getChlid())
            .toString();
    }
}
