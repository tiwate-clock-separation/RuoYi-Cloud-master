package com.ruoyi.domain;

import java.math.BigDecimal;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.core.annotation.Excel;
import com.ruoyi.common.core.web.domain.BaseEntity;

/**
 * 车辆套餐对象 cars_meal
 * 
 * @author ruoyi
 * @date 2023-12-13
 */
public class CarsMeal extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 自增ID */
    private Long cmid;

    /** 唯一编码,前台使用 */
    private String code;

    /** 状态 */
    private Long status;

    /** 是否删除 */
    private Long deleted;

    /** 套餐名称 */
    @Excel(name = "套餐名称")
    private String mealName;

    /** 套餐类型 */
    @Excel(name = "套餐类型")
    private Long mealType;

    private MealType mealTypeName;

    /** 车场id */
    @Excel(name = "车场id")
    private Long chid;

    private CarsHome carsHome;

    /** 生效时长 */
    @Excel(name = "生效时长")
    private Long validityTime;

    /** 销量 */
    @Excel(name = "销量")
    private Long salesVolume;

    /** 库存 */
    @Excel(name = "库存")
    private Long inventory;

    /** 真实价格 */
    @Excel(name = "真实价格")
    private BigDecimal realPrice;

    /** 原价 */
    @Excel(name = "原价")
    private BigDecimal originalPrice;

    /** 车辆类型id */
    @Excel(name = "车辆类型id")
    private Long ctid;

    private CarsType carsType;

    public void setCmid(Long cmid) 
    {
        this.cmid = cmid;
    }

    public Long getCmid() 
    {
        return cmid;
    }
    public void setCode(String code) 
    {
        this.code = code;
    }

    public String getCode() 
    {
        return code;
    }
    public void setStatus(Long status) 
    {
        this.status = status;
    }

    public Long getStatus() 
    {
        return status;
    }
    public void setDeleted(Long deleted) 
    {
        this.deleted = deleted;
    }

    public Long getDeleted() 
    {
        return deleted;
    }
    public void setMealName(String mealName) 
    {
        this.mealName = mealName;
    }

    public String getMealName() 
    {
        return mealName;
    }
    public void setMealType(Long mealType) 
    {
        this.mealType = mealType;
    }

    public Long getMealType() 
    {
        return mealType;
    }
    public void setChid(Long chid) 
    {
        this.chid = chid;
    }

    public Long getChid() 
    {
        return chid;
    }
    public void setValidityTime(Long validityTime) 
    {
        this.validityTime = validityTime;
    }

    public Long getValidityTime() 
    {
        return validityTime;
    }
    public void setSalesVolume(Long salesVolume) 
    {
        this.salesVolume = salesVolume;
    }

    public Long getSalesVolume() 
    {
        return salesVolume;
    }
    public void setInventory(Long inventory) 
    {
        this.inventory = inventory;
    }

    public Long getInventory() 
    {
        return inventory;
    }
    public void setRealPrice(BigDecimal realPrice) 
    {
        this.realPrice = realPrice;
    }

    public BigDecimal getRealPrice() 
    {
        return realPrice;
    }
    public void setOriginalPrice(BigDecimal originalPrice) 
    {
        this.originalPrice = originalPrice;
    }

    public BigDecimal getOriginalPrice() 
    {
        return originalPrice;
    }
    public void setCtid(Long ctid) 
    {
        this.ctid = ctid;
    }

    public Long getCtid() 
    {
        return ctid;
    }

    public CarsHome getCarsHome() {
        return carsHome;
    }

    public void setCarsHome(CarsHome carsHome) {
        this.carsHome = carsHome;
    }

    public CarsType getCarsType() {
        return carsType;
    }

    public void setCarsType(CarsType carsType) {
        this.carsType = carsType;
    }

    public MealType getMealTypeName() {
        return mealTypeName;
    }

    public void setMealTypeName(MealType mealTypeName) {
        this.mealTypeName = mealTypeName;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("cmid", getCmid())
            .append("code", getCode())
            .append("status", getStatus())
            .append("deleted", getDeleted())
            .append("createTime", getCreateTime())
            .append("updateTime", getUpdateTime())
            .append("mealName", getMealName())
            .append("mealType", getMealType())
            .append("chid", getChid())
            .append("validityTime", getValidityTime())
            .append("salesVolume", getSalesVolume())
            .append("inventory", getInventory())
            .append("realPrice", getRealPrice())
            .append("originalPrice", getOriginalPrice())
            .append("ctid", getCtid())
            .append("remark", getRemark())
            .append("carsHome", getCarsHome())
            .append("carsType", getCarsType())
            .append("mealTypeName", getMealTypeName())
            .toString();
    }
}
