package com.ruoyi.staff.controller;

import java.util.List;
import java.io.IOException;
import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.log.annotation.Log;
import com.ruoyi.common.log.enums.BusinessType;
import com.ruoyi.common.security.annotation.RequiresPermissions;
import com.ruoyi.domain.EmpHome;
import com.ruoyi.staff.service.IEmpHomeService;
import com.ruoyi.common.core.web.controller.BaseController;
import com.ruoyi.common.core.web.domain.AjaxResult;
import com.ruoyi.common.core.utils.poi.ExcelUtil;
import com.ruoyi.common.core.web.page.TableDataInfo;

/**
 * 物业人员Controller
 * 
 * @author ruoyi
 * @date 2023-12-23
 */
@RestController
@RequestMapping("/staffhome")
public class EmpHomeController extends BaseController
{
    @Autowired
    private IEmpHomeService empHomeService;

    /**
     * 查询物业人员列表
     */
    @RequiresPermissions("pxy-staff:staffhome:list")
    @GetMapping("/list")
    public TableDataInfo list(EmpHome empHome)
    {
        startPage();
        List<EmpHome> list = empHomeService.selectEmpHomeList(empHome);
        return getDataTable(list);
    }

    /**
     * 导出物业人员列表
     */
    @RequiresPermissions("pxy-staff:staffhome:export")
    @Log(title = "物业人员", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, EmpHome empHome)
    {
        List<EmpHome> list = empHomeService.selectEmpHomeList(empHome);
        ExcelUtil<EmpHome> util = new ExcelUtil<EmpHome>(EmpHome.class);
        util.exportExcel(response, list, "物业人员数据");
    }

    /**
     * 获取物业人员详细信息
     */
    @RequiresPermissions("pxy-staff:staffhome:query")
    @GetMapping(value = "/{ehid}")
    public AjaxResult getInfo(@PathVariable("ehid") Long ehid)
    {
        return success(empHomeService.selectEmpHomeByEhid(ehid));
    }

    /**
     * 新增物业人员
     */
    @RequiresPermissions("pxy-staff:staffhome:add")
    @Log(title = "物业人员", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody EmpHome empHome)
    {
        return toAjax(empHomeService.insertEmpHome(empHome));
    }

    /**
     * 修改物业人员
     */
    @RequiresPermissions("pxy-staff:staffhome:edit")
    @Log(title = "物业人员", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody EmpHome empHome)
    {
        return toAjax(empHomeService.updateEmpHome(empHome));
    }

    /**
     * 删除物业人员
     */
    @RequiresPermissions("pxy-staff:staffhome:remove")
    @Log(title = "物业人员", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ehids}")
    public AjaxResult remove(@PathVariable Long[] ehids)
    {
        return toAjax(empHomeService.deleteEmpHomeByEhids(ehids));
    }
}
