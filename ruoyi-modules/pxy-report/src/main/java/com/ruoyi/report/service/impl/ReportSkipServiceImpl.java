package com.ruoyi.report.service.impl;

import java.util.List;
import com.ruoyi.common.core.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.report.mapper.ReportSkipMapper;
import com.ruoyi.domain.ReportSkip;
import com.ruoyi.report.service.IReportSkipService;

/**
 * 逃单报Service业务层处理
 * 
 * @author ruoyi
 * @date 2023-12-07
 */
@Service
public class ReportSkipServiceImpl implements IReportSkipService 
{
    @Autowired
    private ReportSkipMapper reportSkipMapper;

    /**
     * 查询逃单报
     * 
     * @param id 逃单报主键
     * @return 逃单报
     */
    @Override
    public ReportSkip selectReportSkipById(Long id)
    {
        return reportSkipMapper.selectReportSkipById(id);
    }

    /**
     * 查询逃单报列表
     * 
     * @param reportSkip 逃单报
     * @return 逃单报
     */
    @Override
    public List<ReportSkip> selectReportSkipList(ReportSkip reportSkip)
    {
        return reportSkipMapper.selectReportSkipList(reportSkip);
    }

    /**
     * 新增逃单报
     * 
     * @param reportSkip 逃单报
     * @return 结果
     */
    @Override
    public int insertReportSkip(ReportSkip reportSkip)
    {
        reportSkip.setCreateTime(DateUtils.getNowDate());
        return reportSkipMapper.insertReportSkip(reportSkip);
    }

    /**
     * 修改逃单报
     * 
     * @param reportSkip 逃单报
     * @return 结果
     */
    @Override
    public int updateReportSkip(ReportSkip reportSkip)
    {
        reportSkip.setUpdateTime(DateUtils.getNowDate());
        return reportSkipMapper.updateReportSkip(reportSkip);
    }

    /**
     * 批量删除逃单报
     * 
     * @param ids 需要删除的逃单报主键
     * @return 结果
     */
    @Override
    public int deleteReportSkipByIds(Long[] ids)
    {
        return reportSkipMapper.deleteReportSkipByIds(ids);
    }

    /**
     * 删除逃单报信息
     * 
     * @param id 逃单报主键
     * @return 结果
     */
    @Override
    public int deleteReportSkipById(Long id)
    {
        return reportSkipMapper.deleteReportSkipById(id);
    }
}
