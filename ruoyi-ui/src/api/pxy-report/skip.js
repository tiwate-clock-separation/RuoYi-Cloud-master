import request from '@/utils/request'

// 查询逃单报列表
export function listSkip(query) {
  return request({
    url: '/pxy-report/skip/list',
    method: 'get',
    params: query
  })
}

// 查询逃单报详细
export function getSkip(id) {
  return request({
    url: '/pxy-report/skip/' + id,
    method: 'get'
  })
}

// 新增逃单报
export function addSkip(data) {
  return request({
    url: '/pxy-report/skip',
    method: 'post',
    data: data
  })
}

// 修改逃单报
export function updateSkip(data) {
  return request({
    url: '/pxy-report/skip',
    method: 'put',
    data: data
  })
}

// 删除逃单报
export function delSkip(id) {
  return request({
    url: '/pxy-report/skip/' + id,
    method: 'delete'
  })
}
