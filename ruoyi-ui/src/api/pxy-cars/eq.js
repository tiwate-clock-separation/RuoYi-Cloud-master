import request from '@/utils/request'

// 查询车场设备列表
export function listEq(query) {
  return request({
    url: '/pxy-cars/eq/list',
    method: 'get',
    params: query
  })
}

// 查询车场设备详细
export function getEq(eqid) {
  return request({
    url: '/pxy-cars/eq/' + eqid,
    method: 'get'
  })
}

// 新增车场设备
export function addEq(data) {
  return request({
    url: '/pxy-cars/eq',
    method: 'post',
    data: data
  })
}

// 修改车场设备
export function updateEq(data) {
  return request({
    url: '/pxy-cars/eq',
    method: 'put',
    data: data
  })
}

// 删除车场设备
export function delEq(eqid) {
  return request({
    url: '/pxy-cars/eq/' + eqid,
    method: 'delete'
  })
}
