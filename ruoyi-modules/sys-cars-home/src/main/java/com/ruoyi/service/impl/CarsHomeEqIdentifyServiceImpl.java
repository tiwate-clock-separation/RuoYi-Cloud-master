package com.ruoyi.service.impl;

import java.util.List;
import com.ruoyi.common.core.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.mapper.CarsHomeEqIdentifyMapper;
import com.ruoyi.domain.CarsHomeEqIdentify;
import com.ruoyi.service.ICarsHomeEqIdentifyService;

/**
 * 车牌识别Service业务层处理
 * 
 * @author chn
 * @date 2023-12-07
 */
@Service
public class CarsHomeEqIdentifyServiceImpl implements ICarsHomeEqIdentifyService 
{
    @Autowired
    private CarsHomeEqIdentifyMapper carsHomeEqIdentifyMapper;

    /**
     * 查询车牌识别
     * 
     * @param entifyid 车牌识别主键
     * @return 车牌识别
     */
    @Override
    public CarsHomeEqIdentify selectCarsHomeEqIdentifyByEntifyid(Long entifyid)
    {
        return carsHomeEqIdentifyMapper.selectCarsHomeEqIdentifyByEntifyid(entifyid);
    }

    /**
     * 查询车牌识别列表
     * 
     * @param carsHomeEqIdentify 车牌识别
     * @return 车牌识别
     */
    @Override
    public List<CarsHomeEqIdentify> selectCarsHomeEqIdentifyList(CarsHomeEqIdentify carsHomeEqIdentify)
    {
        return carsHomeEqIdentifyMapper.selectCarsHomeEqIdentifyList(carsHomeEqIdentify);
    }

    /**
     * 新增车牌识别
     * 
     * @param carsHomeEqIdentify 车牌识别
     * @return 结果
     */
    @Override
    public int insertCarsHomeEqIdentify(CarsHomeEqIdentify carsHomeEqIdentify)
    {
        carsHomeEqIdentify.setCreateTime(DateUtils.getNowDate());
        return carsHomeEqIdentifyMapper.insertCarsHomeEqIdentify(carsHomeEqIdentify);
    }

    /**
     * 修改车牌识别
     * 
     * @param carsHomeEqIdentify 车牌识别
     * @return 结果
     */
    @Override
    public int updateCarsHomeEqIdentify(CarsHomeEqIdentify carsHomeEqIdentify)
    {
        carsHomeEqIdentify.setUpdateTime(DateUtils.getNowDate());
        return carsHomeEqIdentifyMapper.updateCarsHomeEqIdentify(carsHomeEqIdentify);
    }

    /**
     * 批量删除车牌识别
     * 
     * @param entifyids 需要删除的车牌识别主键
     * @return 结果
     */
    @Override
    public int deleteCarsHomeEqIdentifyByEntifyids(Long[] entifyids)
    {
        return carsHomeEqIdentifyMapper.deleteCarsHomeEqIdentifyByEntifyids(entifyids);
    }

    /**
     * 删除车牌识别信息
     * 
     * @param entifyid 车牌识别主键
     * @return 结果
     */
    @Override
    public int deleteCarsHomeEqIdentifyByEntifyid(Long entifyid)
    {
        return carsHomeEqIdentifyMapper.deleteCarsHomeEqIdentifyByEntifyid(entifyid);
    }
}
