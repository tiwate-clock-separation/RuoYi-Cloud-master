package com.ruoyi.service.impl;

import java.util.List;
import com.ruoyi.common.core.utils.DateUtils;
import com.ruoyi.domain.CarsHomeType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.mapper.CarsHomeTypeMapper;
import com.ruoyi.service.ICarsHomeTypeService;

/**
 * 车场类型Service业务层处理
 *
 * @author chn
 * @date 2023-11-29
 */
@Service
public class CarsHomeTypeServiceImpl implements ICarsHomeTypeService
{
    @Autowired
    private CarsHomeTypeMapper carsHomeTypeMapper;

    /**
     * 查询车场类型
     *
     * @param chtid 车场类型主键
     * @return 车场类型
     */
    @Override
    public CarsHomeType selectCarsHomeTypeByChtid(Long chtid)
    {
        return carsHomeTypeMapper.selectCarsHomeTypeByChtid(chtid);
    }

    /**
     * 查询车场类型列表
     *
     * @param carsHomeType 车场类型
     * @return 车场类型
     */
    @Override
    public List<CarsHomeType> selectCarsHomeTypeList(CarsHomeType carsHomeType)
    {
        return carsHomeTypeMapper.selectCarsHomeTypeList(carsHomeType);
    }

    /**
     * 新增车场类型
     *
     * @param carsHomeType 车场类型
     * @return 结果
     */
    @Override
    public int insertCarsHomeType(CarsHomeType carsHomeType)
    {
        carsHomeType.setCreateTime(DateUtils.getNowDate());
        return carsHomeTypeMapper.insertCarsHomeType(carsHomeType);
    }

    /**
     * 修改车场类型
     *
     * @param carsHomeType 车场类型
     * @return 结果
     */
    @Override
    public int updateCarsHomeType(CarsHomeType carsHomeType)
    {
        carsHomeType.setUpdateTime(DateUtils.getNowDate());
        return carsHomeTypeMapper.updateCarsHomeType(carsHomeType);
    }

    /**
     * 批量删除车场类型
     *
     * @param chtids 需要删除的车场类型主键
     * @return 结果
     */
    @Override
    public int deleteCarsHomeTypeByChtids(Long[] chtids)
    {
        return carsHomeTypeMapper.deleteCarsHomeTypeByChtids(chtids);
    }

    /**
     * 删除车场类型信息
     *
     * @param chtid 车场类型主键
     * @return 结果
     */
    @Override
    public int deleteCarsHomeTypeByChtid(Long chtid)
    {
        return carsHomeTypeMapper.deleteCarsHomeTypeByChtid(chtid);
    }
}
