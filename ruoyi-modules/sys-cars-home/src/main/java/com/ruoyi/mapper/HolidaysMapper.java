package com.ruoyi.mapper;

import java.util.List;
import com.ruoyi.domain.Holidays;

/**
 * 车场节假日Mapper接口
 * 
 * @author chn
 * @date 2023-12-15
 */
public interface HolidaysMapper 
{
    /**
     * 查询车场节假日
     * 
     * @param id 车场节假日主键
     * @return 车场节假日
     */
    public Holidays selectHolidaysById(Long id);

    /**
     * 查询车场节假日列表
     * 
     * @param holidays 车场节假日
     * @return 车场节假日集合
     */
    public List<Holidays> selectHolidaysList(Holidays holidays);

    /**
     * 新增车场节假日
     * 
     * @param holidays 车场节假日
     * @return 结果
     */
    public int insertHolidays(Holidays holidays);

    /**
     * 修改车场节假日
     * 
     * @param holidays 车场节假日
     * @return 结果
     */
    public int updateHolidays(Holidays holidays);

    /**
     * 删除车场节假日
     * 
     * @param id 车场节假日主键
     * @return 结果
     */
    public int deleteHolidaysById(Long id);

    /**
     * 批量删除车场节假日
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteHolidaysByIds(Long[] ids);
}
