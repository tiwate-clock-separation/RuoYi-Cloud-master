package com.ruoyi.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.core.annotation.Excel;
import com.ruoyi.common.core.web.domain.BaseEntity;

/**
 * 车场人员对象 emp_car
 * 
 * @author pxy
 * @date 2023-12-23
 */
public class EmpCar extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 自增ID */
    @Excel(name = "自增ID")
    private Long carid;

    /** 唯一编码,前台使用 */
    private String code;

    /** 状态 */
    private Long status;

    /** 是否删除 */
    private Long deleted;

    /** 用户名称 */
    @Excel(name = "用户名称")
    private String carUser;

    /** 手机号 */
    @Excel(name = "手机号")
    private String carTel;

    /** 所属车场 */
    @Excel(name = "所属车场")
    private Long chid;

    private CarsHome carsHome;

    public CarsHome getCarsHome() {
        return carsHome;
    }

    public void setCarsHome(CarsHome carsHome) {
        this.carsHome = carsHome;
    }

    public void setCarid(Long carid)
    {
        this.carid = carid;
    }

    public Long getCarid() 
    {
        return carid;
    }
    public void setCode(String code) 
    {
        this.code = code;
    }

    public String getCode() 
    {
        return code;
    }
    public void setStatus(Long status) 
    {
        this.status = status;
    }

    public Long getStatus() 
    {
        return status;
    }
    public void setDeleted(Long deleted) 
    {
        this.deleted = deleted;
    }

    public Long getDeleted() 
    {
        return deleted;
    }
    public void setCarUser(String carUser) 
    {
        this.carUser = carUser;
    }

    public String getCarUser() 
    {
        return carUser;
    }

    public String getCarTel() {
        return carTel;
    }

    public void setCarTel(String carTel) {
        this.carTel = carTel;
    }

    public void setChid(Long chid)
    {
        this.chid = chid;
    }

    public Long getChid() 
    {
        return chid;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("carid", getCarid())
            .append("code", getCode())
            .append("status", getStatus())
            .append("deleted", getDeleted())
            .append("createTime", getCreateTime())
            .append("updateTime", getUpdateTime())
            .append("carUser", getCarUser())
            .append("carTel", getCarTel())
            .append("chid", getChid())
            .append("remark", getRemark())
            .toString();
    }
}
