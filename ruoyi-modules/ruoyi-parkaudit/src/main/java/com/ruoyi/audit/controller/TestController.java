package com.ruoyi.audit.controller;

import com.ruoyi.audit.api.CarsHomeApi;
import com.ruoyi.audit.api.PropertyApi;
import com.ruoyi.common.core.web.page.TableDataInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping
public class TestController {
    @Autowired
    private CarsHomeApi carsHomeApi;
    @Autowired
    private PropertyApi propertyApi;
    @GetMapping("test")
    public TableDataInfo list(){
        return carsHomeApi.list();
    }
    @GetMapping("test2")
    public TableDataInfo list2(){
        return propertyApi.list();
    }
}
