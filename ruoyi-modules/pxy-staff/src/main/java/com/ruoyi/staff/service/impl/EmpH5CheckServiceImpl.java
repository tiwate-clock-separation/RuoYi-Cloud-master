package com.ruoyi.staff.service.impl;

import java.util.List;
import com.ruoyi.common.core.utils.DateUtils;
import com.ruoyi.domain.EmpH5Check;
import com.ruoyi.staff.mapper.EmpH5CheckMapper;
import com.ruoyi.staff.service.IEmpH5CheckService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * h5消费Service业务层处理
 * 
 * @author ruoyi
 * @date 2023-12-15
 */
@Service
public class EmpH5CheckServiceImpl implements IEmpH5CheckService
{
    @Autowired
    private EmpH5CheckMapper empH5CheckMapper;

    /**
     * 查询h5消费
     * 
     * @param id h5消费主键
     * @return h5消费
     */
    @Override
    public EmpH5Check selectEmpH5CheckById(Long id)
    {
        return empH5CheckMapper.selectEmpH5CheckById(id);
    }

    /**
     * 查询h5消费列表
     * 
     * @param empH5Check h5消费
     * @return h5消费
     */
    @Override
    public List<EmpH5Check> selectEmpH5CheckList(EmpH5Check empH5Check)
    {
        return empH5CheckMapper.selectEmpH5CheckList(empH5Check);
    }

    /**
     * 新增h5消费
     * 
     * @param empH5Check h5消费
     * @return 结果
     */
    @Override
    public int insertEmpH5Check(EmpH5Check empH5Check)
    {
        empH5Check.setCreateTime(DateUtils.getNowDate());
        return empH5CheckMapper.insertEmpH5Check(empH5Check);
    }

    /**
     * 修改h5消费
     * 
     * @param empH5Check h5消费
     * @return 结果
     */
    @Override
    public int updateEmpH5Check(EmpH5Check empH5Check)
    {
        empH5Check.setUpdateTime(DateUtils.getNowDate());
        return empH5CheckMapper.updateEmpH5Check(empH5Check);
    }

    /**
     * 批量删除h5消费
     * 
     * @param ids 需要删除的h5消费主键
     * @return 结果
     */
    @Override
    public int deleteEmpH5CheckByIds(Long[] ids)
    {
        return empH5CheckMapper.deleteEmpH5CheckByIds(ids);
    }

    /**
     * 删除h5消费信息
     * 
     * @param id h5消费主键
     * @return 结果
     */
    @Override
    public int deleteEmpH5CheckById(Long id)
    {
        return empH5CheckMapper.deleteEmpH5CheckById(id);
    }
}
