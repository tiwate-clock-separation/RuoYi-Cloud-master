package com.ruoyi.audit.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;

import com.ruoyi.domain.SpaceAudit;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.log.annotation.Log;
import com.ruoyi.common.log.enums.BusinessType;
import com.ruoyi.common.security.annotation.RequiresPermissions;

import com.ruoyi.audit.service.ISpaceAuditService;
import com.ruoyi.common.core.web.controller.BaseController;
import com.ruoyi.common.core.web.domain.AjaxResult;
import com.ruoyi.common.core.utils.poi.ExcelUtil;
import com.ruoyi.common.core.web.page.TableDataInfo;

/**
 * 车位审核Controller
 *
 * @author ruoyi
 * @date 2023-12-03
 */
@RestController
@RequestMapping("/space")
public class SpaceAuditController extends BaseController
{
    @Autowired
    private ISpaceAuditService spaceAuditService;

    /**
     * 查询车位审核列表
     */
    @RequiresPermissions("audit:space:list")
    @GetMapping("/list")
    public TableDataInfo list(SpaceAudit spaceAudit)
    {
        startPage();
        List<SpaceAudit> list = spaceAuditService.selectSpaceAuditList(spaceAudit);
        return getDataTable(list);
    }

    /**
     * 导出车位审核列表
     */
    @RequiresPermissions("audit:space:export")
    @Log(title = "车位审核", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, SpaceAudit spaceAudit)
    {
        List<SpaceAudit> list = spaceAuditService.selectSpaceAuditList(spaceAudit);
        ExcelUtil<SpaceAudit> util = new ExcelUtil<SpaceAudit>(SpaceAudit.class);
        util.exportExcel(response, list, "车位审核数据");
    }

    /**
     * 获取车位审核详细信息
     */
    @RequiresPermissions("audit:space:query")
    @GetMapping(value = "/{said}")
    public AjaxResult getInfo(@PathVariable("said") Long said)
    {
        return success(spaceAuditService.selectSpaceAuditBySaid(said));
    }

    /**
     * 新增车位审核
     */
    @RequiresPermissions("audit:space:add")
    @Log(title = "车位审核", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody SpaceAudit spaceAudit)
    {
        return toAjax(spaceAuditService.insertSpaceAudit(spaceAudit));
    }

    /**
     * 修改车位审核
     */
    @RequiresPermissions("audit:space:edit")
    @Log(title = "车位审核", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody SpaceAudit spaceAudit)
    {
        return toAjax(spaceAuditService.updateSpaceAudit(spaceAudit));
    }

    /**
     * 删除车位审核
     */
    @RequiresPermissions("audit:space:remove")
    @Log(title = "车位审核", businessType = BusinessType.DELETE)
	@DeleteMapping("/{saids}")
    public AjaxResult remove(@PathVariable Long[] saids)
    {
        return toAjax(spaceAuditService.deleteSpaceAuditBySaids(saids));
    }
}
