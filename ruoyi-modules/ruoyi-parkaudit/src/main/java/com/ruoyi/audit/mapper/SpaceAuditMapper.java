package com.ruoyi.audit.mapper;

import com.ruoyi.domain.SpaceAudit;

import java.util.List;


/**
 * 车位审核Mapper接口
 *
 * @author ruoyi
 * @date 2023-12-03
 */
public interface SpaceAuditMapper
{
    /**
     * 查询车位审核
     *
     * @param said 车位审核主键
     * @return 车位审核
     */
    public SpaceAudit selectSpaceAuditBySaid(Long said);

    /**
     * 查询车位审核列表
     *
     * @param spaceAudit 车位审核
     * @return 车位审核集合
     */
    public List<SpaceAudit> selectSpaceAuditList(SpaceAudit spaceAudit);

    /**
     * 新增车位审核
     *
     * @param spaceAudit 车位审核
     * @return 结果
     */
    public int insertSpaceAudit(SpaceAudit spaceAudit);

    /**
     * 修改车位审核
     *
     * @param spaceAudit 车位审核
     * @return 结果
     */
    public int updateSpaceAudit(SpaceAudit spaceAudit);

    /**
     * 删除车位审核
     *
     * @param said 车位审核主键
     * @return 结果
     */
    public int deleteSpaceAuditBySaid(Long said);

    /**
     * 批量删除车位审核
     *
     * @param saids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteSpaceAuditBySaids(Long[] saids);
}
