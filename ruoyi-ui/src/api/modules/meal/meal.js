import request from '@/utils/request'

// 查询车辆套餐列表
export function listMeal(query) {
  return request({
    url: '/sys-meal/meal/list',
    method: 'get',
    params: query
  })
}

// 查询车辆套餐详细
export function getMeal(cmid) {
  return request({
    url: '/sys-meal/meal/' + cmid,
    method: 'get'
  })
}

// 新增车辆套餐
export function addMeal(data) {
  return request({
    url: '/sys-meal/meal',
    method: 'post',
    data: data
  })
}

// 修改车辆套餐
export function updateMeal(data) {
  return request({
    url: '/sys-meal/meal',
    method: 'put',
    data: data
  })
}

// 删除车辆套餐
export function delMeal(cmid) {
  return request({
    url: '/sys-meal/meal/' + cmid,
    method: 'delete'
  })
}

// 获取车场集合
export function getCarsHomeApi(query) {
  return request({
    url: '/sys-cars-home/home/list',
    method: 'get',
    params: query
  })
}
//
// // 获取车场集合
// export function getCarsHomeApi(query) {
//   return request({
//     url: '/sys-cars-home/home/list',
//     method: 'get',
//     params: query
//   })
// }
//
// // 获取车场集合
// export function getCarsHomeApi(query) {
//   return request({
//     url: '/sys-cars-home/home/list',
//     method: 'get',
//     params: query
//   })
// }

