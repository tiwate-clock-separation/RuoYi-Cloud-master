package com.ruoyi.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;

import com.ruoyi.domain.CarsHome;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.log.annotation.Log;
import com.ruoyi.common.log.enums.BusinessType;
import com.ruoyi.common.security.annotation.RequiresPermissions;
import com.ruoyi.service.ICarsHomeService;
import com.ruoyi.common.core.web.controller.BaseController;
import com.ruoyi.common.core.web.domain.AjaxResult;
import com.ruoyi.common.core.utils.poi.ExcelUtil;
import com.ruoyi.common.core.web.page.TableDataInfo;

/**
 * 车场管理Controller
 *
 * @author chn
 * @date 2023-11-29
 */
@RestController
@RequestMapping("/home")
public class CarsHomeController extends BaseController
{
    @Autowired
    private ICarsHomeService carsHomeService;

    /**
     * 查询车场管理列表
     */
    @RequiresPermissions("sys-cars-home:home:list")
    @GetMapping("/list")
    public TableDataInfo list(CarsHome carsHome)
    {
        startPage();
        List<CarsHome> list = carsHomeService.selectCarsHomeList(carsHome);
        return getDataTable(list);
    }

    /**
     * 导出车场管理列表
     */
    @RequiresPermissions("sys-cars-home:home:export")
    @Log(title = "车场管理", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, CarsHome carsHome)
    {
        List<CarsHome> list = carsHomeService.selectCarsHomeList(carsHome);
        ExcelUtil<CarsHome> util = new ExcelUtil<CarsHome>(CarsHome.class);
        util.exportExcel(response, list, "车场管理数据");
    }

    /**
     * 获取车场管理详细信息
     */
    @RequiresPermissions("sys-cars-home:home:query")
    @GetMapping(value = "/{chid}")
    public AjaxResult getInfo(@PathVariable("chid") Long chid)
    {
        return success(carsHomeService.selectCarsHomeByChid(chid));
    }

    /**
     * 新增车场管理
     */
    @RequiresPermissions("sys-cars-home:home:add")
    @Log(title = "车场管理", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody CarsHome carsHome)
    {
        return toAjax(carsHomeService.insertCarsHome(carsHome));
    }

    /**
     * 修改车场管理
     */
    @RequiresPermissions("sys-cars-home:home:edit")
    @Log(title = "车场管理", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody CarsHome carsHome)
    {
        return toAjax(carsHomeService.updateCarsHome(carsHome));
    }

    /**
     * 删除车场管理
     */
    @RequiresPermissions("sys-cars-home:home:remove")
    @Log(title = "车场管理", businessType = BusinessType.DELETE)
    @DeleteMapping("/{chids}")
    public AjaxResult remove(@PathVariable Long[] chids)
    {
        return toAjax(carsHomeService.deleteCarsHomeByChids(chids));
    }




}
