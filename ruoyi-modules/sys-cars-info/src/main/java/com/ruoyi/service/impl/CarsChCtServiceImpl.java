package com.ruoyi.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.mapper.CarsChCtMapper;
import com.ruoyi.domain.CarsChCt;
import com.ruoyi.service.ICarsChCtService;

/**
 * 【请填写功能名称】Service业务层处理
 *
 * @author ruoyi
 * @date 2023-12-12
 */
@Service
public class CarsChCtServiceImpl implements ICarsChCtService
{
    @Autowired
    private CarsChCtMapper carsChCtMapper;

    /**
     * 查询【请填写功能名称】
     *
     * @param chid 【请填写功能名称】主键
     * @return 【请填写功能名称】
     */
    @Override
    public CarsChCt selectCarsChCtByChid(Long chid)
    {
        return carsChCtMapper.selectCarsChCtByChid(chid);
    }

    /**
     * 查询【请填写功能名称】列表
     *
     * @param carsChCt 【请填写功能名称】
     * @return 【请填写功能名称】
     */
    @Override
    public List<CarsChCt> selectCarsChCtList(CarsChCt carsChCt)
    {
        return carsChCtMapper.selectCarsChCtList(carsChCt);
    }

    /**
     * 新增【请填写功能名称】
     *
     * @param carsChCt 【请填写功能名称】
     * @return 结果
     */
    @Override
    public int insertCarsChCt(CarsChCt carsChCt)
    {
        return carsChCtMapper.insertCarsChCt(carsChCt);
    }

    /**
     * 修改【请填写功能名称】
     *
     * @param carsChCt 【请填写功能名称】
     * @return 结果
     */
    @Override
    public int updateCarsChCt(CarsChCt carsChCt)
    {
        return carsChCtMapper.updateCarsChCt(carsChCt);
    }

    /**
     * 批量删除【请填写功能名称】
     *
     * @param chids 需要删除的【请填写功能名称】主键
     * @return 结果
     */
    @Override
    public int deleteCarsChCtByChids(Long[] chids)
    {
        return carsChCtMapper.deleteCarsChCtByChids(chids);
    }

    /**
     * 删除【请填写功能名称】信息
     *
     * @param chid 【请填写功能名称】主键
     * @return 结果
     */
    @Override
    public int deleteCarsChCtByChid(Long chid)
    {
        return carsChCtMapper.deleteCarsChCtByChid(chid);
    }
}
