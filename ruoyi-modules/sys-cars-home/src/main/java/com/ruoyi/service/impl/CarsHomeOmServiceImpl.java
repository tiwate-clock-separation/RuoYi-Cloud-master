package com.ruoyi.service.impl;

import java.util.List;
import com.ruoyi.common.core.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.mapper.CarsHomeOmMapper;
import com.ruoyi.domain.CarsHomeOm;
import com.ruoyi.service.ICarsHomeOmService;

/**
 * 车场运维Service业务层处理
 * 
 * @author chn
 * @date 2023-12-01
 */
@Service
public class CarsHomeOmServiceImpl implements ICarsHomeOmService 
{
    @Autowired
    private CarsHomeOmMapper carsHomeOmMapper;

    /**
     * 查询车场运维
     * 
     * @param code 车场运维主键
     * @return 车场运维
     */
    @Override
    public CarsHomeOm selectCarsHomeOmByCode(Long code)
    {
        return carsHomeOmMapper.selectCarsHomeOmByCode(code);
    }

    /**
     * 查询车场运维列表
     * 
     * @param carsHomeOm 车场运维
     * @return 车场运维
     */
    @Override
    public List<CarsHomeOm> selectCarsHomeOmList(CarsHomeOm carsHomeOm)
    {
        return carsHomeOmMapper.selectCarsHomeOmList(carsHomeOm);
    }

    /**
     * 新增车场运维
     * 
     * @param carsHomeOm 车场运维
     * @return 结果
     */
    @Override
    public int insertCarsHomeOm(CarsHomeOm carsHomeOm)
    {
        carsHomeOm.setCreateTime(DateUtils.getNowDate());
        return carsHomeOmMapper.insertCarsHomeOm(carsHomeOm);
    }

    /**
     * 修改车场运维
     * 
     * @param carsHomeOm 车场运维
     * @return 结果
     */
    @Override
    public int updateCarsHomeOm(CarsHomeOm carsHomeOm)
    {
        carsHomeOm.setUpdateTime(DateUtils.getNowDate());
        return carsHomeOmMapper.updateCarsHomeOm(carsHomeOm);
    }

    /**
     * 批量删除车场运维
     * 
     * @param codes 需要删除的车场运维主键
     * @return 结果
     */
    @Override
    public int deleteCarsHomeOmByCodes(Long[] codes)
    {
        return carsHomeOmMapper.deleteCarsHomeOmByCodes(codes);
    }

    /**
     * 删除车场运维信息
     * 
     * @param code 车场运维主键
     * @return 结果
     */
    @Override
    public int deleteCarsHomeOmByCode(Long code)
    {
        return carsHomeOmMapper.deleteCarsHomeOmByCode(code);
    }
}
