package com.ruoyi.system.service;

import java.util.List;
import com.ruoyi.domain.MerchantManagement;

/**
 * 商家管理Service接口
 *
 * @author ruoyi
 * @date 2023-12-04
 */
public interface IMerchantManagementService
{
    /**
     * 查询商家管理
     *
     * @param pmid 商家管理主键
     * @return 商家管理
     */
    public MerchantManagement selectMerchantManagementByPmid(Long pmid);

    /**
     * 查询商家管理列表
     *
     * @param merchantManagement 商家管理
     * @return 商家管理集合
     */
    public List<MerchantManagement> selectMerchantManagementList(MerchantManagement merchantManagement);

    /**
     * 新增商家管理
     *
     * @param merchantManagement 商家管理
     * @return 结果
     */
    public int insertMerchantManagement(MerchantManagement merchantManagement);

    /**
     * 修改商家管理
     *
     * @param merchantManagement 商家管理
     * @return 结果
     */
    public int updateMerchantManagement(MerchantManagement merchantManagement);

    /**
     * 商家充值
     *
     * @param  topupNum 商家充值金额
     * @param  pmid 商家
     * @return 结果
     */
    public int topup(Integer pmid,Integer topupNum);

    /**
     * 批量删除商家管理
     *
     * @param pmids 需要删除的商家管理主键集合
     * @return 结果
     */
    public int deleteMerchantManagementByPmids(Long[] pmids);

    /**
     * 删除商家管理信息
     *
     * @param pmid 商家管理主键
     * @return 结果
     */
    public int deleteMerchantManagementByPmid(Long pmid);
}
