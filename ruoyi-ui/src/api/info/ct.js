import request from '@/utils/request'

// 查询【请填写功能名称】列表
export function listCt(query) {
  return request({
    url: '/sys-cars-info/ct/list',
    method: 'get',
    params: query
  })
}

// 查询【请填写功能名称】详细
export function getCt(chid) {
  return request({
    url: '/sys-cars-info/ct/' + chid,
    method: 'get'
  })
}

// 新增【请填写功能名称】
export function addCt(data) {
  return request({
    url: '/sys-cars-info/ct',
    method: 'post',
    data: data
  })
}

// 修改【请填写功能名称】
export function updateCt(data) {
  return request({
    url: '/sys-cars-info/ct',
    method: 'put',
    data: data
  })
}

// 删除【请填写功能名称】
export function delCt(chid) {
  return request({
    url: '/sys-cars-info/ct/' + chid,
    method: 'delete'
  })
}
