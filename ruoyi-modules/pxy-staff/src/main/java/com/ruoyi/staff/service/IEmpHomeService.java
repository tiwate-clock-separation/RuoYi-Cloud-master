package com.ruoyi.staff.service;

import java.util.List;
import com.ruoyi.domain.EmpHome;

/**
 * 物业人员Service接口
 * 
 * @author ruoyi
 * @date 2023-12-23
 */
public interface IEmpHomeService 
{
    /**
     * 查询物业人员
     * 
     * @param ehid 物业人员主键
     * @return 物业人员
     */
    public EmpHome selectEmpHomeByEhid(Long ehid);

    /**
     * 查询物业人员列表
     * 
     * @param empHome 物业人员
     * @return 物业人员集合
     */
    public List<EmpHome> selectEmpHomeList(EmpHome empHome);

    /**
     * 新增物业人员
     * 
     * @param empHome 物业人员
     * @return 结果
     */
    public int insertEmpHome(EmpHome empHome);

    /**
     * 修改物业人员
     * 
     * @param empHome 物业人员
     * @return 结果
     */
    public int updateEmpHome(EmpHome empHome);

    /**
     * 批量删除物业人员
     * 
     * @param ehids 需要删除的物业人员主键集合
     * @return 结果
     */
    public int deleteEmpHomeByEhids(Long[] ehids);

    /**
     * 删除物业人员信息
     * 
     * @param ehid 物业人员主键
     * @return 结果
     */
    public int deleteEmpHomeByEhid(Long ehid);
}
