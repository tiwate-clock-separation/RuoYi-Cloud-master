package com.ruoyi.controller;

import java.util.List;
import java.io.IOException;
import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.log.annotation.Log;
import com.ruoyi.common.log.enums.BusinessType;
import com.ruoyi.common.security.annotation.RequiresPermissions;
import com.ruoyi.domain.CarsHomeTollDay;
import com.ruoyi.service.ICarsHomeTollDayService;
import com.ruoyi.common.core.web.controller.BaseController;
import com.ruoyi.common.core.web.domain.AjaxResult;
import com.ruoyi.common.core.utils.poi.ExcelUtil;
import com.ruoyi.common.core.web.page.TableDataInfo;

/**
 * 按天收费Controller
 * 
 * @author chn
 * @date 2023-12-14
 */
@RestController
@RequestMapping("/day")
public class CarsHomeTollDayController extends BaseController
{
    @Autowired
    private ICarsHomeTollDayService carsHomeTollDayService;

    /**
     * 查询按天收费列表
     */
    @RequiresPermissions("sys-cars-home:day:list")
    @GetMapping("/list")
    public TableDataInfo list(CarsHomeTollDay carsHomeTollDay)
    {
        startPage();
        List<CarsHomeTollDay> list = carsHomeTollDayService.selectCarsHomeTollDayList(carsHomeTollDay);
        return getDataTable(list);
    }

    /**
     * 导出按天收费列表
     */
    @RequiresPermissions("sys-cars-home:day:export")
    @Log(title = "按天收费", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, CarsHomeTollDay carsHomeTollDay)
    {
        List<CarsHomeTollDay> list = carsHomeTollDayService.selectCarsHomeTollDayList(carsHomeTollDay);
        ExcelUtil<CarsHomeTollDay> util = new ExcelUtil<CarsHomeTollDay>(CarsHomeTollDay.class);
        util.exportExcel(response, list, "按天收费数据");
    }

    /**
     * 获取按天收费详细信息
     */
    @RequiresPermissions("sys-cars-home:day:query")
    @GetMapping(value = "/{dayid}")
    public AjaxResult getInfo(@PathVariable("dayid") Long dayid)
    {
        return success(carsHomeTollDayService.selectCarsHomeTollDayByDayid(dayid));
    }

    /**
     * 新增按天收费
     */
    @RequiresPermissions("sys-cars-home:day:add")
    @Log(title = "按天收费", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody CarsHomeTollDay carsHomeTollDay)
    {
        return toAjax(carsHomeTollDayService.insertCarsHomeTollDay(carsHomeTollDay));
    }

    /**
     * 修改按天收费
     */
    @RequiresPermissions("sys-cars-home:day:edit")
    @Log(title = "按天收费", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody CarsHomeTollDay carsHomeTollDay)
    {
        return toAjax(carsHomeTollDayService.updateCarsHomeTollDay(carsHomeTollDay));
    }

    /**
     * 删除按天收费
     */
    @RequiresPermissions("sys-cars-home:day:remove")
    @Log(title = "按天收费", businessType = BusinessType.DELETE)
	@DeleteMapping("/{dayids}")
    public AjaxResult remove(@PathVariable Long[] dayids)
    {
        return toAjax(carsHomeTollDayService.deleteCarsHomeTollDayByDayids(dayids));
    }
}
