package com.ruoyi.system.service.impl;

import java.util.List;
import com.ruoyi.common.core.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.OperationRecordMapper;
import com.ruoyi.system.domain.OperationRecord;
import com.ruoyi.system.service.IOperationRecordService;

/**
 * 设备操作记录Service业务层处理
 * 
 * @author ruoyi
 * @date 2023-11-29
 */
@Service
public class OperationRecordServiceImpl implements IOperationRecordService 
{
    @Autowired
    private OperationRecordMapper operationRecordMapper;

    /**
     * 查询设备操作记录
     * 
     * @param orid 设备操作记录主键
     * @return 设备操作记录
     */
    @Override
    public OperationRecord selectOperationRecordByOrid(Long orid)
    {
        return operationRecordMapper.selectOperationRecordByOrid(orid);
    }

    /**
     * 查询设备操作记录列表
     * 
     * @param operationRecord 设备操作记录
     * @return 设备操作记录
     */
    @Override
    public List<OperationRecord> selectOperationRecordList(OperationRecord operationRecord)
    {
        return operationRecordMapper.selectOperationRecordList(operationRecord);
    }

    /**
     * 新增设备操作记录
     * 
     * @param operationRecord 设备操作记录
     * @return 结果
     */
    @Override
    public int insertOperationRecord(OperationRecord operationRecord)
    {
        operationRecord.setCreateTime(DateUtils.getNowDate());
        return operationRecordMapper.insertOperationRecord(operationRecord);
    }

    /**
     * 修改设备操作记录
     * 
     * @param operationRecord 设备操作记录
     * @return 结果
     */
    @Override
    public int updateOperationRecord(OperationRecord operationRecord)
    {
        operationRecord.setUpdateTime(DateUtils.getNowDate());
        return operationRecordMapper.updateOperationRecord(operationRecord);
    }

    /**
     * 批量删除设备操作记录
     * 
     * @param orids 需要删除的设备操作记录主键
     * @return 结果
     */
    @Override
    public int deleteOperationRecordByOrids(Long[] orids)
    {
        return operationRecordMapper.deleteOperationRecordByOrids(orids);
    }

    /**
     * 删除设备操作记录信息
     * 
     * @param orid 设备操作记录主键
     * @return 结果
     */
    @Override
    public int deleteOperationRecordByOrid(Long orid)
    {
        return operationRecordMapper.deleteOperationRecordByOrid(orid);
    }
}
