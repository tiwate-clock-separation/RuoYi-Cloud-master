package com.ruoyi.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;

import com.ruoyi.domain.CartsBlacklist;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.log.annotation.Log;
import com.ruoyi.common.log.enums.BusinessType;
import com.ruoyi.common.security.annotation.RequiresPermissions;
import com.ruoyi.service.ICartsBlacklistService;
import com.ruoyi.common.core.web.controller.BaseController;
import com.ruoyi.common.core.web.domain.AjaxResult;
import com.ruoyi.common.core.utils.poi.ExcelUtil;
import com.ruoyi.common.core.web.page.TableDataInfo;

/**
 * 车辆黑名单Controller
 * 
 * @author ruoyi
 * @date 2023-12-13
 */
@RestController
@RequestMapping("/blacklist")
public class CartsBlacklistController extends BaseController
{
    @Autowired
    private ICartsBlacklistService cartsBlacklistService;

    /**
     * 查询车辆黑名单列表
     */
    @RequiresPermissions("info:blacklist:list")
    @GetMapping("/list")
    public TableDataInfo list(CartsBlacklist cartsBlacklist)
    {
        startPage();
        List<CartsBlacklist> list = cartsBlacklistService.selectCartsBlacklistList(cartsBlacklist);
        return getDataTable(list);
    }

    /**
     * 导出车辆黑名单列表
     */
    @RequiresPermissions("info:blacklist:export")
    @Log(title = "车辆黑名单", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, CartsBlacklist cartsBlacklist)
    {
        List<CartsBlacklist> list = cartsBlacklistService.selectCartsBlacklistList(cartsBlacklist);
        ExcelUtil<CartsBlacklist> util = new ExcelUtil<CartsBlacklist>(CartsBlacklist.class);
        util.exportExcel(response, list, "车辆黑名单数据");
    }

    /**
     * 获取车辆黑名单详细信息
     */
    @RequiresPermissions("info:blacklist:query")
    @GetMapping(value = "/{cbid}")
    public AjaxResult getInfo(@PathVariable("cbid") Long cbid)
    {
        return success(cartsBlacklistService.selectCartsBlacklistByCbid(cbid));
    }

    /**
     * 新增车辆黑名单
     */
    @RequiresPermissions("info:blacklist:add")
    @Log(title = "车辆黑名单", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody CartsBlacklist cartsBlacklist)
    {
        return toAjax(cartsBlacklistService.insertCartsBlacklist(cartsBlacklist));
    }

    /**
     * 修改车辆黑名单
     */
    @RequiresPermissions("info:blacklist:edit")
    @Log(title = "车辆黑名单", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody CartsBlacklist cartsBlacklist)
    {
        return toAjax(cartsBlacklistService.updateCartsBlacklist(cartsBlacklist));
    }

    /**
     * 删除车辆黑名单
     */
    @RequiresPermissions("info:blacklist:remove")
    @Log(title = "车辆黑名单", businessType = BusinessType.DELETE)
	@DeleteMapping("/{cbids}")
    public AjaxResult remove(@PathVariable Long[] cbids)
    {
        return toAjax(cartsBlacklistService.deleteCartsBlacklistByCbids(cbids));
    }
}
