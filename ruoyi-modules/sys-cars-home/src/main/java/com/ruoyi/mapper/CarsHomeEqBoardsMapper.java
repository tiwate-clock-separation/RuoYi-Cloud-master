package com.ruoyi.mapper;

import java.util.List;
import com.ruoyi.domain.CarsHomeEqBoards;

/**
 * 车场设备板卡Mapper接口
 * 
 * @author chn
 * @date 2023-12-07
 */
public interface CarsHomeEqBoardsMapper 
{
    /**
     * 查询车场设备板卡
     * 
     * @param boardsid 车场设备板卡主键
     * @return 车场设备板卡
     */
    public CarsHomeEqBoards selectCarsHomeEqBoardsByBoardsid(Long boardsid);

    /**
     * 查询车场设备板卡列表
     * 
     * @param carsHomeEqBoards 车场设备板卡
     * @return 车场设备板卡集合
     */
    public List<CarsHomeEqBoards> selectCarsHomeEqBoardsList(CarsHomeEqBoards carsHomeEqBoards);

    /**
     * 新增车场设备板卡
     * 
     * @param carsHomeEqBoards 车场设备板卡
     * @return 结果
     */
    public int insertCarsHomeEqBoards(CarsHomeEqBoards carsHomeEqBoards);

    /**
     * 修改车场设备板卡
     * 
     * @param carsHomeEqBoards 车场设备板卡
     * @return 结果
     */
    public int updateCarsHomeEqBoards(CarsHomeEqBoards carsHomeEqBoards);

    /**
     * 删除车场设备板卡
     * 
     * @param boardsid 车场设备板卡主键
     * @return 结果
     */
    public int deleteCarsHomeEqBoardsByBoardsid(Long boardsid);

    /**
     * 批量删除车场设备板卡
     * 
     * @param boardsids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteCarsHomeEqBoardsByBoardsids(Long[] boardsids);
}
