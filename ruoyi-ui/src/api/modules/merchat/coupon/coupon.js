import request from '@/utils/request'

// 查询优惠卷管理列表
export function listManagement(query) {
  return request({
    url: '/sys-coupon/coupon/list',
    method: 'get',
    params: query
  })
}

// 查询优惠卷管理详细
export function getManagement(cmid) {
  return request({
    url: '/sys-coupon/coupon/' + cmid,
    method: 'get'
  })
}

// 新增优惠卷管理
export function addManagement(data) {
  return request({
    url: '/sys-coupon/coupon',
    method: 'post',
    data: data
  })
}

// 修改优惠卷管理
export function updateManagement(data) {
  return request({
    url: '/sys-coupon/coupon',
    method: 'put',
    data: data
  })
}

// 删除优惠卷管理
export function delManagement(cmid) {
  return request({
    url: '/sys-coupon/coupon/' + cmid,
    method: 'delete'
  })
}
