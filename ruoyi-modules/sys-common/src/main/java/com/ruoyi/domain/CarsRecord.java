package com.ruoyi.domain;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.core.annotation.Excel;
import com.ruoyi.common.core.web.domain.BaseEntity;

/**
 * 进出记录对象 cars_record
 *
 * @author ruoyi
 * @date 2023-11-30
 */
public class CarsRecord extends BaseEntity {
    private static final long serialVersionUID = 1L;

    /**
     * 自增ID
     */
    private Long crid;

    /**
     * 唯一编码,前台使用
     */
    @Excel(name = "唯一编码,前台使用")
    private String code;

    /**
     * 状态
     */
    @Excel(name = "状态")
    private Long status;

    /**
     * 是否删除
     */
    @Excel(name = "是否删除")
    private Long deleted;

    /**
     * 车辆类型
     */
    @Excel(name = "车辆类型")
    private Long ctid;

    /**
     * 进场时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @Excel(name = "进场时间", width = 30, dateFormat = "yyyy-MM-dd HH:mm:ss")
    private Date intoTime;

    /**
     * 出场时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @Excel(name = "出场时间", width = 30, dateFormat = "yyyy-MM-dd HH:mm:ss")
    private Date outsideTime;

    /**
     * 进场设备
     */
    @Excel(name = "进场设备")
    private String intoDevice;

    /**
     * 出场设备
     */
    @Excel(name = "出场设备")
    private String outsideDevice;

    /**
     * 停车状态
     */
    @Excel(name = "停车状态")
    private Long padStatus;

    /**
     * 进场照片
     */
    @Excel(name = "进场照片")
    private String intoPic;

    /**
     * 出场照片
     */
    @Excel(name = "出场照片")
    private String outsidePic;

    /**
     * 车牌颜色(看情况拆表)
     */
    @Excel(name = "车牌颜色(看情况拆表)")
    private String acrtsNumColor;

    /**
     * 车牌号
     */
    @Excel(name = "车牌号")
    private String cartsNum;

    /**
     * 小区id
     */
    @Excel(name = "小区id")
    private Long pmid;

    /**
     * 停车场id
     */
    @Excel(name = "停车场id")
    private Long hpid;

    private CarsHome carsHome;

    private PropertyManagement propertyManagement;

    private CarsType carsType;

    private CarsMsg carsMsg;

    private CarsHomeEqChukou carsHomeEqChukou;

    private CarsHomeEq carsHomeEq;

    public CarsHomeEqChukou getCarsHomeEqChukou() {
        return carsHomeEqChukou;
    }

    public void setCarsHomeEqChukou(CarsHomeEqChukou carsHomeEqChukou) {
        this.carsHomeEqChukou = carsHomeEqChukou;
    }

    public CarsHomeEq getCarsHomeEq() {
        return carsHomeEq;
    }

    public void setCarsHomeEq(CarsHomeEq carsHomeEq) {
        this.carsHomeEq = carsHomeEq;
    }

    private CarsPlateColors carsPlateColors;

    public CarsMsg getCarsMsg() {
        return carsMsg;
    }

    public void setCarsMsg(CarsMsg carsMsg) {
        this.carsMsg = carsMsg;
    }

    public CarsPlateColors getCarsPlateColors() {
        return carsPlateColors;
    }

    public void setCarsPlateColors(CarsPlateColors carsPlateColors) {
        this.carsPlateColors = carsPlateColors;
    }

    public CarsType getCarsType() {
        return carsType;
    }

    public void setCarsType(CarsType carsType) {
        this.carsType = carsType;
    }

    public PropertyManagement getPropertyManagement() {
        return propertyManagement;
    }

    public void setPropertyManagement(PropertyManagement propertyManagement) {
        this.propertyManagement = propertyManagement;
    }

    public CarsHome getCarsHome() {
        return carsHome;
    }

    public void setCarsHome(CarsHome carsHome) {
        this.carsHome = carsHome;
    }

    public void setCrid(Long crid) {
        this.crid = crid;
    }

    public Long getCrid() {
        return crid;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getCode() {
        return code;
    }

    public void setStatus(Long status) {
        this.status = status;
    }

    public Long getStatus() {
        return status;
    }

    public void setDeleted(Long deleted) {
        this.deleted = deleted;
    }

    public Long getDeleted() {
        return deleted;
    }

    public void setCtid(Long ctid) {
        this.ctid = ctid;
    }

    public Long getCtid() {
        return ctid;
    }

    public void setIntoTime(Date intoTime) {
        this.intoTime = intoTime;
    }

    public Date getIntoTime() {
        return intoTime;
    }

    public void setOutsideTime(Date outsideTime) {
        this.outsideTime = outsideTime;
    }

    public Date getOutsideTime() {
        return outsideTime;
    }

    public void setIntoDevice(String intoDevice) {
        this.intoDevice = intoDevice;
    }

    public String getIntoDevice() {
        return intoDevice;
    }

    public void setOutsideDevice(String outsideDevice) {
        this.outsideDevice = outsideDevice;
    }

    public String getOutsideDevice() {
        return outsideDevice;
    }

    public void setPadStatus(Long padStatus) {
        this.padStatus = padStatus;
    }

    public Long getPadStatus() {
        return padStatus;
    }

    public void setIntoPic(String intoPic) {
        this.intoPic = intoPic;
    }

    public String getIntoPic() {
        return intoPic;
    }

    public void setOutsidePic(String outsidePic) {
        this.outsidePic = outsidePic;
    }

    public String getOutsidePic() {
        return outsidePic;
    }

    public void setAcrtsNumColor(String acrtsNumColor) {
        this.acrtsNumColor = acrtsNumColor;
    }

    public String getAcrtsNumColor() {
        return acrtsNumColor;
    }

    public void setCartsNum(String cartsNum) {
        this.cartsNum = cartsNum;
    }

    public String getCartsNum() {
        return cartsNum;
    }

    public Long getPmid() {
        return pmid;
    }

    public void setPmid(Long pmid) {
        this.pmid = pmid;
    }

    public void setHpid(Long hpid) {
        this.hpid = hpid;
    }

    public Long getHpid() {
        return hpid;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this, ToStringStyle.MULTI_LINE_STYLE)
                .append("crid", getCrid())
                .append("code", getCode())
                .append("status", getStatus())
                .append("deleted", getDeleted())
                .append("createTime", getCreateTime())
                .append("updateTime", getUpdateTime())
                .append("ctid", getCtid())
                .append("intoTime", getIntoTime())
                .append("outsideTime", getOutsideTime())
                .append("intoDevice", getIntoDevice())
                .append("outsideDevice", getOutsideDevice())
                .append("padStatus", getPadStatus())
                .append("intoPic", getIntoPic())
                .append("outsidePic", getOutsidePic())
                .append("acrtsNumColor", getAcrtsNumColor())
                .append("cartsNum", getCartsNum())
                .append("pmid", getPmid())
                .append("hpid", getHpid())
                .toString();
    }
}
