package com.ruoyi.system.api;

import com.ruoyi.common.core.web.page.TableDataInfo;
import com.ruoyi.domain.CarsHome;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.GetMapping;

@Component
@FeignClient("sys-cars-home")
public interface CarsHomeApi {
    @GetMapping("/home/list")
    public TableDataInfo list(CarsHome carsHome);
}
