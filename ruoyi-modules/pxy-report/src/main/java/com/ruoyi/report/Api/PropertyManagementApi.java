package com.ruoyi.report.Api;

import com.ruoyi.common.core.web.page.TableDataInfo;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.GetMapping;

@Component
@FeignClient(value = "sys-property")
public interface PropertyManagementApi {

    @GetMapping("/property/list")
    TableDataInfo list();
}
