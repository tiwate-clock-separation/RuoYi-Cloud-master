package com.ruoyi.system.mapper;

import java.util.List;
import com.ruoyi.domain.CouponManagement;

/**
 * 优惠卷管理Mapper接口
 * 
 * @author lbw
 * @date 2023-12-07
 */
public interface CouponManagementMapper 
{
    /**
     * 查询优惠卷管理
     * 
     * @param cmid 优惠卷管理主键
     * @return 优惠卷管理
     */
    public CouponManagement selectCouponManagementByCmid(Long cmid);

    /**
     * 查询优惠卷管理列表
     * 
     * @param couponManagement 优惠卷管理
     * @return 优惠卷管理集合
     */
    public List<CouponManagement> selectCouponManagementList(CouponManagement couponManagement);

    /**
     * 新增优惠卷管理
     * 
     * @param couponManagement 优惠卷管理
     * @return 结果
     */
    public int insertCouponManagement(CouponManagement couponManagement);

    /**
     * 修改优惠卷管理
     * 
     * @param couponManagement 优惠卷管理
     * @return 结果
     */
    public int updateCouponManagement(CouponManagement couponManagement);

    /**
     * 删除优惠卷管理
     * 
     * @param cmid 优惠卷管理主键
     * @return 结果
     */
    public int deleteCouponManagementByCmid(Long cmid);

    /**
     * 批量删除优惠卷管理
     * 
     * @param cmids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteCouponManagementByCmids(Long[] cmids);
}
