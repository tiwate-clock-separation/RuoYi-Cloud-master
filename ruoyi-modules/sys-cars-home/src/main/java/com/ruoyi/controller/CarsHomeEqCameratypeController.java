package com.ruoyi.controller;

import java.util.List;
import java.io.IOException;
import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.log.annotation.Log;
import com.ruoyi.common.log.enums.BusinessType;
import com.ruoyi.common.security.annotation.RequiresPermissions;
import com.ruoyi.domain.CarsHomeEqCameratype;
import com.ruoyi.service.ICarsHomeEqCameratypeService;
import com.ruoyi.common.core.web.controller.BaseController;
import com.ruoyi.common.core.web.domain.AjaxResult;
import com.ruoyi.common.core.utils.poi.ExcelUtil;
import com.ruoyi.common.core.web.page.TableDataInfo;

/**
 * 车厂设备相机类型Controller
 *
 * @author chn
 * @date 2023-12-07
 */
@RestController
@RequestMapping("/camera")
public class CarsHomeEqCameratypeController extends BaseController
{
    @Autowired
    private ICarsHomeEqCameratypeService carsHomeEqCameratypeService;

    /**
     * 查询车厂设备相机类型列表
     */
    @RequiresPermissions("sys-cars-home:camera:list")
    @GetMapping("/list")
    public TableDataInfo list(CarsHomeEqCameratype carsHomeEqCameratype)
    {
        startPage();
        List<CarsHomeEqCameratype> list = carsHomeEqCameratypeService.selectCarsHomeEqCameratypeList(carsHomeEqCameratype);
        return getDataTable(list);
    }
//
//    /**
//     * 导出车厂设备相机类型列表
//     */
//    @RequiresPermissions("sys-cars-home:camera:export")
//    @Log(title = "车厂设备相机类型", businessType = BusinessType.EXPORT)
//    @PostMapping("/export")
//    public void export(HttpServletResponse response, CarsHomeEqCameratype carsHomeEqCameratype)
//    {
//        List<CarsHomeEqCameratype> list = carsHomeEqCameratypeService.selectCarsHomeEqCameratypeList(carsHomeEqCameratype);
//        ExcelUtil<CarsHomeEqCameratype> util = new ExcelUtil<CarsHomeEqCameratype>(CarsHomeEqCameratype.class);
//        util.exportExcel(response, list, "车厂设备相机类型数据");
//    }
//
//    /**
//     * 获取车厂设备相机类型详细信息
//     */
//    @RequiresPermissions("sys-cars-home:camera:query")
//    @GetMapping(value = "/{cameraid}")
//    public AjaxResult getInfo(@PathVariable("cameraid") Long cameraid)
//    {
//        return success(carsHomeEqCameratypeService.selectCarsHomeEqCameratypeByCameraid(cameraid));
//    }
//
//    /**
//     * 新增车厂设备相机类型
//     */
//    @RequiresPermissions("sys-cars-home:camera:add")
//    @Log(title = "车厂设备相机类型", businessType = BusinessType.INSERT)
//    @PostMapping
//    public AjaxResult add(@RequestBody CarsHomeEqCameratype carsHomeEqCameratype)
//    {
//        return toAjax(carsHomeEqCameratypeService.insertCarsHomeEqCameratype(carsHomeEqCameratype));
//    }
//
//    /**
//     * 修改车厂设备相机类型
//     */
//    @RequiresPermissions("sys-cars-home:camera:edit")
//    @Log(title = "车厂设备相机类型", businessType = BusinessType.UPDATE)
//    @PutMapping
//    public AjaxResult edit(@RequestBody CarsHomeEqCameratype carsHomeEqCameratype)
//    {
//        return toAjax(carsHomeEqCameratypeService.updateCarsHomeEqCameratype(carsHomeEqCameratype));
//    }
//
//    /**
//     * 删除车厂设备相机类型
//     */
//    @RequiresPermissions("sys-cars-home:camera:remove")
//    @Log(title = "车厂设备相机类型", businessType = BusinessType.DELETE)
//    @DeleteMapping("/{cameraids}")
//    public AjaxResult remove(@PathVariable Long[] cameraids)
//    {
//        return toAjax(carsHomeEqCameratypeService.deleteCarsHomeEqCameratypeByCameraids(cameraids));
//    }
}
