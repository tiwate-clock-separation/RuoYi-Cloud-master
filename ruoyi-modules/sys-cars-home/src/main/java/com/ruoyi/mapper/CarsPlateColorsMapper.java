package com.ruoyi.mapper;

import java.util.List;
import com.ruoyi.domain.CarsPlateColors;

/**
 * 车辆牌照颜色Mapper接口
 * 
 * @author chn
 * @date 2023-12-04
 */
public interface CarsPlateColorsMapper 
{
    /**
     * 查询车辆牌照颜色
     * 
     * @param lpcid 车辆牌照颜色主键
     * @return 车辆牌照颜色
     */
    public CarsPlateColors selectCarsPlateColorsByLpcid(Long lpcid);

    /**
     * 查询车辆牌照颜色列表
     * 
     * @param carsPlateColors 车辆牌照颜色
     * @return 车辆牌照颜色集合
     */
    public List<CarsPlateColors> selectCarsPlateColorsList(CarsPlateColors carsPlateColors);

    /**
     * 新增车辆牌照颜色
     * 
     * @param carsPlateColors 车辆牌照颜色
     * @return 结果
     */
    public int insertCarsPlateColors(CarsPlateColors carsPlateColors);

    /**
     * 修改车辆牌照颜色
     * 
     * @param carsPlateColors 车辆牌照颜色
     * @return 结果
     */
    public int updateCarsPlateColors(CarsPlateColors carsPlateColors);

    /**
     * 删除车辆牌照颜色
     * 
     * @param lpcid 车辆牌照颜色主键
     * @return 结果
     */
    public int deleteCarsPlateColorsByLpcid(Long lpcid);

    /**
     * 批量删除车辆牌照颜色
     * 
     * @param lpcids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteCarsPlateColorsByLpcids(Long[] lpcids);
}
