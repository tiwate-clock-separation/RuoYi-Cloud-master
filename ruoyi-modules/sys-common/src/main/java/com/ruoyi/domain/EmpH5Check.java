package com.ruoyi.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.core.annotation.Excel;
import com.ruoyi.common.core.web.domain.BaseEntity;

/**
 * h5消费对象 emp_H5_check
 * 
 * @author ruoyi
 * @date 2023-12-15
 */
public class EmpH5Check extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 自增ID */
    private Long id;

    /** 唯一编码,前台使用 */
    private String code;

    /** 状态 */
    private Long status;

    /** 是否删除 */
    private Long deleted;

    /** 物业名称 */
    @Excel(name = "物业名称")
    private Long pmid;

    /** 手机号 */
    @Excel(name = "手机号")
    private Long tel;

    /** 费用类型 */
    @Excel(name = "费用类型")
    private Long ttid;

    /** 支付渠道 */
    @Excel(name = "支付渠道")
    private Long wixin;

    /** 支付状态 */
    @Excel(name = "支付状态")
    private Long zhuant;

    /** 支付金额 */
    @Excel(name = "支付金额")
    private Long picete;

    /** 车牌号 */
    @Excel(name = "车牌号")
    private String licename;


    private PropertyManagement propertyManagement;

    private TradingType tradingType;

    public PropertyManagement getPropertyManagement() {
        return propertyManagement;
    }

    public void setPropertyManagement(PropertyManagement propertyManagement) {
        this.propertyManagement = propertyManagement;
    }

    public TradingType getTradingType() {
        return tradingType;
    }

    public void setTradingType(TradingType tradingType) {
        this.tradingType = tradingType;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setCode(String code) 
    {
        this.code = code;
    }

    public String getCode() 
    {
        return code;
    }
    public void setStatus(Long status) 
    {
        this.status = status;
    }

    public Long getStatus() 
    {
        return status;
    }
    public void setDeleted(Long deleted) 
    {
        this.deleted = deleted;
    }

    public Long getDeleted() 
    {
        return deleted;
    }
    public void setPmid(Long pmid) 
    {
        this.pmid = pmid;
    }

    public Long getPmid() 
    {
        return pmid;
    }
    public void setTel(Long tel) 
    {
        this.tel = tel;
    }

    public Long getTel() 
    {
        return tel;
    }
    public void setTtid(Long ttid) 
    {
        this.ttid = ttid;
    }

    public Long getTtid() 
    {
        return ttid;
    }
    public void setWixin(Long wixin) 
    {
        this.wixin = wixin;
    }

    public Long getWixin() 
    {
        return wixin;
    }
    public void setZhuant(Long zhuant) 
    {
        this.zhuant = zhuant;
    }

    public Long getZhuant() 
    {
        return zhuant;
    }
    public void setPicete(Long picete) 
    {
        this.picete = picete;
    }

    public Long getPicete() 
    {
        return picete;
    }
    public void setLicename(String licename) 
    {
        this.licename = licename;
    }

    public String getLicename() 
    {
        return licename;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("code", getCode())
            .append("status", getStatus())
            .append("deleted", getDeleted())
            .append("createTime", getCreateTime())
            .append("updateTime", getUpdateTime())
            .append("pmid", getPmid())
            .append("tel", getTel())
            .append("ttid", getTtid())
            .append("wixin", getWixin())
            .append("zhuant", getZhuant())
            .append("picete", getPicete())
            .append("licename", getLicename())
            .toString();
    }
}
