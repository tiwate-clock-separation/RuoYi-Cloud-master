package com.ruoyi.staff.controller;

import java.util.List;
import java.io.IOException;
import javax.servlet.http.HttpServletResponse;

import com.ruoyi.domain.EmpH5Check;
import com.ruoyi.staff.service.IEmpH5CheckService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.log.annotation.Log;
import com.ruoyi.common.log.enums.BusinessType;
import com.ruoyi.common.security.annotation.RequiresPermissions;
import com.ruoyi.common.core.web.controller.BaseController;
import com.ruoyi.common.core.web.domain.AjaxResult;
import com.ruoyi.common.core.utils.poi.ExcelUtil;
import com.ruoyi.common.core.web.page.TableDataInfo;

/**
 * h5消费Controller
 * 
 * @author ruoyi
 * @date 2023-12-15
 */
@RestController
@RequestMapping("/check")
public class EmpH5CheckController extends BaseController
{
    @Autowired
    private IEmpH5CheckService empH5CheckService;

    /**
     * 查询h5消费列表
     */
    @RequiresPermissions("pxy-staff:check:list")
    @GetMapping("/list")
    public TableDataInfo list(EmpH5Check empH5Check)
    {
        startPage();
        List<EmpH5Check> list = empH5CheckService.selectEmpH5CheckList(empH5Check);
        return getDataTable(list);
    }

    /**
     * 导出h5消费列表
     */
    @RequiresPermissions("pxy-staff:check:export")
    @Log(title = "h5消费", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, EmpH5Check empH5Check)
    {
        List<EmpH5Check> list = empH5CheckService.selectEmpH5CheckList(empH5Check);
        ExcelUtil<EmpH5Check> util = new ExcelUtil<EmpH5Check>(EmpH5Check.class);
        util.exportExcel(response, list, "h5消费数据");
    }

    /**
     * 获取h5消费详细信息
     */
    @RequiresPermissions("pxy-staff:check:query")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(empH5CheckService.selectEmpH5CheckById(id));
    }

    /**
     * 新增h5消费
     */
    @RequiresPermissions("pxy-staff:check:add")
    @Log(title = "h5消费", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody EmpH5Check empH5Check)
    {
        return toAjax(empH5CheckService.insertEmpH5Check(empH5Check));
    }

    /**
     * 修改h5消费
     */
    @RequiresPermissions("pxy-staff:check:edit")
    @Log(title = "h5消费", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody EmpH5Check empH5Check)
    {
        return toAjax(empH5CheckService.updateEmpH5Check(empH5Check));
    }

    /**
     * 删除h5消费
     */
    @RequiresPermissions("pxy-staff:check:remove")
    @Log(title = "h5消费", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(empH5CheckService.deleteEmpH5CheckByIds(ids));
    }
}
