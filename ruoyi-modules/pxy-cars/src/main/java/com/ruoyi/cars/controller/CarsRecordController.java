package com.ruoyi.cars.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;

import com.ruoyi.domain.CarsHomeEq;
import com.ruoyi.domain.CarsHomeEqChukou;
import com.ruoyi.domain.CarsMsg;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.log.annotation.Log;
import com.ruoyi.common.log.enums.BusinessType;
import com.ruoyi.common.security.annotation.RequiresPermissions;
import com.ruoyi.domain.CarsRecord;
import com.ruoyi.cars.service.ICarsRecordService;
import com.ruoyi.common.core.web.controller.BaseController;
import com.ruoyi.common.core.web.domain.AjaxResult;
import com.ruoyi.common.core.utils.poi.ExcelUtil;
import com.ruoyi.common.core.web.page.TableDataInfo;

/**
 * 进出记录Controller
 *
 * @author ruoyi
 * @date 2023-11-30
 */
@RestController
@RequestMapping("/cars")
public class CarsRecordController extends BaseController
{
    @Autowired
    private ICarsRecordService carsRecordService;

    /**
     * 查询进出记录列表
     */
    @RequiresPermissions("pxy-cars:cars:list")
    @GetMapping("/list")
    public TableDataInfo list(CarsRecord carsRecord, CarsMsg carsMsg, CarsHomeEqChukou carsHomeEqChukou, CarsHomeEq carsHomeEq)
    {

        carsRecord.setCarsMsg(carsMsg);
        carsRecord.setCarsHomeEq(carsHomeEq);
        carsRecord.setCarsHomeEqChukou(carsHomeEqChukou);
        startPage();
        List<CarsRecord> list = carsRecordService.selectCarsRecordList(carsRecord);
        return getDataTable(list);
    }

    /**
     * 导出进出记录列表
     */
    @RequiresPermissions("pxy-cars:cars:export")
    @Log(title = "进出记录", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, CarsRecord carsRecord)
    {
        List<CarsRecord> list = carsRecordService.selectCarsRecordList(carsRecord);
        ExcelUtil<CarsRecord> util = new ExcelUtil<CarsRecord>(CarsRecord.class);
        util.exportExcel(response, list, "进出记录数据");
    }

    /**
     * 获取进出记录详细信息
     */
    @RequiresPermissions("pxy-cars:cars:query")
    @GetMapping(value = "/{crid}")
    public AjaxResult getInfo(@PathVariable("crid") Long crid)
    {
        return success(carsRecordService.selectCarsRecordByCrid(crid));
    }

    /**
     * 新增进出记录
     */
    @RequiresPermissions("pxy-cars:cars:add")
    @Log(title = "进出记录", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody CarsRecord carsRecord)
    {
        return toAjax(carsRecordService.insertCarsRecord(carsRecord));
    }

    /**
     * 修改进出记录
     */
    @RequiresPermissions("pxy-cars:cars:edit")
    @Log(title = "进出记录", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody CarsRecord carsRecord)
    {
        return toAjax(carsRecordService.updateCarsRecord(carsRecord));
    }

    /**
     * 删除进出记录
     */
    @RequiresPermissions("pxy-cars:cars:remove")
    @Log(title = "进出记录", businessType = BusinessType.DELETE)
	@DeleteMapping("/{crids}")
    public AjaxResult remove(@PathVariable Long[] crids)
    {
        return toAjax(carsRecordService.deleteCarsRecordByCrids(crids));
    }
}
