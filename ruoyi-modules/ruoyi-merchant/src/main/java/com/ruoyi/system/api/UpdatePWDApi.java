package com.ruoyi.system.api;

import com.ruoyi.auth.form.RegisterBody;
import com.ruoyi.common.core.domain.R;
import com.ruoyi.common.core.web.domain.AjaxResult;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

@Component
@FeignClient("system")
public interface UpdatePWDApi {

    @PostMapping("/user/profile/updatePwd")
    public AjaxResult register(@RequestBody RegisterBody registerBody);

}

