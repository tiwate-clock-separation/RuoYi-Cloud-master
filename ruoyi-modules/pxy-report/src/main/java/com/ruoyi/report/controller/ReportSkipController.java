package com.ruoyi.report.controller;

import java.util.List;
import java.io.IOException;
import javax.servlet.http.HttpServletResponse;

import com.ruoyi.domain.CarsMsg;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.log.annotation.Log;
import com.ruoyi.common.log.enums.BusinessType;
import com.ruoyi.common.security.annotation.RequiresPermissions;
import com.ruoyi.domain.ReportSkip;
import com.ruoyi.report.service.IReportSkipService;
import com.ruoyi.common.core.web.controller.BaseController;
import com.ruoyi.common.core.web.domain.AjaxResult;
import com.ruoyi.common.core.utils.poi.ExcelUtil;
import com.ruoyi.common.core.web.page.TableDataInfo;

/**
 * 逃单报Controller
 * 
 * @author ruoyi
 * @date 2023-12-07
 */
@RestController
@RequestMapping("/skip")
public class ReportSkipController extends BaseController
{
    @Autowired
    private IReportSkipService reportSkipService;

    /**
     * 查询逃单报列表
     */
    @RequiresPermissions("pxy-report:skip:list")
    @GetMapping("/list")
    public TableDataInfo list(ReportSkip reportSkip, CarsMsg carsMsg)
    {
        reportSkip.setCarsMsg(carsMsg);
        startPage();
        List<ReportSkip> list = reportSkipService.selectReportSkipList(reportSkip);
        return getDataTable(list);
    }

    /**
     * 导出逃单报列表
     */
    @RequiresPermissions("pxy-report:skip:export")
    @Log(title = "逃单报", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, ReportSkip reportSkip)
    {
        List<ReportSkip> list = reportSkipService.selectReportSkipList(reportSkip);
        ExcelUtil<ReportSkip> util = new ExcelUtil<ReportSkip>(ReportSkip.class);
        util.exportExcel(response, list, "逃单报数据");
    }

    /**
     * 获取逃单报详细信息
     */
    @RequiresPermissions("pxy-report:skip:query")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(reportSkipService.selectReportSkipById(id));
    }

    /**
     * 新增逃单报
     */
    @RequiresPermissions("pxy-report:skip:add")
    @Log(title = "逃单报", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody ReportSkip reportSkip)
    {
        return toAjax(reportSkipService.insertReportSkip(reportSkip));
    }

    /**
     * 修改逃单报
     */
    @RequiresPermissions("pxy-report:skip:edit")
    @Log(title = "逃单报", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody ReportSkip reportSkip)
    {
        return toAjax(reportSkipService.updateReportSkip(reportSkip));
    }

    /**
     * 删除逃单报
     */
    @RequiresPermissions("pxy-report:skip:remove")
    @Log(title = "逃单报", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(reportSkipService.deleteReportSkipByIds(ids));
    }
}
