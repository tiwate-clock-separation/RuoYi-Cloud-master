package com.ruoyi.audit.service.impl;

import java.util.List;
import com.ruoyi.common.core.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.audit.mapper.SpaceAuditMapper;
import com.ruoyi.domain.SpaceAudit;
import com.ruoyi.audit.service.ISpaceAuditService;

/**
 * 车位审核Service业务层处理
 *
 * @author ruoyi
 * @date 2023-12-03
 */
@Service
public class SpaceAuditServiceImpl implements ISpaceAuditService
{
    @Autowired
    private SpaceAuditMapper spaceAuditMapper;

    /**
     * 查询车位审核
     *
     * @param said 车位审核主键
     * @return 车位审核
     */
    @Override
    public SpaceAudit selectSpaceAuditBySaid(Long said)
    {
        return spaceAuditMapper.selectSpaceAuditBySaid(said);
    }

    /**
     * 查询车位审核列表
     *
     * @param spaceAudit 车位审核
     * @return 车位审核
     */
    @Override
    public List<SpaceAudit> selectSpaceAuditList(SpaceAudit spaceAudit)
    {
        return spaceAuditMapper.selectSpaceAuditList(spaceAudit);
    }

    /**
     * 新增车位审核
     *
     * @param spaceAudit 车位审核
     * @return 结果
     */
    @Override
    public int insertSpaceAudit(SpaceAudit spaceAudit)
    {
        spaceAudit.setCreateTime(DateUtils.getNowDate());
        return spaceAuditMapper.insertSpaceAudit(spaceAudit);
    }

    /**
     * 修改车位审核
     *
     * @param spaceAudit 车位审核
     * @return 结果
     */
    @Override
    public int updateSpaceAudit(SpaceAudit spaceAudit)
    {
        spaceAudit.setUpdateTime(DateUtils.getNowDate());
        return spaceAuditMapper.updateSpaceAudit(spaceAudit);
    }

    /**
     * 批量删除车位审核
     *
     * @param saids 需要删除的车位审核主键
     * @return 结果
     */
    @Override
    public int deleteSpaceAuditBySaids(Long[] saids)
    {
        return spaceAuditMapper.deleteSpaceAuditBySaids(saids);
    }

    /**
     * 删除车位审核信息
     *
     * @param said 车位审核主键
     * @return 结果
     */
    @Override
    public int deleteSpaceAuditBySaid(Long said)
    {
        return spaceAuditMapper.deleteSpaceAuditBySaid(said);
    }
}
