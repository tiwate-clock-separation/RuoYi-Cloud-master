import request from '@/utils/request'

// 查询车厂设备相机类型列表
export function listCamera(query) {
  return request({
    url: '/sys-cars-home/camera/list',
    method: 'get',
    params: query
  })
}

// 查询车厂设备相机类型详细
export function getCamera(cameraid) {
  return request({
    url: '/sys-cars-home/camera/' + cameraid,
    method: 'get'
  })
}

// 新增车厂设备相机类型
export function addCamera(data) {
  return request({
    url: '/sys-cars-home/camera',
    method: 'post',
    data: data
  })
}

// 修改车厂设备相机类型
export function updateCamera(data) {
  return request({
    url: '/sys-cars-home/camera',
    method: 'put',
    data: data
  })
}

// 删除车厂设备相机类型
export function delCamera(cameraid) {
  return request({
    url: '/sys-cars-home/camera/' + cameraid,
    method: 'delete'
  })
}
