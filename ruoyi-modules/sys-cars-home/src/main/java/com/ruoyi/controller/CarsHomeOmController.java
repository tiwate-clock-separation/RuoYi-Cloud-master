package com.ruoyi.controller;

import java.util.List;

import com.ruoyi.service.ICarsHomeOmService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.log.annotation.Log;
import com.ruoyi.common.log.enums.BusinessType;
import com.ruoyi.common.security.annotation.RequiresPermissions;
import com.ruoyi.domain.CarsHomeOm;
import com.ruoyi.common.core.web.controller.BaseController;
import com.ruoyi.common.core.web.domain.AjaxResult;
import com.ruoyi.common.core.web.page.TableDataInfo;

/**
 * 车场运维Controller
 * 
 * @author chn
 * @date 2023-12-01
 */
@RestController
@RequestMapping("/om")
public class CarsHomeOmController extends BaseController
{
    @Autowired
    private ICarsHomeOmService carsHomeOmService;

    /**
     * 查询车场运维列表
     */
    @RequiresPermissions("sys-cars-home:om:list")
    @GetMapping("/list")
    public TableDataInfo list(CarsHomeOm carsHomeOm)
    {
        startPage();
        List<CarsHomeOm> list = carsHomeOmService.selectCarsHomeOmList(carsHomeOm);
        return getDataTable(list);
    }



    /**
     * 获取车场运维详细信息
     */
//    @RequiresPermissions("sys-cars-home:om:query")
    @GetMapping(value = "/{code}")
    public AjaxResult getInfo(@PathVariable("code") Long code)
    {
        return success(carsHomeOmService.selectCarsHomeOmByCode(code));
    }

    /**
     * 新增车场运维
     */
//    @RequiresPermissions("sys-cars-home:om:add")
    @Log(title = "车场运维", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody CarsHomeOm carsHomeOm)
    {
        return toAjax(carsHomeOmService.insertCarsHomeOm(carsHomeOm));
    }

    /**
     * 修改车场运维
     */
//    @RequiresPermissions("sys-cars-home:om:edit")
    @Log(title = "车场运维", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody CarsHomeOm carsHomeOm)
    {
        return toAjax(carsHomeOmService.updateCarsHomeOm(carsHomeOm));
    }

    /**
     * 删除车场运维
     */
//    @RequiresPermissions("sys-cars-home:om:remove")
    @Log(title = "车场运维", businessType = BusinessType.DELETE)
	@DeleteMapping("/{codes}")
    public AjaxResult remove(@PathVariable Long[] codes)
    {
        return toAjax(carsHomeOmService.deleteCarsHomeOmByCodes(codes));
    }
}
