package com.ruoyi.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.core.annotation.Excel;
import com.ruoyi.common.core.web.domain.BaseEntity;

/**
 * 异常报表对象 report_exception
 * 
 * @author ruoyi
 * @date 2023-12-08
 */
public class ReportException extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 自增ID */
    @Excel(name = "自增ID")
    private Long exceid;

    /** 唯一编码,前台使用 */
    private String code;

    /** 状态 */
    private Long status;

    /** 是否删除 */
    private Long deleted;

    /** 物业id */
    @Excel(name = "物业id")
    private Long pmid;

    /** 车场id */
    @Excel(name = "车场id")
    private Long chid;

    /** 车辆信息 */
    @Excel(name = "车辆信息")
    private Long cmid;

    /** 设备id */
    @Excel(name = "设备id")
    private Long eqid;

    /** 收费金额 */
    @Excel(name = "收费金额")
    private String fee;

    private CarsHome carsHome;

    private CarsMsg carsMsg;

    private PropertyManagement propertyManagement;

    private CarsHomeEq carsHomeEq;

    public CarsHome getCarsHome() {
        return carsHome;
    }

    public void setCarsHome(CarsHome carsHome) {
        this.carsHome = carsHome;
    }

    public CarsMsg getCarsMsg() {
        return carsMsg;
    }

    public void setCarsMsg(CarsMsg carsMsg) {
        this.carsMsg = carsMsg;
    }

    public PropertyManagement getPropertyManagement() {
        return propertyManagement;
    }

    public void setPropertyManagement(PropertyManagement propertyManagement) {
        this.propertyManagement = propertyManagement;
    }

    public CarsHomeEq getCarsHomeEq() {
        return carsHomeEq;
    }

    public void setCarsHomeEq(CarsHomeEq carsHomeEq) {
        this.carsHomeEq = carsHomeEq;
    }

    public void setExceid(Long exceid)
    {
        this.exceid = exceid;
    }

    public Long getExceid() 
    {
        return exceid;
    }
    public void setCode(String code) 
    {
        this.code = code;
    }

    public String getCode() 
    {
        return code;
    }
    public void setStatus(Long status) 
    {
        this.status = status;
    }

    public Long getStatus() 
    {
        return status;
    }
    public void setDeleted(Long deleted) 
    {
        this.deleted = deleted;
    }

    public Long getDeleted() 
    {
        return deleted;
    }
    public void setPmid(Long pmid) 
    {
        this.pmid = pmid;
    }

    public Long getPmid() 
    {
        return pmid;
    }
    public void setChid(Long chid) 
    {
        this.chid = chid;
    }

    public Long getChid() 
    {
        return chid;
    }
    public void setCmid(Long cmid) 
    {
        this.cmid = cmid;
    }

    public Long getCmid() 
    {
        return cmid;
    }
    public void setEqid(Long eqid) 
    {
        this.eqid = eqid;
    }

    public Long getEqid() 
    {
        return eqid;
    }
    public void setFee(String fee) 
    {
        this.fee = fee;
    }

    public String getFee() 
    {
        return fee;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("exceid", getExceid())
            .append("code", getCode())
            .append("status", getStatus())
            .append("deleted", getDeleted())
            .append("createTime", getCreateTime())
            .append("updateTime", getUpdateTime())
            .append("pmid", getPmid())
            .append("chid", getChid())
            .append("cmid", getCmid())
            .append("eqid", getEqid())
            .append("remark", getRemark())
            .append("fee", getFee())
            .toString();
    }
}
