package com.ruoyi.domain;

import com.fasterxml.jackson.databind.annotation.JsonAppend;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.core.annotation.Excel;
import com.ruoyi.common.core.web.domain.BaseEntity;

/**
 * 开闸记录对象 cutoff_record
 *
 * @author ruoyi
 * @date 2023-12-05
 */
public class CutoffRecord extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 自增ID */
    private Long corid;

    /** 唯一编码,前台使用 */
    @Excel(name = "唯一编码,前台使用")
    private String code;

    /** 状态 */
    @Excel(name = "状态")
    private Long status;

    /** 是否删除 */
    @Excel(name = "是否删除")
    private Long deleted;

    /** 设备ip */
    @Excel(name = "设备ip")
    private String driverIp;

    /** 备注 */
    @Excel(name = "备注")
    private String cutoffRemark;

    /** 操作人 */
    @Excel(name = "操作人")
    private String cutoffUser;

    /** 小区id */
    @Excel(name = "小区id")
    private Long pmid;

    /** 停车场id */
    @Excel(name = "停车场id")
    private Long hpid;

    /** 出入口 */
    @Excel(name = "出入口")
    private Long gate;

    /** 相机品牌 */
    @Excel(name = "相机品牌")
    private String camera;

    /** 开关类型 */
    @Excel(name = "开关类型")
    private Long off;

    private CarsHome carsHome;

    private PropertyManagement propertyManagement;

    public CarsHome getCarsHome() {
        return carsHome;
    }

    public void setCarsHome(CarsHome carsHome) {
        this.carsHome = carsHome;
    }

    public PropertyManagement getPropertyManagement() {
        return propertyManagement;
    }

    public void setPropertyManagement(PropertyManagement propertyManagement) {
        this.propertyManagement = propertyManagement;
    }

    public void setCorid(Long corid)
    {
        this.corid = corid;
    }

    public Long getCorid()
    {
        return corid;
    }
    public void setCode(String code)
    {
        this.code = code;
    }

    public String getCode()
    {
        return code;
    }
    public void setStatus(Long status)
    {
        this.status = status;
    }

    public Long getStatus()
    {
        return status;
    }
    public void setDeleted(Long deleted)
    {
        this.deleted = deleted;
    }

    public Long getDeleted()
    {
        return deleted;
    }
    public void setDriverIp(String driverIp)
    {
        this.driverIp = driverIp;
    }

    public String getDriverIp()
    {
        return driverIp;
    }
    public void setCutoffRemark(String cutoffRemark)
    {
        this.cutoffRemark = cutoffRemark;
    }

    public String getCutoffRemark()
    {
        return cutoffRemark;
    }
    public void setCutoffUser(String cutoffUser)
    {
        this.cutoffUser = cutoffUser;
    }

    public String getCutoffUser()
    {
        return cutoffUser;
    }
    public void setPmid(Long pmid)
    {
        this.pmid = pmid;
    }

    public Long getPmid()
    {
        return pmid;
    }
    public void setHpid(Long hpid)
    {
        this.hpid = hpid;
    }

    public Long getHpid()
    {
        return hpid;
    }
    public void setGate(Long gate)
    {
        this.gate = gate;
    }

    public Long getGate()
    {
        return gate;
    }
    public void setCamera(String camera)
    {
        this.camera = camera;
    }

    public String getCamera()
    {
        return camera;
    }
    public void setOff(Long off)
    {
        this.off = off;
    }

    public Long getOff()
    {
        return off;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("corid", getCorid())
            .append("code", getCode())
            .append("status", getStatus())
            .append("deleted", getDeleted())
            .append("createTime", getCreateTime())
            .append("updateTime", getUpdateTime())
            .append("driverIp", getDriverIp())
            .append("cutoffRemark", getCutoffRemark())
            .append("cutoffUser", getCutoffUser())
            .append("pmid", getPmid())
            .append("hpid", getHpid())
            .append("gate", getGate())
            .append("camera", getCamera())
            .append("off", getOff())
            .toString();
    }
}
