package com.ruoyi.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.mapper.CarsHomeEqBoardsMapper;
import com.ruoyi.domain.CarsHomeEqBoards;
import com.ruoyi.service.ICarsHomeEqBoardsService;

/**
 * 车场设备板卡Service业务层处理
 * 
 * @author chn
 * @date 2023-12-07
 */
@Service
public class CarsHomeEqBoardsServiceImpl implements ICarsHomeEqBoardsService 
{
    @Autowired
    private CarsHomeEqBoardsMapper carsHomeEqBoardsMapper;

    /**
     * 查询车场设备板卡
     * 
     * @param boardsid 车场设备板卡主键
     * @return 车场设备板卡
     */
    @Override
    public CarsHomeEqBoards selectCarsHomeEqBoardsByBoardsid(Long boardsid)
    {
        return carsHomeEqBoardsMapper.selectCarsHomeEqBoardsByBoardsid(boardsid);
    }

    /**
     * 查询车场设备板卡列表
     * 
     * @param carsHomeEqBoards 车场设备板卡
     * @return 车场设备板卡
     */
    @Override
    public List<CarsHomeEqBoards> selectCarsHomeEqBoardsList(CarsHomeEqBoards carsHomeEqBoards)
    {
        return carsHomeEqBoardsMapper.selectCarsHomeEqBoardsList(carsHomeEqBoards);
    }

    /**
     * 新增车场设备板卡
     * 
     * @param carsHomeEqBoards 车场设备板卡
     * @return 结果
     */
    @Override
    public int insertCarsHomeEqBoards(CarsHomeEqBoards carsHomeEqBoards)
    {
        return carsHomeEqBoardsMapper.insertCarsHomeEqBoards(carsHomeEqBoards);
    }

    /**
     * 修改车场设备板卡
     * 
     * @param carsHomeEqBoards 车场设备板卡
     * @return 结果
     */
    @Override
    public int updateCarsHomeEqBoards(CarsHomeEqBoards carsHomeEqBoards)
    {
        return carsHomeEqBoardsMapper.updateCarsHomeEqBoards(carsHomeEqBoards);
    }

    /**
     * 批量删除车场设备板卡
     * 
     * @param boardsids 需要删除的车场设备板卡主键
     * @return 结果
     */
    @Override
    public int deleteCarsHomeEqBoardsByBoardsids(Long[] boardsids)
    {
        return carsHomeEqBoardsMapper.deleteCarsHomeEqBoardsByBoardsids(boardsids);
    }

    /**
     * 删除车场设备板卡信息
     * 
     * @param boardsid 车场设备板卡主键
     * @return 结果
     */
    @Override
    public int deleteCarsHomeEqBoardsByBoardsid(Long boardsid)
    {
        return carsHomeEqBoardsMapper.deleteCarsHomeEqBoardsByBoardsid(boardsid);
    }
}
