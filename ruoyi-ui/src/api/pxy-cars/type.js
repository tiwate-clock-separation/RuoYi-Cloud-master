import request from '@/utils/request'

// 查询车辆类型列表
export function listType(query) {
  return request({
    url: '/pxy-cars/type/list',
    method: 'get',
    params: query
  })
}

// 查询车辆类型详细
export function getType(ctid) {
  return request({
    url: '/pxy-cars/type/' + ctid,
    method: 'get'
  })
}

// 新增车辆类型
export function addType(data) {
  return request({
    url: '/pxy-cars/type',
    method: 'post',
    data: data
  })
}

// 修改车辆类型
export function updateType(data) {
  return request({
    url: '/pxy-cars/type',
    method: 'put',
    data: data
  })
}

// 删除车辆类型
export function delType(ctid) {
  return request({
    url: '/pxy-cars/type/' + ctid,
    method: 'delete'
  })
}
