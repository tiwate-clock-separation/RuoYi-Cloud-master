package com.ruoyi.service.impl;

import java.util.List;
import com.ruoyi.common.core.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.mapper.CarsHomeLaneMapper;
import com.ruoyi.domain.CarsHomeLane;
import com.ruoyi.service.ICarsHomeLaneService;

/**
 * 车道配置Service业务层处理
 * 
 * @author chn
 * @date 2023-12-05
 */
@Service
public class CarsHomeLaneServiceImpl implements ICarsHomeLaneService 
{
    @Autowired
    private CarsHomeLaneMapper carsHomeLaneMapper;

    /**
     * 查询车道配置
     * 
     * @param chlid 车道配置主键
     * @return 车道配置
     */
    @Override
    public CarsHomeLane selectCarsHomeLaneByChlid(Long chlid)
    {
        return carsHomeLaneMapper.selectCarsHomeLaneByChlid(chlid);
    }

    /**
     * 查询车道配置列表
     * 
     * @param carsHomeLane 车道配置
     * @return 车道配置
     */
    @Override
    public List<CarsHomeLane> selectCarsHomeLaneList(CarsHomeLane carsHomeLane)
    {
        return carsHomeLaneMapper.selectCarsHomeLaneList(carsHomeLane);
    }

    /**
     * 新增车道配置
     * 
     * @param carsHomeLane 车道配置
     * @return 结果
     */
    @Override
    public int insertCarsHomeLane(CarsHomeLane carsHomeLane)
    {
        carsHomeLane.setCreateTime(DateUtils.getNowDate());
        return carsHomeLaneMapper.insertCarsHomeLane(carsHomeLane);
    }

    /**
     * 修改车道配置
     * 
     * @param carsHomeLane 车道配置
     * @return 结果
     */
    @Override
    public int updateCarsHomeLane(CarsHomeLane carsHomeLane)
    {
        carsHomeLane.setUpdateTime(DateUtils.getNowDate());
        return carsHomeLaneMapper.updateCarsHomeLane(carsHomeLane);
    }

    /**
     * 批量删除车道配置
     * 
     * @param chlids 需要删除的车道配置主键
     * @return 结果
     */
    @Override
    public int deleteCarsHomeLaneByChlids(Long[] chlids)
    {
        return carsHomeLaneMapper.deleteCarsHomeLaneByChlids(chlids);
    }

    /**
     * 删除车道配置信息
     * 
     * @param chlid 车道配置主键
     * @return 结果
     */
    @Override
    public int deleteCarsHomeLaneByChlid(Long chlid)
    {
        return carsHomeLaneMapper.deleteCarsHomeLaneByChlid(chlid);
    }
}
