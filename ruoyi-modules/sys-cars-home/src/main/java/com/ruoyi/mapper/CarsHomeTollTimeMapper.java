package com.ruoyi.mapper;

import java.util.List;
import com.ruoyi.domain.CarsHomeTollTime;

/**
 * 按次数收费Mapper接口
 * 
 * @author chn
 * @date 2023-12-14
 */
public interface CarsHomeTollTimeMapper 
{
    /**
     * 查询按次数收费
     * 
     * @param timeid 按次数收费主键
     * @return 按次数收费
     */
    public CarsHomeTollTime selectCarsHomeTollTimeByTimeid(Long timeid);

    /**
     * 查询按次数收费列表
     * 
     * @param carsHomeTollTime 按次数收费
     * @return 按次数收费集合
     */
    public List<CarsHomeTollTime> selectCarsHomeTollTimeList(CarsHomeTollTime carsHomeTollTime);

    /**
     * 新增按次数收费
     * 
     * @param carsHomeTollTime 按次数收费
     * @return 结果
     */
    public int insertCarsHomeTollTime(CarsHomeTollTime carsHomeTollTime);

    /**
     * 修改按次数收费
     * 
     * @param carsHomeTollTime 按次数收费
     * @return 结果
     */
    public int updateCarsHomeTollTime(CarsHomeTollTime carsHomeTollTime);

    /**
     * 删除按次数收费
     * 
     * @param timeid 按次数收费主键
     * @return 结果
     */
    public int deleteCarsHomeTollTimeByTimeid(Long timeid);

    /**
     * 批量删除按次数收费
     * 
     * @param timeids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteCarsHomeTollTimeByTimeids(Long[] timeids);
}
