package com.ruoyi.mapper;

import java.util.List;
import com.ruoyi.domain.CarsHomeTollDay;

/**
 * 按天收费Mapper接口
 * 
 * @author chn
 * @date 2023-12-14
 */
public interface CarsHomeTollDayMapper 
{
    /**
     * 查询按天收费
     * 
     * @param dayid 按天收费主键
     * @return 按天收费
     */
    public CarsHomeTollDay selectCarsHomeTollDayByDayid(Long dayid);

    /**
     * 查询按天收费列表
     * 
     * @param carsHomeTollDay 按天收费
     * @return 按天收费集合
     */
    public List<CarsHomeTollDay> selectCarsHomeTollDayList(CarsHomeTollDay carsHomeTollDay);

    /**
     * 新增按天收费
     * 
     * @param carsHomeTollDay 按天收费
     * @return 结果
     */
    public int insertCarsHomeTollDay(CarsHomeTollDay carsHomeTollDay);

    /**
     * 修改按天收费
     * 
     * @param carsHomeTollDay 按天收费
     * @return 结果
     */
    public int updateCarsHomeTollDay(CarsHomeTollDay carsHomeTollDay);

    /**
     * 删除按天收费
     * 
     * @param dayid 按天收费主键
     * @return 结果
     */
    public int deleteCarsHomeTollDayByDayid(Long dayid);

    /**
     * 批量删除按天收费
     * 
     * @param dayids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteCarsHomeTollDayByDayids(Long[] dayids);
}
