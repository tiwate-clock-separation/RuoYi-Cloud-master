package com.ruoyi.service;

import java.util.List;
import com.ruoyi.domain.CarsHomeEq;

/**
 * 车场设备Service接口
 * 
 * @author chn
 * @date 2023-12-11
 */
public interface ICarsHomeEqService 
{
    /**
     * 查询车场设备
     * 
     * @param eqid 车场设备主键
     * @return 车场设备
     */
    public CarsHomeEq selectCarsHomeEqByEqid(Long eqid);

    /**
     * 查询车场设备列表
     * 
     * @param carsHomeEq 车场设备
     * @return 车场设备集合
     */
    public List<CarsHomeEq> selectCarsHomeEqList(CarsHomeEq carsHomeEq);

    /**
     * 新增车场设备
     * 
     * @param carsHomeEq 车场设备
     * @return 结果
     */
    public int insertCarsHomeEq(CarsHomeEq carsHomeEq);

    /**
     * 修改车场设备
     * 
     * @param carsHomeEq 车场设备
     * @return 结果
     */
    public int updateCarsHomeEq(CarsHomeEq carsHomeEq);

    /**
     * 批量删除车场设备
     * 
     * @param eqids 需要删除的车场设备主键集合
     * @return 结果
     */
    public int deleteCarsHomeEqByEqids(Long[] eqids);

    /**
     * 删除车场设备信息
     * 
     * @param eqid 车场设备主键
     * @return 结果
     */
    public int deleteCarsHomeEqByEqid(Long eqid);
}
