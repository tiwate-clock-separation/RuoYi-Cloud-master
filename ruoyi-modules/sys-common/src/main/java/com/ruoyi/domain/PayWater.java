package com.ruoyi.domain;

import com.ruoyi.common.core.annotation.Excel;
import com.ruoyi.common.core.web.domain.BaseEntity;
import org.springframework.data.annotation.Transient;

/**
 * 物业流水对象 pay_water
 *
 * @author ruoyi
 * @date 2023-11-30
 */
public class PayWater extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 交易ID */

    private Long pwid;

    /** 唯一编码,前台使用 */
    @Excel(name = "唯一编码,前台使用")
    private String code;

    /** 状态 */
    @Excel(name = "状态")
    private Long status;

    /** 是否删除 */
    @Excel(name = "是否删除")
    private Long deleted;

    /** 订单编号 */
    @Excel(name = "订单编号")
    private Long orderNum;

    @Transient
    @Excel(name = "订单")
    private PayOrder payOrder;

    /** 实付金额 */
    @Excel(name = "实付金额")
    private Long orderPrice;

    /** 是否分账 */
    @Excel(name = "是否分账")
    private Integer pwStatus;



    public void setPwid(Long pwid)
    {
        this.pwid = pwid;
    }

    public Long getPwid()
    {
        return pwid;
    }
    public void setCode(String code)
    {
        this.code = code;
    }

    public String getCode()
    {
        return code;
    }
    public void setStatus(Long status)
    {
        this.status = status;
    }

    public Long getStatus()
    {
        return status;
    }
    public void setDeleted(Long deleted)
    {
        this.deleted = deleted;
    }

    public Long getDeleted()
    {
        return deleted;
    }
    public void setOrderNum(Long orderNum)
    {
        this.orderNum = orderNum;
    }

    public Long getOrderNum()
    {
        return orderNum;
    }
    public void setOrderPrice(Long orderPrice)
    {
        this.orderPrice = orderPrice;
    }

    public PayOrder getPayOrder() {
        return payOrder;
    }

    public void setPayOrder(PayOrder payOrder) {
        this.payOrder = payOrder;
    }

    public Long getOrderPrice()
    {
        return orderPrice;
    }
    public void setPwStatus(Integer pwStatus)
    {
        this.pwStatus = pwStatus;
    }

    public Integer getPwStatus()
    {
        return pwStatus;
    }

    public PayWater() {
    }

    public PayWater(String code, Long status, Long deleted, Long orderNum, Long orderPrice, Integer pwStatus) {
        this.code = code;
        this.status = status;
        this.deleted = deleted;
        this.orderNum = orderNum;
        this.orderPrice = orderPrice;
        this.pwStatus = pwStatus;
    }

    @Override
    public String toString() {
        return "PayWater{" +
                "pwid=" + pwid +
                ", code='" + code + '\'' +
                ", status=" + status +
                ", deleted=" + deleted +
                ", orderNum='" + orderNum + '\'' +
                ", payOrder=" + payOrder +
                ", orderPrice=" + orderPrice +
                ", pwStatus=" + pwStatus +
                '}';
    }
}
