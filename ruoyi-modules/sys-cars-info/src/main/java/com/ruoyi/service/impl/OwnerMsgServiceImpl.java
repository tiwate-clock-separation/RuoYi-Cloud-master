package com.ruoyi.service.impl;

import java.util.List;
import com.ruoyi.common.core.utils.DateUtils;
import com.ruoyi.domain.OwnerMsg;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.ruoyi.domain.PropertyManagement;
import com.ruoyi.mapper.OwnerMsgMapper;
import com.ruoyi.service.IOwnerMsgService;

/**
 * 业主信息管理Service业务层处理
 *
 * @author ruoyi
 * @date 2023-12-07
 */
@Service
public class OwnerMsgServiceImpl implements IOwnerMsgService
{
    @Autowired
    private OwnerMsgMapper ownerMsgMapper;

    /**
     * 查询业主信息管理
     *
     * @param omid 业主信息管理主键
     * @return 业主信息管理
     */
    @Override
    public OwnerMsg selectOwnerMsgByOmid(Long omid)
    {
        return ownerMsgMapper.selectOwnerMsgByOmid(omid);
    }

    /**
     * 查询业主信息管理列表
     *
     * @param ownerMsg 业主信息管理
     * @return 业主信息管理
     */
    @Override
    public List<OwnerMsg> selectOwnerMsgList(OwnerMsg ownerMsg)
    {
        return ownerMsgMapper.selectOwnerMsgList(ownerMsg);
    }

    /**
     * 新增业主信息管理
     *
     * @param ownerMsg 业主信息管理
     * @return 结果
     */
    @Transactional
    @Override
    public int insertOwnerMsg(OwnerMsg ownerMsg)
    {
        ownerMsg.setCreateTime(DateUtils.getNowDate());
        int rows = ownerMsgMapper.insertOwnerMsg(ownerMsg);
//        insertPropertyManagement(ownerMsg);
        return rows;
    }

    /**
     * 修改业主信息管理
     *
     * @param ownerMsg 业主信息管理
     * @return 结果
     */
    @Transactional
    @Override
    public int updateOwnerMsg(OwnerMsg ownerMsg)
    {
        ownerMsg.setUpdateTime(DateUtils.getNowDate());
        ownerMsgMapper.deletePropertyManagementByPmid(ownerMsg.getOmid());
//        insertPropertyManagement(ownerMsg);
        return ownerMsgMapper.updateOwnerMsg(ownerMsg);
    }

    /**
     * 批量删除业主信息管理
     *
     * @param omids 需要删除的业主信息管理主键
     * @return 结果
     */
    @Transactional
    @Override
    public int deleteOwnerMsgByOmids(Long[] omids)
    {
        ownerMsgMapper.deletePropertyManagementByPmids(omids);
        return ownerMsgMapper.deleteOwnerMsgByOmids(omids);
    }

    /**
     * 删除业主信息管理信息
     *
     * @param omid 业主信息管理主键
     * @return 结果
     */
    @Transactional
    @Override
    public int deleteOwnerMsgByOmid(Long omid)
    {
        ownerMsgMapper.deletePropertyManagementByPmid(omid);
        return ownerMsgMapper.deleteOwnerMsgByOmid(omid);
    }

    /**
     * 新增物业管理信息
     *
     * @param ownerMsg 业主信息管理对象
     */
    public void insertPropertyManagement(OwnerMsg ownerMsg)
    {
        PropertyManagement propertyManagementList = ownerMsg.getPropertyManagement();
//        Long omid = ownerMsg.getOmid();
//        if (StringUtils.isNotNull(propertyManagementList))
//        {
//            List<PropertyManagement> list = new ArrayList<PropertyManagement>();
//            for (PropertyManagement propertyManagement : propertyManagementList)
//            {
//                propertyManagement.setPmid(omid);
//                list.add(propertyManagement);
//            }
//            if (list.size() > 0)
//            {
//                ownerMsgMapper.batchPropertyManagement(list);
//            }
//        }
        ownerMsgMapper.batchPropertyManagement(propertyManagementList);
    }
}
