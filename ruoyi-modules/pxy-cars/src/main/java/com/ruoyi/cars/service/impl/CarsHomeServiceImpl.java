package com.ruoyi.cars.service.impl;

import java.util.List;
import com.ruoyi.common.core.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.cars.mapper.CarsHomeMapper;
import com.ruoyi.domain.CarsHome;
import com.ruoyi.cars.service.ICarsHomeService;

/**
 * 车场管理Service业务层处理
 *
 * @author ruoyi
 * @date 2023-11-30
 */
@Service
public class CarsHomeServiceImpl implements ICarsHomeService
{
    @Autowired
    private CarsHomeMapper carsHomeMapper;

    /**
     * 查询车场管理
     *
     * @param chid 车场管理主键
     * @return 车场管理
     */
    @Override
    public CarsHome selectCarsHomeByChid(Long chid)
    {
        return carsHomeMapper.selectCarsHomeByChid(chid);
    }

    /**
     * 查询车场管理列表
     *
     * @param carsHome 车场管理
     * @return 车场管理
     */
    @Override
    public List<CarsHome> selectCarsHomeList(CarsHome carsHome)
    {
        return carsHomeMapper.selectCarsHomeList(carsHome);
    }

    /**
     * 新增车场管理
     *
     * @param carsHome 车场管理
     * @return 结果
     */
    @Override
    public int insertCarsHome(CarsHome carsHome)
    {
        carsHome.setCreateTime(DateUtils.getNowDate());
        return carsHomeMapper.insertCarsHome(carsHome);
    }

    /**
     * 修改车场管理
     *
     * @param carsHome 车场管理
     * @return 结果
     */
    @Override
    public int updateCarsHome(CarsHome carsHome)
    {
        carsHome.setUpdateTime(DateUtils.getNowDate());
        return carsHomeMapper.updateCarsHome(carsHome);
    }

    /**
     * 批量删除车场管理
     *
     * @param chids 需要删除的车场管理主键
     * @return 结果
     */
    @Override
    public int deleteCarsHomeByChids(Long[] chids)
    {
        return carsHomeMapper.deleteCarsHomeByChids(chids);
    }

    /**
     * 删除车场管理信息
     *
     * @param chid 车场管理主键
     * @return 结果
     */
    @Override
    public int deleteCarsHomeByChid(Long chid)
    {
        return carsHomeMapper.deleteCarsHomeByChid(chid);
    }
}
