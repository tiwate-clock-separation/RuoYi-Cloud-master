package com.ruoyi.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.core.annotation.Excel;
import com.ruoyi.common.core.web.domain.BaseEntity;

/**
 * 物业人员对象 emp_home
 * 
 * @author ruoyi
 * @date 2023-12-23
 */
public class EmpHome extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 自增ID */
    @Excel(name = "自增ID")
    private Long ehid;

    /** 唯一编码,前台使用 */
    private String code;

    /** 状态 */
    private Long status;

    /** 是否删除 */
    private Long deleted;

    /** 用户名称 */
    @Excel(name = "用户名称")
    private String ehUser;

    /** 手机号 */
    @Excel(name = "手机号")
    private String ehTel;

    /** 物业 */
    @Excel(name = "物业")
    private Long pmid;

    private PropertyManagement propertyManagement;

    public PropertyManagement getPropertyManagement() {
        return propertyManagement;
    }

    public void setPropertyManagement(PropertyManagement propertyManagement) {
        this.propertyManagement = propertyManagement;
    }

    public void setEhid(Long ehid)
    {
        this.ehid = ehid;
    }

    public Long getEhid() 
    {
        return ehid;
    }
    public void setCode(String code) 
    {
        this.code = code;
    }

    public String getCode() 
    {
        return code;
    }
    public void setStatus(Long status) 
    {
        this.status = status;
    }

    public Long getStatus() 
    {
        return status;
    }
    public void setDeleted(Long deleted) 
    {
        this.deleted = deleted;
    }

    public Long getDeleted() 
    {
        return deleted;
    }
    public void setEhUser(String ehUser) 
    {
        this.ehUser = ehUser;
    }

    public String getEhUser() 
    {
        return ehUser;
    }

    public String getEhTel() {
        return ehTel;
    }

    public void setEhTel(String ehTel) {
        this.ehTel = ehTel;
    }

    public void setPmid(Long pmid)
    {
        this.pmid = pmid;
    }

    public Long getPmid() 
    {
        return pmid;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("ehid", getEhid())
            .append("code", getCode())
            .append("status", getStatus())
            .append("deleted", getDeleted())
            .append("createTime", getCreateTime())
            .append("updateTime", getUpdateTime())
            .append("ehUser", getEhUser())
            .append("ehTel", getEhTel())
            .append("pmid", getPmid())
            .append("remark", getRemark())
            .toString();
    }
}
