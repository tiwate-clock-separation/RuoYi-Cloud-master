package com.ruoyi.cars.service.impl;

import java.util.List;
import com.ruoyi.common.core.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.cars.mapper.CarsHomeEqMapper;
import com.ruoyi.domain.CarsHomeEq;
import com.ruoyi.cars.service.ICarsHomeEqService;

/**
 * 车场设备Service业务层处理
 *
 * @author ruoyi
 * @date 2023-11-30
 */
@Service
public class CarsHomeEqServiceImpl implements ICarsHomeEqService
{
    @Autowired
    private CarsHomeEqMapper carsHomeEqMapper;

    /**
     * 查询车场设备
     *
     * @param eqid 车场设备主键
     * @return 车场设备
     */
    @Override
    public CarsHomeEq selectCarsHomeEqByEqid(Long eqid)
    {
        return carsHomeEqMapper.selectCarsHomeEqByEqid(eqid);
    }

    /**
     * 查询车场设备列表
     *
     * @param carsHomeEq 车场设备
     * @return 车场设备
     */
    @Override
    public List<CarsHomeEq> selectCarsHomeEqList(CarsHomeEq carsHomeEq)
    {
        return carsHomeEqMapper.selectCarsHomeEqList(carsHomeEq);
    }

    /**
     * 新增车场设备
     *
     * @param carsHomeEq 车场设备
     * @return 结果
     */
    @Override
    public int insertCarsHomeEq(CarsHomeEq carsHomeEq)
    {
        carsHomeEq.setCreateTime(DateUtils.getNowDate());
        return carsHomeEqMapper.insertCarsHomeEq(carsHomeEq);
    }

    /**
     * 修改车场设备
     *
     * @param carsHomeEq 车场设备
     * @return 结果
     */
    @Override
    public int updateCarsHomeEq(CarsHomeEq carsHomeEq)
    {
        carsHomeEq.setUpdateTime(DateUtils.getNowDate());
        return carsHomeEqMapper.updateCarsHomeEq(carsHomeEq);
    }

    /**
     * 批量删除车场设备
     *
     * @param eqids 需要删除的车场设备主键
     * @return 结果
     */
    @Override
    public int deleteCarsHomeEqByEqids(Long[] eqids)
    {
        return carsHomeEqMapper.deleteCarsHomeEqByEqids(eqids);
    }

    /**
     * 删除车场设备信息
     *
     * @param eqid 车场设备主键
     * @return 结果
     */
    @Override
    public int deleteCarsHomeEqByEqid(Long eqid)
    {
        return carsHomeEqMapper.deleteCarsHomeEqByEqid(eqid);
    }
}
