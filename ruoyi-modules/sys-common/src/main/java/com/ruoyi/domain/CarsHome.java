package com.ruoyi.domain;

import com.ruoyi.common.core.annotation.Excel;
import com.ruoyi.common.core.web.domain.BaseEntity;

/**
 * 车场管理对象 cars_home
 *
 * @author chn
 * @date 2023-11-29
 */
public class CarsHome extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 自增ID */
    private Long chid;

    /** 车场名称 */
    @Excel(name = "车场名称")
    private String chname;

    /** 车场类别 */
    @Excel(name = "车场类别")
    private Long chtid;

    private CarsHomeType carsHomeType;

    /** 车场地址 */
    @Excel(name = "车场地址")
    private String addr;



    public String getAddrcode() {
        return addrcode;
    }

    public void setAddrcode(String addrcode) {
        this.addrcode = addrcode;
    }

    /** 车场地址编码 */
    @Excel(name = "车场地址编码")
    private String addrcode;

    /** 所属物业id */
    @Excel(name = "所属物业id")
    private Long pmid;

    private PropertyManagement propertyManagement;

    public PropertyManagement getPropertyManagement() {
        return propertyManagement;
    }

    public void setPropertyManagement(PropertyManagement propertyManagement) {
        this.propertyManagement = propertyManagement;
    }

    public CarsHomeType getCarsHomeType() {
        return carsHomeType;
    }

    public void setCarsHomeType(CarsHomeType carsHomeType) {
        this.carsHomeType = carsHomeType;
    }

    /** 泊位总数 */
    @Excel(name = "泊位总数")
    private Long allNumber;

    /** 剩余泊位 */
    @Excel(name = "剩余泊位")
    private Long num;

    /** 满车位是否开闸 */
    @Excel(name = "满车位是否开闸")
    private Long info;

    public Long getFree() {
        return free;
    }

    public void setFree(Long free) {
        this.free = free;
    }

    /** 支付离场时间 */
    @Excel(name = "支付离场时间")
    private Long goTime;

    /** 状态 */
    @Excel(name = "状态")
    private Long status;
    /** 状态 */
    @Excel(name = "是否免费")
    private Long free;

    /** 是否删除 */
    @Excel(name = "是否删除")
    private Long deleted;

    /** 唯一编码，前台使用 */
    @Excel(name = "唯一编码，前台使用")
    private String code;

    public void setChid(Long chid)
    {
        this.chid = chid;
    }

    public Long getChid()
    {
        return chid;
    }
    public void setChname(String chname)
    {
        this.chname = chname;
    }

    public String getChname()
    {
        return chname;
    }
    public void setChtid(Long chtid)
    {
        this.chtid = chtid;
    }

    public Long getChtid()
    {
        return chtid;
    }
    public void setAddr(String addr)
    {
        this.addr = addr;
    }

    public String getAddr()
    {
        return addr;
    }
    public void setPmid(Long pmid)
    {
        this.pmid = pmid;
    }

    public Long getPmid()
    {
        return pmid;
    }
    public void setAllNumber(Long allNumber)
    {
        this.allNumber = allNumber;
    }

    public Long getAllNumber()
    {
        return allNumber;
    }
    public void setNum(Long num)
    {
        this.num = num;
    }

    public Long getNum()
    {
        return num;
    }
    public void setInfo(Long info)
    {
        this.info = info;
    }

    public Long getInfo()
    {
        return info;
    }
    public void setGoTime(Long goTime)
    {
        this.goTime = goTime;
    }

    public Long getGoTime()
    {
        return goTime;
    }
    public void setStatus(Long status)
    {
        this.status = status;
    }

    public Long getStatus()
    {
        return status;
    }
    public void setDeleted(Long deleted)
    {
        this.deleted = deleted;
    }

    public Long getDeleted()
    {
        return deleted;
    }
    public void setCode(String code)
    {
        this.code = code;
    }

    public String getCode()
    {
        return code;
    }

    @Override
    public String toString() {
        return "CarsHome{" +
                "chid=" + chid +
                ", chname='" + chname + '\'' +
                ", chtid=" + chtid +
                ", addr='" + addr + '\'' +
                ", addrcode='" + addrcode + '\'' +
                ", pid=" + pmid +
                ", allNumber=" + allNumber +
                ", num=" + num +
                ", info=" + info +
                ", goTime=" + goTime +
                ", status=" + status +
                ", free=" + free +
                ", deleted=" + deleted +
                ", code='" + code + '\'' +
                "} " + super.toString();
    }
}
