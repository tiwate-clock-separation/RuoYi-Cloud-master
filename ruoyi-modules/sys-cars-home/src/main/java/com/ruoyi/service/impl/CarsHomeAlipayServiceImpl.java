package com.ruoyi.service.impl;

import java.util.List;
import com.ruoyi.common.core.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.mapper.CarsHomeAlipayMapper;
import com.ruoyi.domain.CarsHomeAlipay;
import com.ruoyi.service.ICarsHomeAlipayService;

/**
 * 支付宝支付配置Service业务层处理
 * 
 * @author chn
 * @date 2023-12-27
 */
@Service
public class CarsHomeAlipayServiceImpl implements ICarsHomeAlipayService 
{
    @Autowired
    private CarsHomeAlipayMapper carsHomeAlipayMapper;

    /**
     * 查询支付宝支付配置
     * 
     * @param id 支付宝支付配置主键
     * @return 支付宝支付配置
     */
    @Override
    public CarsHomeAlipay selectCarsHomeAlipayById(Long id)
    {
        return carsHomeAlipayMapper.selectCarsHomeAlipayById(id);
    }

    /**
     * 查询支付宝支付配置列表
     * 
     * @param carsHomeAlipay 支付宝支付配置
     * @return 支付宝支付配置
     */
    @Override
    public List<CarsHomeAlipay> selectCarsHomeAlipayList(CarsHomeAlipay carsHomeAlipay)
    {
        return carsHomeAlipayMapper.selectCarsHomeAlipayList(carsHomeAlipay);
    }

    /**
     * 新增支付宝支付配置
     * 
     * @param carsHomeAlipay 支付宝支付配置
     * @return 结果
     */
    @Override
    public int insertCarsHomeAlipay(CarsHomeAlipay carsHomeAlipay)
    {
        carsHomeAlipay.setCreateTime(DateUtils.getNowDate());
        return carsHomeAlipayMapper.insertCarsHomeAlipay(carsHomeAlipay);
    }

    /**
     * 修改支付宝支付配置
     * 
     * @param carsHomeAlipay 支付宝支付配置
     * @return 结果
     */
    @Override
    public int updateCarsHomeAlipay(CarsHomeAlipay carsHomeAlipay)
    {
        carsHomeAlipay.setUpdateTime(DateUtils.getNowDate());
        return carsHomeAlipayMapper.updateCarsHomeAlipay(carsHomeAlipay);
    }

    /**
     * 批量删除支付宝支付配置
     * 
     * @param ids 需要删除的支付宝支付配置主键
     * @return 结果
     */
    @Override
    public int deleteCarsHomeAlipayByIds(Long[] ids)
    {
        return carsHomeAlipayMapper.deleteCarsHomeAlipayByIds(ids);
    }

    /**
     * 删除支付宝支付配置信息
     * 
     * @param id 支付宝支付配置主键
     * @return 结果
     */
    @Override
    public int deleteCarsHomeAlipayById(Long id)
    {
        return carsHomeAlipayMapper.deleteCarsHomeAlipayById(id);
    }
}
