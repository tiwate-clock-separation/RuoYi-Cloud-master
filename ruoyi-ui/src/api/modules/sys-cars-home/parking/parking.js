import request from '@/utils/request'

// 查询车场车位列表
export function listParking(query) {
  return request({
    url: '/sys-cars-home/parking/list',
    method: 'get',
    params: query
  })
}

// 查询车场车位详细
export function getParking(chpid) {
  return request({
    url: '/sys-cars-home/parking/' + chpid,
    method: 'get'
  })
}

// 新增车场车位
export function addParking(data) {
  return request({
    url: '/sys-cars-home/parking',
    method: 'post',
    data: data
  })
}

// 修改车场车位
export function updateParking(data) {
  return request({
    url: '/sys-cars-home/parking',
    method: 'put',
    data: data
  })
}

// 删除车场车位
export function delParking(chpid) {
  return request({
    url: '/sys-cars-home/parking/' + chpid,
    method: 'delete'
  })
}
