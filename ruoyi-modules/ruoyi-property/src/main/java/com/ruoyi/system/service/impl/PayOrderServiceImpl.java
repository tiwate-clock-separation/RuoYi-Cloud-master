package com.ruoyi.system.service.impl;

import java.util.List;
import com.ruoyi.common.core.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.PayOrderMapper;
import com.ruoyi.domain.PayOrder;
import com.ruoyi.system.service.IPayOrderService;

/**
 * 停车订单Service业务层处理
 *
 * @author ruoyi
 * @date 2023-11-30
 */
@Service
public class PayOrderServiceImpl implements IPayOrderService
{
    @Autowired
    private PayOrderMapper payOrderMapper;

    /**
     * 查询停车订单
     *
     * @param poid 停车订单主键
     * @return 停车订单
     */
    @Override
    public PayOrder selectPayOrderByPoid(Long poid)
    {
        return payOrderMapper.selectPayOrderByPoid(poid);
    }

    /**
     * 查询停车订单列表
     *
     * @param payOrder 停车订单
     * @return 停车订单
     */
    @Override
    public List<PayOrder> selectPayOrderList(PayOrder payOrder)
    {
        return payOrderMapper.selectPayOrderList(payOrder);
    }

    /**
     * 新增停车订单
     *
     * @param payOrder 停车订单
     * @return 结果
     */
    @Override
    public int insertPayOrder(PayOrder payOrder)
    {
        payOrder.setCreateTime(DateUtils.getNowDate());
        return payOrderMapper.insertPayOrder(payOrder);
    }

    /**
     * 修改停车订单
     *
     * @param payOrder 停车订单
     * @return 结果
     */
    @Override
    public int updatePayOrder(PayOrder payOrder)
    {
        payOrder.setUpdateTime(DateUtils.getNowDate());
        return payOrderMapper.updatePayOrder(payOrder);
    }

    /**
     * 批量删除停车订单
     *
     * @param poids 需要删除的停车订单主键
     * @return 结果
     */
    @Override
    public int deletePayOrderByPoids(Long[] poids)
    {
        return payOrderMapper.deletePayOrderByPoids(poids);
    }

    /**
     * 删除停车订单信息
     *
     * @param poid 停车订单主键
     * @return 结果
     */
    @Override
    public int deletePayOrderByPoid(Long poid)
    {
        return payOrderMapper.deletePayOrderByPoid(poid);
    }
}
