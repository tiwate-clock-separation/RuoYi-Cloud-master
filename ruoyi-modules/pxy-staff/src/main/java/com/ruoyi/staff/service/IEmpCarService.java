package com.ruoyi.staff.service;

import java.util.List;
import com.ruoyi.domain.EmpCar;

/**
 * 车场人员Service接口
 * 
 * @author pxy
 * @date 2023-12-23
 */
public interface IEmpCarService 
{
    /**
     * 查询车场人员
     * 
     * @param carid 车场人员主键
     * @return 车场人员
     */
    public EmpCar selectEmpCarByCarid(Long carid);

    /**
     * 查询车场人员列表
     * 
     * @param empCar 车场人员
     * @return 车场人员集合
     */
    public List<EmpCar> selectEmpCarList(EmpCar empCar);

    /**
     * 新增车场人员
     * 
     * @param empCar 车场人员
     * @return 结果
     */
    public int insertEmpCar(EmpCar empCar);

    /**
     * 修改车场人员
     * 
     * @param empCar 车场人员
     * @return 结果
     */
    public int updateEmpCar(EmpCar empCar);

    /**
     * 批量删除车场人员
     * 
     * @param carids 需要删除的车场人员主键集合
     * @return 结果
     */
    public int deleteEmpCarByCarids(Long[] carids);

    /**
     * 删除车场人员信息
     * 
     * @param carid 车场人员主键
     * @return 结果
     */
    public int deleteEmpCarByCarid(Long carid);
}
