package com.ruoyi.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.core.annotation.Excel;
import com.ruoyi.common.core.web.domain.BaseEntity;

/**
 * 异常车辆记录对象 exception_record
 * 
 * @author ruoyi
 * @date 2023-12-06
 */
public class ExceptionRecord extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 自增ID */
    private Long erid;

    /** 唯一编码,前台使用 */
    private String code;

    /** 状态 */
    private Long status;

    /** 是否删除 */
    private Long deleted;

    /** 车辆类型 */
    @Excel(name = "车辆类型")
    private Long ctid;

    /** 备注 */
    @Excel(name = "备注")
    private String exceptionRemark;

    /** 照片 */
    @Excel(name = "照片")
    private String exceptionPic;

    /** 小区id */
    @Excel(name = "小区id")
    private Long pmid;

    /** 停车场id */
    @Excel(name = "停车场id")
    private Long hpid;

    public Long getCmid() {
        return cmid;
    }

    public void setCmid(Long cmid) {
        this.cmid = cmid;
    }

    @Excel(name = "车辆信息id")
    private Long cmid;

    private Long eqid;

    public Long getEqid() {
        return eqid;
    }

    public void setEqid(Long eqid) {
        this.eqid = eqid;
    }

    private PropertyManagement propertyManagement;

    private CarsHome carsHome;

    private CarsMsg carsMsg;

    private CarsHomeEq carsHomeEq;

    public CarsHomeEq getCarsHomeEq() {
        return carsHomeEq;
    }

    public void setCarsHomeEq(CarsHomeEq carsHomeEq) {
        this.carsHomeEq = carsHomeEq;
    }

    public CarsMsg getCarsMsg() {
        return carsMsg;
    }

    public void setCarsMsg(CarsMsg carsMsg) {
        this.carsMsg = carsMsg;
    }

    public PropertyManagement getPropertyManagement() {
        return propertyManagement;
    }

    public void setPropertyManagement(PropertyManagement propertyManagement) {
        this.propertyManagement = propertyManagement;
    }

    public CarsHome getCarsHome() {
        return carsHome;
    }

    public void setCarsHome(CarsHome carsHome) {
        this.carsHome = carsHome;
    }

    public void setErid(Long erid)
    {
        this.erid = erid;
    }

    public Long getErid() 
    {
        return erid;
    }
    public void setCode(String code) 
    {
        this.code = code;
    }

    public String getCode() 
    {
        return code;
    }
    public void setStatus(Long status) 
    {
        this.status = status;
    }

    public Long getStatus() 
    {
        return status;
    }
    public void setDeleted(Long deleted) 
    {
        this.deleted = deleted;
    }

    public Long getDeleted() 
    {
        return deleted;
    }
    public void setCtid(Long ctid) 
    {
        this.ctid = ctid;
    }

    public Long getCtid() 
    {
        return ctid;
    }
    public void setExceptionRemark(String exceptionRemark) 
    {
        this.exceptionRemark = exceptionRemark;
    }

    public String getExceptionRemark() 
    {
        return exceptionRemark;
    }
    public void setExceptionPic(String exceptionPic) 
    {
        this.exceptionPic = exceptionPic;
    }

    public String getExceptionPic() 
    {
        return exceptionPic;
    }

    public Long getPmid() {
        return pmid;
    }

    public void setPmid(Long pmid) {
        this.pmid = pmid;
    }

    public void setHpid(Long hpid)
    {
        this.hpid = hpid;
    }

    public Long getHpid() 
    {
        return hpid;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("erid", getErid())
            .append("code", getCode())
            .append("status", getStatus())
            .append("deleted", getDeleted())
            .append("createTime", getCreateTime())
            .append("updateTime", getUpdateTime())
            .append("ctid", getCtid())
            .append("exceptionRemark", getExceptionRemark())
            .append("exceptionPic", getExceptionPic())
            .append("ohid", getPmid())
            .append("hpid", getHpid())
            .append("cmid", getCmid())
            .append("eqid", getEqid())
            .toString();
    }
}
