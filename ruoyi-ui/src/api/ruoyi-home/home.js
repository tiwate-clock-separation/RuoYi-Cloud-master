import request from '@/utils/request'

// 查询车场数据列表 (下拉框)
export function listHome(query) {
  return request({
    url: '/sys-cars-home/home/list',
    method: 'get',
    params: query
  })
}
// 查询物业数据列表 (下拉框)
export function listProperty(query) {
  return request({
    url: '/sys-property/property/list',
    method: 'get',
    params: query
  })
}
// 查询业主信息管理列表
export function listOwner_info(query) {
  return request({
    url: '/sys-cars-info/owner_info/list',
    method: 'get',
    params: query
  })
}
// 查询车辆牌照颜色列表
export function listColors(query) {
  return request({
    url: '/sys-cars-home/colors/list',
    method: 'get',
    params: query
  })
}
