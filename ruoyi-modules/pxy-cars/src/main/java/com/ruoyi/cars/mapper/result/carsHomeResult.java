package com.ruoyi.cars.mapper.result;

import com.ruoyi.domain.CarsHome;
import org.apache.ibatis.annotations.Select;

public interface carsHomeResult {
    @Select("select * from cars_home where chid = ${hpid}")
    CarsHome getCarsHome();
}
