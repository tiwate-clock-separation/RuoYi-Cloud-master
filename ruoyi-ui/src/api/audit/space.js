import request from '@/utils/request'

// 查询车位审核列表
export function listSpace(query) {
  return request({
    url: '/ruoyi-audit/space/list',
    method: 'get',
    params: query
  })
}

// 查询车位审核详细
export function getSpace(said) {
  return request({
    url: '/ruoyi-audit/space/' + said,
    method: 'get'
  })
}

// 新增车位审核
export function addSpace(data) {
  return request({
    url: '/ruoyi-audit/space',
    method: 'post',
    data: data
  })
}

// 修改车位审核
export function updateSpace(data) {
  return request({
    url: '/ruoyi-audit/space',
    method: 'put',
    data: data
  })
}

// 删除车位审核
export function delSpace(said) {
  return request({
    url: '/ruoyi-audit/space/' + said,
    method: 'delete'
  })
}
