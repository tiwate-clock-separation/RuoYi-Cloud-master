package com.ruoyi.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;

import com.ruoyi.domain.CarsMsg;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.log.annotation.Log;
import com.ruoyi.common.log.enums.BusinessType;
import com.ruoyi.common.security.annotation.RequiresPermissions;
import com.ruoyi.service.ICarsMsgService;
import com.ruoyi.common.core.web.controller.BaseController;
import com.ruoyi.common.core.web.domain.AjaxResult;
import com.ruoyi.common.core.utils.poi.ExcelUtil;
import com.ruoyi.common.core.web.page.TableDataInfo;

/**
 * 车辆信息Controller
 *
 * @author ruoyi
 * @date 2023-12-07
 */
@RestController
@RequestMapping("/cars_info")
public class CarsMsgController extends BaseController
{
    @Autowired
    private ICarsMsgService carsMsgService;

    /**
     * 查询车辆信息列表
     */
    @RequiresPermissions("info:cars_info:list")
    @GetMapping("/list")
    public TableDataInfo list(CarsMsg carsMsg)
    {
        startPage();
        List<CarsMsg> list = carsMsgService.selectCarsMsgList(carsMsg);
        return getDataTable(list);
    }

    /**
     * 导出车辆信息列表
     */
    @RequiresPermissions("info:cars_info:export")
    @Log(title = "车辆信息", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, CarsMsg carsMsg)
    {
        List<CarsMsg> list = carsMsgService.selectCarsMsgList(carsMsg);
        ExcelUtil<CarsMsg> util = new ExcelUtil<CarsMsg>(CarsMsg.class);
        util.exportExcel(response, list, "车辆信息数据");
    }

    /**
     * 获取车辆信息详细信息
     */
    @RequiresPermissions("info:cars_info:query")
    @GetMapping(value = "/{cmid}")
    public AjaxResult getInfo(@PathVariable("cmid") Long cmid)
    {
        return success(carsMsgService.selectCarsMsgByCmid(cmid));
    }

    /**
     * 新增车辆信息
     */
    @RequiresPermissions("info:cars_info:add")
    @Log(title = "车辆信息", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody CarsMsg carsMsg)
    {
        return toAjax(carsMsgService.insertCarsMsg(carsMsg));
    }

    /**
     * 修改车辆信息
     */
    @RequiresPermissions("info:cars_info:edit")
    @Log(title = "车辆信息", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody CarsMsg carsMsg)
    {
        return toAjax(carsMsgService.updateCarsMsg(carsMsg));
    }

    /**
     * 删除车辆信息
     */
    @RequiresPermissions("info:cars_info:remove")
    @Log(title = "车辆信息", businessType = BusinessType.DELETE)
	@DeleteMapping("/{cmids}")
    public AjaxResult remove(@PathVariable Long[] cmids)
    {
        return toAjax(carsMsgService.deleteCarsMsgByCmids(cmids));
    }
}
