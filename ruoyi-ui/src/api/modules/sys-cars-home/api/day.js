import request from '@/utils/request'

// 查询按天收费列表
export function listDay(query) {
  return request({
    url: '/sys-cars-home/day/list',
    method: 'get',
    params: query
  })
}

// 查询按天收费详细
export function getDay(dayid) {
  return request({
    url: '/sys-cars-home/day/' + dayid,
    method: 'get'
  })
}

// 新增按天收费
export function addDay(data) {
  return request({
    url: '/sys-cars-home/day',
    method: 'post',
    data: data
  })
}

// 修改按天收费
export function updateDay(data) {
  return request({
    url: '/sys-cars-home/day',
    method: 'put',
    data: data
  })
}

// 删除按天收费
export function delDay(dayid) {
  return request({
    url: '/sys-cars-home/day/' + dayid,
    method: 'delete'
  })
}
