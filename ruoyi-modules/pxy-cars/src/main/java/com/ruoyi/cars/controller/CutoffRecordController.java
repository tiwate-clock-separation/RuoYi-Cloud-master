package com.ruoyi.cars.controller;

import com.ruoyi.common.core.utils.poi.ExcelUtil;
import com.ruoyi.common.core.web.controller.BaseController;
import com.ruoyi.common.core.web.domain.AjaxResult;
import com.ruoyi.common.core.web.page.TableDataInfo;
import com.ruoyi.common.log.annotation.Log;
import com.ruoyi.common.log.enums.BusinessType;
import com.ruoyi.common.security.annotation.RequiresPermissions;
import com.ruoyi.domain.CutoffRecord;
import com.ruoyi.cars.service.ICutoffRecordService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * 开闸记录Controller
 *
 * @author ruoyi
 * @date 2023-11-29
 */
@RestController
@RequestMapping("/record")
public class CutoffRecordController extends BaseController
{
    @Autowired
    private ICutoffRecordService cutoffRecordService;

    /**
     * 查询开闸记录列表
     */
    @RequiresPermissions("pxy-cars:record:list")
    @GetMapping("/list")
    public TableDataInfo list(CutoffRecord cutoffRecord)
    {
        startPage();
        List<CutoffRecord> list = cutoffRecordService.selectCutoffRecordList(cutoffRecord);
        return getDataTable(list);
    }

    /**
     * 导出开闸记录列表
     */
    @RequiresPermissions("pxy-cars:record:export")
    @Log(title = "开闸记录", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, CutoffRecord cutoffRecord)
    {
        List<CutoffRecord> list = cutoffRecordService.selectCutoffRecordList(cutoffRecord);
        ExcelUtil<CutoffRecord> util = new ExcelUtil<CutoffRecord>(CutoffRecord.class);
        util.exportExcel(response, list, "开闸记录数据");
    }

    /**
     * 获取开闸记录详细信息
     */
    @RequiresPermissions("pxy-cars:record:query")
    @GetMapping(value = "/{corid}")
    public AjaxResult getInfo(@PathVariable("corid") Long corid)
    {
        return success(cutoffRecordService.selectCutoffRecordByCorid(corid));
    }

    /**
     * 新增开闸记录
     */
    @RequiresPermissions("pxy-cars:record:add")
    @Log(title = "开闸记录", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody CutoffRecord cutoffRecord)
    {
        return toAjax(cutoffRecordService.insertCutoffRecord(cutoffRecord));
    }

    /**
     * 修改开闸记录
     */
    @RequiresPermissions("pxy-cars:record:edit")
    @Log(title = "开闸记录", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody CutoffRecord cutoffRecord)
    {
        return toAjax(cutoffRecordService.updateCutoffRecord(cutoffRecord));
    }

    /**
     * 删除开闸记录
     */
    @RequiresPermissions("pxy-cars:record:remove")
    @Log(title = "开闸记录", businessType = BusinessType.DELETE)
	@DeleteMapping("/{corids}")
    public AjaxResult remove(@PathVariable Long[] corids)
    {
        return toAjax(cutoffRecordService.deleteCutoffRecordByCorids(corids));
    }
}
