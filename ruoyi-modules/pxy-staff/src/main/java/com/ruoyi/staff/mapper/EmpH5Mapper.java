package com.ruoyi.staff.mapper;

import java.util.List;
import com.ruoyi.domain.EmpH5;

/**
 * H5人员Mapper接口
 * 
 * @author ruoyi
 * @date 2023-12-15
 */
public interface EmpH5Mapper 
{
    /**
     * 查询H5人员
     * 
     * @param ehid H5人员主键
     * @return H5人员
     */
    public EmpH5 selectEmpH5ByEhid(Long ehid);

    /**
     * 查询H5人员列表
     * 
     * @param empH5 H5人员
     * @return H5人员集合
     */
    public List<EmpH5> selectEmpH5List(EmpH5 empH5);

    /**
     * 新增H5人员
     * 
     * @param empH5 H5人员
     * @return 结果
     */
    public int insertEmpH5(EmpH5 empH5);

    /**
     * 修改H5人员
     * 
     * @param empH5 H5人员
     * @return 结果
     */
    public int updateEmpH5(EmpH5 empH5);

    /**
     * 删除H5人员
     * 
     * @param ehid H5人员主键
     * @return 结果
     */
    public int deleteEmpH5ByEhid(Long ehid);

    /**
     * 批量删除H5人员
     * 
     * @param ehids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteEmpH5ByEhids(Long[] ehids);
}
