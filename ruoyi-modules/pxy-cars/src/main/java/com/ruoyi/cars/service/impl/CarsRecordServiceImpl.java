package com.ruoyi.cars.service.impl;

import java.util.List;
import com.ruoyi.common.core.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.cars.mapper.CarsRecordMapper;
import com.ruoyi.domain.CarsRecord;
import com.ruoyi.cars.service.ICarsRecordService;

/**
 * 进出记录Service业务层处理
 *
 * @author ruoyi
 * @date 2023-11-30
 */
@Service
public class CarsRecordServiceImpl implements ICarsRecordService
{
    @Autowired
    private CarsRecordMapper carsRecordMapper;

    /**
     * 查询进出记录
     *
     * @param crid 进出记录主键
     * @return 进出记录
     */
    @Override
    public CarsRecord selectCarsRecordByCrid(Long crid)
    {
        return carsRecordMapper.selectCarsRecordByCrid(crid);
    }

    /**
     * 查询进出记录列表
     *
     * @param carsRecord 进出记录
     * @return 进出记录
     */
    @Override
    public List<CarsRecord> selectCarsRecordList(CarsRecord carsRecord)
    {
        return carsRecordMapper.selectCarsRecordList(carsRecord);
    }

    /**
     * 新增进出记录
     *
     * @param carsRecord 进出记录
     * @return 结果
     */
    @Override
    public int insertCarsRecord(CarsRecord carsRecord)
    {
        carsRecord.setCreateTime(DateUtils.getNowDate());
        return carsRecordMapper.insertCarsRecord(carsRecord);
    }

    /**
     * 修改进出记录
     *
     * @param carsRecord 进出记录
     * @return 结果
     */
    @Override
    public int updateCarsRecord(CarsRecord carsRecord)
    {
        carsRecord.setUpdateTime(DateUtils.getNowDate());
        return carsRecordMapper.updateCarsRecord(carsRecord);
    }

    /**
     * 批量删除进出记录
     *
     * @param crids 需要删除的进出记录主键
     * @return 结果
     */
    @Override
    public int deleteCarsRecordByCrids(Long[] crids)
    {
        return carsRecordMapper.deleteCarsRecordByCrids(crids);
    }

    /**
     * 删除进出记录信息
     *
     * @param crid 进出记录主键
     * @return 结果
     */
    @Override
    public int deleteCarsRecordByCrid(Long crid)
    {
        return carsRecordMapper.deleteCarsRecordByCrid(crid);
    }
}
