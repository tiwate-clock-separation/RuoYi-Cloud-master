package com.ruoyi.service;

import java.util.List;
import com.ruoyi.domain.CarsHomeParking;

/**
 * 车场车位Service接口
 * 
 * @author chn
 * @date 2023-12-08
 */
public interface ICarsHomeParkingService 
{
    /**
     * 查询车场车位
     * 
     * @param chpid 车场车位主键
     * @return 车场车位
     */
    public CarsHomeParking selectCarsHomeParkingByChpid(Long chpid);

    /**
     * 查询车场车位列表
     * 
     * @param carsHomeParking 车场车位
     * @return 车场车位集合
     */
    public List<CarsHomeParking> selectCarsHomeParkingList(CarsHomeParking carsHomeParking);

    /**
     * 新增车场车位
     * 
     * @param carsHomeParking 车场车位
     * @return 结果
     */
    public int insertCarsHomeParking(CarsHomeParking carsHomeParking);

    /**
     * 修改车场车位
     * 
     * @param carsHomeParking 车场车位
     * @return 结果
     */
    public int updateCarsHomeParking(CarsHomeParking carsHomeParking);

    /**
     * 批量删除车场车位
     * 
     * @param chpids 需要删除的车场车位主键集合
     * @return 结果
     */
    public int deleteCarsHomeParkingByChpids(Long[] chpids);

    /**
     * 删除车场车位信息
     * 
     * @param chpid 车场车位主键
     * @return 结果
     */
    public int deleteCarsHomeParkingByChpid(Long chpid);
}
