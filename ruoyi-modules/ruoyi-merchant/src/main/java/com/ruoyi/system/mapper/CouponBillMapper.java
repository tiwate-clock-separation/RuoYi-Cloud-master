package com.ruoyi.system.mapper;

import com.ruoyi.domain.CouponBill;

import java.util.List;


/**
 * 优惠卷账单Mapper接口
 *
 * @author ruoyi
 * @date 2023-12-26
 */
public interface CouponBillMapper
{
    /**
     * 查询优惠卷账单
     *
     * @param id 优惠卷账单主键
     * @return 优惠卷账单
     */
    public CouponBill selectCouponBillById(Long id);

    /**
     * 查询优惠卷账单列表
     *
     * @param couponBill 优惠卷账单
     * @return 优惠卷账单集合
     */
    public List<CouponBill> selectCouponBillList(CouponBill couponBill);

    /**
     * 新增优惠卷账单
     *
     * @param couponBill 优惠卷账单
     * @return 结果
     */
    public int insertCouponBill(CouponBill couponBill);

    /**
     * 修改优惠卷账单
     *
     * @param couponBill 优惠卷账单
     * @return 结果
     */
    public int updateCouponBill(CouponBill couponBill);

    /**
     * 删除优惠卷账单
     *
     * @param id 优惠卷账单主键
     * @return 结果
     */
    public int deleteCouponBillById(Long id);

    /**
     * 批量删除优惠卷账单
     *
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteCouponBillByIds(Long[] ids);
}
