package com.ruoyi.system.mapper;

import com.ruoyi.domain.CouponCars;
import com.ruoyi.domain.CouponManagement;

import java.util.List;
import java.util.Map;

public interface StopCarsMapper {

    public List<CouponManagement> getCouponByCars(String licenseNumber);
    public long getpmidBychid(long chid);
    public int delete(Map map);


}
