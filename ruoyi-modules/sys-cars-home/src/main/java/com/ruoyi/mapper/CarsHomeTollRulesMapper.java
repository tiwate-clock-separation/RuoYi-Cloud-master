package com.ruoyi.mapper;

import java.util.List;

import com.ruoyi.domain.CarsHomeTollRules;

/**
 * 车场收费规则类型Mapper接口
 * 
 * @author ruoyi
 * @date 2023-12-04
 */
public interface CarsHomeTollRulesMapper 
{
    /**
     * 查询车场收费规则类型
     * 
     * @param chtrid 车场收费规则类型主键
     * @return 车场收费规则类型
     */
    public CarsHomeTollRules selectCarsHomeTollRulesByChtrid(Long chtrid);

    /**
     * 查询车场收费规则类型列表
     * 
     * @param carsHomeTollRules 车场收费规则类型
     * @return 车场收费规则类型集合
     */
    public List<CarsHomeTollRules> selectCarsHomeTollRulesList(CarsHomeTollRules carsHomeTollRules);

    /**
     * 新增车场收费规则类型
     * 
     * @param carsHomeTollRules 车场收费规则类型
     * @return 结果
     */
    public int insertCarsHomeTollRules(CarsHomeTollRules carsHomeTollRules);

    /**
     * 修改车场收费规则类型
     * 
     * @param carsHomeTollRules 车场收费规则类型
     * @return 结果
     */
    public int updateCarsHomeTollRules(CarsHomeTollRules carsHomeTollRules);

    /**
     * 删除车场收费规则类型
     * 
     * @param chtrid 车场收费规则类型主键
     * @return 结果
     */
    public int deleteCarsHomeTollRulesByChtrid(Long chtrid);

    /**
     * 批量删除车场收费规则类型
     * 
     * @param chtrids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteCarsHomeTollRulesByChtrids(Long[] chtrids);
}
