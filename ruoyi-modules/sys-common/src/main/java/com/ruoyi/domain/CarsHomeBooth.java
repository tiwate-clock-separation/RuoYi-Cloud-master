package com.ruoyi.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.core.annotation.Excel;
import com.ruoyi.common.core.web.domain.BaseEntity;

import java.util.List;

/**
 * 车场岗亭对象 cars_home_booth
 * 
 * @author chn
 * @date 2023-12-04
 */
public class CarsHomeBooth extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 岗亭ID */
    private Long chbid;

    /** 车场id */
    @Excel(name = "车场id")
    private Long chid;

    private CarsHome carsHome;

    public List<CarsHomeLane> getCarsHomeLaneList() {
        return carsHomeLaneList;
    }

    public void setCarsHomeLaneList(List<CarsHomeLane> carsHomeLaneList) {
        this.carsHomeLaneList = carsHomeLaneList;
    }

    private List<CarsHomeLane> carsHomeLaneList;



    public CarsHome getCarsHome() {
        return carsHome;
    }

    public void setCarsHome(CarsHome carsHome) {
        this.carsHome = carsHome;
    }

    private PropertyManagement propertyManagement;

    public PropertyManagement getPropertyManagement() {
        return propertyManagement;
    }

    public void setPropertyManagement(PropertyManagement propertyManagement) {
        this.propertyManagement = propertyManagement;
    }

    /** 岗亭名称 */
    @Excel(name = "岗亭名称")
    private String boothname;

    /** 岗亭ip */
    @Excel(name = "岗亭ip")
    private String boothip;

    /** 所处地址 */
    @Excel(name = "所处地址")
    private String boothaddr;

    /** 唯一编码,前台使用 */
    @Excel(name = "唯一编码,前台使用")
    private String code;

    /** 状态 */
    @Excel(name = "状态")
    private Long status;

    /** 是否删除 */
    @Excel(name = "是否删除")
    private Long deleted;

    public void setChbid(Long chbid) 
    {
        this.chbid = chbid;
    }

    public Long getChbid() 
    {
        return chbid;
    }
    public void setChid(Long chid) 
    {
        this.chid = chid;
    }

    public Long getChid() 
    {
        return chid;
    }
    public void setBoothname(String boothname) 
    {
        this.boothname = boothname;
    }

    public String getBoothname() 
    {
        return boothname;
    }
    public void setBoothip(String boothip) 
    {
        this.boothip = boothip;
    }

    public String getBoothip() 
    {
        return boothip;
    }
    public void setBoothaddr(String boothaddr) 
    {
        this.boothaddr = boothaddr;
    }

    public String getBoothaddr() 
    {
        return boothaddr;
    }
    public void setCode(String code) 
    {
        this.code = code;
    }

    public String getCode() 
    {
        return code;
    }
    public void setStatus(Long status) 
    {
        this.status = status;
    }

    public Long getStatus() 
    {
        return status;
    }
    public void setDeleted(Long deleted) 
    {
        this.deleted = deleted;
    }

    public Long getDeleted() 
    {
        return deleted;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("chbid", getChbid())
            .append("chid", getChid())
            .append("boothname", getBoothname())
            .append("boothip", getBoothip())
            .append("boothaddr", getBoothaddr())
            .append("remark", getRemark())
            .append("code", getCode())
            .append("status", getStatus())
            .append("deleted", getDeleted())
            .append("createTime", getCreateTime())
            .append("updateTime", getUpdateTime())
            .toString();
    }
}
