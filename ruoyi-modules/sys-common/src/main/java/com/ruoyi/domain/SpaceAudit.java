package com.ruoyi.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.core.annotation.Excel;
import com.ruoyi.common.core.web.domain.BaseEntity;

/**
 * 车位审核对象 space_audit
 *
 * @author ruoyi
 * @date 2023-12-03
 */
public class SpaceAudit extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 自增ID */
    private Long said;

    /** 唯一编码,前台使用 */
    @Excel(name = "唯一编码,前台使用")
    private String code;

    /** 状态 */
    @Excel(name = "状态")
    private Long status;

    /** 是否删除 */
    @Excel(name = "是否删除")
    private Long deleted;

    /** 小区id */
    @Excel(name = "小区id")
    private Long ohid;

    /** 车场id */
    @Excel(name = "车场id")
    private Long hpid;

    /** 业主id */
    @Excel(name = "业主id")
    private Long omid;

    /** 车位编号 */
    @Excel(name = "车位编号")
    private Long parkNum;

    /** 0 1 2 审核状态 */
    @Excel(name = "0 1 2 审核状态")
    private Long suditState;

    public void setSaid(Long said)
    {
        this.said = said;
    }

    public Long getSaid()
    {
        return said;
    }
    public void setCode(String code)
    {
        this.code = code;
    }

    public String getCode()
    {
        return code;
    }
    public void setStatus(Long status)
    {
        this.status = status;
    }

    public Long getStatus()
    {
        return status;
    }
    public void setDeleted(Long deleted)
    {
        this.deleted = deleted;
    }

    public Long getDeleted()
    {
        return deleted;
    }
    public void setOhid(Long ohid)
    {
        this.ohid = ohid;
    }

    public Long getOhid()
    {
        return ohid;
    }
    public void setHpid(Long hpid)
    {
        this.hpid = hpid;
    }

    public Long getHpid()
    {
        return hpid;
    }
    public void setOmid(Long omid)
    {
        this.omid = omid;
    }

    public Long getOmid()
    {
        return omid;
    }
    public void setParkNum(Long parkNum)
    {
        this.parkNum = parkNum;
    }

    public Long getParkNum()
    {
        return parkNum;
    }
    public void setSuditState(Long suditState)
    {
        this.suditState = suditState;
    }

    public Long getSuditState()
    {
        return suditState;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("said", getSaid())
            .append("code", getCode())
            .append("status", getStatus())
            .append("deleted", getDeleted())
            .append("createTime", getCreateTime())
            .append("updateTime", getUpdateTime())
            .append("ohid", getOhid())
            .append("hpid", getHpid())
            .append("omid", getOmid())
            .append("parkNum", getParkNum())
            .append("suditState", getSuditState())
            .toString();
    }
}
