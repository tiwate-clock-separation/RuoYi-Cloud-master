import request from '@/utils/request'

// 查询停车报列表
export function listPark(query) {
  return request({
    url: '/pxy-report/park/list',
    method: 'get',
    params: query
  })
}

// 查询停车报详细
export function getPark(parkid) {
  return request({
    url: '/pxy-report/park/' + parkid,
    method: 'get'
  })
}

// 新增停车报
export function addPark(data) {
  return request({
    url: '/pxy-report/park',
    method: 'post',
    data: data
  })
}

// 修改停车报
export function updatePark(data) {
  return request({
    url: '/pxy-report/park',
    method: 'put',
    data: data
  })
}

// 删除停车报
export function delPark(parkid) {
  return request({
    url: '/pxy-report/park/' + parkid,
    method: 'delete'
  })
}
