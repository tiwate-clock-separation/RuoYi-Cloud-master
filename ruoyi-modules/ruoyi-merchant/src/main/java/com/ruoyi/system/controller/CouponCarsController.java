package com.ruoyi.system.controller;

import java.util.List;
import java.io.IOException;
import javax.servlet.http.HttpServletResponse;

import com.ruoyi.domain.CouponCars;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.log.annotation.Log;
import com.ruoyi.common.log.enums.BusinessType;
import com.ruoyi.common.security.annotation.RequiresPermissions;
import com.ruoyi.system.service.ICouponCarsService;
import com.ruoyi.common.core.web.controller.BaseController;
import com.ruoyi.common.core.web.domain.AjaxResult;
import com.ruoyi.common.core.utils.poi.ExcelUtil;
import com.ruoyi.common.core.web.page.TableDataInfo;

/**
 * 优惠卷车辆连Controller
 *
 * @author ruoyi
 * @date 2023-12-15
 */
@RestController
@RequestMapping("/cars")
public class CouponCarsController extends BaseController
{
    @Autowired
    private ICouponCarsService couponCarsService;

    /**
     * 查询优惠卷车辆连列表
     */
    @RequiresPermissions("system:cars:list")
    @GetMapping("/list")
    public TableDataInfo list(CouponCars couponCars)
    {
        startPage();
        List<CouponCars> list = couponCarsService.selectCouponCarsList(couponCars);
        return getDataTable(list);
    }

    /**
     * 导出优惠卷车辆连列表
     */
    @RequiresPermissions("system:cars:export")
    @Log(title = "优惠卷车辆连", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, CouponCars couponCars)
    {
        List<CouponCars> list = couponCarsService.selectCouponCarsList(couponCars);
        ExcelUtil<CouponCars> util = new ExcelUtil<CouponCars>(CouponCars.class);
        util.exportExcel(response, list, "优惠卷车辆连数据");
    }

    /**
     * 获取优惠卷车辆连详细信息
     */
    @RequiresPermissions("system:cars:query")
    @GetMapping(value = "/{ccid}")
    public AjaxResult getInfo(@PathVariable("ccid") Long ccid)
    {
        return success(couponCarsService.selectCouponCarsByCcid(ccid));
    }

    /**
     * 新增优惠卷车辆连
     */
    @RequiresPermissions("system:cars:add")
    @Log(title = "优惠卷车辆连", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody CouponCars couponCars)
    {
        return toAjax(couponCarsService.insertCouponCars(couponCars));
    }

    /**
     * 修改优惠卷车辆连
     */
    @RequiresPermissions("system:cars:edit")
    @Log(title = "优惠卷车辆连", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody CouponCars couponCars)
    {
        return toAjax(couponCarsService.updateCouponCars(couponCars));
    }

    /**
     * 删除优惠卷车辆连
     */
    @RequiresPermissions("system:cars:remove")
    @Log(title = "优惠卷车辆连", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ccids}")
    public AjaxResult remove(@PathVariable Long[] ccids)
    {
        return toAjax(couponCarsService.deleteCouponCarsByCcids(ccids));
    }
}
