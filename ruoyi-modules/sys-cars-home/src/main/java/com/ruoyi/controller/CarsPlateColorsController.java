package com.ruoyi.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.log.annotation.Log;
import com.ruoyi.common.log.enums.BusinessType;
import com.ruoyi.common.security.annotation.RequiresPermissions;
import com.ruoyi.domain.CarsPlateColors;
import com.ruoyi.service.ICarsPlateColorsService;
import com.ruoyi.common.core.web.controller.BaseController;
import com.ruoyi.common.core.web.domain.AjaxResult;
import com.ruoyi.common.core.utils.poi.ExcelUtil;
import com.ruoyi.common.core.web.page.TableDataInfo;

/**
 * 车辆牌照颜色Controller
 * 
 * @author chn
 * @date 2023-12-04
 */
@RestController
@RequestMapping("/colors")
public class CarsPlateColorsController extends BaseController
{
    @Autowired
    private ICarsPlateColorsService carsPlateColorsService;

    /**
     * 查询车辆牌照颜色列表
     */
    @RequiresPermissions("sys-cars-home:colors:list")
    @GetMapping("/list")
    public TableDataInfo list(CarsPlateColors carsPlateColors)
    {
        startPage();
        List<CarsPlateColors> list = carsPlateColorsService.selectCarsPlateColorsList(carsPlateColors);
        return getDataTable(list);
    }

    /**
     * 导出车辆牌照颜色列表
     */
    @RequiresPermissions("sys-cars-home:colors:export")
    @Log(title = "车辆牌照颜色", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, CarsPlateColors carsPlateColors)
    {
        List<CarsPlateColors> list = carsPlateColorsService.selectCarsPlateColorsList(carsPlateColors);
        ExcelUtil<CarsPlateColors> util = new ExcelUtil<CarsPlateColors>(CarsPlateColors.class);
        util.exportExcel(response, list, "车辆牌照颜色数据");
    }

    /**
     * 获取车辆牌照颜色详细信息
     */
    @RequiresPermissions("sys-cars-home:colors:query")
    @GetMapping(value = "/{lpcid}")
    public AjaxResult getInfo(@PathVariable("lpcid") Long lpcid)
    {
        return success(carsPlateColorsService.selectCarsPlateColorsByLpcid(lpcid));
    }

    /**
     * 新增车辆牌照颜色
     */
    @RequiresPermissions("sys-cars-home:colors:add")
    @Log(title = "车辆牌照颜色", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody CarsPlateColors carsPlateColors)
    {
        return toAjax(carsPlateColorsService.insertCarsPlateColors(carsPlateColors));
    }

    /**
     * 修改车辆牌照颜色
     */
    @RequiresPermissions("sys-cars-home:colors:edit")
    @Log(title = "车辆牌照颜色", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody CarsPlateColors carsPlateColors)
    {
        return toAjax(carsPlateColorsService.updateCarsPlateColors(carsPlateColors));
    }

    /**
     * 删除车辆牌照颜色
     */
    @RequiresPermissions("sys-cars-home:colors:remove")
    @Log(title = "车辆牌照颜色", businessType = BusinessType.DELETE)
	@DeleteMapping("/{lpcids}")
    public AjaxResult remove(@PathVariable Long[] lpcids)
    {
        return toAjax(carsPlateColorsService.deleteCarsPlateColorsByLpcids(lpcids));
    }
}
