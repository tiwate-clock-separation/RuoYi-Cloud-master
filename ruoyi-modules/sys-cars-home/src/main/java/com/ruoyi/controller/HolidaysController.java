package com.ruoyi.controller;

import java.util.List;
import java.io.IOException;
import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.log.annotation.Log;
import com.ruoyi.common.log.enums.BusinessType;
import com.ruoyi.common.security.annotation.RequiresPermissions;
import com.ruoyi.domain.Holidays;
import com.ruoyi.service.IHolidaysService;
import com.ruoyi.common.core.web.controller.BaseController;
import com.ruoyi.common.core.web.domain.AjaxResult;
import com.ruoyi.common.core.utils.poi.ExcelUtil;
import com.ruoyi.common.core.web.page.TableDataInfo;

/**
 * 车场节假日Controller
 * 
 * @author chn
 * @date 2023-12-15
 */
@RestController
@RequestMapping("/holidays")
public class HolidaysController extends BaseController
{
    @Autowired
    private IHolidaysService holidaysService;

    /**
     * 查询车场节假日列表
     */
    @RequiresPermissions("sys-cars-home:holidays:list")
    @GetMapping("/list")
    public TableDataInfo list(Holidays holidays)
    {
        startPage();
        List<Holidays> list = holidaysService.selectHolidaysList(holidays);
        return getDataTable(list);
    }

    /**
     * 导出车场节假日列表
     */
    @RequiresPermissions("sys-cars-home:holidays:export")
    @Log(title = "车场节假日", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, Holidays holidays)
    {
        List<Holidays> list = holidaysService.selectHolidaysList(holidays);
        ExcelUtil<Holidays> util = new ExcelUtil<Holidays>(Holidays.class);
        util.exportExcel(response, list, "车场节假日数据");
    }

    /**
     * 获取车场节假日详细信息
     */
    @RequiresPermissions("sys-cars-home:holidays:query")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(holidaysService.selectHolidaysById(id));
    }

    /**
     * 新增车场节假日
     */
    @RequiresPermissions("sys-cars-home:holidays:add")
    @Log(title = "车场节假日", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody Holidays holidays)
    {
        return toAjax(holidaysService.insertHolidays(holidays));
    }

    /**
     * 修改车场节假日
     */
    @RequiresPermissions("sys-cars-home:holidays:edit")
    @Log(title = "车场节假日", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody Holidays holidays)
    {
        return toAjax(holidaysService.updateHolidays(holidays));
    }

    /**
     * 删除车场节假日
     */
    @RequiresPermissions("sys-cars-home:holidays:remove")
    @Log(title = "车场节假日", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(holidaysService.deleteHolidaysByIds(ids));
    }
}
