package com.ruoyi.mapper;

import java.util.List;

import com.ruoyi.domain.CarsHomeBooth;

/**
 * 车场岗亭Mapper接口
 * 
 * @author chn
 * @date 2023-12-04
 */
public interface CarsHomeBoothMapper 
{
    /**
     * 查询车场岗亭
     * 
     * @param chbid 车场岗亭主键
     * @return 车场岗亭
     */
    public CarsHomeBooth selectCarsHomeBoothByChbid(Long chbid);

    //查询一对多岗亭下多个车厂
    public List<CarsHomeBooth> getBoothLaneList(Long chid);

    /**
     * 查询车场岗亭列表
     * 
     * @param carsHomeBooth 车场岗亭
     * @return 车场岗亭集合
     */
    public List<CarsHomeBooth> selectCarsHomeBoothList(CarsHomeBooth carsHomeBooth);

    /**
     * 新增车场岗亭
     * 
     * @param carsHomeBooth 车场岗亭
     * @return 结果
     */
    public int insertCarsHomeBooth(CarsHomeBooth carsHomeBooth);

    /**
     * 修改车场岗亭
     * 
     * @param carsHomeBooth 车场岗亭
     * @return 结果
     */
    public int updateCarsHomeBooth(CarsHomeBooth carsHomeBooth);

    /**
     * 删除车场岗亭
     * 
     * @param chbid 车场岗亭主键
     * @return 结果
     */
    public int deleteCarsHomeBoothByChbid(Long chbid);

    /**
     * 批量删除车场岗亭
     * 
     * @param chbids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteCarsHomeBoothByChbids(Long[] chbids);
}
