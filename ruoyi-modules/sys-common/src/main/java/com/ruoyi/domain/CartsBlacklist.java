package com.ruoyi.domain;

import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.core.annotation.Excel;
import com.ruoyi.common.core.web.domain.BaseEntity;

/**
 * 车辆黑名单对象 carts_blacklist
 *
 * @author ruoyi
 * @date 2023-12-13
 */
public class CartsBlacklist extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 自增ID */
    private Long cbid;

    /** 唯一编码,前台使用 */
    @Excel(name = "唯一编码,前台使用")
    private String code;

    /** 状态 */
    @Excel(name = "状态")
    private Long status;

    /** 是否删除 */
    @Excel(name = "是否删除")
    private Long deleted;

    /** 车牌号 */
    @Excel(name = "车牌号")
    private String licenseNumber;

    /** 所停小区名称 */
    @Excel(name = "所停小区名称")
    private Long pmid;

    /** 记录设备id */
    @Excel(name = "记录设备id")
    private Long entifyid;

    /** 开始时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "开始时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date startTime;

    /** 车场id */
    @Excel(name = "车场id")
    private Long chid;

    /** 结束时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "结束时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date overTime;

    /** 0 黑 1 白 */
    @Excel(name = "0 黑 1 白")
    private Long carsState;

    private CarsHome carsHome;
    private PropertyManagement propertyManagement;
    private CarsHomeEqIdentify carsHomeEqIdentify;

    @Override
    public String toString() {
        return "CartsBlacklist{" +
                "cbid=" + cbid +
                ", code='" + code + '\'' +
                ", status=" + status +
                ", deleted=" + deleted +
                ", licenseNumber='" + licenseNumber + '\'' +
                ", pmid=" + pmid +
                ", entifyid=" + entifyid +
                ", startTime=" + startTime +
                ", chid=" + chid +
                ", overTime=" + overTime +
                ", carsState=" + carsState +
                ", carsHome=" + carsHome +
                ", propertyManagement=" + propertyManagement +
                ", carsHomeEqIdentify=" + carsHomeEqIdentify +
                "} " + super.toString();
    }

    public Long getCbid() {
        return cbid;
    }

    public void setCbid(Long cbid) {
        this.cbid = cbid;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Long getStatus() {
        return status;
    }

    public void setStatus(Long status) {
        this.status = status;
    }

    public Long getDeleted() {
        return deleted;
    }

    public void setDeleted(Long deleted) {
        this.deleted = deleted;
    }

    public String getLicenseNumber() {
        return licenseNumber;
    }

    public void setLicenseNumber(String licenseNumber) {
        this.licenseNumber = licenseNumber;
    }

    public Long getPmid() {
        return pmid;
    }

    public void setPmid(Long pmid) {
        this.pmid = pmid;
    }

    public Long getEntifyid() {
        return entifyid;
    }

    public void setEntifyid(Long entifyid) {
        this.entifyid = entifyid;
    }

    public Date getStartTime() {
        return startTime;
    }

    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }

    public Long getChid() {
        return chid;
    }

    public void setChid(Long chid) {
        this.chid = chid;
    }

    public Date getOverTime() {
        return overTime;
    }

    public void setOverTime(Date overTime) {
        this.overTime = overTime;
    }

    public Long getCarsState() {
        return carsState;
    }

    public void setCarsState(Long carsState) {
        this.carsState = carsState;
    }

    public CarsHome getCarsHome() {
        return carsHome;
    }

    public void setCarsHome(CarsHome carsHome) {
        this.carsHome = carsHome;
    }

    public PropertyManagement getPropertyManagement() {
        return propertyManagement;
    }

    public void setPropertyManagement(PropertyManagement propertyManagement) {
        this.propertyManagement = propertyManagement;
    }

    public CarsHomeEqIdentify getCarsHomeEqIdentify() {
        return carsHomeEqIdentify;
    }

    public void setCarsHomeEqIdentify(CarsHomeEqIdentify carsHomeEqIdentify) {
        this.carsHomeEqIdentify = carsHomeEqIdentify;
    }

    public CartsBlacklist() {
    }

    public CartsBlacklist(Long cbid, String code, Long status, Long deleted, String licenseNumber, Long pmid, Long entifyid, Date startTime, Long chid, Date overTime, Long carsState, CarsHome carsHome, PropertyManagement propertyManagement, CarsHomeEqIdentify carsHomeEqIdentify) {
        this.cbid = cbid;
        this.code = code;
        this.status = status;
        this.deleted = deleted;
        this.licenseNumber = licenseNumber;
        this.pmid = pmid;
        this.entifyid = entifyid;
        this.startTime = startTime;
        this.chid = chid;
        this.overTime = overTime;
        this.carsState = carsState;
        this.carsHome = carsHome;
        this.propertyManagement = propertyManagement;
        this.carsHomeEqIdentify = carsHomeEqIdentify;
    }
}
