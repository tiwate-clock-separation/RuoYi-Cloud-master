package com.ruoyi.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.core.annotation.Excel;
import com.ruoyi.common.core.web.domain.BaseEntity;

/**
 * 逃单报对象 report_skip
 * 
 * @author ruoyi
 * @date 2023-12-07
 */
public class ReportSkip extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 自增ID */
    private Long skid;

    /** 唯一编码,前台使用 */
    private String code;

    /** 状态 */
    @Excel(name = "状态")
    private Long status;

    /** 是否删除 */
    @Excel(name = "是否删除")
    private Long deleted;

    /** 物业名称 */
    @Excel(name = "物业名称")
    private Long pmid;

    /** 停车名称 */
    @Excel(name = "停车名称")
    private Long chid;

    /** 车牌号 */
    @Excel(name = "车牌号")
    private Long cmid;

    /** 车辆类型 */
    @Excel(name = "车辆类型")
    private Long ctid;

    /** 订单id */
    @Excel(name = "订单id")
    private Long poid;

    private CarsHome carsHome;

    private CarsMsg carsMsg;

    private CarsType carsType;

    private PayOrder payOrder;

    private PropertyManagement propertyManagement;

    public CarsHome getCarsHome() {
        return carsHome;
    }

    public void setCarsHome(CarsHome carsHome) {
        this.carsHome = carsHome;
    }

    public CarsMsg getCarsMsg() {
        return carsMsg;
    }

    public void setCarsMsg(CarsMsg carsMsg) {
        this.carsMsg = carsMsg;
    }

    public CarsType getCarsType() {
        return carsType;
    }

    public void setCarsType(CarsType carsType) {
        this.carsType = carsType;
    }

    public PayOrder getPayOrder() {
        return payOrder;
    }

    public void setPayOrder(PayOrder payOrder) {
        this.payOrder = payOrder;
    }

    public PropertyManagement getPropertyManagement() {
        return propertyManagement;
    }

    public void setPropertyManagement(PropertyManagement propertyManagement) {
        this.propertyManagement = propertyManagement;
    }

    public Long getSkid() {
        return skid;
    }

    public void setSkid(Long skid) {
        this.skid = skid;
    }

    public void setCode(String code)
    {
        this.code = code;
    }

    public String getCode() 
    {
        return code;
    }
    public void setStatus(Long status) 
    {
        this.status = status;
    }

    public Long getStatus() 
    {
        return status;
    }
    public void setDeleted(Long deleted) 
    {
        this.deleted = deleted;
    }

    public Long getDeleted() 
    {
        return deleted;
    }
    public void setPmid(Long pmid) 
    {
        this.pmid = pmid;
    }

    public Long getPmid() 
    {
        return pmid;
    }
    public void setChid(Long chid) 
    {
        this.chid = chid;
    }

    public Long getChid() 
    {
        return chid;
    }
    public void setCmid(Long cmid) 
    {
        this.cmid = cmid;
    }

    public Long getCmid() 
    {
        return cmid;
    }
    public void setCtid(Long ctid) 
    {
        this.ctid = ctid;
    }

    public Long getCtid() 
    {
        return ctid;
    }
    public void setPoid(Long poid) 
    {
        this.poid = poid;
    }

    public Long getPoid() 
    {
        return poid;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getSkid())
            .append("code", getCode())
            .append("status", getStatus())
            .append("deleted", getDeleted())
            .append("createTime", getCreateTime())
            .append("updateTime", getUpdateTime())
            .append("pmid", getPmid())
            .append("chid", getChid())
            .append("cmid", getCmid())
            .append("ctid", getCtid())
            .append("poid", getPoid())
            .toString();
    }
}
