package com.ruoyi.domain;

import com.ruoyi.common.core.annotation.Excel;
import com.ruoyi.common.core.web.domain.BaseEntity;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

/**
 * 停车报对象 report_park
 * 
 * @author ruoyi
 * @date 2023-12-07
 */
public class ReportPark extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 自增ID */
    private Long parkid;

    /** 唯一编码,前台使用 */
    @Excel(name = "唯一编码,前台使用")
    private String code;

    /** 状态 */
    @Excel(name = "状态")
    private Long status;

    /** 是否删除 */
    @Excel(name = "是否删除")
    private Long deleted;

    /** 物业名称 */
    @Excel(name = "物业名称")
    private Long pmid;

    /** 车场名称 */
    @Excel(name = "车场名称")
    private Long chid;

    /** 车辆名称 */
    @Excel(name = "车辆名称")
    private Long cmid;

    /** 车辆类型 */
    @Excel(name = "车辆类型")
    private Long ctid;

    /** 订单金额 */
    @Excel(name = "订单金额")
    private Long poid;

    /** 支付类型 */
    @Excel(name = "支付类型")
    private Long ptid;

    /** 放行原因 */
    @Excel(name = "放行原因")
    private String permit;

    private Long eqid;

    private Long ckid;

    private Long uuid;

    private Long youmian;

    public Long getYoumian() {
        return youmian;
    }

    public void setYoumian(Long youmian) {
        this.youmian = youmian;
    }

    private CarsHome carsHome;

    private CarsMsg carsMsg;

    private PropertyManagement propertyManagement;

    private CarsType carsType;

    private PayOrder payOrder;

    private PayType payType;

    private CarsHomeEq carsHomeEq;

    private CarsHomeEqChukou carsHomeEqChukou;


    public Long getUuid() {
        return uuid;
    }

    public void setUuid(Long uuid) {
        this.uuid = uuid;
    }

    public CarsHomeEq getCarsHomeEq() {
        return carsHomeEq;
    }

    public void setCarsHomeEq(CarsHomeEq carsHomeEq) {
        this.carsHomeEq = carsHomeEq;
    }

    public CarsHomeEqChukou getCarsHomeEqChukou() {
        return carsHomeEqChukou;
    }

    public void setCarsHomeEqChukou(CarsHomeEqChukou carsHomeEqChukou) {
        this.carsHomeEqChukou = carsHomeEqChukou;
    }

    public Long getEqid() {
        return eqid;
    }

    public void setEqid(Long eqid) {
        this.eqid = eqid;
    }

    public Long getCkid() {
        return ckid;
    }

    public void setCkid(Long ckid) {
        this.ckid = ckid;
    }

    public PropertyManagement getPropertyManagement() {
        return propertyManagement;
    }

    public void setPropertyManagement(PropertyManagement propertyManagement) {
        this.propertyManagement = propertyManagement;
    }

    public CarsHome getCarsHome() {
        return carsHome;
    }

    public void setCarsHome(CarsHome carsHome) {
        this.carsHome = carsHome;
    }

    public CarsMsg getCarsMsg() {
        return carsMsg;
    }

    public void setCarsMsg(CarsMsg carsMsg) {
        this.carsMsg = carsMsg;
    }

    public CarsType getCarsType() {
        return carsType;
    }

    public void setCarsType(CarsType carsType) {
        this.carsType = carsType;
    }

    public PayOrder getPayOrder() {
        return payOrder;
    }

    public void setPayOrder(PayOrder payOrder) {
        this.payOrder = payOrder;
    }

    public PayType getPayType() {
        return payType;
    }

    public void setPayType(PayType payType) {
        this.payType = payType;
    }

    public void setParkid(Long parkid)
    {
        this.parkid = parkid;
    }

    public Long getParkid() 
    {
        return parkid;
    }
    public void setCode(String code) 
    {
        this.code = code;
    }

    public String getCode() 
    {
        return code;
    }
    public void setStatus(Long status) 
    {
        this.status = status;
    }

    public Long getStatus() 
    {
        return status;
    }
    public void setDeleted(Long deleted) 
    {
        this.deleted = deleted;
    }

    public Long getDeleted() 
    {
        return deleted;
    }
    public void setPmid(Long pmid) 
    {
        this.pmid = pmid;
    }

    public Long getPmid() 
    {
        return pmid;
    }
    public void setChid(Long chid) 
    {
        this.chid = chid;
    }

    public Long getChid() 
    {
        return chid;
    }
    public void setCmid(Long cmid) 
    {
        this.cmid = cmid;
    }

    public Long getCmid() 
    {
        return cmid;
    }
    public void setCtid(Long ctid) 
    {
        this.ctid = ctid;
    }

    public Long getCtid() 
    {
        return ctid;
    }
    public void setPoid(Long poid) 
    {
        this.poid = poid;
    }

    public Long getPoid() 
    {
        return poid;
    }
    public void setPtid(Long ptid) 
    {
        this.ptid = ptid;
    }

    public Long getPtid() 
    {
        return ptid;
    }
    public void setPermit(String permit) 
    {
        this.permit = permit;
    }

    public String getPermit() 
    {
        return permit;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("parkid", getParkid())
            .append("code", getCode())
            .append("status", getStatus())
            .append("deleted", getDeleted())
            .append("createTime", getCreateTime())
            .append("updateTime", getUpdateTime())
            .append("pmid", getPmid())
            .append("chid", getChid())
            .append("cmid", getCmid())
            .append("ctid", getCtid())
            .append("poid", getPoid())
            .append("ptid", getPtid())
            .append("permit", getPermit())
            .append("uuid", getUuid())
            .toString();
    }
}
