package com.ruoyi.system.api;

import com.ruoyi.common.core.constant.HttpStatus;
import com.ruoyi.common.core.web.page.TableDataInfo;
import com.ruoyi.common.security.annotation.RequiresPermissions;
import com.ruoyi.domain.PayType;
import com.ruoyi.domain.PropertyManagement;
import com.ruoyi.domain.TradingType;
import com.ruoyi.system.mapper.result.PayTypeResult;
import com.ruoyi.system.mapper.result.TradingTypeResult;
import com.ruoyi.system.service.IPropertyManagementService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

import static com.ruoyi.common.core.utils.PageUtils.startPage;

@RestController
@RequestMapping("/propertyApi")
public class PropertyApl {
    @Autowired
    private IPropertyManagementService propertyManagementService;

    @Autowired
    private TradingTypeResult tradingTypeResult;

    @Autowired
    private PayTypeResult payTypeResult;

    /**
     * 查询物业管理列表
     */
    @RequiresPermissions("sys-property:property:list")
    @GetMapping("/list")
    public TableDataInfo list(PropertyManagement propertyManagement)
    {
        List<PropertyManagement> list = propertyManagementService.selectPropertyManagementList(propertyManagement);
        return getDataTable(list);
    }

    @RequiresPermissions("sys-property:property:list")
    @GetMapping("/ttlist")
    public TableDataInfo tradingTypeList()
    {
        List<TradingType> list = tradingTypeResult.selectTradingType();
        return getDataTable(list);
    }

    @RequiresPermissions("sys-property:property:list")
    @GetMapping("/ptlist")
    public TableDataInfo payTypeList()
    {
        List<PayType> list = payTypeResult.ptlist();
        return getDataTable(list);
    }

    protected TableDataInfo getDataTable(List<?> list)
    {
        TableDataInfo rspData = new TableDataInfo();
        rspData.setCode(HttpStatus.SUCCESS);
        rspData.setRows(list);
        rspData.setMsg("查询成功");
        rspData.setTotal(list.size());
        return rspData;
    }
}
