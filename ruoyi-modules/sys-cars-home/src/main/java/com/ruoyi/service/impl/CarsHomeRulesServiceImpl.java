package com.ruoyi.service.impl;

import java.util.List;
import com.ruoyi.common.core.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.mapper.CarsHomeRulesMapper;
import com.ruoyi.domain.CarsHomeRules;
import com.ruoyi.service.ICarsHomeRulesService;

/**
 * 车场计费规则Service业务层处理
 * 
 * @author ruoyi
 * @date 2023-12-02
 */
@Service
public class CarsHomeRulesServiceImpl implements ICarsHomeRulesService 
{
    @Autowired
    private CarsHomeRulesMapper carsHomeRulesMapper;

    /**
     * 查询车场计费规则
     * 
     * @param chrid 车场计费规则主键
     * @return 车场计费规则
     */
    @Override
    public CarsHomeRules selectCarsHomeRulesByChrid(Long chrid)
    {
        return carsHomeRulesMapper.selectCarsHomeRulesByChrid(chrid);
    }

    /**
     * 查询车场计费规则列表
     * 
     * @param carsHomeRules 车场计费规则
     * @return 车场计费规则
     */
    @Override
    public List<CarsHomeRules> selectCarsHomeRulesList(CarsHomeRules carsHomeRules)
    {
        return carsHomeRulesMapper.selectCarsHomeRulesList(carsHomeRules);
    }

    /**
     * 新增车场计费规则
     * 
     * @param carsHomeRules 车场计费规则
     * @return 结果
     */
    @Override
    public int insertCarsHomeRules(CarsHomeRules carsHomeRules)
    {
        carsHomeRules.setCreateTime(DateUtils.getNowDate());
        return carsHomeRulesMapper.insertCarsHomeRules(carsHomeRules);
    }

    /**
     * 修改车场计费规则
     * 
     * @param carsHomeRules 车场计费规则
     * @return 结果
     */
    @Override
    public int updateCarsHomeRules(CarsHomeRules carsHomeRules)
    {
        carsHomeRules.setUpdateTime(DateUtils.getNowDate());
        return carsHomeRulesMapper.updateCarsHomeRules(carsHomeRules);
    }

    /**
     * 批量删除车场计费规则
     * 
     * @param chrids 需要删除的车场计费规则主键
     * @return 结果
     */
    @Override
    public int deleteCarsHomeRulesByChrids(Long[] chrids)
    {
        return carsHomeRulesMapper.deleteCarsHomeRulesByChrids(chrids);
    }

    /**
     * 删除车场计费规则信息
     * 
     * @param chrid 车场计费规则主键
     * @return 结果
     */
    @Override
    public int deleteCarsHomeRulesByChrid(Long chrid)
    {
        return carsHomeRulesMapper.deleteCarsHomeRulesByChrid(chrid);
    }
}
