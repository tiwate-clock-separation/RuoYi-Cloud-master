import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

/* Layout */
import Layout from '@/layout'

/**
 * Note: 路由配置项
 *
 * hidden: true                     // 当设置 true 的时候该路由不会再侧边栏出现 如401，login等页面，或者如一些编辑页面/edit/1
 * alwaysShow: true                 // 当你一个路由下面的 children 声明的路由大于1个时，自动会变成嵌套的模式--如组件页面
 *                                  // 只有一个时，会将那个子路由当做根路由显示在侧边栏--如引导页面
 *                                  // 若你想不管路由下面的 children 声明的个数都显示你的根路由
 *                                  // 你可以设置 alwaysShow: true，这样它就会忽略之前定义的规则，一直显示根路由
 * redirect: noRedirect             // 当设置 noRedirect 的时候该路由在面包屑导航中不可被点击
 * name:'router-name'               // 设定路由的名字，一定要填写不然使用<keep-alive>时会出现各种问题
 * query: '{"id": 1, "name": "ry"}' // 访问路由的默认传递参数
 * roles: ['admin', 'common']       // 访问路由的角色权限
 * permissions: ['a:a:a', 'b:b:b']  // 访问路由的菜单权限
 * meta : {
    noCache: true                   // 如果设置为true，则不会被 <keep-alive> 缓存(默认 false)
    title: 'title'                  // 设置该路由在侧边栏和面包屑中展示的名字
    icon: 'svg-name'                // 设置该路由的图标，对应路径src/assets/icons/svg
    breadcrumb: false               // 如果设置为false，则不会在breadcrumb面包屑中显示
    activeMenu: '/system/user'      // 当路由设置了该属性，则会高亮相对应的侧边栏。
  }
 */

// 公共路由
export const constantRoutes = [
  {
    path: '/redirect',
    component: Layout,
    hidden: true,
    children: [
      {
        path: '/redirect/:path(.*)',
        component: () => import('@/views/redirect')
      }
    ]
  },
  {
    path: '/login',
    component: () => import('@/views/login'),
    hidden: true
  },
  {
    path: '/register',
    component: () => import('@/views/register'),
    hidden: true
  },
  {
    path: '/404',
    component: () => import('@/views/error/404'),
    hidden: true
  },
  {
    path: '/401',
    component: () => import('@/views/error/401'),
    hidden: true
  },
  {
    path: '',
    component: Layout,
    redirect: 'index',
    children: [
      {
        path: 'index',
        component: () => import('@/views/index'),
        name: 'Index',
        meta: {title: '首页', icon: 'dashboard', affix: true}
      }
    ]
  },
  {
    path: '/user',
    component: Layout,
    hidden: true,
    redirect: 'noredirect',
    children: [
      {
        path: 'profile',
        component: () => import('@/views/system/user/profile/index'),
        name: 'Profile',
        meta: {title: '个人中心', icon: 'user'}
      }
    ]
  }
]

// 动态路由，基于用户权限动态去加载
export const dynamicRoutes = [
  {
    path: '/system/user-auth',
    component: Layout,
    hidden: true,
    permissions: ['system:user:edit'],
    children: [
      {
        path: 'role/:userId(\\d+)',
        component: () => import('@/views/system/user/authRole'),
        name: 'AuthRole',
        meta: {title: '分配角色', activeMenu: '/system/user'}
      }
    ]
  },
  {
    path: '/system/role-auth',
    component: Layout,
    hidden: true,
    permissions: ['system:role:edit'],
    children: [
      {
        path: 'user/:roleId(\\d+)',
        component: () => import('@/views/system/role/authUser'),
        name: 'AuthUser',
        meta: {title: '分配用户', activeMenu: '/system/role'}
      }
    ]
  },
  {
    path: '/system/dict-data',
    component: Layout,
    hidden: true,
    permissions: ['system:dict:list'],
    children: [
      {
        path: 'index/:dictId(\\d+)',
        component: () => import('@/views/system/dict/data'),
        name: 'Data',
        meta: {title: '字典数据', activeMenu: '/system/dict'}
      }
    ]
  },
  {
    path: '/monitor/job-log',
    component: Layout,
    hidden: true,
    permissions: ['monitor:job:list'],
    children: [
      {
        path: 'index/:jobId(\\d+)',
        component: () => import('@/views/monitor/job/log'),
        name: 'JobLog',
        meta: {title: '调度日志', activeMenu: '/monitor/job'}
      }
    ]
  },
  {
    path: '/tool/gen-edit',
    component: Layout,
    hidden: true,
    permissions: ['tool:gen:edit'],
    children: [
      {
        path: 'index/:tableId(\\d+)',
        component: () => import('@/views/tool/gen/editTable'),
        name: 'GenEdit',
        meta: {title: '修改生成配置', activeMenu: '/tool/gen'}
      }
    ]
  },



  //新增路由



  {
    path: '/carshome',
    component: Layout,
    hidden: true,
    permissions: ['sys-cars-home:home:list'],
    children: [
      {
        path: 'youmian',
        component: () => import('@/views/modules/sys-cars-home/home/youmian'),
        name: 'youmian',
        meta: {title: '优免规则'}
      },

      {
        path: 'carshomeedit',
        component: () => import('@/views/modules/sys-cars-home/home/carshomeedit'),
        name: 'carshomeedit',
        meta: {title: '车场信息编辑'}
      },
      {
        path: 'carshomerule',
        component: () => import('@/views/modules/sys-cars-home/home/carshomerule.vue'),
        name: 'carshomerule',
        meta: {title: '车场规则编辑'},
      },
      {
        path: 'ruleedit',
        component: () => import('@/views/modules/sys-cars-home/home/edit/ruleedit.vue'),
        name: 'ruleedit',
        meta: {title: '新增/修改/规则'}
      },
      {
        path: 'holidaysedit',
        component: () => import('@/views/modules/sys-cars-home/home/edit/holidaysedit.vue'),
        name: 'holidaysedit',
        meta: {title: '节假日'}
      },
      {
        path: 'boothedit',
        component: () => import('@/views/modules/sys-cars-home/booth/boothedit.vue'),
        name: 'boothedit',
        meta: {title: '岗亭信息编辑'}
      },
      {
        path: 'laneedit',
        component: () => import('@/views/modules/sys-cars-home/lane/laneedit.vue'),
        name: 'laneedit',
        meta: {title: '车道编辑'}
      }
      ,
      {
        path: 'parkedit',
        component: () => import('@/views/modules/sys-cars-home/parking/parkedit.vue'),
        name: 'parkedit',
        meta: {title: '车位编辑'}
      },


    ]
  },
  {
    path: '/merchant',
    component: Layout,
    hidden: true,
    permissions: ['sys-merchant-home:home:list'],
    children: [
      {
        path: 'addmerchant',
        component: () => import('@/views/modules/merchat/merchatm/addMerchant.vue'),
        name: 'addmerchant',
        meta: {title: '商家信息编辑'}
      },
      {
        path: 'topup',
        component: () => import('@/views/modules/merchat/merchatm/topup.vue'),
        name: 'topup',
        meta: {title: '商家充值'}
      },
      {
        path: 'update',
        component: () => import('@/views/modules/merchat/coupon/couponUpdate.vue'),
        name: 'update',
        meta: {title: '优惠券修改'}
      },
      {
        path: 'test/:id',
        component: () => import('@/views/modules/merchat/coupon/test.vue'),
        name: 'test',
        meta: {title: '领取优惠券'}
      }
      ,
    ]
  },
  {
    path: '/meal',
    component: Layout,
    hidden: true,
    permissions: ['sys-meal-home:home:list'],
    children: [
      {
        path: 'addmeal',
        component: () => import('@/views/modules/meal/addmeal.vue'),
        name: 'addmeal',
        meta: {title: '套餐信息编辑'}
      }
    ]
  },
  {
    path: '/carss',
    component: Layout,
    hidden: true,
    permissions: ['pxy-cars:cars:list'],
    children: [
      {
        path: 'cars',
        component: () => import('@/views/pxy-cars/cars/index.vue'),
        name: 'cars',
        meta: {title: '进出记录'}
      },
      {
        path: 'indexcheck',
        component: () => import('@/views/pxy-cars/cars/indexcheck.vue'),
        name: 'indexcheck',
        meta: {title: '进出信息'}
      },
      {
        path: 'departure',
        component: () => import('@/views/pxy-cars/cars/departure.vue'),
        name: 'departure',
        meta: {title: '离场记录'}
      },
      {
        path: 'hand',
        component: () => import('@/views/pxy-cars/cars/hand.vue'),
        name: 'hand',
        meta: {title: '在场记录'}
      },
      {
        path: 'handcheck',
        component: () => import('@/views/pxy-cars/cars/handcheck.vue'),
        name: 'handcheck',
        meta: {title: '在场信息'}
      },
      {
        path: 'record',
        component: () => import('@/views/pxy-cars/cars/record.vue'),
        name: 'record',
        meta: {title: '开闸记录'}
      },
      {
        path: 'operation',
        component: () => import('@/views/pxy-cars/cars/operation.vue'),
        name: 'operation',
        meta: {title: '设备记录'}
      },
      {
        path: 'excrecord',
        component: () => import('@/views/pxy-cars/cars/excrecord.vue'),
        name: 'excrecord',
        meta: {title: '异常车辆'}
      },
      {
        path: 'excrecordcheck',
        component: () => import('@/views/pxy-cars/cars/excrecordcheck.vue'),
        name: 'excrecordcheck',
        meta: {title: '异常车辆'}
      }
    ]
  },
  {
    path: '/report',
    component: Layout,
    hidden: true,
    permissions: ['pxy-report:park:list'],
    children: [
      {
        path: 'park',
        component: () => import('@/views/pxy-report/park/index.vue'),
        name: 'park',
        meta: {title: '停车报表'}
      },
      {
        path: 'parkcheck',
        component: () => import('@/views/pxy-report/park/indexcheck.vue'),
        name: 'parkcheck',
        meta: {title: '停车报表信息'}
      },
      {
        path: 'skip',
        component: () => import('@/views/pxy-report/skip/index.vue'),
        name: 'skip',
        meta: {title: '逃单报表'}
      },
      {
        path: 'skipcheck',
        component: () => import('@/views/pxy-report/skip/indexcheck.vue'),
        name: 'skipcheck',
        meta: {title: '逃单报表信息'}
      },
      {
        path: 'combo',
        component: () => import('@/views/pxy-report/combo/index.vue'),
        name: 'combo',
        meta: {title: '卡套餐报表'}
      },
      {
        path: 'combocheck',
        component: () => import('@/views/pxy-report/combo/indexcheck.vue'),
        name: 'combocheck',
        meta: {title: '逃单报表信息'}
      },
      {
        path: 'exception',
        component: () => import('@/views/pxy-report/exception/index.vue'),
        name: 'exception',
        meta: {title: '异常报表'}
      },
      {
        path: 'exceptioncheck',
        component: () => import('@/views/pxy-report/exception/indexcheck.vue'),
        name: 'exceptioncheck',
        meta: {title: '逃单报表信息'}
      },
    ]
  },
  {
    path: '/staff',
    component: Layout,
    hidden: true,
    permissions: ['pxy-staff:h5:list'],
    children: [
      {
        path: 'h5',
        component: () => import('@/views/pxy-staff/h5/index.vue'),
        name: 'h5',
        meta: {title: 'h5人员'}
      },
      {
        path: 'check',
        component: () => import('@/views/pxy-staff/h5/indexcheck.vue'),
        name: 'check',
        meta: {title: 'h5消费记录'}
      },
      {
        path: 'watchhouse',
        component: () => import('@/views/pxy-staff/watchhouse/index.vue'),
        name: 'watchhouse',
        meta: {title: '岗亭人员'}
      },
      {
        path: 'car',
        component: () => import('@/views/pxy-staff/car/index.vue'),
        name: 'car',
        meta: {title: '岗亭人员'}
      },
      {
        path: 'staffhome',
        component: () => import('@/views/pxy-staff/staffhome/index.vue'),
        name: 'staffhome',
        meta: {title: '岗亭人员'}
      },
    ]
  },
  // {
  //   path: '/carshome',
  //   component: Layout,
  //   hidden: true,
  //   permissions: ['sys-cars-home:home:list'],
  //   children: [
  //     {
  //       path: 'laneedit',
  //       component: () => import('@/views/modules/sys-cars-home/lane/laneedit.vue'),
  //       name: 'laneedit',
  //       meta: {title: '车道编辑'}
  //     }
  //   ]
  // },
  {
    path: '/info',
    component: Layout,
    hidden: true,
    permissions: ['info:cars_info:list'],
    children: [
      {
        path: 'cars_info',
        component: () => import('@/views/info/cars_info/index.vue'),
        name: 'cars_info',
        meta: {title: '车辆信息'},
      },
      {
        path: 'upcars',
        component: () => import('@/views/info/cars_info/upcars.vue'),
        name: 'upcars',
        meta: {title: '修改车辆信息'},
      },
      {
        path: 'addcars',
        component: () => import('@/views/info/cars_info/addcars.vue'),
        name: 'addcars',
        meta: {title: '新增车辆信息'},
      },
      {
        path: 'addcarsType',
        component: () => import('@/views/info/cars_info/newType.vue'),
        name: 'addcarsType',
        meta: {title: '新增车辆类型信息'},
      },
    ]
  },
]

// 防止连续点击多次路由报错
let routerPush = Router.prototype.push;
let routerReplace = Router.prototype.replace;
// push
Router.prototype.push = function push(location) {
  return routerPush.call(this, location).catch(err => err)
}
// replace
Router.prototype.replace = function push(location) {
  return routerReplace.call(this, location).catch(err => err)
}

export default new Router({
  mode: 'history', // 去掉url中的#
  scrollBehavior: () => ({ y: 0 }),
  routes: constantRoutes
})
