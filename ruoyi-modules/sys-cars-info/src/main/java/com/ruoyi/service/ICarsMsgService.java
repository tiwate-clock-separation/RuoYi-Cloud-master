package com.ruoyi.service;

import com.ruoyi.domain.CarsMsg;

import java.util.List;

/**
 * 车辆信息Service接口
 *
 * @author ruoyi
 * @date 2023-12-07
 */
public interface ICarsMsgService
{
    /**
     * 查询车辆信息
     *
     * @param cmid 车辆信息主键
     * @return 车辆信息
     */
    public CarsMsg selectCarsMsgByCmid(Long cmid);

    /**
     * 查询车辆信息列表
     *
     * @param carsMsg 车辆信息
     * @return 车辆信息集合
     */
    public List<CarsMsg> selectCarsMsgList(CarsMsg carsMsg);

    /**
     * 新增车辆信息
     *
     * @param carsMsg 车辆信息
     * @return 结果
     */
    public int insertCarsMsg(CarsMsg carsMsg);

    /**
     * 修改车辆信息
     *
     * @param carsMsg 车辆信息
     * @return 结果
     */
    public int updateCarsMsg(CarsMsg carsMsg);

    /**
     * 批量删除车辆信息
     *
     * @param cmids 需要删除的车辆信息主键集合
     * @return 结果
     */
    public int deleteCarsMsgByCmids(Long[] cmids);

    /**
     * 删除车辆信息信息
     *
     * @param cmid 车辆信息主键
     * @return 结果
     */
    public int deleteCarsMsgByCmid(Long cmid);
}
