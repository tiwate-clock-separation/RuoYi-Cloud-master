package com.ruoyi.system.service;

import java.util.List;
import com.ruoyi.system.domain.OperationRecord;

/**
 * 设备操作记录Service接口
 * 
 * @author ruoyi
 * @date 2023-11-29
 */
public interface IOperationRecordService 
{
    /**
     * 查询设备操作记录
     * 
     * @param orid 设备操作记录主键
     * @return 设备操作记录
     */
    public OperationRecord selectOperationRecordByOrid(Long orid);

    /**
     * 查询设备操作记录列表
     * 
     * @param operationRecord 设备操作记录
     * @return 设备操作记录集合
     */
    public List<OperationRecord> selectOperationRecordList(OperationRecord operationRecord);

    /**
     * 新增设备操作记录
     * 
     * @param operationRecord 设备操作记录
     * @return 结果
     */
    public int insertOperationRecord(OperationRecord operationRecord);

    /**
     * 修改设备操作记录
     * 
     * @param operationRecord 设备操作记录
     * @return 结果
     */
    public int updateOperationRecord(OperationRecord operationRecord);

    /**
     * 批量删除设备操作记录
     * 
     * @param orids 需要删除的设备操作记录主键集合
     * @return 结果
     */
    public int deleteOperationRecordByOrids(Long[] orids);

    /**
     * 删除设备操作记录信息
     * 
     * @param orid 设备操作记录主键
     * @return 结果
     */
    public int deleteOperationRecordByOrid(Long orid);
}
