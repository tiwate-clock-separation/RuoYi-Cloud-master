package com.ruoyi.mapper;

import java.util.List;

import com.ruoyi.domain.CarsHomeRules;

/**
 * 车场计费规则Mapper接口
 * 
 * @author ruoyi
 * @date 2023-12-02
 */
public interface CarsHomeRulesMapper 
{
    /**
     * 查询车场计费规则
     * 
     * @param chrid 车场计费规则主键
     * @return 车场计费规则
     */
    public CarsHomeRules selectCarsHomeRulesByChrid(Long chrid);

    /**
     * 查询车场计费规则列表
     * 
     * @param carsHomeRules 车场计费规则
     * @return 车场计费规则集合
     */
    public List<CarsHomeRules> selectCarsHomeRulesList(CarsHomeRules carsHomeRules);

    /**
     * 新增车场计费规则
     * 
     * @param carsHomeRules 车场计费规则
     * @return 结果
     */
    public int insertCarsHomeRules(CarsHomeRules carsHomeRules);

    /**
     * 修改车场计费规则
     * 
     * @param carsHomeRules 车场计费规则
     * @return 结果
     */
    public int updateCarsHomeRules(CarsHomeRules carsHomeRules);

    /**
     * 删除车场计费规则
     * 
     * @param chrid 车场计费规则主键
     * @return 结果
     */
    public int deleteCarsHomeRulesByChrid(Long chrid);

    /**
     * 批量删除车场计费规则
     * 
     * @param chrids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteCarsHomeRulesByChrids(Long[] chrids);
}
