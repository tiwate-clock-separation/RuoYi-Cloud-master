package com.ruoyi.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.core.annotation.Excel;
import com.ruoyi.common.core.web.domain.BaseEntity;

/**
 * 车场运维对象 cars_home_om
 * 
 * @author chn
 * @date 2023-12-01
 */
public class CarsHomeOm extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 车场编码 */
    @Excel(name = "车场编码")
    private Long code;

    /** 唯一编码,前台使用 */
    @Excel(name = "唯一编码,前台使用")
    private String codes;

    /** 状态 */
    @Excel(name = "状态")
    private Long status;

    /** 是否删除 */
    @Excel(name = "是否删除")
    private Long deleted;

    /** 运维人员手机号 */
    @Excel(name = "运维人员手机号")
    private String telephone;

    /** 描述 */
    @Excel(name = "描述")
    private String description;

    public void setCode(Long code) 
    {
        this.code = code;
    }

    public Long getCode() 
    {
        return code;
    }
    public void setCodes(String codes) 
    {
        this.codes = codes;
    }

    public String getCodes() 
    {
        return codes;
    }
    public void setStatus(Long status) 
    {
        this.status = status;
    }

    public Long getStatus() 
    {
        return status;
    }
    public void setDeleted(Long deleted) 
    {
        this.deleted = deleted;
    }

    public Long getDeleted() 
    {
        return deleted;
    }
    public void setTelephone(String telephone) 
    {
        this.telephone = telephone;
    }

    public String getTelephone() 
    {
        return telephone;
    }
    public void setDescription(String description) 
    {
        this.description = description;
    }

    public String getDescription() 
    {
        return description;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("code", getCode())
            .append("codes", getCodes())
            .append("status", getStatus())
            .append("deleted", getDeleted())
            .append("createTime", getCreateTime())
            .append("updateTime", getUpdateTime())
            .append("telephone", getTelephone())
            .append("description", getDescription())
            .toString();
    }
}
