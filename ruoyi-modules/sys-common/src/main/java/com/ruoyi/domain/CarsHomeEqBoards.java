package com.ruoyi.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.core.annotation.Excel;
import com.ruoyi.common.core.web.domain.BaseEntity;

/**
 * 车场设备板卡对象 cars_home_eq_boards
 * 
 * @author chn
 * @date 2023-12-07
 */
public class CarsHomeEqBoards extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 板卡ID */
    private Long boardsid;

    /** 板卡名称 */
    @Excel(name = "板卡名称")
    private String boardsname;

    public void setBoardsid(Long boardsid) 
    {
        this.boardsid = boardsid;
    }

    public Long getBoardsid() 
    {
        return boardsid;
    }
    public void setBoardsname(String boardsname) 
    {
        this.boardsname = boardsname;
    }

    public String getBoardsname() 
    {
        return boardsname;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("boardsid", getBoardsid())
            .append("boardsname", getBoardsname())
            .toString();
    }
}
