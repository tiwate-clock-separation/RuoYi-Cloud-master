import request from '@/utils/request'

// 查询车道配置列表
export function listLane(query) {
  return request({
    url: '/sys-cars-home/lane/list',
    method: 'get',
    params: query
  })
}

// 查询车道配置详细
export function getLane(chlid) {
  return request({
    url: '/sys-cars-home/lane/' + chlid,
    method: 'get'
  })
}

// 新增车道配置
export function addLane(data) {
  return request({
    url: '/sys-cars-home/lane',
    method: 'post',
    data: data
  })
}

// 修改车道配置
export function updateLane(data) {
  return request({
    url: '/sys-cars-home/lane',
    method: 'put',
    data: data
  })
}

// 删除车道配置
export function delLane(chlid) {
  return request({
    url: '/sys-cars-home/lane/' + chlid,
    method: 'delete'
  })
}
