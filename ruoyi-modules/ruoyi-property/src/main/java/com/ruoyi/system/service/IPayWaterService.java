package com.ruoyi.system.service;

import java.util.List;
import com.ruoyi.domain.PayWater;

/**
 * 物业流水Service接口
 *
 * @author ruoyi
 * @date 2023-11-30
 */
public interface IPayWaterService
{
    /**
     * 查询物业流水
     *
     * @param pwid 物业流水主键
     * @return 物业流水
     */
    public PayWater selectPayWaterByPwid(Long pwid);

    /**
     * 查询物业流水列表
     *
     * @param payWater 物业流水
     * @return 物业流水集合
     */
    public List<PayWater> selectPayWaterList(PayWater payWater);

    /**
     * 新增物业流水
     *
     * @param payWater 物业流水
     * @return 结果
     */
    public int insertPayWater(PayWater payWater);

    /**
     * 修改物业流水
     *
     * @param payWater 物业流水
     * @return 结果
     */
    public int updatePayWater(PayWater payWater);

    /**
     * 批量删除物业流水
     *
     * @param pwids 需要删除的物业流水主键集合
     * @return 结果
     */
    public int deletePayWaterByPwids(Long[] pwids);

    /**
     * 删除物业流水信息
     *
     * @param pwid 物业流水主键
     * @return 结果
     */
    public int deletePayWaterByPwid(Long pwid);
}
