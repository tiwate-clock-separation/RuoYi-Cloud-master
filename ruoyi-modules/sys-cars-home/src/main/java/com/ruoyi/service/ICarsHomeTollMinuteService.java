package com.ruoyi.service;

import java.util.List;
import com.ruoyi.domain.CarsHomeTollMinute;

/**
 * 按分钟收费Service接口
 * 
 * @author chn
 * @date 2023-12-14
 */
public interface ICarsHomeTollMinuteService 
{
    /**
     * 查询按分钟收费
     * 
     * @param minuteid 按分钟收费主键
     * @return 按分钟收费
     */
    public CarsHomeTollMinute selectCarsHomeTollMinuteByMinuteid(Long minuteid);

    /**
     * 查询按分钟收费列表
     * 
     * @param carsHomeTollMinute 按分钟收费
     * @return 按分钟收费集合
     */
    public List<CarsHomeTollMinute> selectCarsHomeTollMinuteList(CarsHomeTollMinute carsHomeTollMinute);

    /**
     * 新增按分钟收费
     * 
     * @param carsHomeTollMinute 按分钟收费
     * @return 结果
     */
    public int insertCarsHomeTollMinute(CarsHomeTollMinute carsHomeTollMinute);

    /**
     * 修改按分钟收费
     * 
     * @param carsHomeTollMinute 按分钟收费
     * @return 结果
     */
    public int updateCarsHomeTollMinute(CarsHomeTollMinute carsHomeTollMinute);

    /**
     * 批量删除按分钟收费
     * 
     * @param minuteids 需要删除的按分钟收费主键集合
     * @return 结果
     */
    public int deleteCarsHomeTollMinuteByMinuteids(Long[] minuteids);

    /**
     * 删除按分钟收费信息
     * 
     * @param minuteid 按分钟收费主键
     * @return 结果
     */
    public int deleteCarsHomeTollMinuteByMinuteid(Long minuteid);
}
