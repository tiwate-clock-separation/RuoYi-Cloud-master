package com.ruoyi.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.core.annotation.Excel;
import com.ruoyi.common.core.web.domain.BaseEntity;

/**
 * 按次数收费对象 cars_home_toll_time
 * 
 * @author chn
 * @date 2023-12-14
 */
public class CarsHomeTollTime extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 每次金额 */
    @Excel(name = "每次金额")
    private Long amount;

    /** 按次收费规则id */
    private Long timeid;

    /** 关联码 */
    @Excel(name = "关联码")
    private String code;

    public void setAmount(Long amount) 
    {
        this.amount = amount;
    }

    public Long getAmount() 
    {
        return amount;
    }
    public void setTimeid(Long timeid) 
    {
        this.timeid = timeid;
    }

    public Long getTimeid() 
    {
        return timeid;
    }
    public void setCode(String code) 
    {
        this.code = code;
    }

    public String getCode() 
    {
        return code;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("amount", getAmount())
            .append("timeid", getTimeid())
            .append("code", getCode())
            .toString();
    }
}
