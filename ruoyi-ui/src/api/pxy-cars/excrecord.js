import request from '@/utils/request'

// 查询异常车辆记录列表
export function listExcrecord(query) {
  return request({
    url: '/pxy-cars/excrecord/list',
    method: 'get',
    params: query
  })
}

// 查询异常车辆记录详细
export function getExcrecord(erid) {
  return request({
    url: '/pxy-cars/excrecord/' + erid,
    method: 'get'
  })
}

// 新增异常车辆记录
export function addExcrecord(data) {
  return request({
    url: '/pxy-cars/excrecord',
    method: 'post',
    data: data
  })
}

// 修改异常车辆记录
export function updateExcrecord(data) {
  return request({
    url: '/pxy-cars/excrecord',
    method: 'put',
    data: data
  })
}

// 删除异常车辆记录
export function delExcrecord(erid) {
  return request({
    url: '/pxy-cars/excrecord/' + erid,
    method: 'delete'
  })
}
