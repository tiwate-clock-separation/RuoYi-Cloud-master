package com.ruoyi.report.controller;

import java.util.List;
import java.io.IOException;
import javax.servlet.http.HttpServletResponse;

import com.ruoyi.domain.CarsMsg;
import com.ruoyi.domain.PayOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.log.annotation.Log;
import com.ruoyi.common.log.enums.BusinessType;
import com.ruoyi.common.security.annotation.RequiresPermissions;
import com.ruoyi.domain.ReportPark;
import com.ruoyi.report.service.IReportParkService;
import com.ruoyi.common.core.web.controller.BaseController;
import com.ruoyi.common.core.web.domain.AjaxResult;
import com.ruoyi.common.core.utils.poi.ExcelUtil;
import com.ruoyi.common.core.web.page.TableDataInfo;

/**
 * 停车报Controller
 * 
 * @author ruoyi
 * @date 2023-12-07
 */
@RestController
@RequestMapping("/park")
public class ReportParkController extends BaseController
{
    @Autowired
    private IReportParkService reportParkService;

    /**
     * 查询停车报列表
     */
    @RequiresPermissions("pxy-report:park:list")
    @GetMapping("/list")
    public TableDataInfo list(ReportPark reportPark, PayOrder payOrder, CarsMsg carsMsg)
    {
        reportPark.setPayOrder(payOrder);
        reportPark.setCarsMsg(carsMsg);
        startPage();
        List<ReportPark> list = reportParkService.selectReportParkList(reportPark);
        return getDataTable(list);
    }

    /**
     * 导出停车报列表
     */
    @RequiresPermissions("pxy-report:park:export")
    @Log(title = "停车报", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, ReportPark reportPark)
    {
        List<ReportPark> list = reportParkService.selectReportParkList(reportPark);
        ExcelUtil<ReportPark> util = new ExcelUtil<ReportPark>(ReportPark.class);
        util.exportExcel(response, list, "停车报数据");
    }

    /**
     * 获取停车报详细信息
     */
    @RequiresPermissions("pxy-report:park:query")
    @GetMapping(value = "/{parkid}")
    public AjaxResult getInfo(@PathVariable("parkid") Long parkid)
    {
        return success(reportParkService.selectReportParkByParkid(parkid));
    }

    /**
     * 新增停车报
     */
    @RequiresPermissions("pxy-report:park:add")
    @Log(title = "停车报", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody ReportPark reportPark)
    {
        return toAjax(reportParkService.insertReportPark(reportPark));
    }

    /**
     * 修改停车报
     */
    @RequiresPermissions("pxy-report:park:edit")
    @Log(title = "停车报", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody ReportPark reportPark)
    {
        return toAjax(reportParkService.updateReportPark(reportPark));
    }

    /**
     * 删除停车报
     */
    @RequiresPermissions("pxy-report:park:remove")
    @Log(title = "停车报", businessType = BusinessType.DELETE)
	@DeleteMapping("/{parkids}")
    public AjaxResult remove(@PathVariable Long[] parkids)
    {
        return toAjax(reportParkService.deleteReportParkByParkids(parkids));
    }
}
