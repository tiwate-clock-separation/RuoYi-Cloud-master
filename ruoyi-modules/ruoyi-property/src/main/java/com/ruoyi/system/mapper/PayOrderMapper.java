package com.ruoyi.system.mapper;

import java.util.List;
import com.ruoyi.domain.PayOrder;
import org.apache.ibatis.annotations.Many;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Select;

/**
 * 停车订单Mapper接口
 *
 * @author ruoyi
 * @date 2023-11-30
 */
public interface PayOrderMapper
{
    /**
     * 查询停车订单
     *
     * @param poid 停车订单主键
     * @return 停车订单
     */
    public PayOrder selectPayOrderByPoid(Long poid);

    /**
     * 查询停车订单列表
     *
     * @param payOrder 停车订单
     * @return 停车订单集合
     */
    @Select(
            "<script>" +
            "select poid, code, status, deleted, create_time, update_time, ohid, ttid, cmid, order_price, order_status, ptid from pay_order" +
                    " where deleted = '0'" +
                    "<if test=\"code != null  and code != ''\"> and code = #{code}</if>\n" +
                    "<if test=\"status != null \"> and status = #{status}</if>\n" +
                    "<if test=\"deleted != null \"> and deleted = #{deleted}</if>\n" +
                    "<if test=\"params.beginCreateTime != null and params.beginCreateTime != '' and params.endCreateTime != null and params.endCreateTime != ''\"> and create_time between #{params.beginCreateTime} and #{params.endCreateTime}</if>" +
                    "<if test=\"ohid != null \"> and ohid = #{ohid}</if>\n" +
                    "<if test=\"ttid != null \"> and ttid = #{ttid}</if>\n" +
                    "<if test=\"cmid != null \"> and cmid = #{cmid}</if>\n" +
                    "<if test=\"orderPrice != null \"> and order_price = #{orderPrice}</if>\n" +
                    "<if test=\"orderStatus != null \"> and order_status = #{orderStatus}</if>\n" +
                    "<if test=\"ptid != null \"> and ptid = #{ptid}</if>\n" +
            "</script>"
    )

    @Results({
            @Result(property = "createTime",column = "create_time"),
            @Result(property = "updateTime",column = "update_time"),
            @Result(property = "ohid",column = "ohid"),
            @Result(property = "ttid",column = "ttid"),
            @Result(property = "cmid",column = "cmid"),
            @Result(property = "orderPrice",column = "order_price"),
            @Result(property = "orderStatus",column = "order_status"),
            @Result(property = "ptid",column = "ptid"),
            @Result(property = "carsMsg", column = "cmid", many = @Many(select = "com.ruoyi.system.mapper.result.CarsMsgResult.selectCarsMsgByCmid")),
            @Result(property = "propertyManagement", column = "ohid", many = @Many(select = "com.ruoyi.system.mapper.PropertyManagementMapper.selectPropertyManagementByPmid")),
            @Result(property = "payType", column = "ptid", many = @Many(select = "com.ruoyi.system.mapper.result.PayTypeResult.selectPayTypeByPoid")),
            @Result(property = "tradingType", column = "ttid", many = @Many(select = "com.ruoyi.system.mapper.result.TradingTypeResult.selectTradingTypeByPoid"))
    })
    public List<PayOrder> selectPayOrderList(PayOrder payOrder);

    /**
     * 新增停车订单
     *
     * @param payOrder 停车订单
     * @return 结果
     */
    public int insertPayOrder(PayOrder payOrder);

    /**
     * 修改停车订单
     *
     * @param payOrder 停车订单
     * @return 结果
     */
    public int updatePayOrder(PayOrder payOrder);

    /**
     * 删除停车订单
     *
     * @param poid 停车订单主键
     * @return 结果
     */
    public int deletePayOrderByPoid(Long poid);

    /**
     * 批量删除停车订单
     *
     * @param poids 需要删除的数据主键集合
     * @return 结果
     */
    public int deletePayOrderByPoids(Long[] poids);
}
