package com.ruoyi.service.impl;

import java.util.List;
import com.ruoyi.common.core.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.mapper.CarsHomeTollRulesMapper;
import com.ruoyi.domain.CarsHomeTollRules;
import com.ruoyi.service.ICarsHomeTollRulesService;

/**
 * 车场收费规则类型Service业务层处理
 * 
 * @author ruoyi
 * @date 2023-12-04
 */
@Service
public class CarsHomeTollRulesServiceImpl implements ICarsHomeTollRulesService 
{
    @Autowired
    private CarsHomeTollRulesMapper carsHomeTollRulesMapper;

    /**
     * 查询车场收费规则类型
     * 
     * @param chtrid 车场收费规则类型主键
     * @return 车场收费规则类型
     */
    @Override
    public CarsHomeTollRules selectCarsHomeTollRulesByChtrid(Long chtrid)
    {
        return carsHomeTollRulesMapper.selectCarsHomeTollRulesByChtrid(chtrid);
    }

    /**
     * 查询车场收费规则类型列表
     * 
     * @param carsHomeTollRules 车场收费规则类型
     * @return 车场收费规则类型
     */
    @Override
    public List<CarsHomeTollRules> selectCarsHomeTollRulesList(CarsHomeTollRules carsHomeTollRules)
    {
        return carsHomeTollRulesMapper.selectCarsHomeTollRulesList(carsHomeTollRules);
    }

    /**
     * 新增车场收费规则类型
     * 
     * @param carsHomeTollRules 车场收费规则类型
     * @return 结果
     */
    @Override
    public int insertCarsHomeTollRules(CarsHomeTollRules carsHomeTollRules)
    {
        carsHomeTollRules.setCreateTime(DateUtils.getNowDate());
        return carsHomeTollRulesMapper.insertCarsHomeTollRules(carsHomeTollRules);
    }

    /**
     * 修改车场收费规则类型
     * 
     * @param carsHomeTollRules 车场收费规则类型
     * @return 结果
     */
    @Override
    public int updateCarsHomeTollRules(CarsHomeTollRules carsHomeTollRules)
    {
        carsHomeTollRules.setUpdateTime(DateUtils.getNowDate());
        return carsHomeTollRulesMapper.updateCarsHomeTollRules(carsHomeTollRules);
    }

    /**
     * 批量删除车场收费规则类型
     * 
     * @param chtrids 需要删除的车场收费规则类型主键
     * @return 结果
     */
    @Override
    public int deleteCarsHomeTollRulesByChtrids(Long[] chtrids)
    {
        return carsHomeTollRulesMapper.deleteCarsHomeTollRulesByChtrids(chtrids);
    }

    /**
     * 删除车场收费规则类型信息
     * 
     * @param chtrid 车场收费规则类型主键
     * @return 结果
     */
    @Override
    public int deleteCarsHomeTollRulesByChtrid(Long chtrid)
    {
        return carsHomeTollRulesMapper.deleteCarsHomeTollRulesByChtrid(chtrid);
    }
}
