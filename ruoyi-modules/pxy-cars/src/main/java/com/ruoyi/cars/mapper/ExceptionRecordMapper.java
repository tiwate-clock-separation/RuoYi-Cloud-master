package com.ruoyi.cars.mapper;

import java.util.List;
import com.ruoyi.domain.ExceptionRecord;

/**
 * 异常车辆记录Mapper接口
 * 
 * @author ruoyi
 * @date 2023-12-06
 */
public interface ExceptionRecordMapper 
{
    /**
     * 查询异常车辆记录
     * 
     * @param erid 异常车辆记录主键
     * @return 异常车辆记录
     */
    public ExceptionRecord selectExceptionRecordByErid(Long erid);

    /**
     * 查询异常车辆记录列表
     * 
     * @param exceptionRecord 异常车辆记录
     * @return 异常车辆记录集合
     */
    public List<ExceptionRecord> selectExceptionRecordList(ExceptionRecord exceptionRecord);

    /**
     * 新增异常车辆记录
     * 
     * @param exceptionRecord 异常车辆记录
     * @return 结果
     */
    public int insertExceptionRecord(ExceptionRecord exceptionRecord);

    /**
     * 修改异常车辆记录
     * 
     * @param exceptionRecord 异常车辆记录
     * @return 结果
     */
    public int updateExceptionRecord(ExceptionRecord exceptionRecord);

    /**
     * 删除异常车辆记录
     * 
     * @param erid 异常车辆记录主键
     * @return 结果
     */
    public int deleteExceptionRecordByErid(Long erid);

    /**
     * 批量删除异常车辆记录
     * 
     * @param erids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteExceptionRecordByErids(Long[] erids);
}
