import request from '@/utils/request'

// 查询车场设备板卡列表
export function listBoards(query) {
  return request({
    url: '/sys-cars-home/boards/list',
    method: 'get',
    params: query
  })
}

// 查询车场设备板卡详细
export function getBoards(boardsid) {
  return request({
    url: '/sys-cars-home/boards/' + boardsid,
    method: 'get'
  })
}

// 新增车场设备板卡
export function addBoards(data) {
  return request({
    url: '/sys-cars-home/boards',
    method: 'post',
    data: data
  })
}

// 修改车场设备板卡
export function updateBoards(data) {
  return request({
    url: '/sys-cars-home/boards',
    method: 'put',
    data: data
  })
}

// 删除车场设备板卡
export function delBoards(boardsid) {
  return request({
    url: '/sys-cars-home/boards/' + boardsid,
    method: 'delete'
  })
}
