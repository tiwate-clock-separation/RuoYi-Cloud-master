package com.ruoyi.controller;

import java.util.List;
import java.io.IOException;
import javax.servlet.http.HttpServletResponse;

import com.ruoyi.domain.CarsHomeEqIdentify;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.log.annotation.Log;
import com.ruoyi.common.log.enums.BusinessType;
import com.ruoyi.common.security.annotation.RequiresPermissions;

import com.ruoyi.service.ICarsHomeEqIdentifyService;
import com.ruoyi.common.core.web.controller.BaseController;
import com.ruoyi.common.core.web.domain.AjaxResult;
import com.ruoyi.common.core.utils.poi.ExcelUtil;
import com.ruoyi.common.core.web.page.TableDataInfo;

/**
 * 车牌识别Controller
 * 
 * @author chn
 * @date 2023-12-07
 */
@RestController
@RequestMapping("/entify")
public class CarsHomeEqIdentifyController extends BaseController
{
    @Autowired
    private ICarsHomeEqIdentifyService carsHomeEqIdentifyService;

    /**
     * 查询车牌识别列表
     */
    @RequiresPermissions("sys-cars-home:entify:list")
    @GetMapping("/list")
    public TableDataInfo list(CarsHomeEqIdentify carsHomeEqIdentify)
    {
        startPage();
        List<CarsHomeEqIdentify> list = carsHomeEqIdentifyService.selectCarsHomeEqIdentifyList(carsHomeEqIdentify);
        return getDataTable(list);
    }

    /**
     * 导出车牌识别列表
     */
    @RequiresPermissions("sys-cars-home:entify:export")
    @Log(title = "车牌识别", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, CarsHomeEqIdentify carsHomeEqIdentify)
    {
        List<CarsHomeEqIdentify> list = carsHomeEqIdentifyService.selectCarsHomeEqIdentifyList(carsHomeEqIdentify);
        ExcelUtil<CarsHomeEqIdentify> util = new ExcelUtil<CarsHomeEqIdentify>(CarsHomeEqIdentify.class);
        util.exportExcel(response, list, "车牌识别数据");
    }

    /**
     * 获取车牌识别详细信息
     */
    @RequiresPermissions("sys-cars-home:entify:query")
    @GetMapping(value = "/{entifyid}")
    public AjaxResult getInfo(@PathVariable("entifyid") Long entifyid)
    {
        return success(carsHomeEqIdentifyService.selectCarsHomeEqIdentifyByEntifyid(entifyid));
    }

    /**
     * 新增车牌识别
     */
    @RequiresPermissions("sys-cars-home:entify:add")
    @Log(title = "车牌识别", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody CarsHomeEqIdentify carsHomeEqIdentify)
    {
        return toAjax(carsHomeEqIdentifyService.insertCarsHomeEqIdentify(carsHomeEqIdentify));
    }

    /**
     * 修改车牌识别
     */
    @RequiresPermissions("sys-cars-home:entify:edit")
    @Log(title = "车牌识别", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody CarsHomeEqIdentify carsHomeEqIdentify)
    {
        return toAjax(carsHomeEqIdentifyService.updateCarsHomeEqIdentify(carsHomeEqIdentify));
    }

    /**
     * 删除车牌识别
     */
    @RequiresPermissions("sys-cars-home:entify:remove")
    @Log(title = "车牌识别", businessType = BusinessType.DELETE)
	@DeleteMapping("/{entifyids}")
    public AjaxResult remove(@PathVariable Long[] entifyids)
    {
        return toAjax(carsHomeEqIdentifyService.deleteCarsHomeEqIdentifyByEntifyids(entifyids));
    }
}
