package com.ruoyi.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.core.annotation.Excel;
import com.ruoyi.common.core.web.domain.BaseEntity;

/**
 * 车场类型对象 cars_home_type
 *
 * @author chn
 * @date 2023-11-29
 */
public class CarsHomeType extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 自增ID */
    private Long chtid;

    /** 唯一编码,前台使用 */
    @Excel(name = "唯一编码,前台使用")
    private String code;

    /** 状态 */
    @Excel(name = "状态")
    private Long status;

    /** 是否删除 */
    @Excel(name = "是否删除")
    private Long deleted;

    /** 类型名 */
    @Excel(name = "类型名")
    private String tname;

    public void setChtid(Long chtid)
    {
        this.chtid = chtid;
    }

    public Long getChtid()
    {
        return chtid;
    }
    public void setCode(String code)
    {
        this.code = code;
    }

    public String getCode()
    {
        return code;
    }
    public void setStatus(Long status)
    {
        this.status = status;
    }

    public Long getStatus()
    {
        return status;
    }
    public void setDeleted(Long deleted)
    {
        this.deleted = deleted;
    }

    public Long getDeleted()
    {
        return deleted;
    }
    public void setTname(String tname)
    {
        this.tname = tname;
    }

    public String getTname()
    {
        return tname;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("chtid", getChtid())
            .append("code", getCode())
            .append("status", getStatus())
            .append("deleted", getDeleted())
            .append("createTime", getCreateTime())
            .append("updateTime", getUpdateTime())
            .append("tname", getTname())
            .toString();
    }
}
