package com.ruoyi.service;

import java.util.List;
import com.ruoyi.domain.CarsHomeLane;

/**
 * 车道配置Service接口
 * 
 * @author chn
 * @date 2023-12-05
 */
public interface ICarsHomeLaneService 
{
    /**
     * 查询车道配置
     * 
     * @param chlid 车道配置主键
     * @return 车道配置
     */
    public CarsHomeLane selectCarsHomeLaneByChlid(Long chlid);

    /**
     * 查询车道配置列表
     * 
     * @param carsHomeLane 车道配置
     * @return 车道配置集合
     */
    public List<CarsHomeLane> selectCarsHomeLaneList(CarsHomeLane carsHomeLane);

    /**
     * 新增车道配置
     * 
     * @param carsHomeLane 车道配置
     * @return 结果
     */
    public int insertCarsHomeLane(CarsHomeLane carsHomeLane);

    /**
     * 修改车道配置
     * 
     * @param carsHomeLane 车道配置
     * @return 结果
     */
    public int updateCarsHomeLane(CarsHomeLane carsHomeLane);

    /**
     * 批量删除车道配置
     * 
     * @param chlids 需要删除的车道配置主键集合
     * @return 结果
     */
    public int deleteCarsHomeLaneByChlids(Long[] chlids);

    /**
     * 删除车道配置信息
     * 
     * @param chlid 车道配置主键
     * @return 结果
     */
    public int deleteCarsHomeLaneByChlid(Long chlid);
}
