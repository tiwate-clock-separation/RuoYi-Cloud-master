package com.ruoyi.service.impl;

import java.util.List;
import com.ruoyi.common.core.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.mapper.CarsHomeParkingMapper;
import com.ruoyi.domain.CarsHomeParking;
import com.ruoyi.service.ICarsHomeParkingService;

/**
 * 车场车位Service业务层处理
 * 
 * @author chn
 * @date 2023-12-08
 */
@Service
public class CarsHomeParkingServiceImpl implements ICarsHomeParkingService 
{
    @Autowired
    private CarsHomeParkingMapper carsHomeParkingMapper;

    /**
     * 查询车场车位
     * 
     * @param chpid 车场车位主键
     * @return 车场车位
     */
    @Override
    public CarsHomeParking selectCarsHomeParkingByChpid(Long chpid)
    {
        return carsHomeParkingMapper.selectCarsHomeParkingByChpid(chpid);
    }

    /**
     * 查询车场车位列表
     * 
     * @param carsHomeParking 车场车位
     * @return 车场车位
     */
    @Override
    public List<CarsHomeParking> selectCarsHomeParkingList(CarsHomeParking carsHomeParking)
    {
        return carsHomeParkingMapper.selectCarsHomeParkingList(carsHomeParking);
    }

    /**
     * 新增车场车位
     * 
     * @param carsHomeParking 车场车位
     * @return 结果
     */
    @Override
    public int insertCarsHomeParking(CarsHomeParking carsHomeParking)
    {
        carsHomeParking.setCreateTime(DateUtils.getNowDate());
        return carsHomeParkingMapper.insertCarsHomeParking(carsHomeParking);
    }

    /**
     * 修改车场车位
     * 
     * @param carsHomeParking 车场车位
     * @return 结果
     */
    @Override
    public int updateCarsHomeParking(CarsHomeParking carsHomeParking)
    {
        carsHomeParking.setUpdateTime(DateUtils.getNowDate());
        return carsHomeParkingMapper.updateCarsHomeParking(carsHomeParking);
    }

    /**
     * 批量删除车场车位
     * 
     * @param chpids 需要删除的车场车位主键
     * @return 结果
     */
    @Override
    public int deleteCarsHomeParkingByChpids(Long[] chpids)
    {
        return carsHomeParkingMapper.deleteCarsHomeParkingByChpids(chpids);
    }

    /**
     * 删除车场车位信息
     * 
     * @param chpid 车场车位主键
     * @return 结果
     */
    @Override
    public int deleteCarsHomeParkingByChpid(Long chpid)
    {
        return carsHomeParkingMapper.deleteCarsHomeParkingByChpid(chpid);
    }
}
