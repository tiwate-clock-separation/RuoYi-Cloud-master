import request from '@/utils/request'

// 查询优惠卷车辆连列表
export function listCars(query) {
  return request({
    url: '/sys-merchant/cars/list',
    method: 'get',
    params: query
  })
}

// 查询优惠卷车辆连详细
export function getCars(ccid) {
  return request({
    url: '/sys-merchant/cars/' + ccid,
    method: 'get'
  })
}

// 新增优惠卷车辆连
export function addCars(data) {
  return request({
    url: '/sys-merchant/cars',
    method: 'post',
    data: data
  })
}

// 修改优惠卷车辆连
export function updateCars(data) {
  return request({
    url: '/sys-merchant/cars',
    method: 'put',
    data: data
  })
}

// 删除优惠卷车辆连
export function delCars(ccid) {
  return request({
    url: '/sys-merchant/cars/' + ccid,
    method: 'delete'
  })
}
