import request from '@/utils/request'

// 查询停车订单列表
export function listOrder(query) {
  return request({
    url: '/sys-property/order/list',
    method: 'get',
    params: query
  })
}

// 查询停车订单详细
export function getOrder(poid) {
  return request({
    url: '/sys-property/order/' + poid,
    method: 'get'
  })
}

// 新增停车订单
export function addOrder(data) {
  return request({
    url: '/sys-property/order',
    method: 'post',
    data: data
  })
}

// 修改停车订单
export function updateOrder(data) {
  return request({
    url: '/sys-property/order',
    method: 'put',
    data: data
  })
}

// 删除停车订单
export function delOrder(poid) {
  return request({
    url: '/sys-property/order/' + poid,
    method: 'delete'
  })
}
//查询所有物业方法
export function listProperty(query) {
  return request({
    url: '/sys-property/propertyApi/list',
    method: 'get',
    params: query
  })
}

export function listTradingType(query) {
  return request({
    url: '/sys-property/propertyApi/ttlist',
    method: 'get',
    params: query
  })
}
