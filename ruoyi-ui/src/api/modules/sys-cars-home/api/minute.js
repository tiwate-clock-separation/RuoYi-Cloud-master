import request from '@/utils/request'

// 查询按分钟收费列表
export function listMinute(query) {
  return request({
    url: '/sys-cars-home/minute/list',
    method: 'get',
    params: query
  })
}

// 查询按分钟收费详细
export function getMinute(minuteid) {
  return request({
    url: '/sys-cars-home/minute/' + minuteid,
    method: 'get'
  })
}

// 新增按分钟收费
export function addMinute(data) {
  return request({
    url: '/sys-cars-home/minute',
    method: 'post',
    data: data
  })
}

// 修改按分钟收费
export function updateMinute(data) {
  return request({
    url: '/sys-cars-home/minute',
    method: 'put',
    data: data
  })
}

// 删除按分钟收费
export function delMinute(minuteid) {
  return request({
    url: '/sys-cars-home/minute/' + minuteid,
    method: 'delete'
  })
}
