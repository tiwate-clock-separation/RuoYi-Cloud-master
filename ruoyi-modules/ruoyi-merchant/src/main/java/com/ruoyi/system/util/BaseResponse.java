package com.ruoyi.system.util;

public class BaseResponse {
    private String Code;
    private String msg;
    private Object data;

    public String getCode() {
        return Code;
    }

    public void setCode(String code) {
        Code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }

    public BaseResponse() {
    }

    public BaseResponse(String code, String msg, Object data) {
        Code = code;
        this.msg = msg;
        this.data = data;
    }

    @Override
    public String toString() {
        return "BaseResponse{" +
                "Code='" + Code + '\'' +
                ", msg='" + msg + '\'' +
                ", data=" + data +
                '}';
    }
}
