package com.ruoyi.staff.service;

import java.util.List;
import com.ruoyi.domain.EmpWatchhouse;

/**
 * 岗亭人员Service接口
 * 
 * @author pxy
 * @date 2023-12-23
 */
public interface IEmpWatchhouseService 
{
    /**
     * 查询岗亭人员
     * 
     * @param watchid 岗亭人员主键
     * @return 岗亭人员
     */
    public EmpWatchhouse selectEmpWatchhouseByWatchid(Long watchid);

    /**
     * 查询岗亭人员列表
     * 
     * @param empWatchhouse 岗亭人员
     * @return 岗亭人员集合
     */
    public List<EmpWatchhouse> selectEmpWatchhouseList(EmpWatchhouse empWatchhouse);

    /**
     * 新增岗亭人员
     * 
     * @param empWatchhouse 岗亭人员
     * @return 结果
     */
    public int insertEmpWatchhouse(EmpWatchhouse empWatchhouse);

    /**
     * 修改岗亭人员
     * 
     * @param empWatchhouse 岗亭人员
     * @return 结果
     */
    public int updateEmpWatchhouse(EmpWatchhouse empWatchhouse);

    /**
     * 批量删除岗亭人员
     * 
     * @param watchids 需要删除的岗亭人员主键集合
     * @return 结果
     */
    public int deleteEmpWatchhouseByWatchids(Long[] watchids);

    /**
     * 删除岗亭人员信息
     * 
     * @param watchid 岗亭人员主键
     * @return 结果
     */
    public int deleteEmpWatchhouseByWatchid(Long watchid);
}
