package com.ruoyi.domain;

import com.ruoyi.common.core.annotation.Excel;
import com.ruoyi.common.core.web.domain.BaseEntity;
import org.springframework.data.annotation.Transient;

/**
 * 停车订单对象 pay_order
 *
 * @author ruoyi
 * @date 2023-11-30
 */
public class
PayOrder extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 订单ID */
    @Excel(name = "订单id")
    private Long poid;

    /** 唯一编码,前台使用 */
    @Excel(name = "唯一编码,前台使用")
    private String code;

    /** 状态 */
    @Excel(name = "状态")
    private Long status;

    /** 是否删除 */
    @Excel(name = "是否删除")
    private Long deleted;

    /** 物业id */
    @Excel(name = "物业id")
    private Long ohid;
    @Transient
    private PropertyManagement propertyManagement;

    /** 费用类型id */
    @Excel(name = "费用类型id")
    private Long ttid;

    @Transient
    private TradingType tradingType;

    /** 车辆信息id */
    @Excel(name = "车辆信息id")
    private Long cmid;

    @Transient
    private CarsMsg carsMsg;

    /** 费用金额 */
    @Excel(name = "费用金额")
    private Long orderPrice;

    /** 是否支付 */
    @Excel(name = "是否支付")
    private Long orderStatus;

    /** 支付类型 */
    @Excel(name = "支付类型")
    private Long ptid;

    @Transient
    private PayType payType;

    public void setPoid(Long poid)
    {
        this.poid = poid;
    }

    public Long getPoid()
    {
        return poid;
    }
    public void setCode(String code)
    {
        this.code = code;
    }

    public String getCode()
    {
        return code;
    }
    public void setStatus(Long status)
    {
        this.status = status;
    }

    public Long getStatus()
    {
        return status;
    }
    public void setDeleted(Long deleted)
    {
        this.deleted = deleted;
    }

    public Long getDeleted()
    {
        return deleted;
    }
    public void setOhid(Long ohid)
    {
        this.ohid = ohid;
    }

    public PropertyManagement getPropertyManagement() {
        return propertyManagement;
    }

    public void setPropertyManagement(PropertyManagement propertyManagement) {
        this.propertyManagement = propertyManagement;
    }

    public Long getOhid()
    {
        return ohid;
    }
    public void setTtid(Long ttid)
    {
        this.ttid = ttid;
    }

    public Long getTtid()
    {
        return ttid;
    }
    public void setCmid(Long cmid)
    {
        this.cmid = cmid;
    }

    public Long getCmid()
    {
        return cmid;
    }
    public void setOrderPrice(Long orderPrice)
    {
        this.orderPrice = orderPrice;
    }

    public Long getOrderPrice()
    {
        return orderPrice;
    }
    public void setOrderStatus(Long orderStatus)
    {
        this.orderStatus = orderStatus;
    }

    public Long getOrderStatus()
    {
        return orderStatus;
    }
    public void setPtid(Long ptid)
    {
        this.ptid = ptid;
    }

    public Long getPtid()
    {
        return ptid;
    }

    public TradingType getTradingType() {
        return tradingType;
    }

    public void setTradingType(TradingType tradingType) {
        this.tradingType = tradingType;
    }

    public CarsMsg getCarsMsg() {
        return carsMsg;
    }

    public void setCarsMsg(CarsMsg carsMsg) {
        this.carsMsg = carsMsg;
    }

    public PayType getPayType() {
        return payType;
    }

    public void setPayType(PayType payType) {
        this.payType = payType;
    }

    public PayOrder(Long poid, String code, Long status, Long deleted, Long ohid, Long ttid, Long cmid, Long orderPrice, Long orderStatus, Long ptid) {
        this.poid = poid;
        this.code = code;
        this.status = status;
        this.deleted = deleted;
        this.ohid = ohid;
        this.ttid = ttid;
        this.cmid = cmid;
        this.orderPrice = orderPrice;
        this.orderStatus = orderStatus;
        this.ptid = ptid;
    }

    public PayOrder() {
    }

    @Override
    public String toString() {
        return "PayOrder{" +
                "poid=" + poid +
                ", code='" + code + '\'' +
                ", status=" + status +
                ", deleted=" + deleted +
                ", ohid=" + ohid +
                ", propertyManagement=" + propertyManagement +
                ", ttid=" + ttid +
                ", tradingType=" + tradingType +
                ", cmid=" + cmid +
                ", carsMsg=" + carsMsg +
                ", orderPrice=" + orderPrice +
                ", orderStatus=" + orderStatus +
                ", ptid=" + ptid +
                ", payType=" + payType +
                '}';
    }
}
