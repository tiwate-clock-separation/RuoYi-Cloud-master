package com.ruoyi.system.service;

import java.util.List;
import com.ruoyi.domain.PayOrder;

/**
 * 停车订单Service接口
 *
 * @author ruoyi
 * @date 2023-11-30
 */
public interface IPayOrderService
{
    /**
     * 查询停车订单
     *
     * @param poid 停车订单主键
     * @return 停车订单
     */
    public PayOrder selectPayOrderByPoid(Long poid);

    /**
     * 查询停车订单列表
     *
     * @param payOrder 停车订单
     * @return 停车订单集合
     */
    public List<PayOrder> selectPayOrderList(PayOrder payOrder);

    /**
     * 新增停车订单
     *
     * @param payOrder 停车订单
     * @return 结果
     */
    public int insertPayOrder(PayOrder payOrder);

    /**
     * 修改停车订单
     *
     * @param payOrder 停车订单
     * @return 结果
     */
    public int updatePayOrder(PayOrder payOrder);

    /**
     * 批量删除停车订单
     *
     * @param poids 需要删除的停车订单主键集合
     * @return 结果
     */
    public int deletePayOrderByPoids(Long[] poids);

    /**
     * 删除停车订单信息
     *
     * @param poid 停车订单主键
     * @return 结果
     */
    public int deletePayOrderByPoid(Long poid);
}
