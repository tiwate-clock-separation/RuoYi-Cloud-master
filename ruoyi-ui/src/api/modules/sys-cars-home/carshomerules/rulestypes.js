import request from '@/utils/request'

// 查询车场收费规则类型列表
export function listRulestype(query) {
  return request({
    url: '/sys-cars-home/rulestype/list',
    method: 'get',
    params: query
  })
}
//
// // 查询车场收费规则类型详细
// export function getRules(chtrid) {
//   return request({
//     url: '/sys-cars-home/rules/' + chtrid,
//     method: 'get'
//   })
// }
//
// // 新增车场收费规则类型
// export function addRules(data) {
//   return request({
//     url: '/sys-cars-home/rules',
//     method: 'post',
//     data: data
//   })
// }
//
// // 修改车场收费规则类型
// export function updateRules(data) {
//   return request({
//     url: '/sys-cars-home/rules',
//     method: 'put',
//     data: data
//   })
// }
//
// // 删除车场收费规则类型
// export function delRules(chtrid) {
//   return request({
//     url: '/sys-cars-home/rules/' + chtrid,
//     method: 'delete'
//   })
// }
