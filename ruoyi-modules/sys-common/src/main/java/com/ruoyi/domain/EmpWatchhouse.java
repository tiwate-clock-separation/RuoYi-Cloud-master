package com.ruoyi.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.core.annotation.Excel;
import com.ruoyi.common.core.web.domain.BaseEntity;

/**
 * 岗亭人员对象 emp_watchhouse
 *
 * @author pxy
 * @date 2023-12-23
 */
public class EmpWatchhouse extends BaseEntity {
    private static final long serialVersionUID = 1L;

    /**
     * 自增ID
     */
    @Excel(name = "自增ID")
    private Long watchid;

    /**
     * 唯一编码,前台使用
     */
    private String code;

    /**
     * 状态
     */
    private Long status;

    /**
     * 是否删除
     */
    private Long deleted;

    /**
     * 车场名称
     */
    @Excel(name = "车场名称")
    private Long chid;

    /**
     * 岗亭名称
     */
    @Excel(name = "岗亭名称")
    private Long chbid;

    /**
     * 用户名
     */
    @Excel(name = "用户名")
    private String watchUser;

    /**
     * 手机号
     */
    @Excel(name = "手机号")
    private String watchTel;

    private CarsHome carsHome;

    private CarsHomeBooth carsHomeBooth;

    public CarsHome getCarsHome() {
        return carsHome;
    }

    public void setCarsHome(CarsHome carsHome) {
        this.carsHome = carsHome;
    }

    public CarsHomeBooth getCarsHomeBooth() {
        return carsHomeBooth;
    }

    public void setCarsHomeBooth(CarsHomeBooth carsHomeBooth) {
        this.carsHomeBooth = carsHomeBooth;
    }

    public void setWatchid(Long watchid) {
        this.watchid = watchid;
    }

    public Long getWatchid() {
        return watchid;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getCode() {
        return code;
    }

    public void setStatus(Long status) {
        this.status = status;
    }

    public Long getStatus() {
        return status;
    }

    public void setDeleted(Long deleted) {
        this.deleted = deleted;
    }

    public Long getDeleted() {
        return deleted;
    }

    public void setChid(Long chid) {
        this.chid = chid;
    }

    public Long getChid() {
        return chid;
    }

    public void setChbid(Long chbid) {
        this.chbid = chbid;
    }

    public Long getChbid() {
        return chbid;
    }

    public void setWatchUser(String watchUser) {
        this.watchUser = watchUser;
    }

    public String getWatchUser() {
        return watchUser;
    }

    public String getWatchTel() {
        return watchTel;
    }

    public void setWatchTel(String watchTel) {
        this.watchTel = watchTel;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this, ToStringStyle.MULTI_LINE_STYLE)
                .append("watchid", getWatchid())
                .append("code", getCode())
                .append("status", getStatus())
                .append("deleted", getDeleted())
                .append("createTime", getCreateTime())
                .append("updateTime", getUpdateTime())
                .append("chid", getChid())
                .append("chbid", getChbid())
                .append("watchUser", getWatchUser())
                .append("watchTel", getWatchTel())
                .append("remark", getRemark())
                .toString();
    }
}
