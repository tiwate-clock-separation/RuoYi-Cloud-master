import request from '@/utils/request'

// 查询岗亭人员列表
export function listWatchhouse(query) {
  return request({
    url: '/pxy-staff/watchhouse/list',
    method: 'get',
    params: query
  })
}

// 查询岗亭人员详细
export function getWatchhouse(watchid) {
  return request({
    url: '/pxy-staff/watchhouse/' + watchid,
    method: 'get'
  })
}

// 新增岗亭人员
export function addWatchhouse(data) {
  return request({
    url: '/pxy-staff/watchhouse',
    method: 'post',
    data: data
  })
}

// 修改岗亭人员
export function updateWatchhouse(data) {
  return request({
    url: '/pxy-staff/watchhouse',
    method: 'put',
    data: data
  })
}

// 删除岗亭人员
export function delWatchhouse(watchid) {
  return request({
    url: '/pxy-staff/watchhouse/' + watchid,
    method: 'delete'
  })
}
