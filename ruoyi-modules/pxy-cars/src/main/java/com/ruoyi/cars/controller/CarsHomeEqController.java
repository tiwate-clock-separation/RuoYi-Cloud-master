package com.ruoyi.cars.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.log.annotation.Log;
import com.ruoyi.common.log.enums.BusinessType;
import com.ruoyi.common.security.annotation.RequiresPermissions;
import com.ruoyi.domain.CarsHomeEq;
import com.ruoyi.cars.service.ICarsHomeEqService;
import com.ruoyi.common.core.web.controller.BaseController;
import com.ruoyi.common.core.web.domain.AjaxResult;
import com.ruoyi.common.core.utils.poi.ExcelUtil;
import com.ruoyi.common.core.web.page.TableDataInfo;

/**
 * 车场设备Controller
 *
 * @author ruoyi
 * @date 2023-11-30
 */
@RestController
@RequestMapping("/eq")
public class CarsHomeEqController extends BaseController
{
    @Autowired
    private ICarsHomeEqService carsHomeEqService;

    /**
     * 查询车场设备列表
     */
    @RequiresPermissions("pxy-cars:eq:list")
    @GetMapping("/list")
    public TableDataInfo list(CarsHomeEq carsHomeEq)
    {
        startPage();
        List<CarsHomeEq> list = carsHomeEqService.selectCarsHomeEqList(carsHomeEq);
        return getDataTable(list);
    }

    /**
     * 导出车场设备列表
     */
    @RequiresPermissions("pxy-cars:eq:export")
    @Log(title = "车场设备", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, CarsHomeEq carsHomeEq)
    {
        List<CarsHomeEq> list = carsHomeEqService.selectCarsHomeEqList(carsHomeEq);
        ExcelUtil<CarsHomeEq> util = new ExcelUtil<CarsHomeEq>(CarsHomeEq.class);
        util.exportExcel(response, list, "车场设备数据");
    }

    /**
     * 获取车场设备详细信息
     */
    @RequiresPermissions("pxy-cars:eq:query")
    @GetMapping(value = "/{eqid}")
    public AjaxResult getInfo(@PathVariable("eqid") Long eqid)
    {
        return success(carsHomeEqService.selectCarsHomeEqByEqid(eqid));
    }

    /**
     * 新增车场设备
     */
    @RequiresPermissions("pxy-cars:eq:add")
    @Log(title = "车场设备", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody CarsHomeEq carsHomeEq)
    {
        return toAjax(carsHomeEqService.insertCarsHomeEq(carsHomeEq));
    }

    /**
     * 修改车场设备
     */
    @RequiresPermissions("pxy-cars:eq:edit")
    @Log(title = "车场设备", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody CarsHomeEq carsHomeEq)
    {
        return toAjax(carsHomeEqService.updateCarsHomeEq(carsHomeEq));
    }

    /**
     * 删除车场设备
     */
    @RequiresPermissions("pxy-cars:eq:remove")
    @Log(title = "车场设备", businessType = BusinessType.DELETE)
	@DeleteMapping("/{eqids}")
    public AjaxResult remove(@PathVariable Long[] eqids)
    {
        return toAjax(carsHomeEqService.deleteCarsHomeEqByEqids(eqids));
    }
}
