package com.ruoyi.audit.service;

import java.util.List;
import com.ruoyi.domain.SpaceAudit;

/**
 * 车位审核Service接口
 *
 * @author ruoyi
 * @date 2023-12-03
 */
public interface ISpaceAuditService
{
    /**
     * 查询车位审核
     *
     * @param said 车位审核主键
     * @return 车位审核
     */
    public SpaceAudit selectSpaceAuditBySaid(Long said);

    /**
     * 查询车位审核列表
     *
     * @param spaceAudit 车位审核
     * @return 车位审核集合
     */
    public List<SpaceAudit> selectSpaceAuditList(SpaceAudit spaceAudit);

    /**
     * 新增车位审核
     *
     * @param spaceAudit 车位审核
     * @return 结果
     */
    public int insertSpaceAudit(SpaceAudit spaceAudit);

    /**
     * 修改车位审核
     *
     * @param spaceAudit 车位审核
     * @return 结果
     */
    public int updateSpaceAudit(SpaceAudit spaceAudit);

    /**
     * 批量删除车位审核
     *
     * @param saids 需要删除的车位审核主键集合
     * @return 结果
     */
    public int deleteSpaceAuditBySaids(Long[] saids);

    /**
     * 删除车位审核信息
     *
     * @param said 车位审核主键
     * @return 结果
     */
    public int deleteSpaceAuditBySaid(Long said);
}
