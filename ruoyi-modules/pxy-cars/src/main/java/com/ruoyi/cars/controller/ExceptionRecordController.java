package com.ruoyi.cars.controller;

import java.util.List;
import java.io.IOException;
import javax.servlet.http.HttpServletResponse;

import com.ruoyi.domain.CarsMsg;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.log.annotation.Log;
import com.ruoyi.common.log.enums.BusinessType;
import com.ruoyi.common.security.annotation.RequiresPermissions;
import com.ruoyi.domain.ExceptionRecord;
import com.ruoyi.cars.service.IExceptionRecordService;
import com.ruoyi.common.core.web.controller.BaseController;
import com.ruoyi.common.core.web.domain.AjaxResult;
import com.ruoyi.common.core.utils.poi.ExcelUtil;
import com.ruoyi.common.core.web.page.TableDataInfo;

/**
 * 异常车辆记录Controller
 * 
 * @author ruoyi
 * @date 2023-12-06
 */
@RestController
@RequestMapping("/excrecord")
public class ExceptionRecordController extends BaseController
{
    @Autowired
    private IExceptionRecordService exceptionRecordService;

    /**
     * 查询异常车辆记录列表
     */
    @RequiresPermissions("pxy-cars:excrecord:list")
    @GetMapping("/list")
    public TableDataInfo list(ExceptionRecord exceptionRecord, CarsMsg carsMsg)
    {
        exceptionRecord.setCarsMsg(carsMsg);
        startPage();
        List<ExceptionRecord> list = exceptionRecordService.selectExceptionRecordList(exceptionRecord);
        return getDataTable(list);
    }

    /**
     * 导出异常车辆记录列表
     */
    @RequiresPermissions("pxy-cars:excrecord:export")
    @Log(title = "异常车辆记录", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, ExceptionRecord exceptionRecord)
    {
        List<ExceptionRecord> list = exceptionRecordService.selectExceptionRecordList(exceptionRecord);
        ExcelUtil<ExceptionRecord> util = new ExcelUtil<ExceptionRecord>(ExceptionRecord.class);
        util.exportExcel(response, list, "异常车辆记录数据");
    }

    /**
     * 获取异常车辆记录详细信息
     */
    @RequiresPermissions("pxy-cars:excrecord:query")
    @GetMapping(value = "/{erid}")
    public AjaxResult getInfo(@PathVariable("erid") Long erid)
    {
        return success(exceptionRecordService.selectExceptionRecordByErid(erid));
    }

    /**
     * 新增异常车辆记录
     */
    @RequiresPermissions("pxy-cars:excrecord:add")
    @Log(title = "异常车辆记录", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody ExceptionRecord exceptionRecord)
    {
        return toAjax(exceptionRecordService.insertExceptionRecord(exceptionRecord));
    }

    /**
     * 修改异常车辆记录
     */
    @RequiresPermissions("pxy-cars:excrecord:edit")
    @Log(title = "异常车辆记录", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody ExceptionRecord exceptionRecord)
    {
        return toAjax(exceptionRecordService.updateExceptionRecord(exceptionRecord));
    }

    /**
     * 删除异常车辆记录
     */
    @RequiresPermissions("pxy-cars:excrecord:remove")
    @Log(title = "异常车辆记录", businessType = BusinessType.DELETE)
	@DeleteMapping("/{erids}")
    public AjaxResult remove(@PathVariable Long[] erids)
    {
        return toAjax(exceptionRecordService.deleteExceptionRecordByErids(erids));
    }
}
