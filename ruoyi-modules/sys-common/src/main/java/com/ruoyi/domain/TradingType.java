package com.ruoyi.domain;

import com.ruoyi.common.core.annotation.Excel;

/**
 * 费用类型
 */
public class TradingType {
    @Excel(name = "费用编号")
    private Long Ttid;
    @Excel(name = "费用类型")
    private String TypeName;

    public Long getTtid() {
        return Ttid;
    }

    public void setTtid(Long ttid) {
        Ttid = ttid;
    }

    public String getTypeName() {
        return TypeName;
    }

    public void setTypeName(String typeName) {
        TypeName = typeName;
    }
}
