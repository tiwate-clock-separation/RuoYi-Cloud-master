package com.ruoyi.audit.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;

import com.ruoyi.domain.CarsAduit;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.log.annotation.Log;
import com.ruoyi.common.log.enums.BusinessType;
import com.ruoyi.common.security.annotation.RequiresPermissions;

import com.ruoyi.audit.service.ICarsAduitService;
import com.ruoyi.common.core.web.controller.BaseController;
import com.ruoyi.common.core.web.domain.AjaxResult;
import com.ruoyi.common.core.utils.poi.ExcelUtil;
import com.ruoyi.common.core.web.page.TableDataInfo;

/**
 * 业主车辆审核Controller
 *
 * @author ruoyi
 * @date 2023-12-03
 */
@RestController
@RequestMapping("/cars")
public class CarsAduitController extends BaseController
{
    @Autowired
    private ICarsAduitService carsAduitService;

    /**
     * 查询业主车辆审核列表
     */
    @RequiresPermissions("audit:cars:list")
    @GetMapping("/list")
    public TableDataInfo list(CarsAduit carsAduit)
    {
        startPage();
        List<CarsAduit> list = carsAduitService.selectCarsAduitList(carsAduit);
        return getDataTable(list);
    }

    /**
     * 导出业主车辆审核列表
     */
    @RequiresPermissions("audit:cars:export")
    @Log(title = "业主车辆审核", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, CarsAduit carsAduit)
    {
        List<CarsAduit> list = carsAduitService.selectCarsAduitList(carsAduit);
        ExcelUtil<CarsAduit> util = new ExcelUtil<CarsAduit>(CarsAduit.class);
        util.exportExcel(response, list, "业主车辆审核数据");
    }

    /**
     * 获取业主车辆审核详细信息
     */
    @RequiresPermissions("audit:cars:query")
    @GetMapping(value = "/{caid}")
    public AjaxResult getInfo(@PathVariable("caid") Long caid)
    {
        return success(carsAduitService.selectCarsAduitByCaid(caid));
    }

    /**
     * 新增业主车辆审核
     */
    @RequiresPermissions("audit:cars:add")
    @Log(title = "业主车辆审核", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody CarsAduit carsAduit)
    {
        return toAjax(carsAduitService.insertCarsAduit(carsAduit));
    }

    /**
     * 修改业主车辆审核
     */
    @RequiresPermissions("audit:cars:edit")
    @Log(title = "业主车辆审核", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody CarsAduit carsAduit)
    {
        return toAjax(carsAduitService.updateCarsAduit(carsAduit));
    }

    /**
     * 删除业主车辆审核
     */
    @RequiresPermissions("audit:cars:remove")
    @Log(title = "业主车辆审核", businessType = BusinessType.DELETE)
	@DeleteMapping("/{caids}")
    public AjaxResult remove(@PathVariable Long[] caids)
    {
        return toAjax(carsAduitService.deleteCarsAduitByCaids(caids));
    }
}
