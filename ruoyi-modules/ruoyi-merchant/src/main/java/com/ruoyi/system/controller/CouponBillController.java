package com.ruoyi.system.controller;

import java.util.List;
import java.io.IOException;
import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.log.annotation.Log;
import com.ruoyi.common.log.enums.BusinessType;
import com.ruoyi.common.security.annotation.RequiresPermissions;
import com.ruoyi.domain.CouponBill;
import com.ruoyi.system.service.ICouponBillService;
import com.ruoyi.common.core.web.controller.BaseController;
import com.ruoyi.common.core.web.domain.AjaxResult;
import com.ruoyi.common.core.utils.poi.ExcelUtil;
import com.ruoyi.common.core.web.page.TableDataInfo;

/**
 * 优惠卷账单Controller
 *
 * @author ruoyi
 * @date 2023-12-26
 */
@RestController
@RequestMapping("/bill")
public class CouponBillController extends BaseController
{
    @Autowired
    private ICouponBillService couponBillService;

    /**
     * 查询优惠卷账单列表
     */
    @RequiresPermissions("system:bill:list")
    @GetMapping("/list")
    public TableDataInfo list(CouponBill couponBill)
    {
        startPage();
        List<CouponBill> list = couponBillService.selectCouponBillList(couponBill);
        return getDataTable(list);
    }

    /**
     * 导出优惠卷账单列表
     */
    @RequiresPermissions("system:bill:export")
    @Log(title = "优惠卷账单", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, CouponBill couponBill)
    {
        List<CouponBill> list = couponBillService.selectCouponBillList(couponBill);
        ExcelUtil<CouponBill> util = new ExcelUtil<CouponBill>(CouponBill.class);
        util.exportExcel(response, list, "优惠卷账单数据");
    }

    /**
     * 获取优惠卷账单详细信息
     */
    @RequiresPermissions("system:bill:query")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(couponBillService.selectCouponBillById(id));
    }

    /**
     * 新增优惠卷账单
     */
    @RequiresPermissions("system:bill:add")
    @Log(title = "优惠卷账单", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody CouponBill couponBill)
    {
        return toAjax(couponBillService.insertCouponBill(couponBill));
    }

    /**
     * 修改优惠卷账单
     */
    @RequiresPermissions("system:bill:edit")
    @Log(title = "优惠卷账单", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody CouponBill couponBill)
    {
        return toAjax(couponBillService.updateCouponBill(couponBill));
    }

    /**
     * 删除优惠卷账单
     */
    @RequiresPermissions("system:bill:remove")
    @Log(title = "优惠卷账单", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(couponBillService.deleteCouponBillByIds(ids));
    }
}
