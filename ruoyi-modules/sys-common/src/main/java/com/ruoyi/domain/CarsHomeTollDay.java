package com.ruoyi.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.core.annotation.Excel;
import com.ruoyi.common.core.web.domain.BaseEntity;

/**
 * 按天收费对象 cars_home_toll_day
 * 
 * @author chn
 * @date 2023-12-14
 */
public class CarsHomeTollDay extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 按天收费自增id */
    private Long dayid;

    /** 免受时长 */
    @Excel(name = "免受时长")
    private Long freeMinute;

    /** 每天收费 */
    @Excel(name = "每天收费")
    private Long dayMoney;

    /** 关联码 */
    @Excel(name = "关联码")
    private String code;

    public void setDayid(Long dayid) 
    {
        this.dayid = dayid;
    }

    public Long getDayid() 
    {
        return dayid;
    }
    public void setFreeMinute(Long freeMinute) 
    {
        this.freeMinute = freeMinute;
    }

    public Long getFreeMinute() 
    {
        return freeMinute;
    }
    public void setDayMoney(Long dayMoney) 
    {
        this.dayMoney = dayMoney;
    }

    public Long getDayMoney() 
    {
        return dayMoney;
    }
    public void setCode(String code) 
    {
        this.code = code;
    }

    public String getCode() 
    {
        return code;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("dayid", getDayid())
            .append("freeMinute", getFreeMinute())
            .append("dayMoney", getDayMoney())
            .append("code", getCode())
            .toString();
    }
}
