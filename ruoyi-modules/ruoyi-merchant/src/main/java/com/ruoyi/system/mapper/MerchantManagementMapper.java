package com.ruoyi.system.mapper;

import java.util.List;
import com.ruoyi.domain.MerchantManagement;

/**
 * 商家管理Mapper接口
 *
 * @author ruoyi
 * @date 2023-12-04
 */
public interface MerchantManagementMapper
{
    /**
     * 查询商家管理
     *
     * @param pmid 商家管理主键
     * @return 商家管理
     */
    public MerchantManagement selectMerchantManagementByPmid(Long pmid);

    /**
     * 查询商家管理列表
     *
     * @param merchantManagement 商家管理
     * @return 商家管理集合
     */
    public List<MerchantManagement> selectMerchantManagementList(MerchantManagement merchantManagement);

    /**
     * 新增商家管理
     *
     * @param merchantManagement 商家管理
     * @return 结果
     */
    public int insertMerchantManagement(MerchantManagement merchantManagement);

    /**
     * 修改商家管理
     *
     * @param merchantManagement 商家管理
     * @return 结果
     */
    public int updateMerchantManagement(MerchantManagement merchantManagement);

    /**
     * 删除商家管理
     *
     * @param pmid 商家管理主键
     * @return 结果
     */
    public int deleteMerchantManagementByPmid(Long pmid);

    /**
     * 批量删除商家管理
     *
     * @param pmids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteMerchantManagementByPmids(Long[] pmids);
}
