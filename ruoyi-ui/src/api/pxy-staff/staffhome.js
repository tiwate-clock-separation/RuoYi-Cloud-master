import request from '@/utils/request'

// 查询物业人员列表
export function listStaffhome(query) {
  return request({
    url: '/pxy-staff/staffhome/list',
    method: 'get',
    params: query
  })
}

// 查询物业人员详细
export function getStaffhome(ehid) {
  return request({
    url: '/pxy-staff/staffhome/' + ehid,
    method: 'get'
  })
}

// 新增物业人员
export function addStaffhome(data) {
  return request({
    url: '/pxy-staff/staffhome',
    method: 'post',
    data: data
  })
}

// 修改物业人员
export function updateStaffhome(data) {
  return request({
    url: '/pxy-staff/staffhome',
    method: 'put',
    data: data
  })
}

// 删除物业人员
export function delStaffhome(ehid) {
  return request({
    url: '/pxy-staff/staffhome/' + ehid,
    method: 'delete'
  })
}
