package com.ruoyi.staff.service.impl;

import java.util.List;
import com.ruoyi.common.core.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.staff.mapper.EmpCarMapper;
import com.ruoyi.domain.EmpCar;
import com.ruoyi.staff.service.IEmpCarService;

/**
 * 车场人员Service业务层处理
 * 
 * @author pxy
 * @date 2023-12-23
 */
@Service
public class EmpCarServiceImpl implements IEmpCarService 
{
    @Autowired
    private EmpCarMapper empCarMapper;

    /**
     * 查询车场人员
     * 
     * @param carid 车场人员主键
     * @return 车场人员
     */
    @Override
    public EmpCar selectEmpCarByCarid(Long carid)
    {
        return empCarMapper.selectEmpCarByCarid(carid);
    }

    /**
     * 查询车场人员列表
     * 
     * @param empCar 车场人员
     * @return 车场人员
     */
    @Override
    public List<EmpCar> selectEmpCarList(EmpCar empCar)
    {
        return empCarMapper.selectEmpCarList(empCar);
    }

    /**
     * 新增车场人员
     * 
     * @param empCar 车场人员
     * @return 结果
     */
    @Override
    public int insertEmpCar(EmpCar empCar)
    {
        empCar.setCreateTime(DateUtils.getNowDate());
        return empCarMapper.insertEmpCar(empCar);
    }

    /**
     * 修改车场人员
     * 
     * @param empCar 车场人员
     * @return 结果
     */
    @Override
    public int updateEmpCar(EmpCar empCar)
    {
        empCar.setUpdateTime(DateUtils.getNowDate());
        return empCarMapper.updateEmpCar(empCar);
    }

    /**
     * 批量删除车场人员
     * 
     * @param carids 需要删除的车场人员主键
     * @return 结果
     */
    @Override
    public int deleteEmpCarByCarids(Long[] carids)
    {
        return empCarMapper.deleteEmpCarByCarids(carids);
    }

    /**
     * 删除车场人员信息
     * 
     * @param carid 车场人员主键
     * @return 结果
     */
    @Override
    public int deleteEmpCarByCarid(Long carid)
    {
        return empCarMapper.deleteEmpCarByCarid(carid);
    }
}
