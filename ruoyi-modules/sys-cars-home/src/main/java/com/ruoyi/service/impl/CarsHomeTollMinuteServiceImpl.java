package com.ruoyi.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.mapper.CarsHomeTollMinuteMapper;
import com.ruoyi.domain.CarsHomeTollMinute;
import com.ruoyi.service.ICarsHomeTollMinuteService;

/**
 * 按分钟收费Service业务层处理
 * 
 * @author chn
 * @date 2023-12-14
 */
@Service
public class CarsHomeTollMinuteServiceImpl implements ICarsHomeTollMinuteService 
{
    @Autowired
    private CarsHomeTollMinuteMapper carsHomeTollMinuteMapper;

    /**
     * 查询按分钟收费
     * 
     * @param minuteid 按分钟收费主键
     * @return 按分钟收费
     */
    @Override
    public CarsHomeTollMinute selectCarsHomeTollMinuteByMinuteid(Long minuteid)
    {
        return carsHomeTollMinuteMapper.selectCarsHomeTollMinuteByMinuteid(minuteid);
    }

    /**
     * 查询按分钟收费列表
     * 
     * @param carsHomeTollMinute 按分钟收费
     * @return 按分钟收费
     */
    @Override
    public List<CarsHomeTollMinute> selectCarsHomeTollMinuteList(CarsHomeTollMinute carsHomeTollMinute)
    {
        return carsHomeTollMinuteMapper.selectCarsHomeTollMinuteList(carsHomeTollMinute);
    }

    /**
     * 新增按分钟收费
     * 
     * @param carsHomeTollMinute 按分钟收费
     * @return 结果
     */
    @Override
    public int insertCarsHomeTollMinute(CarsHomeTollMinute carsHomeTollMinute)
    {
        return carsHomeTollMinuteMapper.insertCarsHomeTollMinute(carsHomeTollMinute);
    }

    /**
     * 修改按分钟收费
     * 
     * @param carsHomeTollMinute 按分钟收费
     * @return 结果
     */
    @Override
    public int updateCarsHomeTollMinute(CarsHomeTollMinute carsHomeTollMinute)
    {
        return carsHomeTollMinuteMapper.updateCarsHomeTollMinute(carsHomeTollMinute);
    }

    /**
     * 批量删除按分钟收费
     * 
     * @param minuteids 需要删除的按分钟收费主键
     * @return 结果
     */
    @Override
    public int deleteCarsHomeTollMinuteByMinuteids(Long[] minuteids)
    {
        return carsHomeTollMinuteMapper.deleteCarsHomeTollMinuteByMinuteids(minuteids);
    }

    /**
     * 删除按分钟收费信息
     * 
     * @param minuteid 按分钟收费主键
     * @return 结果
     */
    @Override
    public int deleteCarsHomeTollMinuteByMinuteid(Long minuteid)
    {
        return carsHomeTollMinuteMapper.deleteCarsHomeTollMinuteByMinuteid(minuteid);
    }
}
