package com.ruoyi.system.api;

import com.ruoyi.common.core.web.domain.AjaxResult;
import com.ruoyi.common.core.web.page.TableDataInfo;
import com.ruoyi.domain.PayOrder;
import com.ruoyi.domain.PayWater;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

@Component
@FeignClient("sys-property")
public interface PropertyApi {
    @GetMapping("/propertyApi/list")
    public TableDataInfo list();

    @PostMapping("/order")
    public TableDataInfo add(PayOrder payOrder);

    @PostMapping("/propertyWater")
    public AjaxResult add(PayWater payWater);
}
