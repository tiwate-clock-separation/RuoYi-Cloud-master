package com.ruoyi.system.mapper.result;

import com.ruoyi.domain.PayOrder;
import org.apache.ibatis.annotations.Many;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Select;

public interface PayOrderResult {
    @Select(
            "select poid, code, status, deleted, create_time, update_time, ohid, ttid, cmid, order_price, order_status, ptid from pay_order where poid = #{poid}"
    )
    @Results({
            @Result(property = "createTime",column = "create_time"),
            @Result(property = "updateTime",column = "update_time"),
            @Result(property = "ohid",column = "ohid"),
            @Result(property = "ttid",column = "ttid"),
            @Result(property = "cmid",column = "cmid"),
            @Result(property = "orderPrice",column = "order_price"),
            @Result(property = "orderStatus",column = "order_status"),
            @Result(property = "ptid",column = "ptid"),
            @Result(property = "carsMsg", column = "cmid", many = @Many(select = "com.ruoyi.system.mapper.result.CarsMsgResult.selectCarsMsgByCmid")),
            @Result(property = "propertyManagement", column = "ohid", many = @Many(select = "com.ruoyi.system.mapper.PropertyManagementMapper.selectPropertyManagementByPmid")),
            @Result(property = "payType", column = "ptid", many = @Many(select = "com.ruoyi.system.mapper.result.PayTypeResult.selectPayTypeByPoid")),
            @Result(property = "tradingType", column = "ttid", many = @Many(select = "com.ruoyi.system.mapper.result.TradingTypeResult.selectTradingTypeByPoid"))
    })
    public PayOrder selectPayOrderByPoid(Integer poid);
}
