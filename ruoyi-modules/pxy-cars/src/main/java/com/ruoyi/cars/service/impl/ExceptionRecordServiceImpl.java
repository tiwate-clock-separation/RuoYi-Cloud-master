package com.ruoyi.cars.service.impl;

import java.util.List;
import com.ruoyi.common.core.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.cars.mapper.ExceptionRecordMapper;
import com.ruoyi.domain.ExceptionRecord;
import com.ruoyi.cars.service.IExceptionRecordService;

/**
 * 异常车辆记录Service业务层处理
 * 
 * @author ruoyi
 * @date 2023-12-06
 */
@Service
public class ExceptionRecordServiceImpl implements IExceptionRecordService 
{
    @Autowired
    private ExceptionRecordMapper exceptionRecordMapper;

    /**
     * 查询异常车辆记录
     * 
     * @param erid 异常车辆记录主键
     * @return 异常车辆记录
     */
    @Override
    public ExceptionRecord selectExceptionRecordByErid(Long erid)
    {
        return exceptionRecordMapper.selectExceptionRecordByErid(erid);
    }

    /**
     * 查询异常车辆记录列表
     * 
     * @param exceptionRecord 异常车辆记录
     * @return 异常车辆记录
     */
    @Override
    public List<ExceptionRecord> selectExceptionRecordList(ExceptionRecord exceptionRecord)
    {
        return exceptionRecordMapper.selectExceptionRecordList(exceptionRecord);
    }

    /**
     * 新增异常车辆记录
     * 
     * @param exceptionRecord 异常车辆记录
     * @return 结果
     */
    @Override
    public int insertExceptionRecord(ExceptionRecord exceptionRecord)
    {
        exceptionRecord.setCreateTime(DateUtils.getNowDate());
        return exceptionRecordMapper.insertExceptionRecord(exceptionRecord);
    }

    /**
     * 修改异常车辆记录
     * 
     * @param exceptionRecord 异常车辆记录
     * @return 结果
     */
    @Override
    public int updateExceptionRecord(ExceptionRecord exceptionRecord)
    {
        exceptionRecord.setUpdateTime(DateUtils.getNowDate());
        return exceptionRecordMapper.updateExceptionRecord(exceptionRecord);
    }

    /**
     * 批量删除异常车辆记录
     * 
     * @param erids 需要删除的异常车辆记录主键
     * @return 结果
     */
    @Override
    public int deleteExceptionRecordByErids(Long[] erids)
    {
        return exceptionRecordMapper.deleteExceptionRecordByErids(erids);
    }

    /**
     * 删除异常车辆记录信息
     * 
     * @param erid 异常车辆记录主键
     * @return 结果
     */
    @Override
    public int deleteExceptionRecordByErid(Long erid)
    {
        return exceptionRecordMapper.deleteExceptionRecordByErid(erid);
    }
}
