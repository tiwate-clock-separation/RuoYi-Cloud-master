package com.ruoyi.service.impl;

import java.util.List;
import com.ruoyi.common.core.utils.DateUtils;
import com.ruoyi.domain.CarsMsg;
import com.ruoyi.domain.PropertyManagement;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.ArrayList;
import com.ruoyi.common.core.utils.StringUtils;
import org.springframework.transaction.annotation.Transactional;
import com.ruoyi.mapper.CarsMsgMapper;
import com.ruoyi.service.ICarsMsgService;

/**
 * 车辆信息Service业务层处理
 *
 * @author ruoyi
 * @date 2023-12-07
 */
@Service
public class CarsMsgServiceImpl implements ICarsMsgService
{
    @Autowired
    private CarsMsgMapper carsMsgMapper;

    /**
     * 查询车辆信息
     *
     * @param cmid 车辆信息主键
     * @return 车辆信息
     */
    @Override
    public CarsMsg selectCarsMsgByCmid(Long cmid)
    {
        return carsMsgMapper.selectCarsMsgByCmid(cmid);
    }

    /**
     * 查询车辆信息列表
     *
     * @param carsMsg 车辆信息
     * @return 车辆信息
     */
    @Override
    public List<CarsMsg> selectCarsMsgList(CarsMsg carsMsg)
    {
        return carsMsgMapper.selectCarsMsgList(carsMsg);
    }

    /**
     * 新增车辆信息
     *
     * @param carsMsg 车辆信息
     * @return 结果
     */
    @Transactional
    @Override
    public int insertCarsMsg(CarsMsg carsMsg)
    {
        carsMsg.setCreateTime(DateUtils.getNowDate());
        int rows = carsMsgMapper.insertCarsMsg(carsMsg);
//        insertPropertyManagement(carsMsg);
        return rows;
    }

    /**
     * 修改车辆信息
     *
     * @param carsMsg 车辆信息
     * @return 结果
     */
    @Transactional
    @Override
    public int updateCarsMsg(CarsMsg carsMsg)
    {
        carsMsg.setUpdateTime(DateUtils.getNowDate());
        carsMsgMapper.deletePropertyManagementByPmid(carsMsg.getCmid());
//        insertPropertyManagement(carsMsg);
        return carsMsgMapper.updateCarsMsg(carsMsg);
    }

    /**
     * 批量删除车辆信息
     *
     * @param cmids 需要删除的车辆信息主键
     * @return 结果
     */
    @Transactional
    @Override
    public int deleteCarsMsgByCmids(Long[] cmids)
    {
        carsMsgMapper.deletePropertyManagementByPmids(cmids);
        return carsMsgMapper.deleteCarsMsgByCmids(cmids);
    }

    /**
     * 删除车辆信息信息
     *
     * @param cmid 车辆信息主键
     * @return 结果
     */
    @Transactional
    @Override
    public int deleteCarsMsgByCmid(Long cmid)
    {
        carsMsgMapper.deletePropertyManagementByPmid(cmid);
        return carsMsgMapper.deleteCarsMsgByCmid(cmid);
    }

    /**
     * 新增物业管理信息
     *
     * @param carsMsg 车辆信息对象
     */
    public void insertPropertyManagement(CarsMsg carsMsg)
    {
        PropertyManagement propertyManagementList = carsMsg.getPropertyManagement();
//        Long cmid = carsMsg.getCmid();
//        if (StringUtils.isNotNull(propertyManagementList))
//        {
//            List<PropertyManagement> list = new ArrayList<PropertyManagement>();
//            for (PropertyManagement propertyManagement : propertyManagementList)
//            {
//                propertyManagement.setPmid(cmid);
//                list.add(propertyManagement);
//            }
//            if (list.size() > 0)
//            {
                carsMsgMapper.batchPropertyManagement(propertyManagementList);
//            }
//        }
    }
}
