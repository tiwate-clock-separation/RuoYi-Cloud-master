package com.ruoyi.controller;

import java.util.List;
import java.util.concurrent.ConcurrentHashMap;
import javax.servlet.http.HttpServletResponse;

import com.ruoyi.domain.CarsHomeBooth;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.log.annotation.Log;
import com.ruoyi.common.log.enums.BusinessType;
import com.ruoyi.common.security.annotation.RequiresPermissions;
import com.ruoyi.service.ICarsHomeBoothService;
import com.ruoyi.common.core.web.controller.BaseController;
import com.ruoyi.common.core.web.domain.AjaxResult;
import com.ruoyi.common.core.utils.poi.ExcelUtil;
import com.ruoyi.common.core.web.page.TableDataInfo;

/**
 * 车场岗亭Controller
 * 
 * @author chn
 * @date 2023-12-04
 */
@RestController
@RequestMapping("/booth")
public class CarsHomeBoothController extends BaseController
{
    @Autowired
    private ICarsHomeBoothService carsHomeBoothService;


    /**
     * 查询车场岗亭列表
     */
    @RequiresPermissions("sys-cars-home:booth:list")
    @GetMapping("/list")
    public TableDataInfo list(CarsHomeBooth carsHomeBooth)
    {
        startPage();
        List<CarsHomeBooth> list = carsHomeBoothService.selectCarsHomeBoothList(carsHomeBooth);
        return getDataTable(list);
    }

    @RequiresPermissions("sys-cars-home:booth:list")
    @GetMapping("/boothlanelist/{chid}")
    public TableDataInfo getBoothLaneList(@PathVariable("chid") Long chid)
    {
        startPage();
        List<CarsHomeBooth> list = carsHomeBoothService.getBoothLaneList(chid);
        return getDataTable(list);
    }

    /**
     * 导出车场岗亭列表
     */
    @RequiresPermissions("sys-cars-home:booth:export")
    @Log(title = "车场岗亭", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, CarsHomeBooth carsHomeBooth)
    {
        List<CarsHomeBooth> list = carsHomeBoothService.selectCarsHomeBoothList(carsHomeBooth);
        ExcelUtil<CarsHomeBooth> util = new ExcelUtil<CarsHomeBooth>(CarsHomeBooth.class);
        util.exportExcel(response, list, "车场岗亭数据");
    }

    /**
     * 获取车场岗亭详细信息
     */
    @RequiresPermissions("sys-cars-home:booth:query")
    @GetMapping(value = "/{chbid}")
    public AjaxResult getInfo(@PathVariable("chbid") Long chbid)
    {
        return success(carsHomeBoothService.selectCarsHomeBoothByChbid(chbid));
    }

    /**
     * 新增车场岗亭
     */
    @RequiresPermissions("sys-cars-home:booth:add")
    @Log(title = "车场岗亭", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody CarsHomeBooth carsHomeBooth)
    {
        return toAjax(carsHomeBoothService.insertCarsHomeBooth(carsHomeBooth));
    }

    /**
     * 修改车场岗亭
     */
    @RequiresPermissions("sys-cars-home:booth:edit")
    @Log(title = "车场岗亭", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody CarsHomeBooth carsHomeBooth)
    {
        return toAjax(carsHomeBoothService.updateCarsHomeBooth(carsHomeBooth));
    }

    /**
     * 删除车场岗亭
     */
    @RequiresPermissions("sys-cars-home:booth:remove")
    @Log(title = "车场岗亭", businessType = BusinessType.DELETE)
	@DeleteMapping("/{chbids}")
    public AjaxResult remove(@PathVariable Long[] chbids)
    {
        return toAjax(carsHomeBoothService.deleteCarsHomeBoothByChbids(chbids));
    }
}
