import request from '@/utils/request'

// 查询h5消费列表
export function listCheck(query) {
  return request({
    url: '/pxy-staff/check/list',
    method: 'get',
    params: query
  })
}

// 查询h5消费详细
export function getCheck(id) {
  return request({
    url: '/pxy-staff/check/' + id,
    method: 'get'
  })
}

// 新增h5消费
export function addCheck(data) {
  return request({
    url: '/pxy-staff/check',
    method: 'post',
    data: data
  })
}

// 修改h5消费
export function updateCheck(data) {
  return request({
    url: '/pxy-staff/check',
    method: 'put',
    data: data
  })
}

// 删除h5消费
export function delCheck(id) {
  return request({
    url: '/pxy-staff/check/' + id,
    method: 'delete'
  })
}
