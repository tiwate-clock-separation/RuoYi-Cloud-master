package com.ruoyi.cars.mapper;

import com.ruoyi.domain.CutoffRecord;
import org.apache.ibatis.annotations.One;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * 开闸记录Mapper接口
 *
 * @author ruoyi
 * @date 2023-11-29
 */
public interface CutoffRecordMapper
{
    /**
     * 查询开闸记录
     *
     * @param corid 开闸记录主键
     * @return 开闸记录
     */
    public CutoffRecord selectCutoffRecordByCorid(Long corid);

    /**
     * 查询开闸记录列表
     *
     * @param cutoffRecord 开闸记录
     * @return 开闸记录集合
     */
//    @Select("<script>" +
//            "select * from cutoff_record" +
//            "<where>" +
//            "<if test=\"pmid != null and pmid != ''\">and pmid = ${pmid} </if>" +
//            "<if test=\"hpid != null and hpid != ''\">and hpid = ${hpid} </if>" +
//            "<if test=\"gate != null and gate != ''\">and gate = ${gate} </if>" +
//            "<if test=\"camera != null and camera != ''\">and camera = ${camera} </if>" +
//            "<if test=\"off != null and off != ''\">and off = ${off} </if>" +
//            "</where>" +
//            "</script>")
//    @Results(value = {
//            @Result(id = true,property = "corid",column = "corid"),
//            @Result(property = "driverIp",column = "driver_ip"),
//            @Result(property = "cutoffRemark",column = "cutoff_remark"),
//            @Result(property = "cutoffUser",column = "cutoff_user"),
//            @Result(property = "pmid",column = "pmid"),
//            @Result(property = "hpid",column = "hpid"),
//            @Result(property = "gate",column = "gate"),
//            @Result(property = "camera",column = "camera"),
//            @Result(property = "off",column = "off"),
//            @Result(property = "propertyManagement",column = "pmid",one = @One(select = "com.ruoyi.cars.mapper.result.properManagementResult.getPeoperty")),
//            @Result(property = "carsHome",column = "hpid",one = @One(select = "com.ruoyi.cars.mapper.result.carsHomeResult.getCarsHome"))
//    })
    public List<CutoffRecord> selectCutoffRecordList(CutoffRecord cutoffRecord);

    /**
     * 新增开闸记录
     *
     * @param cutoffRecord 开闸记录
     * @return 结果
     */
    public int insertCutoffRecord(CutoffRecord cutoffRecord);

    /**
     * 修改开闸记录
     *
     * @param cutoffRecord 开闸记录
     * @return 结果
     */
    public int updateCutoffRecord(CutoffRecord cutoffRecord);

    /**
     * 删除开闸记录
     *
     * @param corid 开闸记录主键
     * @return 结果
     */
    public int deleteCutoffRecordByCorid(Long corid);

    /**
     * 批量删除开闸记录
     *
     * @param corids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteCutoffRecordByCorids(Long[] corids);
}
