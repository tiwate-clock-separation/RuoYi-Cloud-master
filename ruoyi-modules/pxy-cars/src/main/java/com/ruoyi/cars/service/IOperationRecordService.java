package com.ruoyi.cars.service;

import java.util.List;
import com.ruoyi.domain.OperationRecord;

/**
 * 设备记录Service接口
 *
 * @author ruoyi
 * @date 2023-12-04
 */
public interface IOperationRecordService
{
    /**
     * 查询设备记录
     *
     * @param orid 设备记录主键
     * @return 设备记录
     */
    public OperationRecord selectOperationRecordByOrid(Long orid);

    /**
     * 查询设备记录列表
     *
     * @param operationRecord 设备记录
     * @return 设备记录集合
     */
    public List<OperationRecord> selectOperationRecordList(OperationRecord operationRecord);

    /**
     * 新增设备记录
     *
     * @param operationRecord 设备记录
     * @return 结果
     */
    public int insertOperationRecord(OperationRecord operationRecord);

    /**
     * 修改设备记录
     *
     * @param operationRecord 设备记录
     * @return 结果
     */
    public int updateOperationRecord(OperationRecord operationRecord);

    /**
     * 批量删除设备记录
     *
     * @param orids 需要删除的设备记录主键集合
     * @return 结果
     */
    public int deleteOperationRecordByOrids(Long[] orids);

    /**
     * 删除设备记录信息
     *
     * @param orid 设备记录主键
     * @return 结果
     */
    public int deleteOperationRecordByOrid(Long orid);
}
