package com.ruoyi.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.core.annotation.Excel;
import com.ruoyi.common.core.web.domain.BaseEntity;

/**
 * 按分钟收费对象 cars_home_toll_minute
 * 
 * @author chn
 * @date 2023-12-14
 */
public class CarsHomeTollMinute extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 分钟自增id */
    private Long minuteid;

    /** 免收分钟 */
    @Excel(name = "免收分钟")
    private Long freeMinute;

    /** 起步分钟 */
    @Excel(name = "起步分钟")
    private Long beginMinute;

    /** 起步金额 */
    @Excel(name = "起步金额")
    private Long beginMoney;

    /** 每超多少分钟 */
    @Excel(name = "每超多少分钟")
    private Long thanMinute;

    /** 收费金额 */
    @Excel(name = "收费金额")
    private Long getMoney;

    /** 每日最高金额 */
    @Excel(name = "每日最高金额")
    private Long maxMoney;

    /** 关联码 */
    @Excel(name = "关联码")
    private String code;

    public void setMinuteid(Long minuteid) 
    {
        this.minuteid = minuteid;
    }

    public Long getMinuteid() 
    {
        return minuteid;
    }
    public void setFreeMinute(Long freeMinute) 
    {
        this.freeMinute = freeMinute;
    }

    public Long getFreeMinute() 
    {
        return freeMinute;
    }
    public void setBeginMinute(Long beginMinute) 
    {
        this.beginMinute = beginMinute;
    }

    public Long getBeginMinute() 
    {
        return beginMinute;
    }
    public void setBeginMoney(Long beginMoney) 
    {
        this.beginMoney = beginMoney;
    }

    public Long getBeginMoney() 
    {
        return beginMoney;
    }
    public void setThanMinute(Long thanMinute) 
    {
        this.thanMinute = thanMinute;
    }

    public Long getThanMinute() 
    {
        return thanMinute;
    }
    public void setGetMoney(Long getMoney) 
    {
        this.getMoney = getMoney;
    }

    public Long getGetMoney() 
    {
        return getMoney;
    }
    public void setMaxMoney(Long maxMoney) 
    {
        this.maxMoney = maxMoney;
    }

    public Long getMaxMoney() 
    {
        return maxMoney;
    }
    public void setCode(String code) 
    {
        this.code = code;
    }

    public String getCode() 
    {
        return code;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("minuteid", getMinuteid())
            .append("freeMinute", getFreeMinute())
            .append("beginMinute", getBeginMinute())
            .append("beginMoney", getBeginMoney())
            .append("thanMinute", getThanMinute())
            .append("getMoney", getGetMoney())
            .append("maxMoney", getMaxMoney())
            .append("code", getCode())
            .toString();
    }
}
