import request from '@/utils/request'

// 查询支付宝支付配置列表
export function listAlipay(query) {
  return request({
    url: '/sys-cars-home/alipay/list',
    method: 'get',
    params: query
  })
}

// 查询支付宝支付配置详细
export function getAlipay(id) {
  return request({
    url: '/sys-cars-home/alipay/' + id,
    method: 'get'
  })
}

// 新增支付宝支付配置
export function addAlipay(data) {
  return request({
    url: '/sys-cars-home/alipay',
    method: 'post',
    data: data
  })
}

// 修改支付宝支付配置
export function updateAlipay(data) {
  return request({
    url: '/sys-cars-home/alipay',
    method: 'put',
    data: data
  })
}

// 删除支付宝支付配置
export function delAlipay(id) {
  return request({
    url: '/sys-cars-home/alipay/' + id,
    method: 'delete'
  })
}
