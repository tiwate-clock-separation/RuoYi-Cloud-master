import request from '@/utils/request'

// 查询车场节假日列表
export function listHolidays(query) {
  return request({
    url: '/sys-cars-home/holidays/list',
    method: 'get',
    params: query
  })
}

// 查询车场节假日详细
export function getHolidays(id) {
  return request({
    url: '/sys-cars-home/holidays/' + id,
    method: 'get'
  })
}

// 新增车场节假日
export function addHolidays(data) {
  return request({
    url: '/sys-cars-home/holidays',
    method: 'post',
    data: data
  })
}

// 修改车场节假日
export function updateHolidays(data) {
  return request({
    url: '/sys-cars-home/holidays',
    method: 'put',
    data: data
  })
}

// 删除车场节假日
export function delHolidays(id) {
  return request({
    url: '/sys-cars-home/holidays/' + id,
    method: 'delete'
  })
}
