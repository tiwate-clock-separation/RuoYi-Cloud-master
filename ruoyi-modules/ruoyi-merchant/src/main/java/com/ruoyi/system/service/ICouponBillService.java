package com.ruoyi.system.service;

import java.util.List;
import com.ruoyi.domain.CouponBill;

/**
 * 优惠卷账单Service接口
 *
 * @author ruoyi
 * @date 2023-12-26
 */
public interface ICouponBillService
{
    /**
     * 查询优惠卷账单
     *
     * @param id 优惠卷账单主键
     * @return 优惠卷账单
     */
    public CouponBill selectCouponBillById(Long id);

    /**
     * 查询优惠卷账单列表
     *
     * @param couponBill 优惠卷账单
     * @return 优惠卷账单集合
     */
    public List<CouponBill> selectCouponBillList(CouponBill couponBill);

    /**
     * 新增优惠卷账单
     *
     * @param couponBill 优惠卷账单
     * @return 结果
     */
    public int insertCouponBill(CouponBill couponBill);

    /**
     * 修改优惠卷账单
     *
     * @param couponBill 优惠卷账单
     * @return 结果
     */
    public int updateCouponBill(CouponBill couponBill);

    /**
     * 批量删除优惠卷账单
     *
     * @param ids 需要删除的优惠卷账单主键集合
     * @return 结果
     */
    public int deleteCouponBillByIds(Long[] ids);

    /**
     * 删除优惠卷账单信息
     *
     * @param id 优惠卷账单主键
     * @return 结果
     */
    public int deleteCouponBillById(Long id);
}
