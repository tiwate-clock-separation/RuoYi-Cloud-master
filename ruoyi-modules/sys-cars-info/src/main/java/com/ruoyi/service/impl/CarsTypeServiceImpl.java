package com.ruoyi.service.impl;

import java.util.List;
import com.ruoyi.common.core.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.mapper.CarsTypeMapper;
import com.ruoyi.domain.CarsType;
import com.ruoyi.service.ICarsTypeService;

/**
 * 车辆类型Service业务层处理
 *
 * @author ruoyi
 * @date 2023-12-12
 */
@Service
public class CarsTypeServiceImpl implements ICarsTypeService
{
    @Autowired
    private CarsTypeMapper carsTypeMapper;

    /**
     * 查询车辆类型
     *
     * @param ctid 车辆类型主键
     * @return 车辆类型
     */
    @Override
    public CarsType selectCarsTypeByCtid(Long ctid)
    {
        return carsTypeMapper.selectCarsTypeByCtid(ctid);
    }

    /**
     * 查询车辆类型列表
     *
     * @param carsType 车辆类型
     * @return 车辆类型
     */
    @Override
    public List<CarsType> selectCarsTypeList(CarsType carsType)
    {
        return carsTypeMapper.selectCarsTypeList(carsType);
    }

    /**
     * 新增车辆类型
     *
     * @param carsType 车辆类型
     * @return 结果
     */
    @Override
    public int insertCarsType(CarsType carsType)
    {
        carsType.setCreateTime(DateUtils.getNowDate());
        return carsTypeMapper.insertCarsType(carsType);
    }

    /**
     * 修改车辆类型
     *
     * @param carsType 车辆类型
     * @return 结果
     */
    @Override
    public int updateCarsType(CarsType carsType)
    {
        carsType.setUpdateTime(DateUtils.getNowDate());
        return carsTypeMapper.updateCarsType(carsType);
    }

    /**
     * 批量删除车辆类型
     *
     * @param ctids 需要删除的车辆类型主键
     * @return 结果
     */
    @Override
    public int deleteCarsTypeByCtids(Long[] ctids)
    {
        return carsTypeMapper.deleteCarsTypeByCtids(ctids);
    }

    /**
     * 删除车辆类型信息
     *
     * @param ctid 车辆类型主键
     * @return 结果
     */
    @Override
    public int deleteCarsTypeByCtid(Long ctid)
    {
        return carsTypeMapper.deleteCarsTypeByCtid(ctid);
    }
}
