package com.ruoyi.controller;

import java.util.List;
import java.io.IOException;
import javax.servlet.http.HttpServletResponse;

import com.ruoyi.domain.CarsHome;
import com.ruoyi.domain.PropertyManagement;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.log.annotation.Log;
import com.ruoyi.common.log.enums.BusinessType;
import com.ruoyi.common.security.annotation.RequiresPermissions;
import com.ruoyi.domain.CarsHomeParking;
import com.ruoyi.service.ICarsHomeParkingService;
import com.ruoyi.common.core.web.controller.BaseController;
import com.ruoyi.common.core.web.domain.AjaxResult;
import com.ruoyi.common.core.utils.poi.ExcelUtil;
import com.ruoyi.common.core.web.page.TableDataInfo;

/**
 * 车场车位Controller
 * 
 * @author chn
 * @date 2023-12-08
 */
@RestController
@RequestMapping("/parking")
public class CarsHomeParkingController extends BaseController
{
    @Autowired
    private ICarsHomeParkingService carsHomeParkingService;

    /**
     * 查询车场车位列表
     */
    @RequiresPermissions("sys-cars-home:parking:list")
    @GetMapping("/list")
    public TableDataInfo list(CarsHomeParking carsHomeParking, CarsHome carsHome, PropertyManagement propertyManagement)
    {
        startPage();
        carsHomeParking.setCarsHome(carsHome);
        carsHomeParking.setPropertyManagement(propertyManagement);
        List<CarsHomeParking> list = carsHomeParkingService.selectCarsHomeParkingList(carsHomeParking);
        return getDataTable(list);
    }

    /**
     * 导出车场车位列表
     */
    @RequiresPermissions("sys-cars-home:parking:export")
    @Log(title = "车场车位", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, CarsHomeParking carsHomeParking)
    {
        List<CarsHomeParking> list = carsHomeParkingService.selectCarsHomeParkingList(carsHomeParking);
        ExcelUtil<CarsHomeParking> util = new ExcelUtil<CarsHomeParking>(CarsHomeParking.class);
        util.exportExcel(response, list, "车场车位数据");
    }

    /**
     * 获取车场车位详细信息
     */
    @RequiresPermissions("sys-cars-home:parking:query")
    @GetMapping(value = "/{chpid}")
    public AjaxResult getInfo(@PathVariable("chpid") Long chpid)
    {
        return success(carsHomeParkingService.selectCarsHomeParkingByChpid(chpid));
    }

    /**
     * 新增车场车位
     */
    @RequiresPermissions("sys-cars-home:parking:add")
    @Log(title = "车场车位", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody CarsHomeParking carsHomeParking)
    {
        return toAjax(carsHomeParkingService.insertCarsHomeParking(carsHomeParking));
    }

    /**
     * 修改车场车位
     */
    @RequiresPermissions("sys-cars-home:parking:edit")
    @Log(title = "车场车位", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody CarsHomeParking carsHomeParking)
    {
        return toAjax(carsHomeParkingService.updateCarsHomeParking(carsHomeParking));
    }

    /**
     * 删除车场车位
     */
    @RequiresPermissions("sys-cars-home:parking:remove")
    @Log(title = "车场车位", businessType = BusinessType.DELETE)
	@DeleteMapping("/{chpids}")
    public AjaxResult remove(@PathVariable Long[] chpids)
    {
        return toAjax(carsHomeParkingService.deleteCarsHomeParkingByChpids(chpids));
    }
}
