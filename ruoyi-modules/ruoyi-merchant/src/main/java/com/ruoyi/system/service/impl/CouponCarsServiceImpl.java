package com.ruoyi.system.service.impl;

import java.util.List;
import com.ruoyi.common.core.utils.DateUtils;
import com.ruoyi.domain.CouponCars;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.CouponCarsMapper;
import com.ruoyi.system.service.ICouponCarsService;

/**
 * 优惠卷车辆连Service业务层处理
 *
 * @author ruoyi
 * @date 2023-12-15
 */
@Service
public class CouponCarsServiceImpl implements ICouponCarsService
{
    @Autowired
    private CouponCarsMapper couponCarsMapper;

    /**
     * 查询优惠卷车辆连
     *
     * @param ccid 优惠卷车辆连主键
     * @return 优惠卷车辆连
     */
    @Override
    public CouponCars selectCouponCarsByCcid(Long ccid)
    {
        return couponCarsMapper.selectCouponCarsByCcid(ccid);
    }

    /**
     * 查询优惠卷车辆连列表
     *
     * @param couponCars 优惠卷车辆连
     * @return 优惠卷车辆连
     */
    @Override
    public List<CouponCars> selectCouponCarsList(CouponCars couponCars)
    {
        return couponCarsMapper.selectCouponCarsList(couponCars);
    }

    /**
     * 新增优惠卷车辆连
     *
     * @param couponCars 优惠卷车辆连
     * @return 结果
     */
    @Override
    public int insertCouponCars(CouponCars couponCars)
    {
        couponCars.setCreateTime(DateUtils.getNowDate());
        return couponCarsMapper.insertCouponCars(couponCars);
    }

    /**
     * 修改优惠卷车辆连
     *
     * @param couponCars 优惠卷车辆连
     * @return 结果
     */
    @Override
    public int updateCouponCars(CouponCars couponCars)
    {
        couponCars.setUpdateTime(DateUtils.getNowDate());
        return couponCarsMapper.updateCouponCars(couponCars);
    }

    /**
     * 批量删除优惠卷车辆连
     *
     * @param ccids 需要删除的优惠卷车辆连主键
     * @return 结果
     */
    @Override
    public int deleteCouponCarsByCcids(Long[] ccids)
    {
        return couponCarsMapper.deleteCouponCarsByCcids(ccids);
    }

    /**
     * 删除优惠卷车辆连信息
     *
     * @param ccid 优惠卷车辆连主键
     * @return 结果
     */
    @Override
    public int deleteCouponCarsByCcid(Long ccid)
    {
        return couponCarsMapper.deleteCouponCarsByCcid(ccid);
    }
}
