package com.ruoyi.domain;

import com.ruoyi.common.core.annotation.Excel;
import com.ruoyi.common.core.web.domain.BaseEntity;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

/**
 * 物业管理对象 property_management
 *
 * @author ruoyi
 * @date 2023-11-29
 */
public class PropertyManagement extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 自增ID */
    private Long pmid;

    /** 唯一编码,前台使用 */
    @Excel(name = "唯一编码,前台使用")
    private String code;

    /** 状态 */
    @Excel(name = "状态")
    private Long status;

    /** 是否删除 */
    @Excel(name = "是否删除")
    private Long deleted;

    /** 物业名称 */
    @Excel(name = "物业名称")
    private String propertyName;

    /** 负责人名字 */
    @Excel(name = "负责人名字")
    private String managerName;

    /** 负责人电话 */
    @Excel(name = "负责人电话")
    private String managerTel;

    /** 物业地址 */
    @Excel(name = "物业地址")
    private String propertyAddress;

    public void setPmid(Long pmid)
    {
        this.pmid = pmid;
    }

    public Long getPmid()
    {
        return pmid;
    }
    public void setCode(String code)
    {
        this.code = code;
    }

    public String getCode()
    {
        return code;
    }
    public void setStatus(Long status)
    {
        this.status = status;
    }

    public Long getStatus()
    {
        return status;
    }
    public void setDeleted(Long deleted)
    {
        this.deleted = deleted;
    }

    public Long getDeleted()
    {
        return deleted;
    }
    public void setPropertyName(String propertyName)
    {
        this.propertyName = propertyName;
    }

    public String getPropertyName()
    {
        return propertyName;
    }
    public void setManagerName(String managerName)
    {
        this.managerName = managerName;
    }

    public String getManagerName()
    {
        return managerName;
    }
    public void setManagerTel(String managerTel)
    {
        this.managerTel = managerTel;
    }

    public String getManagerTel()
    {
        return managerTel;
    }
    public void setPropertyAddress(String propertyAddress)
    {
        this.propertyAddress = propertyAddress;
    }

    public String getPropertyAddress()
    {
        return propertyAddress;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("pmid", getPmid())
            .append("code", getCode())
            .append("status", getStatus())
            .append("deleted", getDeleted())
            .append("createTime", getCreateTime())
            .append("updateTime", getUpdateTime())
            .append("propertyName", getPropertyName())
            .append("managerName", getManagerName())
            .append("managerTel", getManagerTel())
            .append("propertyAddress", getPropertyAddress())
            .toString();
    }
}
