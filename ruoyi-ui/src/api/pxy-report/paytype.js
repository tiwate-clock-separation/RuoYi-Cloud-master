import request from '@/utils/request'
// 获取支付类型集合
export function getptApiList(query) {
  return request({
    url: '/sys-property/propertyApi/ptlist',
    method: 'get',
    params: query
  })
}

// 获取费用类型集合
export function getttApiList(query) {
  return request({
    url: '/sys-property/propertyApi/ttlist',
    method: 'get',
    params: query
  })
}

