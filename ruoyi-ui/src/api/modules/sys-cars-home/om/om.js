import request from '@/utils/request'

// 查询车场运维列表
export function listOm(query) {
  return request({
    url: '/sys-cars-home/om/list',
    method: 'get',
    params: query
  })
}

// 查询车场运维详细
export function getOm(code) {
  return request({
    url: '/sys-cars-home/om/' + code,
    method: 'get'
  })
}

// 新增车场运维
export function addOm(data) {
  return request({
    url: '/sys-cars-home/om',
    method: 'post',
    data: data
  })
}

// 修改车场运维
export function updateOm(data) {
  return request({
    url: '/sys-cars-home/om',
    method: 'put',
    data: data
  })
}

// 删除车场运维
export function delOm(code) {
  return request({
    url: '/sys-cars-home/om/' + code,
    method: 'delete'
  })
}
