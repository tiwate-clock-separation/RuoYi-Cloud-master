package com.ruoyi.report.service;

import java.util.List;
import com.ruoyi.domain.ReportSkip;

/**
 * 逃单报Service接口
 * 
 * @author ruoyi
 * @date 2023-12-07
 */
public interface IReportSkipService 
{
    /**
     * 查询逃单报
     * 
     * @param id 逃单报主键
     * @return 逃单报
     */
    public ReportSkip selectReportSkipById(Long id);

    /**
     * 查询逃单报列表
     * 
     * @param reportSkip 逃单报
     * @return 逃单报集合
     */
    public List<ReportSkip> selectReportSkipList(ReportSkip reportSkip);

    /**
     * 新增逃单报
     * 
     * @param reportSkip 逃单报
     * @return 结果
     */
    public int insertReportSkip(ReportSkip reportSkip);

    /**
     * 修改逃单报
     * 
     * @param reportSkip 逃单报
     * @return 结果
     */
    public int updateReportSkip(ReportSkip reportSkip);

    /**
     * 批量删除逃单报
     * 
     * @param ids 需要删除的逃单报主键集合
     * @return 结果
     */
    public int deleteReportSkipByIds(Long[] ids);

    /**
     * 删除逃单报信息
     * 
     * @param id 逃单报主键
     * @return 结果
     */
    public int deleteReportSkipById(Long id);
}
