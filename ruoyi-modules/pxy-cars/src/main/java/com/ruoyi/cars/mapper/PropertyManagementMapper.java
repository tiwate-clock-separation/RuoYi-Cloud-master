package com.ruoyi.cars.mapper;

import java.util.List;
import com.ruoyi.domain.PropertyManagement;

/**
 * 物业管理Mapper接口
 *
 * @author ruoyi
 * @date 2023-11-30
 */
public interface PropertyManagementMapper
{
    /**
     * 查询物业管理
     *
     * @param pmid 物业管理主键
     * @return 物业管理
     */
    public PropertyManagement selectPropertyManagementByPmid(Long pmid);

    /**
     * 查询物业管理列表
     *
     * @param propertyManagement 物业管理
     * @return 物业管理集合
     */
    public List<PropertyManagement> selectPropertyManagementList(PropertyManagement propertyManagement);

    /**
     * 新增物业管理
     *
     * @param propertyManagement 物业管理
     * @return 结果
     */
    public int insertPropertyManagement(PropertyManagement propertyManagement);

    /**
     * 修改物业管理
     *
     * @param propertyManagement 物业管理
     * @return 结果
     */
    public int updatePropertyManagement(PropertyManagement propertyManagement);

    /**
     * 删除物业管理
     *
     * @param pmid 物业管理主键
     * @return 结果
     */
    public int deletePropertyManagementByPmid(Long pmid);

    /**
     * 批量删除物业管理
     *
     * @param pmids 需要删除的数据主键集合
     * @return 结果
     */
    public int deletePropertyManagementByPmids(Long[] pmids);
}
