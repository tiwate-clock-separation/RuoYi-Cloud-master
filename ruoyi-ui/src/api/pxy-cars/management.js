import request from '@/utils/request'

// 查询物业管理列表
export function listManagement(query) {
  return request({
    url: '/pxy-cars/management/list',
    method: 'get',
    params: query
  })
}

// 查询物业管理详细
export function getManagement(pmid) {
  return request({
    url: '/pxy-cars/management/' + pmid,
    method: 'get'
  })
}

// 新增物业管理
export function addManagement(data) {
  return request({
    url: '/pxy-cars/management',
    method: 'post',
    data: data
  })
}

// 修改物业管理
export function updateManagement(data) {
  return request({
    url: '/pxy-cars/management',
    method: 'put',
    data: data
  })
}

// 删除物业管理
export function delManagement(pmid) {
  return request({
    url: '/pxy-cars/management/' + pmid,
    method: 'delete'
  })
}
