package com.ruoyi.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.core.annotation.Excel;
import com.ruoyi.common.core.web.domain.BaseEntity;

/**
 * 车场车位对象 cars_home_parking
 * 
 * @author chn
 * @date 2023-12-08
 */
public class CarsHomeParking extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 车位id */
    private Long chpid;

    /** 唯一编码,前台使用 */
    @Excel(name = "唯一编码,前台使用")
    private String code;

    /** 状态(0启用1禁用） */
    @Excel(name = "状态(0启用1禁用）")
    private Long status;

    /** 是否删除 */
    @Excel(name = "是否删除")
    private Long deleted;

    /** 停车位编号 */
    @Excel(name = "停车位编号")
    private String sparkcode;

    /** 购买情况(0空闲1已被购买2已被租赁3审核中) */
    @Excel(name = "购买情况(0空闲1已被购买2已被租赁3审核中)")
    private Long purchasesid;

    /** 停车位尺寸(0小车型1大车型) */
    @Excel(name = "停车位尺寸(0小车型1大车型)")
    private Long ctid;

    /** 停车位排列方式(0平行1垂直2侧斜） */
    @Excel(name = "停车位排列方式(0平行1垂直2侧斜）")
    private Long arrangeid;

    /** 是否有充电桩(0没有1有) */
    @Excel(name = "是否有充电桩(0没有1有)")
    private Integer charge;

    /** 数据来源（0PDA1监控2所有） */
    @Excel(name = "数据来源", readConverterExp = "0=PDA1监控2所有")
    private Long source;

    /** 停车状态(0空闲1正在停车) */
    @Excel(name = "停车状态(0空闲1正在停车)")
    private Integer sparkstatus;

    /** 车场id */
    @Excel(name = "车场id")
    private Long chid;

    private CarsHome carsHome;

    private PropertyManagement propertyManagement;

    public CarsHome getCarsHome() {
        return carsHome;
    }

    public void setCarsHome(CarsHome carsHome) {
        this.carsHome = carsHome;
    }

    public PropertyManagement getPropertyManagement() {
        return propertyManagement;
    }

    public void setPropertyManagement(PropertyManagement propertyManagement) {
        this.propertyManagement = propertyManagement;
    }

    /** 车位买主 */
    @Excel(name = "车位买主")
    private String buyers;

    /** 门牌号 */
    @Excel(name = "门牌号")
    private String number;

    /** 用户类型(0业主1租户) */
    @Excel(name = "用户类型(0业主1租户)")
    private Long buyertype;

    public void setChpid(Long chpid) 
    {
        this.chpid = chpid;
    }

    public Long getChpid() 
    {
        return chpid;
    }
    public void setCode(String code) 
    {
        this.code = code;
    }

    public String getCode() 
    {
        return code;
    }
    public void setStatus(Long status) 
    {
        this.status = status;
    }

    public Long getStatus() 
    {
        return status;
    }
    public void setDeleted(Long deleted) 
    {
        this.deleted = deleted;
    }

    public Long getDeleted() 
    {
        return deleted;
    }
    public void setSparkcode(String sparkcode) 
    {
        this.sparkcode = sparkcode;
    }

    public String getSparkcode() 
    {
        return sparkcode;
    }
    public void setPurchasesid(Long purchasesid) 
    {
        this.purchasesid = purchasesid;
    }

    public Long getPurchasesid() 
    {
        return purchasesid;
    }
    public void setCtid(Long ctid) 
    {
        this.ctid = ctid;
    }

    public Long getCtid() 
    {
        return ctid;
    }
    public void setArrangeid(Long arrangeid) 
    {
        this.arrangeid = arrangeid;
    }

    public Long getArrangeid() 
    {
        return arrangeid;
    }
    public void setCharge(Integer charge) 
    {
        this.charge = charge;
    }

    public Integer getCharge() 
    {
        return charge;
    }
    public void setSource(Long source) 
    {
        this.source = source;
    }

    public Long getSource() 
    {
        return source;
    }
    public void setSparkstatus(Integer sparkstatus) 
    {
        this.sparkstatus = sparkstatus;
    }

    public Integer getSparkstatus() 
    {
        return sparkstatus;
    }
    public void setChid(Long chid) 
    {
        this.chid = chid;
    }

    public Long getChid() 
    {
        return chid;
    }
    public void setBuyers(String buyers) 
    {
        this.buyers = buyers;
    }

    public String getBuyers() 
    {
        return buyers;
    }
    public void setNumber(String number) 
    {
        this.number = number;
    }

    public String getNumber() 
    {
        return number;
    }
    public void setBuyertype(Long buyertype) 
    {
        this.buyertype = buyertype;
    }

    public Long getBuyertype() 
    {
        return buyertype;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("chpid", getChpid())
            .append("code", getCode())
            .append("status", getStatus())
            .append("deleted", getDeleted())
            .append("createTime", getCreateTime())
            .append("updateTime", getUpdateTime())
            .append("sparkcode", getSparkcode())
            .append("purchasesid", getPurchasesid())
            .append("ctid", getCtid())
            .append("arrangeid", getArrangeid())
            .append("charge", getCharge())
            .append("source", getSource())
            .append("sparkstatus", getSparkstatus())
            .append("chid", getChid())
            .append("buyers", getBuyers())
            .append("number", getNumber())
            .append("buyertype", getBuyertype())
            .toString();
    }
}
