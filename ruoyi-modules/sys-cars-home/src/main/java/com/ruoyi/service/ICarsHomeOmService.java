package com.ruoyi.service;

import java.util.List;
import com.ruoyi.domain.CarsHomeOm;

/**
 * 车场运维Service接口
 * 
 * @author chn
 * @date 2023-12-01
 */
public interface ICarsHomeOmService 
{
    /**
     * 查询车场运维
     * 
     * @param code 车场运维主键
     * @return 车场运维
     */
    public CarsHomeOm selectCarsHomeOmByCode(Long code);

    /**
     * 查询车场运维列表
     * 
     * @param carsHomeOm 车场运维
     * @return 车场运维集合
     */
    public List<CarsHomeOm> selectCarsHomeOmList(CarsHomeOm carsHomeOm);

    /**
     * 新增车场运维
     * 
     * @param carsHomeOm 车场运维
     * @return 结果
     */
    public int insertCarsHomeOm(CarsHomeOm carsHomeOm);

    /**
     * 修改车场运维
     * 
     * @param carsHomeOm 车场运维
     * @return 结果
     */
    public int updateCarsHomeOm(CarsHomeOm carsHomeOm);

    /**
     * 批量删除车场运维
     * 
     * @param codes 需要删除的车场运维主键集合
     * @return 结果
     */
    public int deleteCarsHomeOmByCodes(Long[] codes);

    /**
     * 删除车场运维信息
     * 
     * @param code 车场运维主键
     * @return 结果
     */
    public int deleteCarsHomeOmByCode(Long code);
}
