import request from '@/utils/request'

// 查询业主信息管理列表
export function listOwner_info(query) {
  return request({
    url: '/sys-cars-info/owner_info/list',
    method: 'get',
    params: query
  })
}

// 查询业主信息管理详细
export function getOwner_info(omid) {
  return request({
    url: '/sys-cars-info/owner_info/' + omid,
    method: 'get'
  })
}

// 新增业主信息管理
export function addOwner_info(data) {
  return request({
    url: '/sys-cars-info/owner_info',
    method: 'post',
    data: data
  })
}

// 修改业主信息管理
export function updateOwner_info(data) {
  return request({
    url: '/sys-cars-info/owner_info',
    method: 'put',
    data: data
  })
}

// 删除业主信息管理
export function delOwner_info(omid) {
  return request({
    url: '/sys-cars-info/owner_info/' + omid,
    method: 'delete'
  })
}
