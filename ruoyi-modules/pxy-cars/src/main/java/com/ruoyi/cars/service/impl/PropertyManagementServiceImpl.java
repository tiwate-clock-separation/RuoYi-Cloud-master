package com.ruoyi.cars.service.impl;

import java.util.List;
import com.ruoyi.common.core.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.cars.mapper.PropertyManagementMapper;
import com.ruoyi.domain.PropertyManagement;
import com.ruoyi.cars.service.IPropertyManagementService;

/**
 * 物业管理Service业务层处理
 *
 * @author ruoyi
 * @date 2023-11-30
 */
@Service
public class PropertyManagementServiceImpl implements IPropertyManagementService
{
    @Autowired
    private PropertyManagementMapper propertyManagementMapper;

    /**
     * 查询物业管理
     *
     * @param pmid 物业管理主键
     * @return 物业管理
     */
    @Override
    public PropertyManagement selectPropertyManagementByPmid(Long pmid)
    {
        return propertyManagementMapper.selectPropertyManagementByPmid(pmid);
    }

    /**
     * 查询物业管理列表
     *
     * @param propertyManagement 物业管理
     * @return 物业管理
     */
    @Override
    public List<PropertyManagement> selectPropertyManagementList(PropertyManagement propertyManagement)
    {
        return propertyManagementMapper.selectPropertyManagementList(propertyManagement);
    }

    /**
     * 新增物业管理
     *
     * @param propertyManagement 物业管理
     * @return 结果
     */
    @Override
    public int insertPropertyManagement(PropertyManagement propertyManagement)
    {
        propertyManagement.setCreateTime(DateUtils.getNowDate());
        return propertyManagementMapper.insertPropertyManagement(propertyManagement);
    }

    /**
     * 修改物业管理
     *
     * @param propertyManagement 物业管理
     * @return 结果
     */
    @Override
    public int updatePropertyManagement(PropertyManagement propertyManagement)
    {
        propertyManagement.setUpdateTime(DateUtils.getNowDate());
        return propertyManagementMapper.updatePropertyManagement(propertyManagement);
    }

    /**
     * 批量删除物业管理
     *
     * @param pmids 需要删除的物业管理主键
     * @return 结果
     */
    @Override
    public int deletePropertyManagementByPmids(Long[] pmids)
    {
        return propertyManagementMapper.deletePropertyManagementByPmids(pmids);
    }

    /**
     * 删除物业管理信息
     *
     * @param pmid 物业管理主键
     * @return 结果
     */
    @Override
    public int deletePropertyManagementByPmid(Long pmid)
    {
        return propertyManagementMapper.deletePropertyManagementByPmid(pmid);
    }
}
