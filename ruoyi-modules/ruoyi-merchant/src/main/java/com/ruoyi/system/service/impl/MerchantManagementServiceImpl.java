package com.ruoyi.system.service.impl;

import java.util.List;

import com.ruoyi.common.core.utils.DateUtils;
import com.ruoyi.domain.CouponBill;
import com.ruoyi.domain.PayOrder;
import com.ruoyi.domain.PayWater;
import com.ruoyi.domain.util.SnowFlake;
import com.ruoyi.system.api.AuthUserApi;
import com.ruoyi.system.api.PropertyApi;
import com.ruoyi.system.api.UpdatePWDApi;
import com.ruoyi.system.mapper.CouponBillMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.MerchantManagementMapper;
import com.ruoyi.domain.MerchantManagement;
import com.ruoyi.system.service.IMerchantManagementService;

/**
 * 商家管理Service业务层处理
 *
 * @author ruoyi
 * @date 2023-12-04
 */
@Service
public class MerchantManagementServiceImpl implements IMerchantManagementService
{
    @Autowired
    private MerchantManagementMapper merchantManagementMapper;

    @Autowired
    private AuthUserApi authUserApi;
    @Autowired
    private UpdatePWDApi updatePWDApi;
    @Autowired
    private PropertyApi propertyApi;
    @Autowired
    private SnowFlake idWorker;
    @Autowired
    private CouponBillMapper couponBillMapper;

    /**
     * 查询商家管理
     *
     * @param pmid 商家管理主键
     * @return 商家管理
     */
    @Override
    public MerchantManagement selectMerchantManagementByPmid(Long pmid)
    {
        return merchantManagementMapper.selectMerchantManagementByPmid(pmid);
    }

    /**
     * 查询商家管理列表
     *
     * @param merchantManagement 商家管理
     * @return 商家管理
     */
    @Override
    public List<MerchantManagement> selectMerchantManagementList(MerchantManagement merchantManagement)
    {
        return merchantManagementMapper.selectMerchantManagementList(merchantManagement);
    }

    /**
     * 新增商家管理
     *
     * @param merchantManagement 商家管理
     * @return 结果
     */
    @Override
    public int insertMerchantManagement(MerchantManagement merchantManagement)
    {
        authUserApi.register(merchantManagement.getUser());

        merchantManagement.setCreateTime(DateUtils.getNowDate());

        return merchantManagementMapper.insertMerchantManagement(merchantManagement);
    }

    /**
     * 修改商家管理
     *
     * @param merchantManagement 商家管理
     * @return 结果
     */
    @Override
    public int updateMerchantManagement(MerchantManagement merchantManagement)
    {
        merchantManagement.setUpdateTime(DateUtils.getNowDate());
        return merchantManagementMapper.updateMerchantManagement(merchantManagement);
    }

    @Override
    public int topup(Integer pmid, Integer topupNum) {
        MerchantManagement merchantManagement = merchantManagementMapper.selectMerchantManagementByPmid(Long.valueOf(pmid));
        merchantManagement.setBalance(merchantManagement.getBalance()+topupNum);
        return topupOrder(merchantManagement,topupNum);
    }


    public synchronized int topupOrder(MerchantManagement merchantManagement,Integer topupNum){
        //生成订单
        long l = idWorker.nextId();
        System.out.println(l);
        long ohid = merchantManagement.getMcid();
        long ttid = 4;
        long orderStatus = 1;
        long ptid = 1;
        PayOrder payOrder = new PayOrder(l,"200",0l,0l,ohid,ttid,null,Long.valueOf(topupNum),orderStatus,ptid);
        propertyApi.add(payOrder);

        //创建一个优惠券账单
        CouponBill couponBill = new CouponBill();
        couponBill.setOrderNum(String.valueOf(l));
        couponBill.setMmid(merchantManagement.getPmid());
        couponBill.setCouponNum(Long.valueOf(topupNum));
        couponBill.setCouponType("充值");

        couponBillMapper.insertCouponBill(couponBill);

        //生成流水
        PayWater payWater = new PayWater();
        payWater.setOrderNum(l);
        payWater.setOrderPrice(topupNum * merchantManagement.getDiscount() /100);
        payWater.setPwStatus(0);
        propertyApi.add(payWater);

        return merchantManagementMapper.updateMerchantManagement(merchantManagement);
    }

    /**
     * 批量删除商家管理
     *
     * @param pmids 需要删除的商家管理主键
     * @return 结果
     */
    @Override
    public int deleteMerchantManagementByPmids(Long[] pmids)
    {
        return merchantManagementMapper.deleteMerchantManagementByPmids(pmids);
    }

    /**
     * 删除商家管理信息
     *
     * @param pmid 商家管理主键
     * @return 结果
     */
    @Override
    public int deleteMerchantManagementByPmid(Long pmid)
    {
        return merchantManagementMapper.deleteMerchantManagementByPmid(pmid);
    }
}
