import request from '@/utils/request'

// 查询车场管理列表
export function listHome(query) {
  return request({
    url: '/pxy-cars/home/list',
    method: 'get',
    params: query
  })
}

// 查询车场管理详细
export function getHome(chid) {
  return request({
    url: '/pxy-cars/home/' + chid,
    method: 'get'
  })
}

// 新增车场管理
export function addHome(data) {
  return request({
    url: '/pxy-cars/home',
    method: 'post',
    data: data
  })
}

// 修改车场管理
export function updateHome(data) {
  return request({
    url: '/pxy-cars/home',
    method: 'put',
    data: data
  })
}

// 删除车场管理
export function delHome(chid) {
  return request({
    url: '/pxy-cars/home/' + chid,
    method: 'delete'
  })
}
