import request from '@/utils/request'

// 查询车牌识别列表
export function listEntify(query) {
  return request({
    url: '/sys-cars-home/entify/list',
    method: 'get',
    params: query
  })
}

// 查询车牌识别详细
export function getEntify(entifyid) {
  return request({
    url: '/sys-cars-home/entify/' + entifyid,
    method: 'get'
  })
}

// 新增车牌识别
export function addEntify(data) {
  return request({
    url: '/sys-cars-home/entify',
    method: 'post',
    data: data
  })
}

// 修改车牌识别
export function updateEntify(data) {
  return request({
    url: '/sys-cars-home/entify',
    method: 'put',
    data: data
  })
}

// 删除车牌识别
export function delEntify(entifyid) {
  return request({
    url: '/sys-cars-home/entify/' + entifyid,
    method: 'delete'
  })
}
