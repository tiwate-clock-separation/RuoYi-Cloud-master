package com.ruoyi.cars.service;

import java.util.List;
import com.ruoyi.domain.CarsHome;

/**
 * 车场管理Service接口
 *
 * @author ruoyi
 * @date 2023-11-30
 */
public interface ICarsHomeService
{
    /**
     * 查询车场管理
     *
     * @param chid 车场管理主键
     * @return 车场管理
     */
    public CarsHome selectCarsHomeByChid(Long chid);

    /**
     * 查询车场管理列表
     *
     * @param carsHome 车场管理
     * @return 车场管理集合
     */
    public List<CarsHome> selectCarsHomeList(CarsHome carsHome);

    /**
     * 新增车场管理
     *
     * @param carsHome 车场管理
     * @return 结果
     */
    public int insertCarsHome(CarsHome carsHome);

    /**
     * 修改车场管理
     *
     * @param carsHome 车场管理
     * @return 结果
     */
    public int updateCarsHome(CarsHome carsHome);

    /**
     * 批量删除车场管理
     *
     * @param chids 需要删除的车场管理主键集合
     * @return 结果
     */
    public int deleteCarsHomeByChids(Long[] chids);

    /**
     * 删除车场管理信息
     *
     * @param chid 车场管理主键
     * @return 结果
     */
    public int deleteCarsHomeByChid(Long chid);
}
