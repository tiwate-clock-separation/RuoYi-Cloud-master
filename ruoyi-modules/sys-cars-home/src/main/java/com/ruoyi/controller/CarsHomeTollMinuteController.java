package com.ruoyi.controller;

import java.util.List;
import java.io.IOException;
import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.log.annotation.Log;
import com.ruoyi.common.log.enums.BusinessType;
import com.ruoyi.common.security.annotation.RequiresPermissions;
import com.ruoyi.domain.CarsHomeTollMinute;
import com.ruoyi.service.ICarsHomeTollMinuteService;
import com.ruoyi.common.core.web.controller.BaseController;
import com.ruoyi.common.core.web.domain.AjaxResult;
import com.ruoyi.common.core.utils.poi.ExcelUtil;
import com.ruoyi.common.core.web.page.TableDataInfo;

/**
 * 按分钟收费Controller
 * 
 * @author chn
 * @date 2023-12-14
 */
@RestController
@RequestMapping("/minute")
public class CarsHomeTollMinuteController extends BaseController
{
    @Autowired
    private ICarsHomeTollMinuteService carsHomeTollMinuteService;

    /**
     * 查询按分钟收费列表
     */
    @RequiresPermissions("sys-cars-home:minute:list")
    @GetMapping("/list")
    public TableDataInfo list(CarsHomeTollMinute carsHomeTollMinute)
    {
        startPage();
        List<CarsHomeTollMinute> list = carsHomeTollMinuteService.selectCarsHomeTollMinuteList(carsHomeTollMinute);
        return getDataTable(list);
    }

    /**
     * 导出按分钟收费列表
     */
    @RequiresPermissions("sys-cars-home:minute:export")
    @Log(title = "按分钟收费", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, CarsHomeTollMinute carsHomeTollMinute)
    {
        List<CarsHomeTollMinute> list = carsHomeTollMinuteService.selectCarsHomeTollMinuteList(carsHomeTollMinute);
        ExcelUtil<CarsHomeTollMinute> util = new ExcelUtil<CarsHomeTollMinute>(CarsHomeTollMinute.class);
        util.exportExcel(response, list, "按分钟收费数据");
    }

    /**
     * 获取按分钟收费详细信息
     */
    @RequiresPermissions("sys-cars-home:minute:query")
    @GetMapping(value = "/{minuteid}")
    public AjaxResult getInfo(@PathVariable("minuteid") Long minuteid)
    {
        return success(carsHomeTollMinuteService.selectCarsHomeTollMinuteByMinuteid(minuteid));
    }

    /**
     * 新增按分钟收费
     */
    @RequiresPermissions("sys-cars-home:minute:add")
    @Log(title = "按分钟收费", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody CarsHomeTollMinute carsHomeTollMinute)
    {
        return toAjax(carsHomeTollMinuteService.insertCarsHomeTollMinute(carsHomeTollMinute));
    }

    /**
     * 修改按分钟收费
     */
    @RequiresPermissions("sys-cars-home:minute:edit")
    @Log(title = "按分钟收费", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody CarsHomeTollMinute carsHomeTollMinute)
    {
        return toAjax(carsHomeTollMinuteService.updateCarsHomeTollMinute(carsHomeTollMinute));
    }

    /**
     * 删除按分钟收费
     */
    @RequiresPermissions("sys-cars-home:minute:remove")
    @Log(title = "按分钟收费", businessType = BusinessType.DELETE)
	@DeleteMapping("/{minuteids}")
    public AjaxResult remove(@PathVariable Long[] minuteids)
    {
        return toAjax(carsHomeTollMinuteService.deleteCarsHomeTollMinuteByMinuteids(minuteids));
    }
}
