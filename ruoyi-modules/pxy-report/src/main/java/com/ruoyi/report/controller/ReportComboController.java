package com.ruoyi.report.controller;

import java.util.List;
import java.io.IOException;
import javax.servlet.http.HttpServletResponse;

import com.ruoyi.domain.CarsMsg;
import com.ruoyi.domain.PayOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.log.annotation.Log;
import com.ruoyi.common.log.enums.BusinessType;
import com.ruoyi.common.security.annotation.RequiresPermissions;
import com.ruoyi.domain.ReportCombo;
import com.ruoyi.report.service.IReportComboService;
import com.ruoyi.common.core.web.controller.BaseController;
import com.ruoyi.common.core.web.domain.AjaxResult;
import com.ruoyi.common.core.utils.poi.ExcelUtil;
import com.ruoyi.common.core.web.page.TableDataInfo;

/**
 * 卡套餐报Controller
 * 
 * @author ruoyi
 * @date 2023-12-08
 */
@RestController
@RequestMapping("/combo")
public class ReportComboController extends BaseController
{
    @Autowired
    private IReportComboService reportComboService;

    /**
     * 查询卡套餐报列表
     */
    @RequiresPermissions("pxy-report:combo:list")
    @GetMapping("/list")
    public TableDataInfo list(ReportCombo reportCombo, CarsMsg carsMsg, PayOrder payOrder)
    {
        reportCombo.setCarsMsg(carsMsg);
        reportCombo.setPayOrder(payOrder);
        startPage();
        List<ReportCombo> list = reportComboService.selectReportComboList(reportCombo);
        return getDataTable(list);
    }

    /**
     * 导出卡套餐报列表
     */
    @RequiresPermissions("pxy-report:combo:export")
    @Log(title = "卡套餐报", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, ReportCombo reportCombo)
    {
        List<ReportCombo> list = reportComboService.selectReportComboList(reportCombo);
        ExcelUtil<ReportCombo> util = new ExcelUtil<ReportCombo>(ReportCombo.class);
        util.exportExcel(response, list, "卡套餐报数据");
    }

    /**
     * 获取卡套餐报详细信息
     */
    @RequiresPermissions("pxy-report:combo:query")
    @GetMapping(value = "/{comid}")
    public AjaxResult getInfo(@PathVariable("comid") Long comid)
    {
        return success(reportComboService.selectReportComboByComid(comid));
    }

    /**
     * 新增卡套餐报
     */
    @RequiresPermissions("pxy-report:combo:add")
    @Log(title = "卡套餐报", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody ReportCombo reportCombo)
    {
        return toAjax(reportComboService.insertReportCombo(reportCombo));
    }

    /**
     * 修改卡套餐报
     */
    @RequiresPermissions("pxy-report:combo:edit")
    @Log(title = "卡套餐报", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody ReportCombo reportCombo)
    {
        return toAjax(reportComboService.updateReportCombo(reportCombo));
    }

    /**
     * 删除卡套餐报
     */
    @RequiresPermissions("pxy-report:combo:remove")
    @Log(title = "卡套餐报", businessType = BusinessType.DELETE)
	@DeleteMapping("/{comids}")
    public AjaxResult remove(@PathVariable Long[] comids)
    {
        return toAjax(reportComboService.deleteReportComboByComids(comids));
    }
}
