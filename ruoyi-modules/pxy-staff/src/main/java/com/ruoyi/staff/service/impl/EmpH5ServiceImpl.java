package com.ruoyi.staff.service.impl;

import java.util.List;
import com.ruoyi.common.core.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.staff.mapper.EmpH5Mapper;
import com.ruoyi.domain.EmpH5;
import com.ruoyi.staff.service.IEmpH5Service;

/**
 * H5人员Service业务层处理
 * 
 * @author ruoyi
 * @date 2023-12-15
 */
@Service
public class EmpH5ServiceImpl implements IEmpH5Service 
{
    @Autowired
    private EmpH5Mapper empH5Mapper;

    /**
     * 查询H5人员
     * 
     * @param ehid H5人员主键
     * @return H5人员
     */
    @Override
    public EmpH5 selectEmpH5ByEhid(Long ehid)
    {
        return empH5Mapper.selectEmpH5ByEhid(ehid);
    }

    /**
     * 查询H5人员列表
     * 
     * @param empH5 H5人员
     * @return H5人员
     */
    @Override
    public List<EmpH5> selectEmpH5List(EmpH5 empH5)
    {
        return empH5Mapper.selectEmpH5List(empH5);
    }

    /**
     * 新增H5人员
     * 
     * @param empH5 H5人员
     * @return 结果
     */
    @Override
    public int insertEmpH5(EmpH5 empH5)
    {
        empH5.setCreateTime(DateUtils.getNowDate());
        return empH5Mapper.insertEmpH5(empH5);
    }

    /**
     * 修改H5人员
     * 
     * @param empH5 H5人员
     * @return 结果
     */
    @Override
    public int updateEmpH5(EmpH5 empH5)
    {
        empH5.setUpdateTime(DateUtils.getNowDate());
        return empH5Mapper.updateEmpH5(empH5);
    }

    /**
     * 批量删除H5人员
     * 
     * @param ehids 需要删除的H5人员主键
     * @return 结果
     */
    @Override
    public int deleteEmpH5ByEhids(Long[] ehids)
    {
        return empH5Mapper.deleteEmpH5ByEhids(ehids);
    }

    /**
     * 删除H5人员信息
     * 
     * @param ehid H5人员主键
     * @return 结果
     */
    @Override
    public int deleteEmpH5ByEhid(Long ehid)
    {
        return empH5Mapper.deleteEmpH5ByEhid(ehid);
    }
}
