package com.ruoyi.system.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;

import com.ruoyi.system.api.AuthUserApi;
import com.ruoyi.system.api.CarsHomeApi;
import com.ruoyi.system.api.PropertyApi;
import com.ruoyi.domain.CarsHome;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.log.annotation.Log;
import com.ruoyi.common.log.enums.BusinessType;
import com.ruoyi.common.security.annotation.RequiresPermissions;
import com.ruoyi.domain.MerchantManagement;
import com.ruoyi.system.service.IMerchantManagementService;
import com.ruoyi.common.core.web.controller.BaseController;
import com.ruoyi.common.core.web.domain.AjaxResult;
import com.ruoyi.common.core.utils.poi.ExcelUtil;
import com.ruoyi.common.core.web.page.TableDataInfo;

/**
 * 商家管理Controller
 *
 * @author ruoyi
 * @date 2023-12-04
 */
@RestController
@RequestMapping("/management")
public class MerchantManagementController extends BaseController
{
    @Autowired
    private IMerchantManagementService merchantManagementService;

    @Autowired
    private CarsHomeApi carsHomeApi;

    @Autowired
    private PropertyApi propertyApi;

    /**
     * 查询商家管理列表
     */
    @RequiresPermissions("system:management:list")
    @GetMapping("/list")
    public TableDataInfo list(MerchantManagement merchantManagement)
    {
        startPage();
        List<MerchantManagement> list = merchantManagementService.selectMerchantManagementList(merchantManagement);
        return getDataTable(list);
    }

    /**
     * 导出商家管理列表
     */
    @RequiresPermissions("system:management:export")
    @Log(title = "商家管理", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, MerchantManagement merchantManagement)
    {
        List<MerchantManagement> list = merchantManagementService.selectMerchantManagementList(merchantManagement);
        ExcelUtil<MerchantManagement> util = new ExcelUtil<MerchantManagement>(MerchantManagement.class);
        util.exportExcel(response, list, "商家管理数据");
    }

    /**
     * 获取商家管理详细信息
     */
    @RequiresPermissions("system:management:query")
    @GetMapping(value = "/{pmid}")
    public AjaxResult getInfo(@PathVariable("pmid") Long pmid)
    {
        return success(merchantManagementService.selectMerchantManagementByPmid(pmid));
    }

    /**
     * 新增商家管理
     */
    @RequiresPermissions("system:management:add")
    @Log(title = "商家管理", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody MerchantManagement merchantManagement)
    {
        return toAjax(merchantManagementService.insertMerchantManagement(merchantManagement));
    }

    /**
     * 修改商家管理
     */
    @RequiresPermissions("system:management:edit")
    @Log(title = "商家管理", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody MerchantManagement merchantManagement)
    {
        return toAjax(merchantManagementService.updateMerchantManagement(merchantManagement));
    }

//    @RequiresPermissions("system:management:edit")
    @Log(title = "充值", businessType = BusinessType.UPDATE)
    @PutMapping("topup/{pmid}/{topupNum}")
    public AjaxResult topup(@PathVariable Integer pmid,@PathVariable Integer topupNum)
    {
        return toAjax(merchantManagementService.topup(pmid, topupNum));
    }

    /**
     * 删除商家管理
     */
    @RequiresPermissions("system:management:remove")
    @Log(title = "商家管理", businessType = BusinessType.DELETE)
	@DeleteMapping("/{pmids}")
    public AjaxResult remove(@PathVariable Long[] pmids)
    {
        return toAjax(merchantManagementService.deleteMerchantManagementByPmids(pmids));
    }

    @RequiresPermissions("system:management:list")
    @GetMapping("carshomelist")
    public TableDataInfo carshomelist(CarsHome carsHome){
        return carsHomeApi.list(carsHome);
    }

    @RequiresPermissions("system:management:list")
    @GetMapping("propertylist")
    public TableDataInfo propertylist(){
        return propertyApi.list();
    }
}
