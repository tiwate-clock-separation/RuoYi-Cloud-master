import request from '@/utils/request'

// 查询车场数据列表 (下拉框)
export function listCars(query) {
  return request({
    url: '/ruoyi-audit/test',
    method: 'get',
    params: query
  })
}
// 查询物业数据列表 (下拉框)
export function listProperty(query) {
  return request({
    url: '/ruoyi-audit/test2',
    method: 'get',
    params: query
  })
}
