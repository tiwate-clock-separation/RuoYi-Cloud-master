package com.ruoyi.report.mapper;

import java.util.List;
import com.ruoyi.domain.ReportException;

/**
 * 异常报表Mapper接口
 * 
 * @author ruoyi
 * @date 2023-12-08
 */
public interface ReportExceptionMapper 
{
    /**
     * 查询异常报表
     * 
     * @param exceid 异常报表主键
     * @return 异常报表
     */
    public ReportException selectReportExceptionByExceid(Long exceid);

    /**
     * 查询异常报表列表
     * 
     * @param reportException 异常报表
     * @return 异常报表集合
     */
    public List<ReportException> selectReportExceptionList(ReportException reportException);

    /**
     * 新增异常报表
     * 
     * @param reportException 异常报表
     * @return 结果
     */
    public int insertReportException(ReportException reportException);

    /**
     * 修改异常报表
     * 
     * @param reportException 异常报表
     * @return 结果
     */
    public int updateReportException(ReportException reportException);

    /**
     * 删除异常报表
     * 
     * @param exceid 异常报表主键
     * @return 结果
     */
    public int deleteReportExceptionByExceid(Long exceid);

    /**
     * 批量删除异常报表
     * 
     * @param exceids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteReportExceptionByExceids(Long[] exceids);
}
