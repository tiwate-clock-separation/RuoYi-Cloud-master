package com.ruoyi.staff.service.impl;

import java.util.List;
import com.ruoyi.common.core.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.staff.mapper.EmpWatchhouseMapper;
import com.ruoyi.domain.EmpWatchhouse;
import com.ruoyi.staff.service.IEmpWatchhouseService;

/**
 * 岗亭人员Service业务层处理
 * 
 * @author pxy
 * @date 2023-12-23
 */
@Service
public class EmpWatchhouseServiceImpl implements IEmpWatchhouseService 
{
    @Autowired
    private EmpWatchhouseMapper empWatchhouseMapper;

    /**
     * 查询岗亭人员
     * 
     * @param watchid 岗亭人员主键
     * @return 岗亭人员
     */
    @Override
    public EmpWatchhouse selectEmpWatchhouseByWatchid(Long watchid)
    {
        return empWatchhouseMapper.selectEmpWatchhouseByWatchid(watchid);
    }

    /**
     * 查询岗亭人员列表
     * 
     * @param empWatchhouse 岗亭人员
     * @return 岗亭人员
     */
    @Override
    public List<EmpWatchhouse> selectEmpWatchhouseList(EmpWatchhouse empWatchhouse)
    {
        return empWatchhouseMapper.selectEmpWatchhouseList(empWatchhouse);
    }

    /**
     * 新增岗亭人员
     * 
     * @param empWatchhouse 岗亭人员
     * @return 结果
     */
    @Override
    public int insertEmpWatchhouse(EmpWatchhouse empWatchhouse)
    {
        empWatchhouse.setCreateTime(DateUtils.getNowDate());
        return empWatchhouseMapper.insertEmpWatchhouse(empWatchhouse);
    }

    /**
     * 修改岗亭人员
     * 
     * @param empWatchhouse 岗亭人员
     * @return 结果
     */
    @Override
    public int updateEmpWatchhouse(EmpWatchhouse empWatchhouse)
    {
        empWatchhouse.setUpdateTime(DateUtils.getNowDate());
        return empWatchhouseMapper.updateEmpWatchhouse(empWatchhouse);
    }

    /**
     * 批量删除岗亭人员
     * 
     * @param watchids 需要删除的岗亭人员主键
     * @return 结果
     */
    @Override
    public int deleteEmpWatchhouseByWatchids(Long[] watchids)
    {
        return empWatchhouseMapper.deleteEmpWatchhouseByWatchids(watchids);
    }

    /**
     * 删除岗亭人员信息
     * 
     * @param watchid 岗亭人员主键
     * @return 结果
     */
    @Override
    public int deleteEmpWatchhouseByWatchid(Long watchid)
    {
        return empWatchhouseMapper.deleteEmpWatchhouseByWatchid(watchid);
    }
}
