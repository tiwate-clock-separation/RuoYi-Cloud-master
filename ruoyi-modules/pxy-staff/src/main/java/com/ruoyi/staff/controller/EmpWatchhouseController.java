package com.ruoyi.staff.controller;

import java.util.List;
import java.io.IOException;
import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.log.annotation.Log;
import com.ruoyi.common.log.enums.BusinessType;
import com.ruoyi.common.security.annotation.RequiresPermissions;
import com.ruoyi.domain.EmpWatchhouse;
import com.ruoyi.staff.service.IEmpWatchhouseService;
import com.ruoyi.common.core.web.controller.BaseController;
import com.ruoyi.common.core.web.domain.AjaxResult;
import com.ruoyi.common.core.utils.poi.ExcelUtil;
import com.ruoyi.common.core.web.page.TableDataInfo;

/**
 * 岗亭人员Controller
 * 
 * @author pxy
 * @date 2023-12-23
 */
@RestController
@RequestMapping("/watchhouse")
public class EmpWatchhouseController extends BaseController
{
    @Autowired
    private IEmpWatchhouseService empWatchhouseService;

    /**
     * 查询岗亭人员列表
     */
    @RequiresPermissions("pxy-staff:watchhouse:list")
    @GetMapping("/list")
    public TableDataInfo list(EmpWatchhouse empWatchhouse)
    {
        startPage();
        List<EmpWatchhouse> list = empWatchhouseService.selectEmpWatchhouseList(empWatchhouse);
        return getDataTable(list);
    }

    /**
     * 导出岗亭人员列表
     */
    @RequiresPermissions("pxy-staff:watchhouse:export")
    @Log(title = "岗亭人员", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, EmpWatchhouse empWatchhouse)
    {
        List<EmpWatchhouse> list = empWatchhouseService.selectEmpWatchhouseList(empWatchhouse);
        ExcelUtil<EmpWatchhouse> util = new ExcelUtil<EmpWatchhouse>(EmpWatchhouse.class);
        util.exportExcel(response, list, "岗亭人员数据");
    }

    /**
     * 获取岗亭人员详细信息
     */
    @RequiresPermissions("pxy-staff:watchhouse:query")
    @GetMapping(value = "/{watchid}")
    public AjaxResult getInfo(@PathVariable("watchid") Long watchid)
    {
        return success(empWatchhouseService.selectEmpWatchhouseByWatchid(watchid));
    }

    /**
     * 新增岗亭人员
     */
    @RequiresPermissions("pxy-staff:watchhouse:add")
    @Log(title = "岗亭人员", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody EmpWatchhouse empWatchhouse)
    {
        return toAjax(empWatchhouseService.insertEmpWatchhouse(empWatchhouse));
    }

    /**
     * 修改岗亭人员
     */
    @RequiresPermissions("pxy-staff:watchhouse:edit")
    @Log(title = "岗亭人员", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody EmpWatchhouse empWatchhouse)
    {
        return toAjax(empWatchhouseService.updateEmpWatchhouse(empWatchhouse));
    }

    /**
     * 删除岗亭人员
     */
    @RequiresPermissions("pxy-staff:watchhouse:remove")
    @Log(title = "岗亭人员", businessType = BusinessType.DELETE)
	@DeleteMapping("/{watchids}")
    public AjaxResult remove(@PathVariable Long[] watchids)
    {
        return toAjax(empWatchhouseService.deleteEmpWatchhouseByWatchids(watchids));
    }
}
