import request from '@/utils/request'

// 查询车场人员列表
export function listCar(query) {
  return request({
    url: '/pxy-staff/car/list',
    method: 'get',
    params: query
  })
}

// 查询车场人员详细
export function getCar(carid) {
  return request({
    url: '/pxy-staff/car/' + carid,
    method: 'get'
  })
}

// 新增车场人员
export function addCar(data) {
  return request({
    url: '/pxy-staff/car',
    method: 'post',
    data: data
  })
}

// 修改车场人员
export function updateCar(data) {
  return request({
    url: '/pxy-staff/car',
    method: 'put',
    data: data
  })
}

// 删除车场人员
export function delCar(carid) {
  return request({
    url: '/pxy-staff/car/' + carid,
    method: 'delete'
  })
}
