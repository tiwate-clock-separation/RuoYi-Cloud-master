package com.ruoyi.cars.mapper.result;

import com.ruoyi.domain.PropertyManagement;
import org.apache.ibatis.annotations.Select;

public interface properManagementResult {
    @Select("select * from property_management where pmid = ${pmid}")
    PropertyManagement getPeoperty();
}
