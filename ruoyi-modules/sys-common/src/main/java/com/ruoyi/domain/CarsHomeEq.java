package com.ruoyi.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.core.annotation.Excel;
import com.ruoyi.common.core.web.domain.BaseEntity;

/**
 * 车场设备对象 cars_home_eq
 *
 * @author ruoyi
 * @date 2023-11-30
 */
public class CarsHomeEq extends BaseEntity {
    private static final long serialVersionUID = 1L;

    /**
     * 车场设备id
     */
    private Long eqid;

    /**
     * 唯一编码,前台使用
     */
    @Excel(name = "唯一编码,前台使用")
    private String code;

    /**
     * 状态(上线下线）
     */
    @Excel(name = "状态(上线下线）")
    private Long status;

    /**
     * 是否删除
     */
    @Excel(name = "是否删除")
    private Long deleted;

    /**
     * 车道id
     */
    @Excel(name = "车道id")
    private Long chlid;

    /**
     * 设备名称
     */
    @Excel(name = "设备名称")
    private String eqname;

    /**
     * 设备IP
     */
    @Excel(name = "设备IP")
    private String eqip;

    /**
     * 开闸关闸
     */
    @Excel(name = "开闸关闸")
    private Integer operate;


    private CarsHome carsHome;
    private CarsHomeLane carsHomeLane;
    private CarsHomeBooth carsHomeBooth;
    private PropertyManagement propertyManagement;

    public CarsHomeEq() {
    }

    public CarsHomeEq(Long eqid, String code, Long status, Long deleted, Long chlid, String eqname, String eqip, Integer operate, CarsHome carsHome, CarsHomeLane carsHomeLane, CarsHomeBooth carsHomeBooth, PropertyManagement propertyManagement) {
        this.eqid = eqid;
        this.code = code;
        this.status = status;
        this.deleted = deleted;
        this.chlid = chlid;
        this.eqname = eqname;
        this.eqip = eqip;
        this.operate = operate;
        this.carsHome = carsHome;
        this.carsHomeLane = carsHomeLane;
        this.carsHomeBooth = carsHomeBooth;
        this.propertyManagement = propertyManagement;
    }

    @Override
    public String toString() {
        return "CarsHomeEq{" +
                "eqid=" + eqid +
                ", code='" + code + '\'' +
                ", status=" + status +
                ", deleted=" + deleted +
                ", chlid=" + chlid +
                ", eqname='" + eqname + '\'' +
                ", eqip='" + eqip + '\'' +
                ", operate=" + operate +
                ", carsHome=" + carsHome +
                ", carsHomeLane=" + carsHomeLane +
                ", carsHomeBooth=" + carsHomeBooth +
                ", propertyManagement=" + propertyManagement +
                "} " + super.toString();
    }

    public Long getEqid() {
        return eqid;
    }

    public void setEqid(Long eqid) {
        this.eqid = eqid;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Long getStatus() {
        return status;
    }

    public void setStatus(Long status) {
        this.status = status;
    }

    public Long getDeleted() {
        return deleted;
    }

    public void setDeleted(Long deleted) {
        this.deleted = deleted;
    }

    public Long getChlid() {
        return chlid;
    }

    public void setChlid(Long chlid) {
        this.chlid = chlid;
    }

    public String getEqname() {
        return eqname;
    }

    public void setEqname(String eqname) {
        this.eqname = eqname;
    }

    public String getEqip() {
        return eqip;
    }

    public void setEqip(String eqip) {
        this.eqip = eqip;
    }

    public Integer getOperate() {
        return operate;
    }

    public void setOperate(Integer operate) {
        this.operate = operate;
    }

    public CarsHome getCarsHome() {
        return carsHome;
    }

    public void setCarsHome(CarsHome carsHome) {
        this.carsHome = carsHome;
    }

    public CarsHomeLane getCarsHomeLane() {
        return carsHomeLane;
    }

    public void setCarsHomeLane(CarsHomeLane carsHomeLane) {
        this.carsHomeLane = carsHomeLane;
    }

    public CarsHomeBooth getCarsHomeBooth() {
        return carsHomeBooth;
    }

    public void setCarsHomeBooth(CarsHomeBooth carsHomeBooth) {
        this.carsHomeBooth = carsHomeBooth;
    }

    public PropertyManagement getPropertyManagement() {
        return propertyManagement;
    }

    public void setPropertyManagement(PropertyManagement propertyManagement) {
        this.propertyManagement = propertyManagement;
    }
}
