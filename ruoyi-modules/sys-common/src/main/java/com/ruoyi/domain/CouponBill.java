package com.ruoyi.domain;

import com.ruoyi.common.core.annotation.Excel;
import com.ruoyi.common.core.web.domain.BaseEntity;

/**
 * 优惠卷账单对象 coupon_bill
 *
 * @author ruoyi
 * @date 2023-12-26
 */
public class CouponBill extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 自增ID */
    private Long id;

    /** 唯一编码,前台使用 */
    @Excel(name = "唯一编码,前台使用")
    private String code;

    /** 状态 */
    @Excel(name = "状态")
    private Long status;

    /** 是否删除 */
    @Excel(name = "是否删除")
    private Long deleted;

    /** 优惠券 */
    private Long cmid;

    /** 商家id */
    private Long mmid;

    /** 实际优惠金额 */
    @Excel(name = "实际优惠金额")
    private Long couponNum;

    /** 订单编号 */
    @Excel(name = "订单编号")
    private String orderNum;

    /** 账单类型 */
    @Excel(name = "账单类型")
    private String couponType;

    /** 备注 */
    @Excel(name = "备注")
    private String couponRemark;

    /** 车牌号 */
    @Excel(name = "车牌号")
    private String licenseNumber;

    /** 支付时间 */
    @Excel(name = "支付时间")
    private String payTime;

    /** 优惠券名称 */
    @Excel(name = "优惠券名称")
    private String couponName;

    /** 商家名称 */
    @Excel(name = "商家名称")
    private String merchantName;

    /** 支付类型 */
    @Excel(name = "支付类型")
    private String typeName;

    /** 创建时间 */
    @Excel(name = "创建时间")
    private String poTime;

    public void setId(Long id)
    {
        this.id = id;
    }

    public Long getId()
    {
        return id;
    }
    public void setCode(String code)
    {
        this.code = code;
    }

    public String getCode()
    {
        return code;
    }
    public void setStatus(Long status)
    {
        this.status = status;
    }

    public Long getStatus()
    {
        return status;
    }
    public void setDeleted(Long deleted)
    {
        this.deleted = deleted;
    }

    public Long getDeleted()
    {
        return deleted;
    }
    public void setCmid(Long cmid)
    {
        this.cmid = cmid;
    }

    public Long getCmid()
    {
        return cmid;
    }
    public void setCouponNum(Long couponNum)
    {
        this.couponNum = couponNum;
    }

    public Long getCouponNum()
    {
        return couponNum;
    }
    public void setOrderNum(String orderNum)
    {
        this.orderNum = orderNum;
    }

    public String getOrderNum()
    {
        return orderNum;
    }
    public void setCouponType(String couponType)
    {
        this.couponType = couponType;
    }

    public String getCouponType()
    {
        return couponType;
    }
    public void setCouponRemark(String couponRemark)
    {
        this.couponRemark = couponRemark;
    }

    public String getCouponRemark()
    {
        return couponRemark;
    }

    public String getLicenseNumber() {
        return licenseNumber;
    }

    public void setLicenseNumber(String licenseNumber) {
        this.licenseNumber = licenseNumber;
    }

    public String getPayTime() {
        return payTime;
    }

    public void setPayTime(String payTime) {
        this.payTime = payTime;
    }

    public String getCouponName() {
        return couponName;
    }

    public void setCouponName(String couponName) {
        this.couponName = couponName;
    }

    public String getMerchantName() {
        return merchantName;
    }

    public void setMerchantName(String merchantName) {
        this.merchantName = merchantName;
    }

    public String getTypeName() {
        return typeName;
    }

    public void setTypeName(String typeName) {
        this.typeName = typeName;
    }

    public String getPoTime() {
        return poTime;
    }

    public void setPoTime(String poTime) {
        this.poTime = poTime;
    }

    public Long getMmid() {
        return mmid;
    }

    public void setMmid(Long mmid) {
        this.mmid = mmid;
    }

    @Override
    public String toString() {
        return "CouponBill{" +
                "id=" + id +
                ", code='" + code + '\'' +
                ", status=" + status +
                ", deleted=" + deleted +
                ", cmid=" + cmid +
                ", mmid=" + mmid +
                ", couponNum=" + couponNum +
                ", orderNum='" + orderNum + '\'' +
                ", couponType='" + couponType + '\'' +
                ", couponRemark='" + couponRemark + '\'' +
                ", licenseNumber='" + licenseNumber + '\'' +
                ", payTime='" + payTime + '\'' +
                ", couponName='" + couponName + '\'' +
                ", merchantName='" + merchantName + '\'' +
                ", typeName='" + typeName + '\'' +
                ", poTime='" + poTime + '\'' +
                '}';
    }
}
