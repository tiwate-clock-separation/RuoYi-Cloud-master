package com.ruoyi.service;

import com.ruoyi.domain.OwnerMsg;

import java.util.List;

/**
 * 业主信息管理Service接口
 *
 * @author ruoyi
 * @date 2023-12-07
 */
public interface IOwnerMsgService
{
    /**
     * 查询业主信息管理
     *
     * @param omid 业主信息管理主键
     * @return 业主信息管理
     */
    public OwnerMsg selectOwnerMsgByOmid(Long omid);

    /**
     * 查询业主信息管理列表
     *
     * @param ownerMsg 业主信息管理
     * @return 业主信息管理集合
     */
    public List<OwnerMsg> selectOwnerMsgList(OwnerMsg ownerMsg);

    /**
     * 新增业主信息管理
     *
     * @param ownerMsg 业主信息管理
     * @return 结果
     */
    public int insertOwnerMsg(OwnerMsg ownerMsg);

    /**
     * 修改业主信息管理
     *
     * @param ownerMsg 业主信息管理
     * @return 结果
     */
    public int updateOwnerMsg(OwnerMsg ownerMsg);

    /**
     * 批量删除业主信息管理
     *
     * @param omids 需要删除的业主信息管理主键集合
     * @return 结果
     */
    public int deleteOwnerMsgByOmids(Long[] omids);

    /**
     * 删除业主信息管理信息
     *
     * @param omid 业主信息管理主键
     * @return 结果
     */
    public int deleteOwnerMsgByOmid(Long omid);
}
