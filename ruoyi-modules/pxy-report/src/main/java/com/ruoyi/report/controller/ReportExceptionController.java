package com.ruoyi.report.controller;

import java.util.List;
import java.io.IOException;
import javax.servlet.http.HttpServletResponse;

import com.ruoyi.domain.CarsMsg;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.log.annotation.Log;
import com.ruoyi.common.log.enums.BusinessType;
import com.ruoyi.common.security.annotation.RequiresPermissions;
import com.ruoyi.domain.ReportException;
import com.ruoyi.report.service.IReportExceptionService;
import com.ruoyi.common.core.web.controller.BaseController;
import com.ruoyi.common.core.web.domain.AjaxResult;
import com.ruoyi.common.core.utils.poi.ExcelUtil;
import com.ruoyi.common.core.web.page.TableDataInfo;

/**
 * 异常报表Controller
 * 
 * @author ruoyi
 * @date 2023-12-08
 */
@RestController
@RequestMapping("/exception")
public class ReportExceptionController extends BaseController
{
    @Autowired
    private IReportExceptionService reportExceptionService;

    /**
     * 查询异常报表列表
     */
    @RequiresPermissions("pxy-report:exception:list")
    @GetMapping("/list")
    public TableDataInfo list(ReportException reportException, CarsMsg carsMsg)
    {
        reportException.setCarsMsg(carsMsg);
        startPage();
        List<ReportException> list = reportExceptionService.selectReportExceptionList(reportException);
        return getDataTable(list);
    }

    /**
     * 导出异常报表列表
     */
    @RequiresPermissions("pxy-report:exception:export")
    @Log(title = "异常报表", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, ReportException reportException)
    {
        List<ReportException> list = reportExceptionService.selectReportExceptionList(reportException);
        ExcelUtil<ReportException> util = new ExcelUtil<ReportException>(ReportException.class);
        util.exportExcel(response, list, "异常报表数据");
    }

    /**
     * 获取异常报表详细信息
     */
    @RequiresPermissions("pxy-report:exception:query")
    @GetMapping(value = "/{exceid}")
    public AjaxResult getInfo(@PathVariable("exceid") Long exceid)
    {
        return success(reportExceptionService.selectReportExceptionByExceid(exceid));
    }

    /**
     * 新增异常报表
     */
    @RequiresPermissions("pxy-report:exception:add")
    @Log(title = "异常报表", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody ReportException reportException)
    {
        return toAjax(reportExceptionService.insertReportException(reportException));
    }

    /**
     * 修改异常报表
     */
    @RequiresPermissions("pxy-report:exception:edit")
    @Log(title = "异常报表", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody ReportException reportException)
    {
        return toAjax(reportExceptionService.updateReportException(reportException));
    }

    /**
     * 删除异常报表
     */
    @RequiresPermissions("pxy-report:exception:remove")
    @Log(title = "异常报表", businessType = BusinessType.DELETE)
	@DeleteMapping("/{exceids}")
    public AjaxResult remove(@PathVariable Long[] exceids)
    {
        return toAjax(reportExceptionService.deleteReportExceptionByExceids(exceids));
    }
}
