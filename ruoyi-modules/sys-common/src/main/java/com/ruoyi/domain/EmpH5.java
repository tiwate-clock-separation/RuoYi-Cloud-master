package com.ruoyi.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.core.annotation.Excel;
import com.ruoyi.common.core.web.domain.BaseEntity;

/**
 * H5人员对象 emp_h5
 * 
 * @author ruoyi
 * @date 2023-12-15
 */
public class EmpH5 extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 自增ID */
    private Long ehid;

    /** 唯一编码,前台使用 */
    private String code;

    /** 状态 */
    private Long status;

    /** 是否删除 */
    private Long deleted;

    /** h5员工手机号 */
    @Excel(name = "h5员工手机号")
    private String h5Tel;

    /** 用户类型 */
    @Excel(name = "用户类型")
    private Long serviceType;

    /** openId */
    @Excel(name = "openId")
    private String openId;

    /** aliOpenId */
    @Excel(name = "aliOpenId")
    private String aliOpenId;

    public void setEhid(Long ehid) 
    {
        this.ehid = ehid;
    }

    public Long getEhid() 
    {
        return ehid;
    }
    public void setCode(String code) 
    {
        this.code = code;
    }

    public String getCode() 
    {
        return code;
    }
    public void setStatus(Long status) 
    {
        this.status = status;
    }

    public Long getStatus() 
    {
        return status;
    }
    public void setDeleted(Long deleted) 
    {
        this.deleted = deleted;
    }

    public Long getDeleted() 
    {
        return deleted;
    }
    public void setH5Tel(String h5Tel) 
    {
        this.h5Tel = h5Tel;
    }

    public String getH5Tel() 
    {
        return h5Tel;
    }
    public void setServiceType(Long serviceType) 
    {
        this.serviceType = serviceType;
    }

    public Long getServiceType() 
    {
        return serviceType;
    }
    public void setOpenId(String openId) 
    {
        this.openId = openId;
    }

    public String getOpenId() 
    {
        return openId;
    }
    public void setAliOpenId(String aliOpenId) 
    {
        this.aliOpenId = aliOpenId;
    }

    public String getAliOpenId() 
    {
        return aliOpenId;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("ehid", getEhid())
            .append("code", getCode())
            .append("status", getStatus())
            .append("deleted", getDeleted())
            .append("createTime", getCreateTime())
            .append("updateTime", getUpdateTime())
            .append("h5Tel", getH5Tel())
            .append("serviceType", getServiceType())
            .append("openId", getOpenId())
            .append("aliOpenId", getAliOpenId())
            .toString();
    }
}
