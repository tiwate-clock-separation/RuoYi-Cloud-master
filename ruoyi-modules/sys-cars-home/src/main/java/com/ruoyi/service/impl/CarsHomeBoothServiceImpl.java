package com.ruoyi.service.impl;

import java.util.List;

import com.ruoyi.domain.CarsHomeBooth;
import com.ruoyi.common.core.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.mapper.CarsHomeBoothMapper;
import com.ruoyi.service.ICarsHomeBoothService;

/**
 * 车场岗亭Service业务层处理
 * 
 * @author chn
 * @date 2023-12-04
 */
@Service
public class CarsHomeBoothServiceImpl implements ICarsHomeBoothService 
{
    @Autowired
    private CarsHomeBoothMapper carsHomeBoothMapper;

    /**
     * 查询车场岗亭
     * 
     * @param chbid 车场岗亭主键
     * @return 车场岗亭
     */
    @Override
    public CarsHomeBooth selectCarsHomeBoothByChbid(Long chbid)
    {
        return carsHomeBoothMapper.selectCarsHomeBoothByChbid(chbid);
    }

    @Override
    public List<CarsHomeBooth> getBoothLaneList(Long chid) {
        return carsHomeBoothMapper.getBoothLaneList(chid);
    }

    /**
     * 查询车场岗亭列表
     * 
     * @param carsHomeBooth 车场岗亭
     * @return 车场岗亭
     */
    @Override
    public List<CarsHomeBooth> selectCarsHomeBoothList(CarsHomeBooth carsHomeBooth)
    {
        return carsHomeBoothMapper.selectCarsHomeBoothList(carsHomeBooth);
    }

    /**
     * 新增车场岗亭
     * 
     * @param carsHomeBooth 车场岗亭
     * @return 结果
     */
    @Override
    public int insertCarsHomeBooth(CarsHomeBooth carsHomeBooth)
    {
        carsHomeBooth.setCreateTime(DateUtils.getNowDate());
        return carsHomeBoothMapper.insertCarsHomeBooth(carsHomeBooth);
    }

    /**
     * 修改车场岗亭
     * 
     * @param carsHomeBooth 车场岗亭
     * @return 结果
     */
    @Override
    public int updateCarsHomeBooth(CarsHomeBooth carsHomeBooth)
    {
        carsHomeBooth.setUpdateTime(DateUtils.getNowDate());
        return carsHomeBoothMapper.updateCarsHomeBooth(carsHomeBooth);
    }

    /**
     * 批量删除车场岗亭
     * 
     * @param chbids 需要删除的车场岗亭主键
     * @return 结果
     */
    @Override
    public int deleteCarsHomeBoothByChbids(Long[] chbids)
    {
        return carsHomeBoothMapper.deleteCarsHomeBoothByChbids(chbids);
    }

    /**
     * 删除车场岗亭信息
     * 
     * @param chbid 车场岗亭主键
     * @return 结果
     */
    @Override
    public int deleteCarsHomeBoothByChbid(Long chbid)
    {
        return carsHomeBoothMapper.deleteCarsHomeBoothByChbid(chbid);
    }
}
