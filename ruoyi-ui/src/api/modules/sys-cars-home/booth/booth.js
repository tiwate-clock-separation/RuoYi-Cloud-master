import request from '@/utils/request'

// 查询车场岗亭列表
export function listBooth(query) {
  return request({
    url: '/sys-cars-home/booth/list',
    method: 'get',
    params: query
  })


}export function listBoothLane(chid) {
  return request({
    url: '/sys-cars-home/booth/boothlanelist/'  + chid,
    method: 'get'

  })
}

// 查询车场岗亭详细
export function getBooth(chbid) {
  return request({
    url: '/sys-cars-home/booth/' + chbid,
    method: 'get'
  })
}

// 新增车场岗亭
export function addBooth(data) {
  return request({
    url: '/sys-cars-home/booth',
    method: 'post',
    data: data
  })
}

// 修改车场岗亭
export function updateBooth(data) {
  return request({
    url: '/sys-cars-home/booth',
    method: 'put',
    data: data
  })
}

// 删除车场岗亭
export function delBooth(chbid) {
  return request({
    url: '/sys-cars-home/booth/' + chbid,
    method: 'delete'
  })
}
