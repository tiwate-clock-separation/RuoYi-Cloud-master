package com.ruoyi.system.mapper.result;

import com.ruoyi.domain.PayOrder;
import com.ruoyi.domain.TradingType;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Select;

import java.util.List;

public interface TradingTypeResult {
    @Select(
            "select ttid,type_name from trading_type where ttid = #{ttid}"
    )
    @Results(
            @Result(property = "typeName",column = "type_name")
    )
    public TradingType selectTradingTypeByPoid(Integer ttid);

    @Select(
            "select ttid,type_name from trading_type "
    )
    @Results(
            @Result(property = "typeName",column = "type_name")
    )
    public List<TradingType> selectTradingType();
}
