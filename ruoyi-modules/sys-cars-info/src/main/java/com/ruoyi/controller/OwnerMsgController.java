package com.ruoyi.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;

import com.ruoyi.domain.OwnerMsg;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.log.annotation.Log;
import com.ruoyi.common.log.enums.BusinessType;
import com.ruoyi.common.security.annotation.RequiresPermissions;
import com.ruoyi.service.IOwnerMsgService;
import com.ruoyi.common.core.web.controller.BaseController;
import com.ruoyi.common.core.web.domain.AjaxResult;
import com.ruoyi.common.core.utils.poi.ExcelUtil;
import com.ruoyi.common.core.web.page.TableDataInfo;

/**
 * 业主信息管理Controller
 * 
 * @author ruoyi
 * @date 2023-12-07
 */
@RestController
@RequestMapping("/owner_info")
public class OwnerMsgController extends BaseController
{
    @Autowired
    private IOwnerMsgService ownerMsgService;

    /**
     * 查询业主信息管理列表
     */
    @RequiresPermissions("info:owner_info:list")
    @GetMapping("/list")
    public TableDataInfo list(OwnerMsg ownerMsg)
    {
        startPage();
        List<OwnerMsg> list = ownerMsgService.selectOwnerMsgList(ownerMsg);
        return getDataTable(list);
    }

    /**
     * 导出业主信息管理列表
     */
    @RequiresPermissions("info:owner_info:export")
    @Log(title = "业主信息管理", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, OwnerMsg ownerMsg)
    {
        List<OwnerMsg> list = ownerMsgService.selectOwnerMsgList(ownerMsg);
        ExcelUtil<OwnerMsg> util = new ExcelUtil<OwnerMsg>(OwnerMsg.class);
        util.exportExcel(response, list, "业主信息管理数据");
    }

    /**
     * 获取业主信息管理详细信息
     */
    @RequiresPermissions("info:owner_info:query")
    @GetMapping(value = "/{omid}")
    public AjaxResult getInfo(@PathVariable("omid") Long omid)
    {
        return success(ownerMsgService.selectOwnerMsgByOmid(omid));
    }

    /**
     * 新增业主信息管理
     */
    @RequiresPermissions("info:owner_info:add")
    @Log(title = "业主信息管理", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody OwnerMsg ownerMsg)
    {
        return toAjax(ownerMsgService.insertOwnerMsg(ownerMsg));
    }

    /**
     * 修改业主信息管理
     */
    @RequiresPermissions("info:owner_info:edit")
    @Log(title = "业主信息管理", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody OwnerMsg ownerMsg)
    {
        return toAjax(ownerMsgService.updateOwnerMsg(ownerMsg));
    }

    /**
     * 删除业主信息管理
     */
    @RequiresPermissions("info:owner_info:remove")
    @Log(title = "业主信息管理", businessType = BusinessType.DELETE)
	@DeleteMapping("/{omids}")
    public AjaxResult remove(@PathVariable Long[] omids)
    {
        return toAjax(ownerMsgService.deleteOwnerMsgByOmids(omids));
    }
}
