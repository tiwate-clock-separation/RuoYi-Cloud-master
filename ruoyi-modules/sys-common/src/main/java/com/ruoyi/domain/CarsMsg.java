package com.ruoyi.domain;

import java.util.List;
import com.ruoyi.common.core.annotation.Excel;
import com.ruoyi.common.core.web.domain.BaseEntity;

/**
 * 车辆信息对象 cars_msg
 *
 * @author ruoyi
 * @date 2023-12-07
 */
public class CarsMsg extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 自增ID */
    private Long cmid;

    /** 唯一编码,前台使用 */
    @Excel(name = "唯一编码,前台使用")
    private String code;

    /** 状态 */
    @Excel(name = "状态")
    private Long status;

    /** 是否删除 */
    @Excel(name = "是否删除")
    private Long deleted;

    /** 车牌号 */
    @Excel(name = "车牌号")
    private String licenseNumber;

    /** 车辆类型id */
    @Excel(name = "车辆类型id")
    private Long ctid;

    /** 车牌颜色id */
    @Excel(name = "车牌颜色id")
    private Long lpcid;

    /** 物业id */
    @Excel(name = "物业id")
    private Long pmid;

    /** 业主电话 */
    @Excel(name = "业主电话")
    private String carsTel;

    /** 业主id */
    @Excel(name = "业主id")
    private Long omid;

    /** 车场id */
    @Excel(name = "车场id")
    private Long chid;

    @Excel(name = "车位id关联编号")
    private Long chpid;
    private CarsType carsType;
    private CarsPlateColors carsPlateColors;
    private PropertyManagement propertyManagement;
    private OwnerMsg ownerMsg;
    private CarsHome carsHome;
    private CarsHomeParking carsHomeParking;

    public CarsMsg(Long cmid, String code, Long status, Long deleted, String licenseNumber, Long ctid, Long lpcid, Long pmid, String carsTel, Long omid, Long chid, Long chpid, CarsType carsType, CarsPlateColors carsPlateColors, PropertyManagement propertyManagement, OwnerMsg ownerMsg, CarsHome carsHome, CarsHomeParking carsHomeParking) {
        this.cmid = cmid;
        this.code = code;
        this.status = status;
        this.deleted = deleted;
        this.licenseNumber = licenseNumber;
        this.ctid = ctid;
        this.lpcid = lpcid;
        this.pmid = pmid;
        this.carsTel = carsTel;
        this.omid = omid;
        this.chid = chid;
        this.chpid = chpid;
        this.carsType = carsType;
        this.carsPlateColors = carsPlateColors;
        this.propertyManagement = propertyManagement;
        this.ownerMsg = ownerMsg;
        this.carsHome = carsHome;
        this.carsHomeParking = carsHomeParking;
    }

    @Override
    public String toString() {
        return "CarsMsg{" +
                "cmid=" + cmid +
                ", code='" + code + '\'' +
                ", status=" + status +
                ", deleted=" + deleted +
                ", licenseNumber='" + licenseNumber + '\'' +
                ", ctid=" + ctid +
                ", lpcid=" + lpcid +
                ", pmid=" + pmid +
                ", carsTel='" + carsTel + '\'' +
                ", omid=" + omid +
                ", chid=" + chid +
                ", chpid=" + chpid +
                ", carsType=" + carsType +
                ", carsPlateColors=" + carsPlateColors +
                ", propertyManagement=" + propertyManagement +
                ", ownerMsg=" + ownerMsg +
                ", carsHome=" + carsHome +
                ", carsHomeParking=" + carsHomeParking +
                "} " + super.toString();
    }

    public Long getChpid() {
        return chpid;
    }

    public void setChpid(Long chpid) {
        this.chpid = chpid;
    }

    public CarsHomeParking getCarsHomeParking() {
        return carsHomeParking;
    }

    public void setCarsHomeParking(CarsHomeParking carsHomeParking) {
        this.carsHomeParking = carsHomeParking;
    }

    public Long getCmid() {
        return cmid;
    }

    public void setCmid(Long cmid) {
        this.cmid = cmid;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Long getStatus() {
        return status;
    }

    public void setStatus(Long status) {
        this.status = status;
    }

    public Long getDeleted() {
        return deleted;
    }

    public void setDeleted(Long deleted) {
        this.deleted = deleted;
    }

    public String getLicenseNumber() {
        return licenseNumber;
    }

    public void setLicenseNumber(String licenseNumber) {
        this.licenseNumber = licenseNumber;
    }

    public Long getCtid() {
        return ctid;
    }

    public void setCtid(Long ctid) {
        this.ctid = ctid;
    }

    public Long getLpcid() {
        return lpcid;
    }

    public void setLpcid(Long lpcid) {
        this.lpcid = lpcid;
    }

    public Long getPmid() {
        return pmid;
    }

    public void setPmid(Long pmid) {
        this.pmid = pmid;
    }

    public String getCarsTel() {
        return carsTel;
    }

    public void setCarsTel(String carsTel) {
        this.carsTel = carsTel;
    }

    public Long getOmid() {
        return omid;
    }

    public void setOmid(Long omid) {
        this.omid = omid;
    }

    public Long getChid() {
        return chid;
    }

    public void setChid(Long chid) {
        this.chid = chid;
    }

    public CarsType getCarsType() {
        return carsType;
    }

    public void setCarsType(CarsType carsType) {
        this.carsType = carsType;
    }

    public CarsPlateColors getCarsPlateColors() {
        return carsPlateColors;
    }

    public void setCarsPlateColors(CarsPlateColors carsPlateColors) {
        this.carsPlateColors = carsPlateColors;
    }

    public PropertyManagement getPropertyManagement() {
        return propertyManagement;
    }

    public void setPropertyManagement(PropertyManagement propertyManagement) {
        this.propertyManagement = propertyManagement;
    }

    public OwnerMsg getOwnerMsg() {
        return ownerMsg;
    }

    public void setOwnerMsg(OwnerMsg ownerMsg) {
        this.ownerMsg = ownerMsg;
    }

    public CarsHome getCarsHome() {
        return carsHome;
    }

    public void setCarsHome(CarsHome carsHome) {
        this.carsHome = carsHome;
    }

    public CarsMsg() {
    }

}
