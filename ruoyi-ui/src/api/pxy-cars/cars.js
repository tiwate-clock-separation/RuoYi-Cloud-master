import request from '@/utils/request'

// 查询进出记录列表
export function listCars(query) {
  return request({
    url: '/pxy-cars/cars/list',
    method: 'get',
    params: query
  })
}

// 查询进出记录详细
export function getCars(crid) {
  return request({
    url: '/pxy-cars/cars/' + crid,
    method: 'get'
  })
}

// 新增进出记录
export function addCars(data) {
  return request({
    url: '/pxy-cars/cars',
    method: 'post',
    data: data
  })
}

// 修改进出记录
export function updateCars(data) {
  return request({
    url: '/pxy-cars/cars',
    method: 'put',
    data: data
  })
}

// 删除进出记录
export function delCars(crid) {
  return request({
    url: '/pxy-cars/cars/' + crid,
    method: 'delete'
  })
}
