package com.ruoyi.service;

import java.util.List;

import com.ruoyi.domain.CarsHomeRules;

/**
 * 车场计费规则Service接口
 * 
 * @author ruoyi
 * @date 2023-12-02
 */
public interface ICarsHomeRulesService 
{
    /**
     * 查询车场计费规则
     * 
     * @param chrid 车场计费规则主键
     * @return 车场计费规则
     */
    public CarsHomeRules selectCarsHomeRulesByChrid(Long chrid);

    /**
     * 查询车场计费规则列表
     * 
     * @param carsHomeRules 车场计费规则
     * @return 车场计费规则集合
     */
    public List<CarsHomeRules> selectCarsHomeRulesList(CarsHomeRules carsHomeRules);

    /**
     * 新增车场计费规则
     * 
     * @param carsHomeRules 车场计费规则
     * @return 结果
     */
    public int insertCarsHomeRules(CarsHomeRules carsHomeRules);

    /**
     * 修改车场计费规则
     * 
     * @param carsHomeRules 车场计费规则
     * @return 结果
     */
    public int updateCarsHomeRules(CarsHomeRules carsHomeRules);

    /**
     * 批量删除车场计费规则
     * 
     * @param chrids 需要删除的车场计费规则主键集合
     * @return 结果
     */
    public int deleteCarsHomeRulesByChrids(Long[] chrids);

    /**
     * 删除车场计费规则信息
     * 
     * @param chrid 车场计费规则主键
     * @return 结果
     */
    public int deleteCarsHomeRulesByChrid(Long chrid);
}
