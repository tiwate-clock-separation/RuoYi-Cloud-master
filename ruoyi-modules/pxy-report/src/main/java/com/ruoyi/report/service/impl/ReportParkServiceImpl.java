package com.ruoyi.report.service.impl;

import java.util.List;
import com.ruoyi.common.core.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.report.mapper.ReportParkMapper;
import com.ruoyi.domain.ReportPark;
import com.ruoyi.report.service.IReportParkService;

/**
 * 停车报Service业务层处理
 * 
 * @author ruoyi
 * @date 2023-12-07
 */
@Service
public class ReportParkServiceImpl implements IReportParkService 
{
    @Autowired
    private ReportParkMapper reportParkMapper;

    /**
     * 查询停车报
     * 
     * @param parkid 停车报主键
     * @return 停车报
     */
    @Override
    public ReportPark selectReportParkByParkid(Long parkid)
    {
        return reportParkMapper.selectReportParkByParkid(parkid);
    }

    /**
     * 查询停车报列表
     * 
     * @param reportPark 停车报
     * @return 停车报
     */
    @Override
    public List<ReportPark> selectReportParkList(ReportPark reportPark)
    {
        return reportParkMapper.selectReportParkList(reportPark);
    }

    /**
     * 新增停车报
     * 
     * @param reportPark 停车报
     * @return 结果
     */
    @Override
    public int insertReportPark(ReportPark reportPark)
    {
        reportPark.setCreateTime(DateUtils.getNowDate());
        return reportParkMapper.insertReportPark(reportPark);
    }

    /**
     * 修改停车报
     * 
     * @param reportPark 停车报
     * @return 结果
     */
    @Override
    public int updateReportPark(ReportPark reportPark)
    {
        reportPark.setUpdateTime(DateUtils.getNowDate());
        return reportParkMapper.updateReportPark(reportPark);
    }

    /**
     * 批量删除停车报
     * 
     * @param parkids 需要删除的停车报主键
     * @return 结果
     */
    @Override
    public int deleteReportParkByParkids(Long[] parkids)
    {
        return reportParkMapper.deleteReportParkByParkids(parkids);
    }

    /**
     * 删除停车报信息
     * 
     * @param parkid 停车报主键
     * @return 结果
     */
    @Override
    public int deleteReportParkByParkid(Long parkid)
    {
        return reportParkMapper.deleteReportParkByParkid(parkid);
    }
}
