import request from '@/utils/request'

// 查询按次数收费列表
export function listTime(query) {
  return request({
    url: '/sys-cars-home/time/list',
    method: 'get',
    params: query
  })
}

// 查询按次数收费详细
export function getTime(timeid) {
  return request({
    url: '/sys-cars-home/time/' + timeid,
    method: 'get'
  })
}

// 新增按次数收费
export function addTime(data) {
  return request({
    url: '/sys-cars-home/time',
    method: 'post',
    data: data
  })
}

// 修改按次数收费
export function updateTime(data) {
  return request({
    url: '/sys-cars-home/time',
    method: 'put',
    data: data
  })
}

// 删除按次数收费
export function delTime(timeid) {
  return request({
    url: '/sys-cars-home/time/' + timeid,
    method: 'delete'
  })
}
