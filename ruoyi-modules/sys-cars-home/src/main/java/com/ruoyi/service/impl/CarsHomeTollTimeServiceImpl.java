package com.ruoyi.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.mapper.CarsHomeTollTimeMapper;
import com.ruoyi.domain.CarsHomeTollTime;
import com.ruoyi.service.ICarsHomeTollTimeService;

/**
 * 按次数收费Service业务层处理
 * 
 * @author chn
 * @date 2023-12-14
 */
@Service
public class CarsHomeTollTimeServiceImpl implements ICarsHomeTollTimeService 
{
    @Autowired
    private CarsHomeTollTimeMapper carsHomeTollTimeMapper;

    /**
     * 查询按次数收费
     * 
     * @param timeid 按次数收费主键
     * @return 按次数收费
     */
    @Override
    public CarsHomeTollTime selectCarsHomeTollTimeByTimeid(Long timeid)
    {
        return carsHomeTollTimeMapper.selectCarsHomeTollTimeByTimeid(timeid);
    }

    /**
     * 查询按次数收费列表
     * 
     * @param carsHomeTollTime 按次数收费
     * @return 按次数收费
     */
    @Override
    public List<CarsHomeTollTime> selectCarsHomeTollTimeList(CarsHomeTollTime carsHomeTollTime)
    {
        return carsHomeTollTimeMapper.selectCarsHomeTollTimeList(carsHomeTollTime);
    }

    /**
     * 新增按次数收费
     * 
     * @param carsHomeTollTime 按次数收费
     * @return 结果
     */
    @Override
    public int insertCarsHomeTollTime(CarsHomeTollTime carsHomeTollTime)
    {
        return carsHomeTollTimeMapper.insertCarsHomeTollTime(carsHomeTollTime);
    }

    /**
     * 修改按次数收费
     * 
     * @param carsHomeTollTime 按次数收费
     * @return 结果
     */
    @Override
    public int updateCarsHomeTollTime(CarsHomeTollTime carsHomeTollTime)
    {
        return carsHomeTollTimeMapper.updateCarsHomeTollTime(carsHomeTollTime);
    }

    /**
     * 批量删除按次数收费
     * 
     * @param timeids 需要删除的按次数收费主键
     * @return 结果
     */
    @Override
    public int deleteCarsHomeTollTimeByTimeids(Long[] timeids)
    {
        return carsHomeTollTimeMapper.deleteCarsHomeTollTimeByTimeids(timeids);
    }

    /**
     * 删除按次数收费信息
     * 
     * @param timeid 按次数收费主键
     * @return 结果
     */
    @Override
    public int deleteCarsHomeTollTimeByTimeid(Long timeid)
    {
        return carsHomeTollTimeMapper.deleteCarsHomeTollTimeByTimeid(timeid);
    }
}
