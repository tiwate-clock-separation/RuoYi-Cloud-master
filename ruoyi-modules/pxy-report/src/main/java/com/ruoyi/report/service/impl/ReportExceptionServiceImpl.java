package com.ruoyi.report.service.impl;

import java.util.List;
import com.ruoyi.common.core.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.report.mapper.ReportExceptionMapper;
import com.ruoyi.domain.ReportException;
import com.ruoyi.report.service.IReportExceptionService;

/**
 * 异常报表Service业务层处理
 * 
 * @author ruoyi
 * @date 2023-12-08
 */
@Service
public class ReportExceptionServiceImpl implements IReportExceptionService 
{
    @Autowired
    private ReportExceptionMapper reportExceptionMapper;

    /**
     * 查询异常报表
     * 
     * @param exceid 异常报表主键
     * @return 异常报表
     */
    @Override
    public ReportException selectReportExceptionByExceid(Long exceid)
    {
        return reportExceptionMapper.selectReportExceptionByExceid(exceid);
    }

    /**
     * 查询异常报表列表
     * 
     * @param reportException 异常报表
     * @return 异常报表
     */
    @Override
    public List<ReportException> selectReportExceptionList(ReportException reportException)
    {
        return reportExceptionMapper.selectReportExceptionList(reportException);
    }

    /**
     * 新增异常报表
     * 
     * @param reportException 异常报表
     * @return 结果
     */
    @Override
    public int insertReportException(ReportException reportException)
    {
        reportException.setCreateTime(DateUtils.getNowDate());
        return reportExceptionMapper.insertReportException(reportException);
    }

    /**
     * 修改异常报表
     * 
     * @param reportException 异常报表
     * @return 结果
     */
    @Override
    public int updateReportException(ReportException reportException)
    {
        reportException.setUpdateTime(DateUtils.getNowDate());
        return reportExceptionMapper.updateReportException(reportException);
    }

    /**
     * 批量删除异常报表
     * 
     * @param exceids 需要删除的异常报表主键
     * @return 结果
     */
    @Override
    public int deleteReportExceptionByExceids(Long[] exceids)
    {
        return reportExceptionMapper.deleteReportExceptionByExceids(exceids);
    }

    /**
     * 删除异常报表信息
     * 
     * @param exceid 异常报表主键
     * @return 结果
     */
    @Override
    public int deleteReportExceptionByExceid(Long exceid)
    {
        return reportExceptionMapper.deleteReportExceptionByExceid(exceid);
    }
}
