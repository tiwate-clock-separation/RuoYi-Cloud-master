package com.ruoyi.controller;

import java.util.List;
import java.io.IOException;
import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.log.annotation.Log;
import com.ruoyi.common.log.enums.BusinessType;
import com.ruoyi.common.security.annotation.RequiresPermissions;
import com.ruoyi.domain.CarsHomeTollTime;
import com.ruoyi.service.ICarsHomeTollTimeService;
import com.ruoyi.common.core.web.controller.BaseController;
import com.ruoyi.common.core.web.domain.AjaxResult;
import com.ruoyi.common.core.utils.poi.ExcelUtil;
import com.ruoyi.common.core.web.page.TableDataInfo;

/**
 * 按次数收费Controller
 * 
 * @author chn
 * @date 2023-12-14
 */
@RestController
@RequestMapping("/time")
public class CarsHomeTollTimeController extends BaseController
{
    @Autowired
    private ICarsHomeTollTimeService carsHomeTollTimeService;

    /**
     * 查询按次数收费列表
     */
    @RequiresPermissions("sys-cars-home:time:list")
    @GetMapping("/list")
    public TableDataInfo list(CarsHomeTollTime carsHomeTollTime)
    {
        startPage();
        List<CarsHomeTollTime> list = carsHomeTollTimeService.selectCarsHomeTollTimeList(carsHomeTollTime);
        return getDataTable(list);
    }

    /**
     * 导出按次数收费列表
     */
    @RequiresPermissions("sys-cars-home:time:export")
    @Log(title = "按次数收费", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, CarsHomeTollTime carsHomeTollTime)
    {
        List<CarsHomeTollTime> list = carsHomeTollTimeService.selectCarsHomeTollTimeList(carsHomeTollTime);
        ExcelUtil<CarsHomeTollTime> util = new ExcelUtil<CarsHomeTollTime>(CarsHomeTollTime.class);
        util.exportExcel(response, list, "按次数收费数据");
    }

    /**
     * 获取按次数收费详细信息
     */
    @RequiresPermissions("sys-cars-home:time:query")
    @GetMapping(value = "/{timeid}")
    public AjaxResult getInfo(@PathVariable("timeid") Long timeid)
    {
        return success(carsHomeTollTimeService.selectCarsHomeTollTimeByTimeid(timeid));
    }

    /**
     * 新增按次数收费
     */
    @RequiresPermissions("sys-cars-home:time:add")
    @Log(title = "按次数收费", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody CarsHomeTollTime carsHomeTollTime)
    {
        return toAjax(carsHomeTollTimeService.insertCarsHomeTollTime(carsHomeTollTime));
    }

    /**
     * 修改按次数收费
     */
    @RequiresPermissions("sys-cars-home:time:edit")
    @Log(title = "按次数收费", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody CarsHomeTollTime carsHomeTollTime)
    {
        return toAjax(carsHomeTollTimeService.updateCarsHomeTollTime(carsHomeTollTime));
    }

    /**
     * 删除按次数收费
     */
    @RequiresPermissions("sys-cars-home:time:remove")
    @Log(title = "按次数收费", businessType = BusinessType.DELETE)
	@DeleteMapping("/{timeids}")
    public AjaxResult remove(@PathVariable Long[] timeids)
    {
        return toAjax(carsHomeTollTimeService.deleteCarsHomeTollTimeByTimeids(timeids));
    }
}
