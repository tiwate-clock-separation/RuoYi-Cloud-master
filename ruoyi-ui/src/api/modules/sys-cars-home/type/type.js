import request from '@/utils/request'

// 查询车场类型列表
export function listType(query) {
  return request({
    url: '/sys-cars-home/type/list',
    method: 'get',
    params: query
  })
}

// 查询车场类型详细
export function getType(chtid) {
  return request({
    url: '/sys-cars-home/type/' + chtid,
    method: 'get'
  })
}
//
// // 新增车场类型
// export function addType(data) {
//   return request({
//     url: '/sys-cars-home/type',
//     method: 'post',
//     data: data
//   })
// }
//
// // 修改车场类型
// export function updateType(data) {
//   return request({
//     url: '/sys-cars-home/type',
//     method: 'put',
//     data: data
//   })
// }
//
// // 删除车场类型
// export function delType(chtid) {
//   return request({
//     url: '/sys-cars-home/type/' + chtid,
//     method: 'delete'
//   })
// }
