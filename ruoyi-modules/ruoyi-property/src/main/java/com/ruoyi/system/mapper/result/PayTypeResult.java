package com.ruoyi.system.mapper.result;

import com.ruoyi.domain.PayOrder;
import com.ruoyi.domain.PayType;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Select;

import java.util.List;

public interface PayTypeResult {
    @Select(
            "select ptid,type_name from pay_type where ptid = #{ptid}"
    )
    @Results(
            @Result(property = "typeName",column = "type_name")
    )
    public PayType selectPayTypeByPoid(Integer ptid);

    @Select(
            "select ptid,type_name from pay_type"
    )
    @Results(
            @Result(property = "typeName",column = "type_name")
    )
    public List<PayType> ptlist();
}
