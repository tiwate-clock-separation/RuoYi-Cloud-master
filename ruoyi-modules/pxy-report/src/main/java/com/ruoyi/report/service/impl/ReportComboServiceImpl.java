package com.ruoyi.report.service.impl;

import java.util.List;
import com.ruoyi.common.core.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.report.mapper.ReportComboMapper;
import com.ruoyi.domain.ReportCombo;
import com.ruoyi.report.service.IReportComboService;

/**
 * 卡套餐报Service业务层处理
 * 
 * @author ruoyi
 * @date 2023-12-08
 */
@Service
public class ReportComboServiceImpl implements IReportComboService 
{
    @Autowired
    private ReportComboMapper reportComboMapper;

    /**
     * 查询卡套餐报
     * 
     * @param comid 卡套餐报主键
     * @return 卡套餐报
     */
    @Override
    public ReportCombo selectReportComboByComid(Long comid)
    {
        return reportComboMapper.selectReportComboByComid(comid);
    }

    /**
     * 查询卡套餐报列表
     * 
     * @param reportCombo 卡套餐报
     * @return 卡套餐报
     */
    @Override
    public List<ReportCombo> selectReportComboList(ReportCombo reportCombo)
    {
        return reportComboMapper.selectReportComboList(reportCombo);
    }

    /**
     * 新增卡套餐报
     * 
     * @param reportCombo 卡套餐报
     * @return 结果
     */
    @Override
    public int insertReportCombo(ReportCombo reportCombo)
    {
        reportCombo.setCreateTime(DateUtils.getNowDate());
        return reportComboMapper.insertReportCombo(reportCombo);
    }

    /**
     * 修改卡套餐报
     * 
     * @param reportCombo 卡套餐报
     * @return 结果
     */
    @Override
    public int updateReportCombo(ReportCombo reportCombo)
    {
        reportCombo.setUpdateTime(DateUtils.getNowDate());
        return reportComboMapper.updateReportCombo(reportCombo);
    }

    /**
     * 批量删除卡套餐报
     * 
     * @param comids 需要删除的卡套餐报主键
     * @return 结果
     */
    @Override
    public int deleteReportComboByComids(Long[] comids)
    {
        return reportComboMapper.deleteReportComboByComids(comids);
    }

    /**
     * 删除卡套餐报信息
     * 
     * @param comid 卡套餐报主键
     * @return 结果
     */
    @Override
    public int deleteReportComboByComid(Long comid)
    {
        return reportComboMapper.deleteReportComboByComid(comid);
    }
}
