import request from '@/utils/request'

// 查询微信支付列表
export function listWeChat(query) {
  return request({
    url: '/sys-cars-home/weChat/list',
    method: 'get',
    params: query
  })
}

// 查询微信支付详细
export function getWeChat(id) {
  return request({
    url: '/sys-cars-home/weChat/' + id,
    method: 'get'
  })
}

// 新增微信支付
export function addWeChat(data) {
  return request({
    url: '/sys-cars-home/weChat',
    method: 'post',
    data: data
  })
}

// 修改微信支付
export function updateWeChat(data) {
  return request({
    url: '/sys-cars-home/weChat',
    method: 'put',
    data: data
  })
}

// 删除微信支付
export function delWeChat(id) {
  return request({
    url: '/sys-cars-home/weChat/' + id,
    method: 'delete'
  })
}
