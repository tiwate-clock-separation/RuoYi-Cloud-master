import request from '@/utils/request'

// 查询开闸记录列表
export function listRecord(query) {
  return request({
    url: '/pxy-cars/record/list',
    method: 'get',
    params: query
  })
}

// 查询开闸记录详细
export function getRecord(corid) {
  return request({
    url: '/pxy-cars/record/' + corid,
    method: 'get'
  })
}

// 新增开闸记录
export function addRecord(data) {
  return request({
    url: '/pxy-cars/record',
    method: 'post',
    data: data
  })
}

// 修改开闸记录
export function updateRecord(data) {
  return request({
    url: '/pxy-cars/record',
    method: 'put',
    data: data
  })
}

// 删除开闸记录
export function delRecord(corid) {
  return request({
    url: '/pxy-cars/record/' + corid,
    method: 'delete'
  })
}
