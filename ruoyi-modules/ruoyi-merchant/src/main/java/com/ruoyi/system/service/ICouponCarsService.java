package com.ruoyi.system.service;

import com.ruoyi.domain.CouponCars;

import java.util.List;

/**
 * 优惠卷车辆连Service接口
 *
 * @author ruoyi
 * @date 2023-12-15
 */
public interface ICouponCarsService
{
    /**
     * 查询优惠卷车辆连
     *
     * @param ccid 优惠卷车辆连主键
     * @return 优惠卷车辆连
     */
    public CouponCars selectCouponCarsByCcid(Long ccid);

    /**
     * 查询优惠卷车辆连列表
     *
     * @param couponCars 优惠卷车辆连
     * @return 优惠卷车辆连集合
     */
    public List<CouponCars> selectCouponCarsList(CouponCars couponCars);

    /**
     * 新增优惠卷车辆连
     *
     * @param couponCars 优惠卷车辆连
     * @return 结果
     */
    public int insertCouponCars(CouponCars couponCars);

    /**
     * 修改优惠卷车辆连
     *
     * @param couponCars 优惠卷车辆连
     * @return 结果
     */
    public int updateCouponCars(CouponCars couponCars);

    /**
     * 批量删除优惠卷车辆连
     *
     * @param ccids 需要删除的优惠卷车辆连主键集合
     * @return 结果
     */
    public int deleteCouponCarsByCcids(Long[] ccids);

    /**
     * 删除优惠卷车辆连信息
     *
     * @param ccid 优惠卷车辆连主键
     * @return 结果
     */
    public int deleteCouponCarsByCcid(Long ccid);
}
