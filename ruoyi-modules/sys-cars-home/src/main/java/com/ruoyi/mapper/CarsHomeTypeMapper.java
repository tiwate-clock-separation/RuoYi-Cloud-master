package com.ruoyi.mapper;

import com.ruoyi.domain.CarsHomeType;

import java.util.List;

/**
 * 车场类型Mapper接口
 *
 * @author chn
 * @date 2023-11-29
 */
public interface CarsHomeTypeMapper
{
    /**
     * 查询车场类型
     *
     * @param chtid 车场类型主键
     * @return 车场类型
     */
    public CarsHomeType selectCarsHomeTypeByChtid(Long chtid);

    /**
     * 查询车场类型列表
     *
     * @param carsHomeType 车场类型
     * @return 车场类型集合
     */
    public List<CarsHomeType> selectCarsHomeTypeList(CarsHomeType carsHomeType);

    /**
     * 新增车场类型
     *
     * @param carsHomeType 车场类型
     * @return 结果
     */
    public int insertCarsHomeType(CarsHomeType carsHomeType);

    /**
     * 修改车场类型
     *
     * @param carsHomeType 车场类型
     * @return 结果
     */
    public int updateCarsHomeType(CarsHomeType carsHomeType);

    /**
     * 删除车场类型
     *
     * @param chtid 车场类型主键
     * @return 结果
     */
    public int deleteCarsHomeTypeByChtid(Long chtid);

    /**
     * 批量删除车场类型
     *
     * @param chtids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteCarsHomeTypeByChtids(Long[] chtids);
}
