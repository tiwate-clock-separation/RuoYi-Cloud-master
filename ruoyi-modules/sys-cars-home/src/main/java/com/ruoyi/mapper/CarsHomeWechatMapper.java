package com.ruoyi.mapper;

import java.util.List;
import com.ruoyi.domain.CarsHomeWechat;

/**
 * 微信支付Mapper接口
 * 
 * @author chn
 * @date 2023-12-27
 */
public interface CarsHomeWechatMapper 
{
    /**
     * 查询微信支付
     * 
     * @param id 微信支付主键
     * @return 微信支付
     */
    public CarsHomeWechat selectCarsHomeWechatById(Long id);

    /**
     * 查询微信支付列表
     * 
     * @param carsHomeWechat 微信支付
     * @return 微信支付集合
     */
    public List<CarsHomeWechat> selectCarsHomeWechatList(CarsHomeWechat carsHomeWechat);

    /**
     * 新增微信支付
     * 
     * @param carsHomeWechat 微信支付
     * @return 结果
     */
    public int insertCarsHomeWechat(CarsHomeWechat carsHomeWechat);

    /**
     * 修改微信支付
     * 
     * @param carsHomeWechat 微信支付
     * @return 结果
     */
    public int updateCarsHomeWechat(CarsHomeWechat carsHomeWechat);

    /**
     * 删除微信支付
     * 
     * @param id 微信支付主键
     * @return 结果
     */
    public int deleteCarsHomeWechatById(Long id);

    /**
     * 批量删除微信支付
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteCarsHomeWechatByIds(Long[] ids);
}
