package com.ruoyi.report.mapper;

import java.util.List;
import com.ruoyi.domain.ReportCombo;

/**
 * 卡套餐报Mapper接口
 * 
 * @author ruoyi
 * @date 2023-12-08
 */
public interface ReportComboMapper 
{
    /**
     * 查询卡套餐报
     * 
     * @param comid 卡套餐报主键
     * @return 卡套餐报
     */
    public ReportCombo selectReportComboByComid(Long comid);

    /**
     * 查询卡套餐报列表
     * 
     * @param reportCombo 卡套餐报
     * @return 卡套餐报集合
     */
    public List<ReportCombo> selectReportComboList(ReportCombo reportCombo);

    /**
     * 新增卡套餐报
     * 
     * @param reportCombo 卡套餐报
     * @return 结果
     */
    public int insertReportCombo(ReportCombo reportCombo);

    /**
     * 修改卡套餐报
     * 
     * @param reportCombo 卡套餐报
     * @return 结果
     */
    public int updateReportCombo(ReportCombo reportCombo);

    /**
     * 删除卡套餐报
     * 
     * @param comid 卡套餐报主键
     * @return 结果
     */
    public int deleteReportComboByComid(Long comid);

    /**
     * 批量删除卡套餐报
     * 
     * @param comids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteReportComboByComids(Long[] comids);
}
