package com.ruoyi.cars.controller;

import com.ruoyi.cars.Api.CarsHomeApi;
import com.ruoyi.cars.Api.PropertyManagementApi;
import com.ruoyi.common.core.web.page.TableDataInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping
public class TestController {
    @Autowired
    private PropertyManagementApi propertyManagementApi;

    @Autowired
    private CarsHomeApi carsHomeApi;

//    物业
    @GetMapping("test")
    public TableDataInfo list(){
        return propertyManagementApi.list();
    }

    //车场
    @GetMapping("test2")
    public TableDataInfo list2(){
        return carsHomeApi.list();
    }
}
