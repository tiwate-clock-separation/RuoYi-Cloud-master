package com.ruoyi.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.mapper.CarsHomeEqCameratypeMapper;
import com.ruoyi.domain.CarsHomeEqCameratype;
import com.ruoyi.service.ICarsHomeEqCameratypeService;

/**
 * 车厂设备相机类型Service业务层处理
 * 
 * @author chn
 * @date 2023-12-07
 */
@Service
public class CarsHomeEqCameratypeServiceImpl implements ICarsHomeEqCameratypeService 
{
    @Autowired
    private CarsHomeEqCameratypeMapper carsHomeEqCameratypeMapper;

    /**
     * 查询车厂设备相机类型
     * 
     * @param cameraid 车厂设备相机类型主键
     * @return 车厂设备相机类型
     */
    @Override
    public CarsHomeEqCameratype selectCarsHomeEqCameratypeByCameraid(Long cameraid)
    {
        return carsHomeEqCameratypeMapper.selectCarsHomeEqCameratypeByCameraid(cameraid);
    }

    /**
     * 查询车厂设备相机类型列表
     * 
     * @param carsHomeEqCameratype 车厂设备相机类型
     * @return 车厂设备相机类型
     */
    @Override
    public List<CarsHomeEqCameratype> selectCarsHomeEqCameratypeList(CarsHomeEqCameratype carsHomeEqCameratype)
    {
        return carsHomeEqCameratypeMapper.selectCarsHomeEqCameratypeList(carsHomeEqCameratype);
    }

    /**
     * 新增车厂设备相机类型
     * 
     * @param carsHomeEqCameratype 车厂设备相机类型
     * @return 结果
     */
    @Override
    public int insertCarsHomeEqCameratype(CarsHomeEqCameratype carsHomeEqCameratype)
    {
        return carsHomeEqCameratypeMapper.insertCarsHomeEqCameratype(carsHomeEqCameratype);
    }

    /**
     * 修改车厂设备相机类型
     * 
     * @param carsHomeEqCameratype 车厂设备相机类型
     * @return 结果
     */
    @Override
    public int updateCarsHomeEqCameratype(CarsHomeEqCameratype carsHomeEqCameratype)
    {
        return carsHomeEqCameratypeMapper.updateCarsHomeEqCameratype(carsHomeEqCameratype);
    }

    /**
     * 批量删除车厂设备相机类型
     * 
     * @param cameraids 需要删除的车厂设备相机类型主键
     * @return 结果
     */
    @Override
    public int deleteCarsHomeEqCameratypeByCameraids(Long[] cameraids)
    {
        return carsHomeEqCameratypeMapper.deleteCarsHomeEqCameratypeByCameraids(cameraids);
    }

    /**
     * 删除车厂设备相机类型信息
     * 
     * @param cameraid 车厂设备相机类型主键
     * @return 结果
     */
    @Override
    public int deleteCarsHomeEqCameratypeByCameraid(Long cameraid)
    {
        return carsHomeEqCameratypeMapper.deleteCarsHomeEqCameratypeByCameraid(cameraid);
    }
}
