package com.ruoyi.system.mapper;

import java.util.List;

import com.ruoyi.domain.PayWater;
import org.apache.ibatis.annotations.Many;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Select;

/**
 * 物业流水Mapper接口
 *
 * @author ruoyi
 * @date 2023-11-30
 */
public interface PayWaterMapper {
    /**
     * 查询物业流水
     *
     * @param pwid 物业流水主键
     * @return 物业流水
     */
    public PayWater selectPayWaterByPwid(Long pwid);

    /**
     * 查询物业流水列表
     *
     * @param payWater 物业流水
     * @return 物业流水集合
     */
//    @Select("<script>" +
//            "select * from pay_water" +
//            "<where>" +
//            "<if test=\"code != null  and code != ''\"> and code = #{code}</if>\n" +
//            "<if test=\"status != null \"> and status = #{status}</if>\n" +
//            "<if test=\"deleted != null \"> and deleted = #{deleted}</if>\n" +
//            "<if test=\"orderNum != null  and orderNum != ''\"> and order_num = #{orderNum}</if>\n" +
//            "<if test=\"orderPrice != null \"> and order_price = #{orderPrice}</if>\n" +
//            "<if test=\"pwStatus != null \"> and pw_status = #{pwStatus}</if>" +
//            "</where>" +
//            "</script>")
//    @Results(value = {
//            @Result(id = true,property = "pwid",column = "pwid"),
//            @Result(id = true,property = "orderPrice",column = "order_price"),
//            @Result(id = true,property = "pwStatus",column = "pw_status"),
//            @Result(id = true,property = "createTime",column = "create_time"),
//            @Result(id = true,property = "updateTime",column = "update_time"),
//            @Result(id = true,property = "orderPrice",column = "order_price"),
//            @Result(property = "payOrder", column = "order_num", many = @Many(select = "com.ruoyi.system.mapper.result.PayOrderResult.selectPayOrderByPoid"))
//    }
//    )
    public List<PayWater> selectPayWaterList(PayWater payWater);

    /**
     * 新增物业流水
     *
     * @param payWater 物业流水
     * @return 结果
     */
    public int insertPayWater(PayWater payWater);

    /**
     * 修改物业流水
     *
     * @param payWater 物业流水
     * @return 结果
     */
    public int updatePayWater(PayWater payWater);

    /**
     * 删除物业流水
     *
     * @param pwid 物业流水主键
     * @return 结果
     */
    public int deletePayWaterByPwid(Long pwid);

    /**
     * 批量删除物业流水
     *
     * @param pwids 需要删除的数据主键集合
     * @return 结果
     */
    public int deletePayWaterByPwids(Long[] pwids);
}
