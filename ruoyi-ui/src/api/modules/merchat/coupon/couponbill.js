import request from '@/utils/request'

// 查询优惠卷账单列表
export function listBill(query) {
  return request({
    url: '/sys-merchant/bill/list',
    method: 'get',
    params: query
  })
}

// 查询优惠卷账单详细
export function getBill(id) {
  return request({
    url: '/sys-merchant/bill/' + id,
    method: 'get'
  })
}

// 新增优惠卷账单
export function addBill(data) {
  return request({
    url: '/sys-merchant/bill',
    method: 'post',
    data: data
  })
}

// 修改优惠卷账单
export function updateBill(data) {
  return request({
    url: '/sys-merchant/bill',
    method: 'put',
    data: data
  })
}

// 删除优惠卷账单
export function delBill(id) {
  return request({
    url: '/sys-merchant/bill/' + id,
    method: 'delete'
  })
}
