package com.ruoyi.mapper;

import com.ruoyi.domain.CarsHome;

import java.util.List;

/**
 * 车场管理Mapper接口
 *
 * @author chn
 * @date 2023-11-29
 */
public interface CarsHomeMapper
{
    /**
     * 查询车场管理
     *
     * @param chid 车场管理主键
     * @return 车场管理
     */
    public CarsHome selectCarsHomeByChid(Long chid);

    /**
     * 查询车场管理列表
     *
     * @param carsHome 车场管理
     * @return 车场管理集合
     */
    public List<CarsHome> selectCarsHomeList(CarsHome carsHome);

    /**
     * 新增车场管理
     *
     * @param carsHome 车场管理
     * @return 结果
     */
    public int insertCarsHome(CarsHome carsHome);

    /**
     * 修改车场管理
     *
     * @param carsHome 车场管理
     * @return 结果
     */
    public int updateCarsHome(CarsHome carsHome);

    /**
     * 删除车场管理
     *
     * @param chid 车场管理主键
     * @return 结果
     */
    public int deleteCarsHomeByChid(Long chid);

    /**
     * 批量删除车场管理
     *
     * @param chids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteCarsHomeByChids(Long[] chids);
}
