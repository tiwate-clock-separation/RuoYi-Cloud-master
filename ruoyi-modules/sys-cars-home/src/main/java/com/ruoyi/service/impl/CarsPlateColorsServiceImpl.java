package com.ruoyi.service.impl;

import java.util.List;
import com.ruoyi.common.core.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.mapper.CarsPlateColorsMapper;
import com.ruoyi.domain.CarsPlateColors;
import com.ruoyi.service.ICarsPlateColorsService;

/**
 * 车辆牌照颜色Service业务层处理
 * 
 * @author chn
 * @date 2023-12-04
 */
@Service
public class CarsPlateColorsServiceImpl implements ICarsPlateColorsService 
{
    @Autowired
    private CarsPlateColorsMapper carsPlateColorsMapper;

    /**
     * 查询车辆牌照颜色
     * 
     * @param lpcid 车辆牌照颜色主键
     * @return 车辆牌照颜色
     */
    @Override
    public CarsPlateColors selectCarsPlateColorsByLpcid(Long lpcid)
    {
        return carsPlateColorsMapper.selectCarsPlateColorsByLpcid(lpcid);
    }

    /**
     * 查询车辆牌照颜色列表
     * 
     * @param carsPlateColors 车辆牌照颜色
     * @return 车辆牌照颜色
     */
    @Override
    public List<CarsPlateColors> selectCarsPlateColorsList(CarsPlateColors carsPlateColors)
    {
        return carsPlateColorsMapper.selectCarsPlateColorsList(carsPlateColors);
    }

    /**
     * 新增车辆牌照颜色
     * 
     * @param carsPlateColors 车辆牌照颜色
     * @return 结果
     */
    @Override
    public int insertCarsPlateColors(CarsPlateColors carsPlateColors)
    {
        carsPlateColors.setCreateTime(DateUtils.getNowDate());
        return carsPlateColorsMapper.insertCarsPlateColors(carsPlateColors);
    }

    /**
     * 修改车辆牌照颜色
     * 
     * @param carsPlateColors 车辆牌照颜色
     * @return 结果
     */
    @Override
    public int updateCarsPlateColors(CarsPlateColors carsPlateColors)
    {
        carsPlateColors.setUpdateTime(DateUtils.getNowDate());
        return carsPlateColorsMapper.updateCarsPlateColors(carsPlateColors);
    }

    /**
     * 批量删除车辆牌照颜色
     * 
     * @param lpcids 需要删除的车辆牌照颜色主键
     * @return 结果
     */
    @Override
    public int deleteCarsPlateColorsByLpcids(Long[] lpcids)
    {
        return carsPlateColorsMapper.deleteCarsPlateColorsByLpcids(lpcids);
    }

    /**
     * 删除车辆牌照颜色信息
     * 
     * @param lpcid 车辆牌照颜色主键
     * @return 结果
     */
    @Override
    public int deleteCarsPlateColorsByLpcid(Long lpcid)
    {
        return carsPlateColorsMapper.deleteCarsPlateColorsByLpcid(lpcid);
    }
}
