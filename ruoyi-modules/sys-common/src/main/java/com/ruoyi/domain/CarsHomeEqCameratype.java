package com.ruoyi.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.core.annotation.Excel;
import com.ruoyi.common.core.web.domain.BaseEntity;

/**
 * 车厂设备相机类型对象 cars_home_eq_cameratype
 * 
 * @author chn
 * @date 2023-12-07
 */
public class CarsHomeEqCameratype extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 相机类型id */
    private Long cameraid;

    /** 相机品牌名字 */
    @Excel(name = "相机品牌名字")
    private String cameraname;

    public void setCameraid(Long cameraid) 
    {
        this.cameraid = cameraid;
    }

    public Long getCameraid() 
    {
        return cameraid;
    }
    public void setCameraname(String cameraname) 
    {
        this.cameraname = cameraname;
    }

    public String getCameraname() 
    {
        return cameraname;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("cameraid", getCameraid())
            .append("cameraname", getCameraname())
            .toString();
    }
}
