package com.ruoyi.system.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;

import com.ruoyi.domain.PayOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.log.annotation.Log;
import com.ruoyi.common.log.enums.BusinessType;
import com.ruoyi.common.security.annotation.RequiresPermissions;
import com.ruoyi.domain.PayWater;
import com.ruoyi.system.service.IPayWaterService;
import com.ruoyi.common.core.web.controller.BaseController;
import com.ruoyi.common.core.web.domain.AjaxResult;
import com.ruoyi.common.core.utils.poi.ExcelUtil;
import com.ruoyi.common.core.web.page.TableDataInfo;

/**
 * 物业流水Controller
 *
 * @author ruoyi
 * @date 2023-11-30
 */
@RestController
@RequestMapping("/propertyWater")
public class PayWaterController extends BaseController
{
    @Autowired
    private IPayWaterService payWaterService;

    /**
     * 查询物业流水列表
     */
    @RequiresPermissions("sys-property-water:propertyWater:list")
    @GetMapping("/list")
    public TableDataInfo list(PayWater payWater, PayOrder payOrder)
    {
        startPage();
        payWater.setPayOrder(payOrder);
        List<PayWater> list = payWaterService.selectPayWaterList(payWater);
        return getDataTable(list);
    }

    /**
     * 导出物业流水列表
     */
    @RequiresPermissions("sys-property-water:propertyWater:export")
    @Log(title = "物业流水", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, PayWater payWater)
    {
        List<PayWater> list = payWaterService.selectPayWaterList(payWater);
        ExcelUtil<PayWater> util = new ExcelUtil<PayWater>(PayWater.class);
        util.exportExcel(response, list, "物业流水数据");
    }

    /**
     * 获取物业流水详细信息
     */
    @RequiresPermissions("sys-property-water:propertyWater:query")
    @GetMapping(value = "/{pwid}")
    public AjaxResult getInfo(@PathVariable("pwid") Long pwid)
    {
        return success(payWaterService.selectPayWaterByPwid(pwid));
    }

    /**
     * 新增物业流水
     */
    @RequiresPermissions("sys-property-water:propertyWater:add")
    @Log(title = "物业流水", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody PayWater payWater)
    {
        return toAjax(payWaterService.insertPayWater(payWater));
    }

    /**
     * 修改物业流水
     */
    @RequiresPermissions("sys-property-water:propertyWater:edit")
    @Log(title = "物业流水", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody PayWater payWater)
    {
        return toAjax(payWaterService.updatePayWater(payWater));
    }

    /**
     * 删除物业流水
     */
    @RequiresPermissions("sys-property-water:propertyWater:remove")
    @Log(title = "物业流水", businessType = BusinessType.DELETE)
	@DeleteMapping("/{pwids}")
    public AjaxResult remove(@PathVariable Long[] pwids)
    {
        return toAjax(payWaterService.deletePayWaterByPwids(pwids));
    }
}
