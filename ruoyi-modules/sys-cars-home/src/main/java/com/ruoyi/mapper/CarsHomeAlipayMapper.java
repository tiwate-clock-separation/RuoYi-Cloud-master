package com.ruoyi.mapper;

import java.util.List;
import com.ruoyi.domain.CarsHomeAlipay;

/**
 * 支付宝支付配置Mapper接口
 * 
 * @author chn
 * @date 2023-12-27
 */
public interface CarsHomeAlipayMapper 
{
    /**
     * 查询支付宝支付配置
     * 
     * @param id 支付宝支付配置主键
     * @return 支付宝支付配置
     */
    public CarsHomeAlipay selectCarsHomeAlipayById(Long id);

    /**
     * 查询支付宝支付配置列表
     * 
     * @param carsHomeAlipay 支付宝支付配置
     * @return 支付宝支付配置集合
     */
    public List<CarsHomeAlipay> selectCarsHomeAlipayList(CarsHomeAlipay carsHomeAlipay);

    /**
     * 新增支付宝支付配置
     * 
     * @param carsHomeAlipay 支付宝支付配置
     * @return 结果
     */
    public int insertCarsHomeAlipay(CarsHomeAlipay carsHomeAlipay);

    /**
     * 修改支付宝支付配置
     * 
     * @param carsHomeAlipay 支付宝支付配置
     * @return 结果
     */
    public int updateCarsHomeAlipay(CarsHomeAlipay carsHomeAlipay);

    /**
     * 删除支付宝支付配置
     * 
     * @param id 支付宝支付配置主键
     * @return 结果
     */
    public int deleteCarsHomeAlipayById(Long id);

    /**
     * 批量删除支付宝支付配置
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteCarsHomeAlipayByIds(Long[] ids);
}
