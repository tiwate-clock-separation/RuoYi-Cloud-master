package com.ruoyi.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;

import com.ruoyi.domain.CarsHomeTollRules;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.log.annotation.Log;
import com.ruoyi.common.log.enums.BusinessType;
import com.ruoyi.common.security.annotation.RequiresPermissions;
import com.ruoyi.service.ICarsHomeTollRulesService;
import com.ruoyi.common.core.web.controller.BaseController;
import com.ruoyi.common.core.web.domain.AjaxResult;
import com.ruoyi.common.core.utils.poi.ExcelUtil;
import com.ruoyi.common.core.web.page.TableDataInfo;

/**
 * 车场收费规则类型Controller
 * 
 * @author ruoyi
 * @date 2023-12-04
 */
@RestController
@RequestMapping("/rulestype")
public class CarsHomeTollRulesController extends BaseController
{
    @Autowired
    private ICarsHomeTollRulesService carsHomeTollRulesService;

    /**
     * 查询车场收费规则类型列表
     */
//    @RequiresPermissions("sys-cars-home:rules:list")
    @GetMapping("/list")
    public TableDataInfo list(CarsHomeTollRules carsHomeTollRules)
    {
        startPage();
        List<CarsHomeTollRules> list = carsHomeTollRulesService.selectCarsHomeTollRulesList(carsHomeTollRules);
        return getDataTable(list);
    }

    /**
     * 导出车场收费规则类型列表
     */
    @RequiresPermissions("sys-cars-home:rules:export")
    @Log(title = "车场收费规则类型", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, CarsHomeTollRules carsHomeTollRules)
    {
        List<CarsHomeTollRules> list = carsHomeTollRulesService.selectCarsHomeTollRulesList(carsHomeTollRules);
        ExcelUtil<CarsHomeTollRules> util = new ExcelUtil<CarsHomeTollRules>(CarsHomeTollRules.class);
        util.exportExcel(response, list, "车场收费规则类型数据");
    }

    /**
     * 获取车场收费规则类型详细信息
     */
    @RequiresPermissions("sys-cars-home:rules:query")
    @GetMapping(value = "/{chtrid}")
    public AjaxResult getInfo(@PathVariable("chtrid") Long chtrid)
    {
        return success(carsHomeTollRulesService.selectCarsHomeTollRulesByChtrid(chtrid));
    }

    /**
     * 新增车场收费规则类型
     */
    @RequiresPermissions("sys-cars-home:rules:add")
    @Log(title = "车场收费规则类型", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody CarsHomeTollRules carsHomeTollRules)
    {
        return toAjax(carsHomeTollRulesService.insertCarsHomeTollRules(carsHomeTollRules));
    }

    /**
     * 修改车场收费规则类型
     */
    @RequiresPermissions("sys-cars-home:rules:edit")
    @Log(title = "车场收费规则类型", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody CarsHomeTollRules carsHomeTollRules)
    {
        return toAjax(carsHomeTollRulesService.updateCarsHomeTollRules(carsHomeTollRules));
    }

    /**
     * 删除车场收费规则类型
     */
    @RequiresPermissions("sys-cars-home:rules:remove")
    @Log(title = "车场收费规则类型", businessType = BusinessType.DELETE)
	@DeleteMapping("/{chtrids}")
    public AjaxResult remove(@PathVariable Long[] chtrids)
    {
        return toAjax(carsHomeTollRulesService.deleteCarsHomeTollRulesByChtrids(chtrids));
    }
}
