package com.ruoyi.domain;

import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.core.annotation.Excel;
import com.ruoyi.common.core.web.domain.BaseEntity;

/**
 * 车道配置对象 cars_home_lane
 * 
 * @author chn
 * @date 2023-12-05
 */
public class CarsHomeLane extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 车道id */
    private Long chlid;

    /** 唯一编码,前台使用 */
    @Excel(name = "唯一编码,前台使用")
    private String code;

    /** 状态 */
    @Excel(name = "状态")
    private Long status;

    /** 是否删除 */
    @Excel(name = "是否删除")
    private Long deleted;

    /** 岗亭id */
    @Excel(name = "岗亭id")
    private Long chbid;

    private CarsHome carsHome;
    private PropertyManagement propertyManagement;
    private CarsHomeBooth carsHomeBooth;

    public CarsHome getCarsHome() {
        return carsHome;
    }

    public void setCarsHome(CarsHome carsHome) {
        this.carsHome = carsHome;
    }

    public PropertyManagement getPropertyManagement() {
        return propertyManagement;
    }

    public void setPropertyManagement(PropertyManagement propertyManagement) {
        this.propertyManagement = propertyManagement;
    }

    public CarsHomeBooth getCarsHomeBooth() {
        return carsHomeBooth;
    }

    public void setCarsHomeBooth(CarsHomeBooth carsHomeBooth) {
        this.carsHomeBooth = carsHomeBooth;
    }

    /** 车道编号 */
    @Excel(name = "车道编号")
    private String lanecode;

    /** 车道名称 */
    @Excel(name = "车道名称")
    private String lanename;

    /** 车道类型0入口1出口 */
    @Excel(name = "车道类型0入口1出口")
    private Integer lanetype;

    /** 是否收费0收费1免费 */
    @Excel(name = "是否收费0收费1免费")
    private Integer toll;

    /** 车位屏0LED/1液晶 */
    @Excel(name = "车位屏0LED/1液晶")
    private Integer screen;

    /** 车辆类型0机动车1非机动车 */
    @Excel(name = "车辆类型0机动车1非机动车")
    private Integer cartype;

    /** 开放时间 */
    @JsonFormat(pattern = "HH:mm:ss")
    @Excel(name = "开放时间", width = 30, dateFormat = "HH:mm:ss")
    private Date begintime;

    /** 关闭时间 */
    @JsonFormat(pattern = "HH:mm:ss")
    @Excel(name = "关闭时间", width = 30, dateFormat = "HH:mm:ss")
    private Date endtime;

    public void setChlid(Long chlid) 
    {
        this.chlid = chlid;
    }

    public Long getChlid() 
    {
        return chlid;
    }
    public void setCode(String code) 
    {
        this.code = code;
    }

    public String getCode() 
    {
        return code;
    }
    public void setStatus(Long status) 
    {
        this.status = status;
    }

    public Long getStatus() 
    {
        return status;
    }
    public void setDeleted(Long deleted) 
    {
        this.deleted = deleted;
    }

    public Long getDeleted() 
    {
        return deleted;
    }
    public void setChbid(Long chbid) 
    {
        this.chbid = chbid;
    }

    public Long getChbid() 
    {
        return chbid;
    }
    public void setLanecode(String lanecode) 
    {
        this.lanecode = lanecode;
    }

    public String getLanecode() 
    {
        return lanecode;
    }
    public void setLanename(String lanename) 
    {
        this.lanename = lanename;
    }

    public String getLanename() 
    {
        return lanename;
    }
    public void setLanetype(Integer lanetype) 
    {
        this.lanetype = lanetype;
    }

    public Integer getLanetype() 
    {
        return lanetype;
    }
    public void setToll(Integer toll) 
    {
        this.toll = toll;
    }

    public Integer getToll() 
    {
        return toll;
    }
    public void setScreen(Integer screen) 
    {
        this.screen = screen;
    }

    public Integer getScreen() 
    {
        return screen;
    }
    public void setCartype(Integer cartype) 
    {
        this.cartype = cartype;
    }

    public Integer getCartype() 
    {
        return cartype;
    }
    public void setBegintime(Date begintime) 
    {
        this.begintime = begintime;
    }

    public Date getBegintime() 
    {
        return begintime;
    }
    public void setEndtime(Date endtime) 
    {
        this.endtime = endtime;
    }

    public Date getEndtime() 
    {
        return endtime;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("chlid", getChlid())
            .append("code", getCode())
            .append("status", getStatus())
            .append("deleted", getDeleted())
            .append("createTime", getCreateTime())
            .append("updateTime", getUpdateTime())
            .append("chbid", getChbid())
            .append("lanecode", getLanecode())
            .append("lanename", getLanename())
            .append("lanetype", getLanetype())
            .append("toll", getToll())
            .append("screen", getScreen())
            .append("cartype", getCartype())
            .append("begintime", getBegintime())
            .append("endtime", getEndtime())
            .toString();
    }
}
