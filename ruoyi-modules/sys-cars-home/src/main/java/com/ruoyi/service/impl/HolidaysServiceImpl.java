package com.ruoyi.service.impl;

import java.util.List;
import com.ruoyi.common.core.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.mapper.HolidaysMapper;
import com.ruoyi.domain.Holidays;
import com.ruoyi.service.IHolidaysService;

/**
 * 车场节假日Service业务层处理
 * 
 * @author chn
 * @date 2023-12-15
 */
@Service
public class HolidaysServiceImpl implements IHolidaysService 
{
    @Autowired
    private HolidaysMapper holidaysMapper;

    /**
     * 查询车场节假日
     * 
     * @param id 车场节假日主键
     * @return 车场节假日
     */
    @Override
    public Holidays selectHolidaysById(Long id)
    {
        return holidaysMapper.selectHolidaysById(id);
    }

    /**
     * 查询车场节假日列表
     * 
     * @param holidays 车场节假日
     * @return 车场节假日
     */
    @Override
    public List<Holidays> selectHolidaysList(Holidays holidays)
    {
        return holidaysMapper.selectHolidaysList(holidays);
    }

    /**
     * 新增车场节假日
     * 
     * @param holidays 车场节假日
     * @return 结果
     */
    @Override
    public int insertHolidays(Holidays holidays)
    {
        holidays.setCreateTime(DateUtils.getNowDate());
        return holidaysMapper.insertHolidays(holidays);
    }

    /**
     * 修改车场节假日
     * 
     * @param holidays 车场节假日
     * @return 结果
     */
    @Override
    public int updateHolidays(Holidays holidays)
    {
        holidays.setUpdateTime(DateUtils.getNowDate());
        return holidaysMapper.updateHolidays(holidays);
    }

    /**
     * 批量删除车场节假日
     * 
     * @param ids 需要删除的车场节假日主键
     * @return 结果
     */
    @Override
    public int deleteHolidaysByIds(Long[] ids)
    {
        return holidaysMapper.deleteHolidaysByIds(ids);
    }

    /**
     * 删除车场节假日信息
     * 
     * @param id 车场节假日主键
     * @return 结果
     */
    @Override
    public int deleteHolidaysById(Long id)
    {
        return holidaysMapper.deleteHolidaysById(id);
    }
}
