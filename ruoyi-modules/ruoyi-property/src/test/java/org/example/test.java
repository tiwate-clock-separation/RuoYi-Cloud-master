package org.example;

import org.junit.jupiter.api.Test;

import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.Date;

public class test {
    @Test
    public static void main(String[] args) {
        Date dateTime = new Date(); // 获取当前的 DATETIME
        ZoneOffset zoneOffset = ZoneOffset.ofHours(8);
        Long timestamp = dateTime.toInstant().toEpochMilli(); // 将 DATETIME 转换为 Long（以毫秒为单位）
        System.out.println(timestamp);
    }
}
