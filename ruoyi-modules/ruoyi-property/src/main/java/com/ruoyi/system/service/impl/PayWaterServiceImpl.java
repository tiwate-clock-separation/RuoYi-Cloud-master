package com.ruoyi.system.service.impl;

import java.util.List;
import com.ruoyi.common.core.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.PayWaterMapper;
import com.ruoyi.domain.PayWater;
import com.ruoyi.system.service.IPayWaterService;

/**
 * 物业流水Service业务层处理
 *
 * @author ruoyi
 * @date 2023-11-30
 */
@Service
public class PayWaterServiceImpl implements IPayWaterService
{
    @Autowired
    private PayWaterMapper payWaterMapper;

    /**
     * 查询物业流水
     *
     * @param pwid 物业流水主键
     * @return 物业流水
     */
    @Override
    public PayWater selectPayWaterByPwid(Long pwid)
    {
        return payWaterMapper.selectPayWaterByPwid(pwid);
    }

    /**
     * 查询物业流水列表
     *
     * @param payWater 物业流水
     * @return 物业流水
     */
    @Override
    public List<PayWater> selectPayWaterList(PayWater payWater)
    {
        return payWaterMapper.selectPayWaterList(payWater);
    }

    /**
     * 新增物业流水
     *
     * @param payWater 物业流水
     * @return 结果
     */
    @Override
    public int insertPayWater(PayWater payWater)
    {
        payWater.setCreateTime(DateUtils.getNowDate());
        return payWaterMapper.insertPayWater(payWater);
    }

    /**
     * 修改物业流水
     *
     * @param payWater 物业流水
     * @return 结果
     */
    @Override
    public int updatePayWater(PayWater payWater)
    {
        payWater.setUpdateTime(DateUtils.getNowDate());
        return payWaterMapper.updatePayWater(payWater);
    }

    /**
     * 批量删除物业流水
     *
     * @param pwids 需要删除的物业流水主键
     * @return 结果
     */
    @Override
    public int deletePayWaterByPwids(Long[] pwids)
    {
        return payWaterMapper.deletePayWaterByPwids(pwids);
    }

    /**
     * 删除物业流水信息
     *
     * @param pwid 物业流水主键
     * @return 结果
     */
    @Override
    public int deletePayWaterByPwid(Long pwid)
    {
        return payWaterMapper.deletePayWaterByPwid(pwid);
    }
}
