package com.ruoyi.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.core.annotation.Excel;
import com.ruoyi.common.core.web.domain.BaseEntity;

/**
 * 卡套餐报对象 report_combo
 * 
 * @author ruoyi
 * @date 2023-12-08
 */
public class ReportCombo extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 自增ID */
    private Long comid;

    /** 唯一编码,前台使用 */
    private String code;

    /** 状态 */
    private Long status;

    /** 是否删除 */
    private Long deleted;

    /** 物业id */
    @Excel(name = "物业id")
    private Long pmid;

    /** 车场id */
    @Excel(name = "车场id")
    private Long chid;

    /** 车辆信息 */
    @Excel(name = "车辆信息")
    private Long cmid;

    /** 订单信息 */
    @Excel(name = "订单信息")
    private Long poid;

    /** 支付类型 */
    @Excel(name = "支付类型")
    private Long ptid;

    /** 费用类型 */
    @Excel(name = "费用类型")
    private Long feiid;

    private CarsHome carsHome;

    private PropertyManagement propertyManagement;

    private CarsMsg carsMsg;

    private PayOrder payOrder;

    private PayType payType;

    private TradingType tradingType;

    public TradingType getTradingType() {
        return tradingType;
    }

    public void setTradingType(TradingType tradingType) {
        this.tradingType = tradingType;
    }

    public CarsHome getCarsHome() {
        return carsHome;
    }

    public void setCarsHome(CarsHome carsHome) {
        this.carsHome = carsHome;
    }

    public PropertyManagement getPropertyManagement() {
        return propertyManagement;
    }

    public void setPropertyManagement(PropertyManagement propertyManagement) {
        this.propertyManagement = propertyManagement;
    }

    public CarsMsg getCarsMsg() {
        return carsMsg;
    }

    public void setCarsMsg(CarsMsg carsMsg) {
        this.carsMsg = carsMsg;
    }

    public PayOrder getPayOrder() {
        return payOrder;
    }

    public void setPayOrder(PayOrder payOrder) {
        this.payOrder = payOrder;
    }

    public PayType getPayType() {
        return payType;
    }

    public void setPayType(PayType payType) {
        this.payType = payType;
    }

    public void setComid(Long comid)
    {
        this.comid = comid;
    }

    public Long getComid() 
    {
        return comid;
    }
    public void setCode(String code) 
    {
        this.code = code;
    }

    public String getCode() 
    {
        return code;
    }
    public void setStatus(Long status) 
    {
        this.status = status;
    }

    public Long getStatus() 
    {
        return status;
    }
    public void setDeleted(Long deleted) 
    {
        this.deleted = deleted;
    }

    public Long getDeleted() 
    {
        return deleted;
    }
    public void setPmid(Long pmid) 
    {
        this.pmid = pmid;
    }

    public Long getPmid() 
    {
        return pmid;
    }
    public void setChid(Long chid) 
    {
        this.chid = chid;
    }

    public Long getChid() 
    {
        return chid;
    }
    public void setCmid(Long cmid) 
    {
        this.cmid = cmid;
    }

    public Long getCmid() 
    {
        return cmid;
    }
    public void setPoid(Long poid) 
    {
        this.poid = poid;
    }

    public Long getPoid() 
    {
        return poid;
    }
    public void setPtid(Long ptid) 
    {
        this.ptid = ptid;
    }

    public Long getPtid() 
    {
        return ptid;
    }
    public void setFeiid(Long feiid) 
    {
        this.feiid = feiid;
    }

    public Long getFeiid() 
    {
        return feiid;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("comid", getComid())
            .append("code", getCode())
            .append("status", getStatus())
            .append("deleted", getDeleted())
            .append("createTime", getCreateTime())
            .append("updateTime", getUpdateTime())
            .append("pmid", getPmid())
            .append("chid", getChid())
            .append("cmid", getCmid())
            .append("poid", getPoid())
            .append("ptid", getPtid())
            .append("feiid", getFeiid())
            .toString();
    }
}
