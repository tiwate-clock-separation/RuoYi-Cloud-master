package com.ruoyi.audit.service;

import java.util.List;
import com.ruoyi.domain.CarsAduit;

/**
 * 业主车辆审核Service接口
 *
 * @author ruoyi
 * @date 2023-12-03
 */
public interface ICarsAduitService
{
    /**
     * 查询业主车辆审核
     *
     * @param caid 业主车辆审核主键
     * @return 业主车辆审核
     */
    public CarsAduit selectCarsAduitByCaid(Long caid);

    /**
     * 查询业主车辆审核列表
     *
     * @param carsAduit 业主车辆审核
     * @return 业主车辆审核集合
     */
    public List<CarsAduit> selectCarsAduitList(CarsAduit carsAduit);

    /**
     * 新增业主车辆审核
     *
     * @param carsAduit 业主车辆审核
     * @return 结果
     */
    public int insertCarsAduit(CarsAduit carsAduit);

    /**
     * 修改业主车辆审核
     *
     * @param carsAduit 业主车辆审核
     * @return 结果
     */
    public int updateCarsAduit(CarsAduit carsAduit);

    /**
     * 批量删除业主车辆审核
     *
     * @param caids 需要删除的业主车辆审核主键集合
     * @return 结果
     */
    public int deleteCarsAduitByCaids(Long[] caids);

    /**
     * 删除业主车辆审核信息
     *
     * @param caid 业主车辆审核主键
     * @return 结果
     */
    public int deleteCarsAduitByCaid(Long caid);
}
