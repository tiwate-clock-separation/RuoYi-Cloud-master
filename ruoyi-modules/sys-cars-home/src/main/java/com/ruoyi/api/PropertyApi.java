//package com.ruoyi.api;
//
//import com.ruoyi.common.core.web.page.TableDataInfo;
//import org.springframework.cloud.openfeign.FeignClient;
//
//import org.springframework.context.annotation.Bean;
//import org.springframework.stereotype.Component;
//import org.springframework.web.bind.annotation.GetMapping;
//
//@Component
//@FeignClient(name = "sys-property")
//public interface PropertyApi {
//    @GetMapping("/property/list")
//    TableDataInfo list();
//}
