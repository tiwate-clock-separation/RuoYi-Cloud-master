package com.ruoyi.mapper;

import java.util.List;

import com.ruoyi.domain.OwnerMsg;
import com.ruoyi.domain.PropertyManagement;

/**
 * 业主信息管理Mapper接口
 *
 * @author ruoyi
 * @date 2023-12-07
 */
public interface OwnerMsgMapper
{
    /**
     * 查询业主信息管理
     *
     * @param omid 业主信息管理主键
     * @return 业主信息管理
     */
    public OwnerMsg selectOwnerMsgByOmid(Long omid);

    /**
     * 查询业主信息管理列表
     *
     * @param ownerMsg 业主信息管理
     * @return 业主信息管理集合
     */
    public List<OwnerMsg> selectOwnerMsgList(OwnerMsg ownerMsg);

    /**
     * 新增业主信息管理
     *
     * @param ownerMsg 业主信息管理
     * @return 结果
     */
    public int insertOwnerMsg(OwnerMsg ownerMsg);

    /**
     * 修改业主信息管理
     *
     * @param ownerMsg 业主信息管理
     * @return 结果
     */
    public int updateOwnerMsg(OwnerMsg ownerMsg);

    /**
     * 删除业主信息管理
     *
     * @param omid 业主信息管理主键
     * @return 结果
     */
    public int deleteOwnerMsgByOmid(Long omid);

    /**
     * 批量删除业主信息管理
     *
     * @param omids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteOwnerMsgByOmids(Long[] omids);

    /**
     * 批量删除物业管理
     *
     * @param omids 需要删除的数据主键集合
     * @return 结果
     */
    public int deletePropertyManagementByPmids(Long[] omids);

    /**
     * 批量新增物业管理
     *
     * @param propertyManagementList 物业管理列表
     * @return 结果
     */
    public int batchPropertyManagement(PropertyManagement propertyManagementList);


    /**
     * 通过业主信息管理主键删除物业管理信息
     *
     * @param omid 业主信息管理ID
     * @return 结果
     */
    public int deletePropertyManagementByPmid(Long omid);
}
