package com.ruoyi.cars.service.impl;

import com.ruoyi.common.core.utils.DateUtils;
import com.ruoyi.domain.CutoffRecord;
import com.ruoyi.cars.mapper.CutoffRecordMapper;
import com.ruoyi.cars.service.ICutoffRecordService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 开闸记录Service业务层处理
 *
 * @author ruoyi
 * @date 2023-11-29
 */
@Service
public class CutoffRecordServiceImpl implements ICutoffRecordService
{
    @Autowired
    private CutoffRecordMapper cutoffRecordMapper;

    /**
     * 查询开闸记录
     *
     * @param corid 开闸记录主键
     * @return 开闸记录
     */
    @Override
    public CutoffRecord selectCutoffRecordByCorid(Long corid)
    {
        return cutoffRecordMapper.selectCutoffRecordByCorid(corid);
    }

    /**
     * 查询开闸记录列表
     *
     * @param cutoffRecord 开闸记录
     * @return 开闸记录
     */
    @Override
    public List<CutoffRecord> selectCutoffRecordList(CutoffRecord cutoffRecord)
    {
        return cutoffRecordMapper.selectCutoffRecordList(cutoffRecord);
    }

    /**
     * 新增开闸记录
     *
     * @param cutoffRecord 开闸记录
     * @return 结果
     */
    @Override
    public int insertCutoffRecord(CutoffRecord cutoffRecord)
    {
        cutoffRecord.setCreateTime(DateUtils.getNowDate());
        return cutoffRecordMapper.insertCutoffRecord(cutoffRecord);
    }

    /**
     * 修改开闸记录
     *
     * @param cutoffRecord 开闸记录
     * @return 结果
     */
    @Override
    public int updateCutoffRecord(CutoffRecord cutoffRecord)
    {
        cutoffRecord.setUpdateTime(DateUtils.getNowDate());
        return cutoffRecordMapper.updateCutoffRecord(cutoffRecord);
    }

    /**
     * 批量删除开闸记录
     *
     * @param corids 需要删除的开闸记录主键
     * @return 结果
     */
    @Override
    public int deleteCutoffRecordByCorids(Long[] corids)
    {
        return cutoffRecordMapper.deleteCutoffRecordByCorids(corids);
    }

    /**
     * 删除开闸记录信息
     *
     * @param corid 开闸记录主键
     * @return 结果
     */
    @Override
    public int deleteCutoffRecordByCorid(Long corid)
    {
        return cutoffRecordMapper.deleteCutoffRecordByCorid(corid);
    }
}
