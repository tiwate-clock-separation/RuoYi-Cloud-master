package com.ruoyi.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.core.annotation.Excel;
import com.ruoyi.common.core.web.domain.BaseEntity;

/**
 * 支付宝支付配置对象 cars_home_alipay
 * 
 * @author chn
 * @date 2023-12-27
 */
public class CarsHomeAlipay extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 自增ID */
    private Long id;

    /** 唯一编码,前台使用 */
    @Excel(name = "唯一编码,前台使用")
    private String code;

    /** 状态 */
    @Excel(name = "状态")
    private Long status;

    /** 是否删除 */
    @Excel(name = "是否删除")
    private Long deleted;

    /** 个人占比 */
    @Excel(name = "个人占比")
    private Long individualRatioAli;

    /** 车场占比 */
    @Excel(name = "车场占比")
    private Long carsHomeRatioAli;

    /** 支付宝账号 */
    @Excel(name = "支付宝账号")
    private String alipayAccount;

    /** 账号真实姓名 */
    @Excel(name = "账号真实姓名")
    private String accountName;

    /** 车场id */
    @Excel(name = "车场id")
    private Long chid;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setCode(String code) 
    {
        this.code = code;
    }

    public String getCode() 
    {
        return code;
    }
    public void setStatus(Long status) 
    {
        this.status = status;
    }

    public Long getStatus() 
    {
        return status;
    }
    public void setDeleted(Long deleted) 
    {
        this.deleted = deleted;
    }

    public Long getDeleted() 
    {
        return deleted;
    }
    public void setIndividualRatioAli(Long individualRatioAli) 
    {
        this.individualRatioAli = individualRatioAli;
    }

    public Long getIndividualRatioAli() 
    {
        return individualRatioAli;
    }
    public void setCarsHomeRatioAli(Long carsHomeRatioAli) 
    {
        this.carsHomeRatioAli = carsHomeRatioAli;
    }

    public Long getCarsHomeRatioAli() 
    {
        return carsHomeRatioAli;
    }
    public void setAlipayAccount(String alipayAccount) 
    {
        this.alipayAccount = alipayAccount;
    }

    public String getAlipayAccount() 
    {
        return alipayAccount;
    }
    public void setAccountName(String accountName) 
    {
        this.accountName = accountName;
    }

    public String getAccountName() 
    {
        return accountName;
    }
    public void setChid(Long chid) 
    {
        this.chid = chid;
    }

    public Long getChid() 
    {
        return chid;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("code", getCode())
            .append("status", getStatus())
            .append("deleted", getDeleted())
            .append("createTime", getCreateTime())
            .append("updateTime", getUpdateTime())
            .append("individualRatioAli", getIndividualRatioAli())
            .append("carsHomeRatioAli", getCarsHomeRatioAli())
            .append("alipayAccount", getAlipayAccount())
            .append("accountName", getAccountName())
            .append("chid", getChid())
            .toString();
    }
}
