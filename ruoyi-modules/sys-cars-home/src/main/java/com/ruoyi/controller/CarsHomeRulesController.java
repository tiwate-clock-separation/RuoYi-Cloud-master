package com.ruoyi.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;

import com.ruoyi.domain.CarsHomeRules;
import com.ruoyi.service.ICarsHomeRulesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.log.annotation.Log;
import com.ruoyi.common.log.enums.BusinessType;
import com.ruoyi.common.security.annotation.RequiresPermissions;
import com.ruoyi.common.core.web.controller.BaseController;
import com.ruoyi.common.core.web.domain.AjaxResult;
import com.ruoyi.common.core.utils.poi.ExcelUtil;
import com.ruoyi.common.core.web.page.TableDataInfo;

/**
 * 车场计费规则Controller
 * 
 * @author ruoyi
 * @date 2023-12-02
 */
@RestController
@RequestMapping("/rules")
public class CarsHomeRulesController extends BaseController
{
    @Autowired
    private ICarsHomeRulesService carsHomeRulesService;




    /**
     * 查询车场计费规则列表
     */
    @RequiresPermissions("sys-cars-home:rules:list")
    @GetMapping("/list")
    public TableDataInfo list(CarsHomeRules carsHomeRules)
    {
        startPage();
        List<CarsHomeRules> list = carsHomeRulesService.selectCarsHomeRulesList(carsHomeRules);
        return getDataTable(list);
    }

    /**
     * 导出车场计费规则列表
     */
    @RequiresPermissions("sys-cars-home:rules:export")
    @Log(title = "车场计费规则", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, CarsHomeRules carsHomeRules)
    {
        List<CarsHomeRules> list = carsHomeRulesService.selectCarsHomeRulesList(carsHomeRules);
        ExcelUtil<CarsHomeRules> util = new ExcelUtil<CarsHomeRules>(CarsHomeRules.class);
        util.exportExcel(response, list, "车场计费规则数据");
    }

    /**
     * 获取车场计费规则详细信息
     */
    @RequiresPermissions("example:rules:query")
    @GetMapping(value = "/{chrid}")
    public AjaxResult getInfo(@PathVariable("chrid") Long chrid)
    {
        return success(carsHomeRulesService.selectCarsHomeRulesByChrid(chrid));
    }

    /**
     * 新增车场计费规则
     */
    @RequiresPermissions("example:rules:add")
    @Log(title = "车场计费规则", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody CarsHomeRules carsHomeRules)
    {
        return toAjax(carsHomeRulesService.insertCarsHomeRules(carsHomeRules));
    }

    /**
     * 修改车场计费规则
     */
    @RequiresPermissions("example:rules:edit")
    @Log(title = "车场计费规则", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody CarsHomeRules carsHomeRules)
    {
        return toAjax(carsHomeRulesService.updateCarsHomeRules(carsHomeRules));
    }

    /**
     * 删除车场计费规则
     */
    @RequiresPermissions("example:rules:remove")
    @Log(title = "车场计费规则", businessType = BusinessType.DELETE)
	@DeleteMapping("/{chrids}")
    public AjaxResult remove(@PathVariable Long[] chrids)
    {
        return toAjax(carsHomeRulesService.deleteCarsHomeRulesByChrids(chrids));
    }
}
