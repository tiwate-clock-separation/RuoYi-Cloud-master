package com.ruoyi.staff.service;

import java.util.List;

import com.ruoyi.domain.EmpH5Check;

/**
 * h5消费Service接口
 * 
 * @author ruoyi
 * @date 2023-12-15
 */
public interface IEmpH5CheckService 
{
    /**
     * 查询h5消费
     * 
     * @param id h5消费主键
     * @return h5消费
     */
    public EmpH5Check selectEmpH5CheckById(Long id);

    /**
     * 查询h5消费列表
     * 
     * @param empH5Check h5消费
     * @return h5消费集合
     */
    public List<EmpH5Check> selectEmpH5CheckList(EmpH5Check empH5Check);

    /**
     * 新增h5消费
     * 
     * @param empH5Check h5消费
     * @return 结果
     */
    public int insertEmpH5Check(EmpH5Check empH5Check);

    /**
     * 修改h5消费
     * 
     * @param empH5Check h5消费
     * @return 结果
     */
    public int updateEmpH5Check(EmpH5Check empH5Check);

    /**
     * 批量删除h5消费
     * 
     * @param ids 需要删除的h5消费主键集合
     * @return 结果
     */
    public int deleteEmpH5CheckByIds(Long[] ids);

    /**
     * 删除h5消费信息
     * 
     * @param id h5消费主键
     * @return 结果
     */
    public int deleteEmpH5CheckById(Long id);
}
