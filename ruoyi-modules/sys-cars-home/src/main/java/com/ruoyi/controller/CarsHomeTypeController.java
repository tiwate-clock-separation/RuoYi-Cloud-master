package com.ruoyi.controller;

import java.util.List;

import com.ruoyi.domain.CarsHomeType;
import com.ruoyi.service.ICarsHomeTypeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.security.annotation.RequiresPermissions;
import com.ruoyi.common.core.web.controller.BaseController;
import com.ruoyi.common.core.web.domain.AjaxResult;
import com.ruoyi.common.core.web.page.TableDataInfo;

/**
 * 车场类型Controller
 *
 * @author chn
 * @date 2023-11-29
 */
@RestController
@RequestMapping("/type")
public class CarsHomeTypeController extends BaseController
{
    @Autowired
    private ICarsHomeTypeService carsHomeTypeService;

    /**
     * 查询车场类型列表
     */
    @RequiresPermissions("sys-cars-home:type:list")
    @GetMapping("/list")
    public TableDataInfo list(CarsHomeType carsHomeType)
    {
        startPage();
        List<CarsHomeType> list = carsHomeTypeService.selectCarsHomeTypeList(carsHomeType);
        return getDataTable(list);
    }

//    /**
//     * 导出车场类型列表
//     */
//    @RequiresPermissions("sys-cars-home:type:export")
//    @Log(title = "车场类型", businessType = BusinessType.EXPORT)
//    @PostMapping("/export")
//    public void export(HttpServletResponse response, CarsHomeType carsHomeType)
//    {
//        List<CarsHomeType> list = carsHomeTypeService.selectCarsHomeTypeList(carsHomeType);
//        ExcelUtil<CarsHomeType> util = new ExcelUtil<CarsHomeType>(CarsHomeType.class);
//        util.exportExcel(response, list, "车场类型数据");
//    }

    /**
     * 获取车场类型详细信息
     */
    @RequiresPermissions("sys-cars-home:type:query")
    @GetMapping(value = "/{chtid}")
    public AjaxResult getInfo(@PathVariable("chtid") Long chtid)
    {
        return success(carsHomeTypeService.selectCarsHomeTypeByChtid(chtid));
    }

//    /**
//     * 新增车场类型
//     */
//    @RequiresPermissions("sys-cars-home:type:add")
//    @Log(title = "车场类型", businessType = BusinessType.INSERT)
//    @PostMapping
//    public AjaxResult add(@RequestBody CarsHomeType carsHomeType)
//    {
//        return toAjax(carsHomeTypeService.insertCarsHomeType(carsHomeType));
//    }
//
//    /**
//     * 修改车场类型
//     */
//    @RequiresPermissions("sys-cars-home:type:edit")
//    @Log(title = "车场类型", businessType = BusinessType.UPDATE)
//    @PutMapping
//    public AjaxResult edit(@RequestBody CarsHomeType carsHomeType)
//    {
//        return toAjax(carsHomeTypeService.updateCarsHomeType(carsHomeType));
//    }
//
//    /**
//     * 删除车场类型
//     */
//    @RequiresPermissions("sys-cars-home:type:remove")
//    @Log(title = "车场类型", businessType = BusinessType.DELETE)
//	@DeleteMapping("/{chtids}")
//    public AjaxResult remove(@PathVariable Long[] chtids)
//    {
//        return toAjax(carsHomeTypeService.deleteCarsHomeTypeByChtids(chtids));
//    }
}
