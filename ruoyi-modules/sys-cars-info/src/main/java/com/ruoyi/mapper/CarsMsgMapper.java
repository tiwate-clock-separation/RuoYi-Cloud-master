package com.ruoyi.mapper;

import com.ruoyi.domain.CarsMsg;
import com.ruoyi.domain.PropertyManagement;

import java.util.List;

/**
 * 车辆信息Mapper接口
 *
 * @author ruoyi
 * @date 2023-12-07
 */
public interface CarsMsgMapper
{
    /**
     * 查询车辆信息
     *
     * @param cmid 车辆信息主键
     * @return 车辆信息
     */
    public CarsMsg selectCarsMsgByCmid(Long cmid);

    /**
     * 查询车辆信息列表
     *
     * @param carsMsg 车辆信息
     * @return 车辆信息集合
     */
    public List<CarsMsg> selectCarsMsgList(CarsMsg carsMsg);

    /**
     * 新增车辆信息
     *
     * @param carsMsg 车辆信息
     * @return 结果
     */
    public int insertCarsMsg(CarsMsg carsMsg);

    /**
     * 修改车辆信息
     *
     * @param carsMsg 车辆信息
     * @return 结果
     */
    public int updateCarsMsg(CarsMsg carsMsg);

    /**
     * 删除车辆信息
     *
     * @param cmid 车辆信息主键
     * @return 结果
     */
    public int deleteCarsMsgByCmid(Long cmid);

    /**
     * 批量删除车辆信息
     *
     * @param cmids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteCarsMsgByCmids(Long[] cmids);

    /**
     * 批量删除物业管理
     *
     * @param cmids 需要删除的数据主键集合
     * @return 结果
     */
    public int deletePropertyManagementByPmids(Long[] cmids);

    /**
     * 批量新增物业管理
     *
     * @param propertyManagementList 物业管理列表
     * @return 结果
     */
    public int batchPropertyManagement(PropertyManagement propertyManagementList);


    /**
     * 通过车辆信息主键删除物业管理信息
     *
     * @param cmid 车辆信息ID
     * @return 结果
     */
    public int deletePropertyManagementByPmid(Long cmid);
}
