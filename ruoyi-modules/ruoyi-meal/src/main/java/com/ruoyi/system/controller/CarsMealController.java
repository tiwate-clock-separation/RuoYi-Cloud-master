package com.ruoyi.system.controller;

import java.util.List;
import java.io.IOException;
import javax.servlet.http.HttpServletResponse;

import com.ruoyi.domain.CarsMeal;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.log.annotation.Log;
import com.ruoyi.common.log.enums.BusinessType;
import com.ruoyi.common.security.annotation.RequiresPermissions;
import com.ruoyi.system.service.ICarsMealService;
import com.ruoyi.common.core.web.controller.BaseController;
import com.ruoyi.common.core.web.domain.AjaxResult;
import com.ruoyi.common.core.utils.poi.ExcelUtil;
import com.ruoyi.common.core.web.page.TableDataInfo;

/**
 * 车辆套餐Controller
 * 
 * @author ruoyi
 * @date 2023-12-13
 */
@RestController
@RequestMapping("/meal")
public class CarsMealController extends BaseController
{
    @Autowired
    private ICarsMealService carsMealService;

    /**
     * 查询车辆套餐列表
     */
    @RequiresPermissions("system:meal:list")
    @GetMapping("/list")
    public TableDataInfo list(CarsMeal carsMeal)
    {
        startPage();
        List<CarsMeal> list = carsMealService.selectCarsMealList(carsMeal);
        return getDataTable(list);
    }

    /**
     * 导出车辆套餐列表
     */
    @RequiresPermissions("system:meal:export")
    @Log(title = "车辆套餐", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, CarsMeal carsMeal)
    {
        List<CarsMeal> list = carsMealService.selectCarsMealList(carsMeal);
        ExcelUtil<CarsMeal> util = new ExcelUtil<CarsMeal>(CarsMeal.class);
        util.exportExcel(response, list, "车辆套餐数据");
    }

    /**
     * 获取车辆套餐详细信息
     */
    @RequiresPermissions("system:meal:query")
    @GetMapping(value = "/{cmid}")
    public AjaxResult getInfo(@PathVariable("cmid") Long cmid)
    {
        return success(carsMealService.selectCarsMealByCmid(cmid));
    }

    /**
     * 新增车辆套餐
     */
    @RequiresPermissions("system:meal:add")
    @Log(title = "车辆套餐", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody CarsMeal carsMeal)
    {
        return toAjax(carsMealService.insertCarsMeal(carsMeal));
    }

    /**
     * 修改车辆套餐
     */
    @RequiresPermissions("system:meal:edit")
    @Log(title = "车辆套餐", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody CarsMeal carsMeal)
    {
        return toAjax(carsMealService.updateCarsMeal(carsMeal));
    }

    /**
     * 删除车辆套餐
     */
    @RequiresPermissions("system:meal:remove")
    @Log(title = "车辆套餐", businessType = BusinessType.DELETE)
	@DeleteMapping("/{cmids}")
    public AjaxResult remove(@PathVariable Long[] cmids)
    {
        return toAjax(carsMealService.deleteCarsMealByCmids(cmids));
    }
}
