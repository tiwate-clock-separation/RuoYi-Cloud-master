//package com.ruoyi.controller;
//
//import com.ruoyi.common.core.web.page.TableDataInfo;
//import com.ruoyi.api.PropertyApi;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.web.bind.annotation.GetMapping;
//import org.springframework.web.bind.annotation.RequestMapping;
//import org.springframework.web.bind.annotation.RestController;
//
//@RestController
//@RequestMapping("api")
//public class ApiController {
//
//    @Autowired
//    private PropertyApi propertyApi;
//
//    @GetMapping("list")
//    public TableDataInfo list(){
//        return propertyApi.list();
//    }
//
//}
