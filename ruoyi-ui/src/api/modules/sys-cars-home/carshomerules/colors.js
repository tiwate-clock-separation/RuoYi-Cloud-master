import request from '@/utils/request'

// 查询车辆牌照颜色列表
export function listColors(query) {
  return request({
    url: '/sys-cars-home/colors/list',
    method: 'get',
    params: query
  })
}

// 查询车辆牌照颜色详细
export function getColors(lpcid) {
  return request({
    url: '/sys-cars-home/colors/' + lpcid,
    method: 'get'
  })
}

// 新增车辆牌照颜色
export function addColors(data) {
  return request({
    url: '/sys-cars-home/colors',
    method: 'post',
    data: data
  })
}

// 修改车辆牌照颜色
export function updateColors(data) {
  return request({
    url: '/sys-cars-home/colors',
    method: 'put',
    data: data
  })
}

// 删除车辆牌照颜色
export function delColors(lpcid) {
  return request({
    url: '/sys-cars-home/colors/' + lpcid,
    method: 'delete'
  })
}
