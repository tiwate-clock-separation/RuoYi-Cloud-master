package com.ruoyi.controller;

import java.util.List;
import java.io.IOException;
import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.log.annotation.Log;
import com.ruoyi.common.log.enums.BusinessType;
import com.ruoyi.common.security.annotation.RequiresPermissions;
import com.ruoyi.domain.CarsType;
import com.ruoyi.service.ICarsTypeService;
import com.ruoyi.common.core.web.controller.BaseController;
import com.ruoyi.common.core.web.domain.AjaxResult;
import com.ruoyi.common.core.utils.poi.ExcelUtil;
import com.ruoyi.common.core.web.page.TableDataInfo;

/**
 * 车辆类型Controller
 *
 * @author ruoyi
 * @date 2023-12-12
 */
@RestController
@RequestMapping("/type")
public class CarsTypeController extends BaseController
{
    @Autowired
    private ICarsTypeService carsTypeService;

    /**
     * 查询车辆类型列表
     */
    @RequiresPermissions("system:type:list")
    @GetMapping("/list")
    public TableDataInfo list(CarsType carsType)
    {
        startPage();
        List<CarsType> list = carsTypeService.selectCarsTypeList(carsType);
        return getDataTable(list);
    }

    /**
     * 导出车辆类型列表
     */
    @RequiresPermissions("system:type:export")
    @Log(title = "车辆类型", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, CarsType carsType)
    {
        List<CarsType> list = carsTypeService.selectCarsTypeList(carsType);
        ExcelUtil<CarsType> util = new ExcelUtil<CarsType>(CarsType.class);
        util.exportExcel(response, list, "车辆类型数据");
    }

    /**
     * 获取车辆类型详细信息
     */
    @RequiresPermissions("system:type:query")
    @GetMapping(value = "/{ctid}")
    public AjaxResult getInfo(@PathVariable("ctid") Long ctid)
    {
        return success(carsTypeService.selectCarsTypeByCtid(ctid));
    }

    /**
     * 新增车辆类型
     */
    @RequiresPermissions("system:type:add")
    @Log(title = "车辆类型", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody CarsType carsType)
    {
        return toAjax(carsTypeService.insertCarsType(carsType));
    }

    /**
     * 修改车辆类型
     */
    @RequiresPermissions("system:type:edit")
    @Log(title = "车辆类型", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody CarsType carsType)
    {
        return toAjax(carsTypeService.updateCarsType(carsType));
    }

    /**
     * 删除车辆类型
     */
    @RequiresPermissions("system:type:remove")
    @Log(title = "车辆类型", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ctids}")
    public AjaxResult remove(@PathVariable Long[] ctids)
    {
        return toAjax(carsTypeService.deleteCarsTypeByCtids(ctids));
    }
}
