package com.ruoyi.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.core.annotation.Excel;
import com.ruoyi.common.core.web.domain.BaseEntity;

/**
 * 车辆类型对象 cars_type
 *
 * @author ruoyi
 * @date 2023-11-30
 */
public class CarsType extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 自增ID */
    private Long ctid;

    /** 唯一编码,前台使用 */
    @Excel(name = "唯一编码,前台使用")
    private String code;

    /** 状态 */
    @Excel(name = "状态")
    private Long status;

    /** 是否删除 */
    @Excel(name = "是否删除")
    private Long deleted;

    /** 类型名字 */
    @Excel(name = "类型名字")
    private String typeName;

    public void setCtid(Long ctid)
    {
        this.ctid = ctid;
    }

    public Long getCtid()
    {
        return ctid;
    }
    public void setCode(String code)
    {
        this.code = code;
    }

    public String getCode()
    {
        return code;
    }
    public void setStatus(Long status)
    {
        this.status = status;
    }

    public Long getStatus()
    {
        return status;
    }
    public void setDeleted(Long deleted)
    {
        this.deleted = deleted;
    }

    public Long getDeleted()
    {
        return deleted;
    }
    public void setTypeName(String typeName)
    {
        this.typeName = typeName;
    }

    public String getTypeName()
    {
        return typeName;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("ctid", getCtid())
            .append("code", getCode())
            .append("status", getStatus())
            .append("deleted", getDeleted())
            .append("createTime", getCreateTime())
            .append("updateTime", getUpdateTime())
            .append("typeName", getTypeName())
            .toString();
    }
}
