package com.ruoyi.system.controller;

import com.ruoyi.common.core.web.controller.BaseController;
import com.ruoyi.common.core.web.domain.AjaxResult;
import com.ruoyi.common.security.annotation.RequiresPermissions;
import com.ruoyi.domain.vo.StopCars;
import com.ruoyi.system.service.IStopCarsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/stop")
public class StopCarsController extends BaseController {

    @Autowired
    private IStopCarsService iStopCarsService;

    @RequiresPermissions("system:management:list")
    @PostMapping("/cars")
    public AjaxResult stopCars(@RequestBody StopCars stopCars){
        System.out.println(stopCars);
        return toAjax(iStopCarsService.stopMoney(stopCars.getLicenseNumber(),stopCars.getMoney(), stopCars.getChid()));
    }
}
