import request from '@/utils/request'

// 查询设备记录列表
export function listOperation(query) {
  return request({
    url: '/pxy-cars/operation/list',
    method: 'get',
    params: query
  })
}

// 查询设备记录详细
export function getOperation(orid) {
  return request({
    url: '/pxy-cars/operation/' + orid,
    method: 'get'
  })
}

// 新增设备记录
export function addOperation(data) {
  return request({
    url: '/pxy-cars/operation',
    method: 'post',
    data: data
  })
}

// 修改设备记录
export function updateOperation(data) {
  return request({
    url: '/pxy-cars/operation',
    method: 'put',
    data: data
  })
}

// 删除设备记录
export function delOperation(orid) {
  return request({
    url: '/pxy-cars/operation/' + orid,
    method: 'delete'
  })
}
