package com.ruoyi.system.service;

import com.ruoyi.domain.CarsMeal;

import java.util.List;

/**
 * 车辆套餐Service接口
 * 
 * @author ruoyi
 * @date 2023-12-13
 */
public interface ICarsMealService 
{
    /**
     * 查询车辆套餐
     * 
     * @param cmid 车辆套餐主键
     * @return 车辆套餐
     */
    public CarsMeal selectCarsMealByCmid(Long cmid);

    /**
     * 查询车辆套餐列表
     * 
     * @param carsMeal 车辆套餐
     * @return 车辆套餐集合
     */
    public List<CarsMeal> selectCarsMealList(CarsMeal carsMeal);

    /**
     * 新增车辆套餐
     * 
     * @param carsMeal 车辆套餐
     * @return 结果
     */
    public int insertCarsMeal(CarsMeal carsMeal);

    /**
     * 修改车辆套餐
     * 
     * @param carsMeal 车辆套餐
     * @return 结果
     */
    public int updateCarsMeal(CarsMeal carsMeal);

    /**
     * 批量删除车辆套餐
     * 
     * @param cmids 需要删除的车辆套餐主键集合
     * @return 结果
     */
    public int deleteCarsMealByCmids(Long[] cmids);

    /**
     * 删除车辆套餐信息
     * 
     * @param cmid 车辆套餐主键
     * @return 结果
     */
    public int deleteCarsMealByCmid(Long cmid);
}
